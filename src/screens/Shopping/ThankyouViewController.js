import { React, importImages, SafeAreaView, KeyboardAwareScrollView, useRef, moment, TouchableOpacity, TextInput, useState, useEffect, showSimpleAlert, Request, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale, Keyboard, Platform, isValidEmail, NavigationService } from '../../utils/importLibrary'
import { AirbnbRating } from 'react-native-ratings';
import CustomButton from '../../components/CustomButton';
import RBSheet from "react-native-raw-bottom-sheet";
import Share from 'react-native-share';
import { BackHandler, Alert } from 'react-native';
export default function ThankyouViewController({ route, navigation }) {
    const [isModalVisible, setModalVisible] = useState(false);
    const [counting, setcounting] = useState(5);
    const [yourReview, setyourReview] = useState('');
    const [countingData, setcountingData] = useState('');
    const [productId, setproductId] = useState('');
    const [smsactive, setsmsactive] = useState(false);
    const [emailactive, setemailactive] = useState(true);
    const [receiptentemailAddress, setreceiptentemailAddress] = useState('');
    const [refsheetopen, setrefsheetopen] = useState(false);
    const [sharebtn, setsharebtn] = useState(true);
    const [sharelink, setsharelink] = useState('');
    const [points, setpoints] = useState('');

    const refRBSheet = useRef()



    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', async () => {
            let params = {
                type: 1
            }
            let response = await Request.post('get-reward-point.php', params)
            console.log("response", response);
            if (response) {
                setpoints(response.data.points)
            }
        });
        return unsubscribe;
    }, [navigation])
    useEffect(() => {
        const backAction = () => {
            navigation.navigate('ShopStackNavigator')
            return true
        };
        const backHandler = BackHandler.addEventListener(
            'hardwareBackPress',
            backAction,
        );

        return () => backHandler.remove();
    }, []);


    const submitReview = async () => {
        const ConfirmValid = validationOfField();
        if (ConfirmValid) {
            Keyboard.dismiss()
            setModalVisible(true)
            let params = {
                review: yourReview,
                order: route.params.orderId,
                rating: countingData == '' ? counting : countingData
            }
            let response = await Request.post('add-order-review.php', params)
            setModalVisible(false)
            if (response.success == true) {
                setyourReview('')
                navigation.navigate('Shareyourpurchase', { orderId: route.params.orderId })
                // const shareItem = response.data.link
                // const shareOptions = {
                //     subject: 'Gift Center',
                //     url: shareItem
                // };
                // const response2 = Share.open(shareOptions)
                //     .then(res => {
                //         console.log('res', res);
                //         navigation.navigate('ShopStackNavigator')
                //     })
                //     .catch(err => {
                //         navigation.navigate('ShopStackNavigator')
                //         err && console.log(err);
                //     });
                // return response2;

            }
            else {
                if (response) {
                    showSimpleAlert(response.message)
                }
            }
        }
    }
    const sharepurchase = async () => {
        // setsharebtn(false)

        let params = {
            order_id: route.params.orderId,
        }
        let response = await Request.post('add-share.php', params)
        console.log("response", response);
        if (response.success == true) {
            setsharelink(response.data.link)
            refRBSheet.current.open()

            // const shareItem = response.data.link
            // const shareOptions = {
            //     subject: 'Gift Center',
            //     url: shareItem
            // };
            // const response2 = Share.open(shareOptions)
            //     .then(res => {
            //         console.log('res', res);
            //     })
            //     .catch(err => {
            //         err && console.log(err);
            //     });
            // return response2;
        }
    }

    const shareyourpurchase = async () => {
        console.log("receiptentemailAddress", receiptentemailAddress);
        const ConfirmValid = sharevalidation();
        console.log("ConfirmValid", ConfirmValid);

        if (ConfirmValid) {
            Keyboard.dismiss()
            let params = {
                email_or_phone: receiptentemailAddress,
                type: emailactive ? '0' : '1'
            }
            let response = await Request.post('share-friend.php', params)
            setModalVisible(false)
            if (response.success == true) {
                showSimpleAlert(response.message)
                setrefsheetopen(false)

                refRBSheet.current.close()
                navigation.navigate('ShopStackNavigator')
            }
            else {
                if (response) {
                    showSimpleAlert(response.message)
                }
            }
        }
    }
    const validationOfField = () => {
        if (yourReview.trim() == '') {
            showSimpleAlert('Please enter your opinion')
            return false;
        }

        else {
            return true;
        }
    }

    const sharevalidation = () => {
        if (smsactive == true) {
            if (receiptentemailAddress.trim() == '') {
                showSimpleAlert('Please enter phone number')
                return false;
            }
            else {
                return true;
            }
        }
        else if (emailactive == true) {
            if (receiptentemailAddress.trim() == '') {
                showSimpleAlert('Please enter email id')
                return false;
            }
            else if (!isValidEmail(receiptentemailAddress)) {
                showSimpleAlert('Please enter valid email')
                return false;
            }
            else {
                return true;
            }
        }

        else {
            return true;
        }
    }

    const ratingCompleted = (rating) => {
        setcountingData(rating)
    }
    const closeonDrag = () => {
        refRBSheet.current.close()
        navigation.navigate('ShopStackNavigator')
    }
    return (
        <View style={styles.mainViewStyle}>
            <KeyboardAwareScrollView>

                <TouchableOpacity style={{ height: scale(30), width: scale(30), alignSelf: "flex-end", marginRight: scale(15), marginTop: scale(15) }} onPress={() => { navigation.navigate('ShopStackNavigator') }}>
                    <View style={{}}>
                        <Image source={importImages.thankyoucross} style={{ height: scale(25), width: scale(25), }} />
                    </View>
                </TouchableOpacity>
                <View style={{ flex: 1, marginTop: scale(30) }}>
                    <View style={{ flexDirection: "column" }}>
                        {/* {refsheetopen ? null :
                            <Image source={importImages.thankyou} resizeMode='contain' style={{ height: scale(80), width: scale(250), alignSelf: "center", }} />} */}
                        <Text style={{ fontFamily: Fonts.montserratMedium, fontWeight: "700", color: commonColors.Blue, fontSize: scale(21), textAlign: "center" }}>{'Thank you for your purchase'}</Text>
                        <Text style={{ fontSize: scale(16), color: commonColors.Grey, fontFamily: Fonts.montserratMedium, textAlign: 'center' }}>{'+' + points + ' ' + 'points'}</Text>

                        <Text style={{ paddingTop: scale(50), fontFamily: Fonts.montserratMedium, fontWeight: "700", color: commonColors.Blue, fontSize: scale(18), textAlign: "center" }}>{'How satisfied were you ?'}</Text>
                        <View style={styles.reviewView}>
                            <Text style={[styles.titleText, { alignSelf: 'flex-start' }]}>YOUR RATING</Text>
                            <View style={{ bottom: Platform.OS == 'ios' ? scale(35) : scale(50), }}>
                                <AirbnbRating
                                    type='custom'
                                    count={counting}
                                    defaultRating={5}
                                    reviews={false}
                                    unSelectedColor={commonColors.Grey}
                                    selectedColor={commonColors.Blue}
                                    onFinishRating={ratingCompleted}
                                    size={20}
                                // starContainerStyle={{height:20,width:20}}
                                />
                            </View>
                        </View>
                        <View style={[styles.yourReviewView, , { alignSelf: 'flex-start', justifyContent: "flex-start" }]}>
                            <TextInput
                                keyboardType={'default'}
                                value={yourReview}
                                multiline={true}
                                placeholder={'Your Opinion...'}
                                autoCapitalize='none'
                                onChangeText={(value) => { setyourReview(value) }}
                                style={styles.textInputStyle2}
                            />
                        </View>
                        <CustomButton
                            onPress={() => submitReview()}
                            title={'SUBMIT'}
                            style={styles.submitButton}
                        />
                        {/* {sharebtn ?
                            <CustomButton
                                onPress={() => sharepurchase()}
                                title={'Share your purchase'}
                                style={styles.submitButton}
                            /> : null} */}
                    </View>
                    <RBSheet
                        ref={refRBSheet}
                        closeOnDragDown={false}
                        closeOnPressMask={false}
                        customStyles={{
                            container: {
                                borderTopLeftRadius: scale(20),
                                borderTopRightRadius: scale(20),
                                height: "50%",
                                marginTop: scale(10),

                            },
                            wrapper: {

                            },
                            draggableIcon: {
                                backgroundColor: "#000",

                            }
                        }}
                    >
                        <View style={{ flex: 1, marginTop: scale(20) }}>
                            <Text style={{ fontSize: scale(14), textAlign: 'center', color: commonColors.Blue }}>{'Share your purchase'}</Text>
                        </View>
                    </RBSheet>

                </View>
            </KeyboardAwareScrollView>
            {isModalVisible &&
                <BallIndicator visible={true} />
            }
        </View>
    )
}

const styles = StyleSheet.create({
    mainViewStyle: {
        flex: 1,
        backgroundColor: 'white',
    },
    line: {
        width: 300,
        height: 1,
        backgroundColor: 'lightgrey',
        alignSelf: 'center',
    },
    reviewtitleView: {
        flexDirection: "row",
        marginVertical: scale(20),
        marginHorizontal: scale(20)
    },
    yourReviewView: {
        // flexDirection: "row",
        marginHorizontal: scale(20),
        borderWidth: 0.8,
        borderColor: commonColors.Blue,
        borderRadius: scale(5),
        color: commonColors.Blue,
        fontSize: scale(12),
        fontFamily: Fonts.montserratMedium,
        paddingLeft: scale(10),
        width: deviceWidth - 50,
        height: scale(100),
    },
    reviewHeader: {
        width: '100%',
        textAlign: 'center',
        position: 'absolute',
        fontFamily: Fonts.montserratSemiBold,
        fontSize: scale(20),
        color: commonColors.Blue
    },
    headerLogoStyle: {
        alignItems: 'center',
        height: scale(44),
        flexDirection: 'row'
    },
    submitButton: {
        width: deviceWidth - 50,
        alignSelf: "center",
    },
    titleText: {
        alignSelf: 'center',
        color: commonColors.Blue,
        paddingHorizontal: scale(10),
        fontSize: scale(14),
        fontFamily: Fonts.montserratMedium
    },
    reviewView: {
        flexDirection: "row",
        marginHorizontal: scale(20),
        marginTop: scale(20)
    },
    textInputStyle: {
        borderWidth: 0.4,
        borderColor: commonColors.Grey,
        color: commonColors.Blue,
        fontSize: scale(16),
        paddingHorizontal: scale(5),
        fontFamily: Fonts.montserratMedium,
        alignSelf: 'center',
        height: scale(40),
        width: deviceWidth - 180
    },
    textInputStyle2: {
        // borderWidth: 1,
        // borderColor: commonColors.Blue,
        // borderRadius: scale(10),
        color: commonColors.Blue,
        fontSize: scale(14),
        // paddingTop: scale(5),
        fontFamily: Fonts.montserratMedium,
        // alignSelf: 'center',
        // height: scale(120),
        // padding: scale(5),
        // width: deviceWidth - 50
    },
    clickText: {
        alignSelf: "center",
        bottom: Platform.OS == 'android' ? scale(40) : scale(25),
        paddingLeft: scale(100),
        color: commonColors.Blue
    },
    popUpTextInputStyle: {
        borderWidth: 0.8,
        borderColor: commonColors.Blue,
        borderRadius: scale(2),
        color: commonColors.Blue,
        fontSize: scale(12),
        justifyContent: "center",
        fontFamily: Fonts.montserratMedium,
        alignSelf: "center",
        marginTop: scale(40),
        marginVertical: scale(10),
        paddingHorizontal: scale(5),
        width: (deviceWidth - 100),
        height: scale(40),
    },
})
