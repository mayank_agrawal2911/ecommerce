import { FlatList } from 'react-native-gesture-handler';
import CustomButton from '../../components/CustomButton';
import CustomHeader from '../../components/CustomHeader';
import { React, Text, useEffect, Request, useState, ScrollView, View, StyleSheet, TouchableOpacity, Image, Fonts, commonColors, deviceWidth, useTheme, importImages, scale, showSimpleAlert, } from '../../utils/importLibrary';
export default function CheckOutViewController({ route, navigation }) {
   const [shippingAdress, setshippingAdress] = useState(false);
   const [billingAddress, setbillingAddress] = useState([])
   const [shippingDate, setshippingDate] = useState('')
   const [shippingData, setshippingData] = useState([])
   const [defaultItem, setdefaultItem] = useState('')
   const [address, setaddress] = useState([])
   const [storeID, setstoreID] = useState('')
   const [isdelivereable, setisdelivereable] = useState('')

   useEffect(() => {
      const unsubscribe = navigation.addListener('focus', async () => {
         await getcheckoutdate()
         await getAddressData()
      });
      return unsubscribe;
   }, [navigation]);

   const getcheckoutdate = async () => {

      const response = await Request.post('check-delivery-date.php')
      console.log("responseresponse", response);
      if (response.success == true) {
         setisdelivereable(response.data.is_deliverable)
      } else {

      }
   }
   const getAddressData = async () => {
      let params = {
         domain_id: 1,
      }
      let shipDate = route?.params?.shippingselecteddate
      let store_id = route?.params?.storeId

      let response = await Request.post('get-address.php', params)
      setshippingData(response.data.shipping_addresses)
      setbillingAddress(response.data.billing_address)
      setshippingDate(shipDate ? shipDate : '')
      setstoreID(store_id ? store_id : '')


   }

   const selectDefaultAddress = async (item, index) => {
      let params = {
         address_id: item.id
      }
      let shipDate = route?.params?.shippingselecteddate

      let store_id = route?.params?.storeId
      let response = await Request.post('selected_address.php', params)
      if (response.success == true) {
         let response2 = await Request.post('get-address.php')
         setshippingData(response2.data.shipping_addresses)
         setbillingAddress(response2.data.billing_address)
         setstoreID(store_id ? store_id : '')
      }
   }

   const editAddress = (item) => {
      navigation.navigate("EditAddressViewController", { addressData: item })
   }
   const deleteAddress = async (item) => {
      let params = {
         address_id: item.id
      }
      let response = await Request.post('delete-address.php', params)
      if (response.success == true) {
         getAddressData()
      }
   }
   const saveCheckout = () => {
      console.log("storeIDstoreID", storeID);
      if (shippingData.length == 0) {
         showSimpleAlert("Please add address")
      }
      else if (isdelivereable == 1) {
         if (!shippingDate) {
            showSimpleAlert('Please select date')
         }
         else{
            const filterData = shippingData.filter((item) => item.is_selected == 1)
            navigation.navigate('PlacedOrderViewController', { shippingId: filterData[0], shippingDate: shippingDate, storeId: storeID, isdelivereable: isdelivereable })         }
      }

      else {
         const filterData = shippingData.filter((item) => item.is_selected == 1)
         navigation.navigate('PlacedOrderViewController', { shippingId: filterData[0], shippingDate: shippingDate, storeId: storeID, isdelivereable: isdelivereable })
      }
   }
   const renderItem = ({ item, index }) => {

      console.log("shippingDataitem", item);
      const Address = item.street_number + ', ' + item.street + ', ' + item.state + ' ' + item.city
      return (
         <View style={[styles.shippingAddressView, { marginVertical: scale(10) }]}>
            <View style={{ paddingTop: scale(5) }}>
               <TouchableOpacity onPress={() => {
                  selectDefaultAddress(item, index)
               }}>
                  <Image source={shippingData.length == 1 ? importImages.Checkbox : item.is_selected == 1 ? importImages.Checkbox : importImages.radioUnCheckImage}></Image>
               </TouchableOpacity>
            </View>

            <View style={styles.addressView}>
               <Text style={styles.addressText}>{Address}</Text>
               <Text style={styles.defaultAdressText}>{item.check_default == '' ? '' : 'Default Address'}</Text>
            </View>
            <TouchableOpacity onPress={() => {
               editAddress(item)
            }}>
               <View style={styles.textUnderline}>
                  <Text style={styles.editnameText}>EDIT</Text>
               </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {
               deleteAddress(item)
            }}>
               <View style={styles.textUnderline}>
                  <Text style={styles.editnameText}>Delete</Text>
               </View>
            </TouchableOpacity>
         </View>
      )
   }
   const selectDataRefresh = (data) => {
      console.log("data", data);
      setTimeout(() => {

      }, 200);
   }
   const selectshippingDate = () => {
      console.log("shippingDate", shippingDate);
      const filterData = shippingData.filter((item) => item.is_selected == 1)
      console.log("filterData", filterData);

      if (filterData.length == 0) {
         showSimpleAlert('Please select shipping address')
      }
      else {
         if (filterData[0].city == 'Amman') {
            var today = new Date();
            var dd = today.getDate() + 1;
            var mm = today.getMonth() + 1; //January is 0 so need to add 1 to make it 1!
            var yyyy = today.getFullYear();
            if (dd < 10) {
               dd = '0' + dd
            }
            if (mm < 10) {
               mm = '0' + mm
            }

            today = yyyy + '-' + mm + '-' + dd;
            navigation.navigate("ShippingDateViewController", { shippingDate: shippingDate, shippData: shippingData, minDate: today, selectDataRefresh: (data) => selectDataRefresh(data) })

         }
         else {
            var today = new Date();
            var dd = today.getDate() + 2;
            var mm = today.getMonth() + 1; //January is 0 so need to add 1 to make it 1!
            var yyyy = today.getFullYear();
            if (dd < 10) {
               dd = '0' + dd
            }
            if (mm < 10) {
               mm = '0' + mm
            }
            today = yyyy + '-' + mm + '-' + dd;
            navigation.navigate("ShippingDateViewController", { shippingDate: shippingDate, shippData: shippingData, minDate: today, selectDataRefresh: (data) => selectDataRefresh(data) })

         }

      }
   }
   console.log("shippingData", shippingAdress, shippingData);
   return (
      <View style={styles.container}>
         <ScrollView bounces={false}>
            <View style={{ marginHorizontal: scale(40) }}>
               <CustomHeader
                  leftBtnOnPress={() => navigation.navigate("MyBasketViewController")}
                  leftBtn={<Image source={importImages.backButtonArrow}></Image>}
                  headerTitle={'CHECKOUT'}
               />
               <View style={styles.subContainer}>
                  <TouchableOpacity onPress={() => {
                     setshippingAdress(!shippingAdress)
                  }}>
                     <View style={styles.checkoutContainer}>
                        <View style={{ flexDirection: "row", }}>
                           {shippingAdress ?

                              <Image source={importImages.greenTick} style={{ alignSelf: "center" }} /> :
                              <View style={styles.numberView}>
                                 <Text style={styles.numberText}>1</Text>
                              </View>}

                           <View style={styles.labelView}>
                              <Text style={styles.labelText}>SHIPPING ADDRESS</Text>
                           </View>
                        </View>

                        <View style={styles.labelView}>
                           <Image source={shippingAdress ? importImages.downArrow : importImages.leftArrow} style={{ alignSelf: "center" }}></Image>
                        </View>
                     </View>
                  </TouchableOpacity>
                  {
                     shippingAdress ?
                        <View>
                           <FlatList

                              style={{ flex: 1, width: deviceWidth - 70 }}
                              contentContainerStyle={{ flexGrow: 1 }}
                              data={shippingData}
                              renderItem={renderItem}
                              extraData={shippingData}
                           />
                           <TouchableOpacity onPress={() => navigation.navigate('NewAddressViewController')} style={styles.addnewAddresstextUnderline}>
                              <Text style={styles.addnewAdrressText}>ADD NEW ADDRESS</Text>
                           </TouchableOpacity>
                        </View> : null}
                  {
                     shippingAdress ?
                        <View style={{ marginTop: scale(20) }}>
                           <View style={styles.borderline} />
                        </View> : null}

                  <TouchableOpacity onPress={() => selectshippingDate()}>
                     {shippingData && shippingData.length > 0 ?
                        <View style={styles.checkoutContainer}>
                           <View style={{ flexDirection: "row", }}>
                              <View style={styles.numberView}>
                                 <Text style={styles.numberText}>2</Text>
                              </View>
                              <View style={styles.labelView}>
                                 <Text style={styles.labelText}>SHIPPING DATE</Text>
                              </View>
                           </View>
                           <View style={styles.labelView}>
                              <Image source={importImages.leftArrow} ></Image>
                           </View>
                        </View> : null}
                  </TouchableOpacity>
                  {shippingDate ?
                     <TouchableOpacity onPress={() => navigation.navigate("BillingAddressViewController", { billingAddress: billingAddress, shippingaddressData: shippingData })}>
                        <View style={styles.checkoutContainer}>
                           <View style={{ flexDirection: "row", }}>
                              <View style={styles.numberView}>
                                 <Text style={styles.numberText}>3</Text>
                              </View>
                              <View style={styles.labelView}>
                                 <Text style={styles.labelText}>BILLING ADDRESS</Text>
                              </View>
                           </View>
                           <View style={styles.labelView}>
                              <Image source={importImages.leftArrow} ></Image>
                           </View>
                        </View>
                     </TouchableOpacity> : null}

                  {
                     shippingAdress ?
                        <View style={styles.borderline} /> : null}
               </View>
            </View>
         </ScrollView>
         <CustomButton
            onPress={() => { saveCheckout() }}
            title={'NEXT'}
         />
      </View>
   );
}

const styles = StyleSheet.create({

   container: {
      backgroundColor: commonColors.White,
      flex: 1
   },
   headerLogoStyle: {
      flexDirection: 'row',
      justifyContent: "flex-start",
      marginVertical: scale(20)
   },
   headerText: {
      color: commonColors.Blue,
      fontSize: scale(18),
      alignSelf: "center",
      paddingLeft: scale(10),
      fontFamily: Fonts.montserratSemiBold,
   },
   borderline: {
      height: 2,
      backgroundColor: commonColors.LightGrey,
   },
   subContainer: {
   },
   checkoutContainer: {
      flexDirection: "row",
      marginVertical: scale(15),
      justifyContent: "space-between"
   },
   numberText: {
      color: commonColors.White,
      textAlign: "center",
      fontFamily: Fonts.montserratBold,
      fontSize: scale(10)
   },
   numberView: {
      height: scale(15),
      width: scale(15),
      backgroundColor: commonColors.Blue,
      borderRadius: scale(12),
      alignSelf: "center",
      justifyContent: "center"
   },
   labelView: {
      justifyContent: "center"
   },
   labelText: {
      color: commonColors.Blue,
      textAlign: "center",
      fontFamily: Fonts.montserratMedium,
      fontSize: scale(16),
      marginHorizontal: scale(15)
   },
   ButtonStyle: {
      height: scale(50),
      borderWidth: 1,
      alignSelf: 'center',
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 5,
      borderColor: commonColors.Blue,
      width: deviceWidth - 70,
      backgroundColor: commonColors.Blue,
      marginTop: scale(200)
   },
   nextText: {
      fontSize: scale(20),
      fontFamily: Fonts.montserratMedium,
      color: commonColors.White,
   },
   addnewAdrressText: {
      color: commonColors.Blue,
      textDecorationLine: 'underline',
      fontSize: scale(10),
   },
   editnameText: {
      color: commonColors.Blue,
      padding: scale(2),
      fontSize: scale(8)
   },
   defaultAdressText: {
      fontSize: scale(11),
      color: commonColors.Blue,
      opacity: 0.4,
      fontStyle: 'italic'
   },
   addressText: {
      fontSize: scale(15),
      color: commonColors.Blue,
      fontFamily: Fonts.montserratMedium,
      width: scale(180),
   },
   addressView: {
      flexDirection: "column",
      justifyContent: "center",
      paddingHorizontal: scale(10)
   },
   shippingAddressView: {
      flexDirection: "row",
   },
   textUnderline: {
      borderBottomWidth: 1,
      borderColor: commonColors.Blue,
      height: scale(15),
      marginLeft: scale(10),
   },
   addnewAddresstextUnderline: {
      borderColor: commonColors.Blue,
      marginTop: scale(10),
      marginLeft: scale(20),
   }
})



