import { React, useRef, importImages, TouchableOpacity, TextInput, useState, useEffect, showSimpleAlert, Request, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale } from '../../utils/importLibrary'
import MapView, { AnimatedRegion, PROVIDER_GOOGLE } from 'react-native-maps';
import { Marker } from 'react-native-maps';
import { Linking, } from "react-native";
import { Platform } from 'react-native';
export default function StorelistDetails({ route, navigation }) {
    const [arrMainCategories, setArrMainCategories] = useState([])
    const [isModalVisible, setModalVisible] = useState(false);
    const [nostorefound, setnostorefound] = useState('')
    const btnBackClick = () => {
        navigation.goBack()
    }

    const mapViewRef = useRef(null);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getStoreList();
        });
        return unsubscribe;
    }, [navigation])
    const getStoreList = async () => {
        console.log("route.parmas", route.params.storeItemData);
        setArrMainCategories(route.params.storeItemData)
        const latitude = 33.189368147027565; // latitude of your desire location
        const longitude = 73.35574341458842; // longitude of your desire location
        const scheme = Platform.select({
            ios: "maps:0,0?q=",  // if device is ios 
            android: "geo:0,0?q=", // if device is android 
        });
        const latLng = `${latitude},${longitude}`;
        const label = route.params.storeItemData.store_name;
        const url = Platform.select({
            ios: `${scheme}${label}@${latLng}`,
            android: `${scheme}${latLng}(${label})`,
        });
        Linking.openURL(url);
    }


   
    const onMarkerPress = () => {
        console.log("mapView", mapViewRef);
        let r = {
            latitude: parseFloat(arrMainCategories.latitude),
            longitude: parseFloat(arrMainCategories.longitude),
            latitudeDelta: 0.002,
            longitudeDelta: 0.005,
        }
        mapViewRef.current.animateToRegion(r, 2000)
    }
    return (
        <View style={styles.mainViewStyle}>

            <View style={styles.headerLogoStyle}>
                <Text style={{ width: '100%', textAlign: 'center', position: 'absolute', fontFamily: Fonts.montserratSemiBold, fontSize: 20, color: commonColors.Blue }}>STORE </Text>
                <TouchableOpacity onPress={() => btnBackClick()} style={{ marginLeft: 16 }}>
                    <Image source={importImages.backButtonArrow} style={{}} />
                </TouchableOpacity>
            </View>
            {isModalVisible &&
                <BallIndicator visible={isModalVisible} />
            }
        </View>
    )
}

const styles = StyleSheet.create({
    mainViewStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    headerLogoStyle: {
        alignItems: 'center',
        height: 44,
        flexDirection: 'row'
    },
    textStyle: {
        marginTop: 4,
        fontFamily: Fonts.montserratMedium,
        fontSize: scale(16),
        color: commonColors.Blue
    },
    textStyle2: {
        marginTop: 4,
        fontFamily: Fonts.montserratRegular,
        fontSize: scale(16),
        color: commonColors.Blue
    },
    mapStyle: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
})