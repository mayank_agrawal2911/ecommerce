import { React, importImages, WebView, TouchableOpacity, Request, TextInput, useState, useEffect, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale } from '../../utils/importLibrary'
import { Linking, } from "react-native";
import { openInbox, openComposer } from "react-native-email-link";
import SkeletonContent from 'react-native-skeleton-content-nonexpo';

export default function ContactUsViewController({ route, navigation }) {

   const btnBackClick = () => {
      navigation.goBack()
   }
   const [isModalVisible, setModalVisible] = useState(false);
   const [contactusData, setcontactusData] = useState([]);
   const [noproduct, setnoproduct] = useState('');

   useEffect(() => {
      const unsubscribe = navigation.addListener('focus', () => {
         contactus();
      });
      return unsubscribe;
   }, [navigation])
   const contactus = async () => {
      setModalVisible(true)
      let response = await Request.post('contact-us.php',)
      setModalVisible(false)
      if (response.success == true) {
         if (response.data.length > 0) {
            setcontactusData(response.data)
         }
         else {
            setnoproduct('No Data Found')
         }
      }
      else {
         if (response) {
            showSimpleAlert(response.message)
         }
      }
   }
   const contactusRedirection = async (slug) => {
      Linking.openURL(slug);
   }
   const renderItem = ({ item, index }) => {
      return (
         <View>

            {(item.value).includes('https') ?
               <TouchableOpacity style={styles.optionsTextStyle} onPress={() => contactusRedirection(item.value)}>
                  <Text style={styles.textStyle}>{item.title}</Text>
               </TouchableOpacity> :

               <TouchableOpacity onPress={() => {

                  item.slug == 'Phone Number' ? Linking.openURL(`tel:${item.value}`) : item.slug == 'Email' ? openComposer({
                     to: item.value
                  }) : null
               }}>
                  <View style={[styles.optionsTextStyle, {}]}>
                     <Text style={styles.textStyle}>{item.title}</Text>
                  </View>
               </TouchableOpacity>
            }
            <View style={styles.seperatoreStyle}></View>
         </View>
      );
   }
   return (
      <View style={styles.mainViewStyle}>
         <TouchableOpacity onPress={() => btnBackClick()} style={{ marginLeft: 16, marginTop: 16 }}>
            <Image source={importImages.backButtonArrow} style={{}} />
         </TouchableOpacity>

         <View style={styles.headerLogoStyle}>
            <Text style={{ fontFamily: Fonts.montserratSemiBold, fontSize: 24, color: commonColors.Blue, marginBottom: 8 }}>Contact Us</Text>
         </View>
         <View style={styles.seperatoreStyle}></View>



         {contactusData.length > 0 ?
            <View>
               <FlatList
                  data={contactusData}
                  renderItem={renderItem}
                  keyExtractor={(item, index) => index.toString()}
                  showsVerticalScrollIndicator={false}
               /></View> :
            <View style={styles.nodatafoundView}>
               <Text style={styles.transactionText}>{noproduct}</Text>
            </View>}
         {isModalVisible ?
            <BallIndicator visible={isModalVisible} /> : null}
      </View>
   )
}
const styles = StyleSheet.create({
   mainViewStyle: {
      flex: 1,
      backgroundColor: 'white'
   },
   headerLogoStyle: {
      marginLeft: 40,
      marginTop: 24
   },
   optionsTextStyle: {
      marginLeft: 52,
      marginVertical: scale(10),
      justifyContent: 'center'
   },
   seperatoreStyle: {
      marginHorizontal: scale(30),
      borderColor: commonColors.Blue,
      borderBottomWidth: StyleSheet.hairlineWidth,
      marginVertical: scale(5),
   },
   textStyle: {
      fontFamily: Fonts.montserratRegular,
      fontSize: scale(15),
      color: commonColors.lightBlue
   },
   valueText: {
      paddingTop: scale(5),
      fontFamily: Fonts.montserratRegular,
      fontSize: scale(13),
      fontWeight: "200",
      color: commonColors.Blue
   },
   transactionText: {
      color: commonColors.Blue,
      marginTop: scale(200),
      fontSize: scale(20),
      justifyContent: "center",
      alignSelf: 'center',
      fontFamily: Fonts.montserratMedium
   },
   nodatafoundView: {
      marginTop: scale(40),
      justifyContent: 'center',
      alignContent: 'center',
      alignSelf: 'center'
   },
   transactionText: {
      fontSize: scale(16),
      color: commonColors.Blue,
      fontFamily: Fonts.montserratMedium,
      alignSelf: "center"
   },
})