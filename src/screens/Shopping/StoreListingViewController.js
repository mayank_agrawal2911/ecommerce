import { Linking } from 'react-native';
import { React, importImages, TouchableOpacity, TextInput, useState, useEffect, showSimpleAlert, Request, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale } from '../../utils/importLibrary'
import SkeletonContent from 'react-native-skeleton-content-nonexpo';

export default function StoreListingViewController({ route, navigation }) {
   const [arrMainCategories, setArrMainCategories] = useState([])
   const [isModalVisible, setModalVisible] = useState(false);
   const [nostorefound, setnostorefound] = useState('')

   const btnBackClick = () => {
      navigation.goBack()
   }

   useEffect(() => {
      getStoreList();
   }, [navigation])
   const getStoreList = async () => {
      setModalVisible(true)
      let params = { domain_id: 8 }
      if (arrMainCategories.length > 0) {
         setTimeout(() => {
            setModalVisible(false)
         }, 100);
      }
      let response = await Request.post('get-store.php', params)
      setModalVisible(false)
      if (response.success == true) {
         if (response.data.length > 0) {
            setArrMainCategories(response.data)
         }
         else {
            setnostorefound('No store found')
         }
      }
      else {
         if (response) {
            showSimpleAlert(response.message)
         }
      }
   }


   const openMaps = (storeItem) => {
      console.log("storeitem234", storeItem);
      const latitude = storeItem.latitude; // latitude of your desire location
      const longitude = storeItem.longitude; // longitude of your desire location
      const scheme = Platform.select({
         ios: "maps:0,0?q=",  // if device is ios 
         android: "geo:0,0?q=", // if device is android 
      });
      const latLng = `${latitude},${longitude}`;
      const label = storeItem.store_name;
      const url = Platform.select({
         ios: `${scheme}${label}@${latLng}`,
         android: `${scheme}${latLng}(${label})`,
      });

      Linking.openURL(url);

   }
   const renderNewArrivalItem = (item, index) => {
      const storeItem = item.item
      console.log("storeItem", item);

      return (
         <TouchableOpacity onPress={() => {
            openMaps(storeItem)
         }}>

            <View style={styles.renderView} >
               <View style={{ marginHorizontal: 12, flex: 1 }}>
                  <Text style={styles.storenameText}>{storeItem.store_name}</Text>
                  <View style={{ flexDirection: 'row', marginTop: 4, }}>
                     <Image source={importImages.LocationIcon} style={{ marginTop: scale(2) }} />
                     <Text style={[styles.textStyle, { marginTop: 0, marginLeft: 4 }]}>{storeItem.address}</Text>
                  </View>
                  <View style={styles.phoneView}>
                     <Image source={importImages.PhoneIcon} style={{}} />
                     <Text style={[styles.textStyle, { marginTop: 0, marginLeft: 4 }]}>{storeItem.ph_no} EXT : {storeItem.extension}</Text>
                  </View>
                  <Text style={styles.textStyle}>{storeItem.opening_hours.replace('<br>', '\n')}</Text>
               </View>
            </View>
         </TouchableOpacity>
      )
   }

   return (
      <View style={styles.mainViewStyle}>

         <View style={styles.headerLogoStyle}>
            <Text style={styles.headerText}>STORE LIST</Text>
            <TouchableOpacity onPress={() => btnBackClick()} style={{ marginLeft: 16 }}>
               <Image source={importImages.backButtonArrow} style={{}} />
            </TouchableOpacity>
         </View>
         <View style={styles.lineView}></View>
         {arrMainCategories.length > 0 ?
            < FlatList
               bounces={true}
               renderItem={renderNewArrivalItem}
               data={arrMainCategories}
               style={{ marginTop: 8, flex: 1, }}
               contentContainerStyle={{ alignItems: 'center' }}
               showsVerticalScrollIndicator={false}
               showsHorizontalScrollIndicator={false}
               keyExtractor={(item, index) => `${index}-id`}
            /> : <View style={styles.noDataView}>
               <Text style={styles.transactionText}>{nostorefound}</Text>
            </View>}

         {/* {isModalVisible ?
            <View style={{ flexDirection: "column", }}>
               <SkeletonContent
                  isLoading={true}
                  containerStyle={{ flexDirection: "column", marginHorizontal: scale(15) }}
                  layout={[
                     { key: 'someotherId2', width: (deviceWidth - 50), marginVertical: scale(5), height: scale(120), borderRadius: scale(10) },
                     { key: 'otherId3', width: (deviceWidth - 50), height: scale(120), marginVertical: scale(5), borderRadius: scale(10) },
                     { key: 'Id4', width: (deviceWidth - 50), height: scale(120), marginVertical: scale(5), borderRadius: scale(10) },
                     { key: 'id5', width: (deviceWidth - 50), height: scale(120), marginVertical: scale(5), borderRadius: scale(10) },
                     { key: 'id6', width: (deviceWidth - 50), height: scale(120), marginVertical: scale(5), borderRadius: scale(10) },
                     { key: 'id7', width: (deviceWidth - 50), height: scale(120), marginVertical: scale(5), borderRadius: scale(10) },

                  ]}
                  animationDirection="horizontalLeft"
                  animationType='shiver'
               />
            </View> : null
         } */}

         {isModalVisible ?
            <BallIndicator visible={isModalVisible} /> : null}
      </View>
   )
}

const styles = StyleSheet.create({
   mainViewStyle: {
      flex: 1,
      backgroundColor: 'white'
   },
   headerLogoStyle: {
      alignItems: 'center',
      height: 44,
      flexDirection: 'row'
   },
   storenameText: {
      fontFamily: Fonts.montserratBold,
      fontSize: scale(13),
      color: commonColors.Blue
   },
   phoneView: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: 4
   },
   renderView: {
      width: scale(300),
      marginVertical: 8,
      backgroundColor: commonColors.White,
      borderRadius: 10,
      shadowColor: commonColors.Blue,
      shadowOffset: { width: 0, height: 0 },
      shadowOpacity: 0.5,
      shadowRadius: 2,
      elevation: 5,
      paddingVertical: 12
   },
   lineView: {
      width: 300,
      height: 1,
      backgroundColor: 'lightgrey',
      alignSelf: 'center',
   },
   headerText: {
      width: '100%',
      textAlign: 'center',
      position: 'absolute',
      fontFamily: Fonts.montserratSemiBold,
      fontSize: 20,
      color: commonColors.Blue
   },
   noDataView: {
      flex: 1,
      height: '100%',
      justifyContent: 'center',
      alignContent: 'center',
      alignSelf: 'center'
   },
   textStyle: {
      marginTop: 4,
      fontFamily: Fonts.montserratMedium,
      fontSize: 13,
      color: commonColors.Blue
   }
})