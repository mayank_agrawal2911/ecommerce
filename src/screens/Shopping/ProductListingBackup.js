import { React, SectionList, Animated, useRef, Request, useCallback, showSimpleAlert, Dimensions, ProductComponent, importImages, TouchableOpacity, TextInput, useState, useEffect, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale, Platform } from '../../utils/importLibrary'
import { Dropdown } from 'react-native-element-dropdown';
import Share from 'react-native-share';
import Voice from '@react-native-community/voice';
import SpeechToText from 'react-native-google-speech-to-text';
import CustomButton from '../../components/CustomButton';
import deviceInfoModule, { hasNotch } from 'react-native-device-info';
import { Alert, Linking, } from "react-native";
import ParallaxScrollView from 'react-native-parallax-scroll-view-fix';
import { checkMultiple, PERMISSIONS, requestMultiple } from 'react-native-permissions';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import FastImage from 'react-native-fast-image';
import RBSheet from "react-native-raw-bottom-sheet";
import MultiSlider from '@ptomasroos/react-native-multi-slider';



export default function Product({ route, navigation }) {
   const { ColorName, colors, icons, setScheme } = useTheme();
   const [userData, setUserDate] = useState('')
   const [StickyHeaderHeightSet, setStickyHeaderHeightSet] = useState(0)
   const [searchText, setSearchText] = useState('')
   const [arrMainCategories, setArrMainCategories] = useState(["NEW", "BRANDS", "FRAGRANCES", "MAKEUP", "SKINCARE", "WATCHES", "WATCHES1"])
   const [colorCombination, setColorCombination] = useState(["#4B241C", "#DA749B", "#832325", "#A70B23", "#A10034", "#A6374A"])
   const [colorSelectedIndex, setColorSelectedIndex] = useState(0)
   const [arrToWhom, setArrToWhom] = useState([])
   const [sortValue, setSortValue] = useState('');
   const [filterValue, setFilterValue] = useState(null);
   const [KeyName, setKeyName] = useState('')
   const [bannerImage, setbannerImage] = useState([])
   const [morecategory, setmorecategory] = useState([])
   const [isModalVisible, setModalVisible] = useState(false);
   const [loginKey, setloginKey] = useState('')
   const [filterData, setfilterData] = useState([])
   const [loadervalue, setloadervalue] = useState(false)
   const [modalvalues, modalVisible] = useState(false);
   const [searchData, setsearchData] = useState([]);
   const [pitch, setPitch] = useState('');
   const [error, setError] = useState('');
   const [end, setEnd] = useState('');
   const [started, setStarted] = useState(false);
   const [results, setResults] = useState([]);
   const [mikestop, setMikeStop] = useState(true);
   const [partialResults, setPartialResults] = useState([]);
   const [noproduct, setnoproduct] = useState('');
   const [arrwhomdata, setarrwhomdata] = useState('');
   const [newfiltertype, setnewfiltertype] = useState('')
   const [maincatId, setmaincatId] = useState('')
   const [keytype, setkeytype] = useState('')
   const [sortfiltertype, setsortfiltertype] = useState('')
   const [sortfilterword, setsortfilterword] = useState('')
   const [KeyWord, setKeyword] = useState('')
   const [pageNumber, setpageNumber] = useState('1')
   const [limit, setlimit] = useState(0)
   const [totaldatalength, settotaldatalength] = useState([])
   const [subcategoryData, setsubcategoryData] = useState([])
   const [footermodalVisible, setfootermodalVisible] = useState(false)
   const [category_id, setcategory_id] = useState('')
   const [segmentName, setsegmentName] = useState('')
   const [maincatName, setmaincatName] = useState('')
   const [subcatName, setsubcatName] = useState('')
   const [catName, setcatName] = useState('')
   const refContainer = useRef();
   const [sectionIndex, setSectionIndex] = useState(true)
   const [showHeader, setshowHeader] = useState(false)
   const [categorymodal, setcategorymodal] = useState(true)
   const [subcategorymodal, setsubcategorymodal] = useState(false)
   const [categorykeytype, setcategorykeytype] = useState('')
   const [categorykeyword, setcategorykeyword] = useState('')
   const [categoryfiltertype, setcategoryfiltertype] = useState('')
   const [categoryfilterword, setcategoryfilterword] = useState('')
   const [categorybrandfilter, setcategorybrandfilter] = useState([])
   const [categorysortvalue, setcategorysortvalue] = useState('')
   const [subcatId, setsubcatId] = useState('')
   const [maincategoryId, setmaincategoryId] = useState('')
   const [filterflag, setfilterflag] = useState('')
   const [filtermodalvisible, setfiltermodalvisible] = useState(false)
   const [favouritemodalvisible, setfavouritemodalvisible] = useState(false)
   /// filterarray
   const [filtersegmentData, setfiltersegmentData] = useState([])
   const [filtersizeData, setfiltersizeData] = useState([])
   const [sizeDataModal, setsizeDataModal] = useState(false)
   const [filterfragtype, setfilterfragtype] = useState([])
   const [filterfragtypeModal, setfilterfragtypeModal] = useState(false)
   const [concernmodal, setconcernmodal] = useState(false)
   const [concernData, setconcernData] = useState([])
   const [collectionData, setcollectionData] = useState([])
   const [collectionDataModal, setcollectionDataModal] = useState(false)
   const [brandData, setbrandData] = useState([])
   const [brandModal, setbrandModal] = useState(false)
   const [minsliderprice, setminsliderprice] = useState('')
   const [maxsliderprice, setmaxsliderprice] = useState('')
   const [priceModal, setpriceModal] = useState(false)
   const [minprice, setminprice] = useState('')
   const [maxprice, setmaxprice] = useState('')
   const [sliderValue, setSliderValue] = useState('')
   const [maincategId, setmaincategId] = useState('')
   const [isfilter, setisfilter] = useState(false)

   const refRBSheet = useRef()
   const INTERVAL_REFRESH = 3000;


   useEffect(() => {
      getProductListing()
      getProductFilterListing()
   }, []);

   const getProductListing = async () => {
      console.log("productType:-->?", route.params);
      let userData = await StorageService.getItem(StorageService.STORAGE_KEYS.USER_DETAILS)
      setloginKey(userData)
      const keyType = route.params.productItem.type
      const Keyword = route.params.productItem.key_word
      const KeyName = route.params.productItem.name ? route.params.productItem.name :''
      const filterype = route.params.productItem.filter_type ? route.params.productItem.filter_type : ''
      const filterWord =  route.params.productItem.filter_word ? route.params.productItem.filter_word :''
      const searchData = route.params.qrData ? route.params.qrData : ''
      const mainCatname = route.params.main_CatName ? route.params.main_CatName : route.params.productItem.name ? route.params.productItem.name : route.params.name ? route.params.name : null
      const subCatname = route.params.sub_catName ? route.params.sub_catName : ''
      const catName = route.params.cat_Name ? route.params.cat_Name : ''
      setnewfiltertype(filterype)
      setmaincatId(filterWord)
      setKeyword(Keyword)
      setkeytype(keyType)
      setarrwhomdata(route.params.arrwhomData)
      setKeyName(KeyName)
      setSearchText(searchData)
      setmaincatName(mainCatname)
      setsubcatName(subCatname)
      setcatName(catName)
      setlimit('0')
      setcatName('')
      setsubcatName('')
      settotaldatalength(0)
      setcategorymodal(true)
      setsubcategorymodal(false)
      let params2 = {
         filter_type: route.params.productItem.filter_type ? route.params.productItem.filter_type : route.params.filtertype ? route.params.filtertype : '',
         filter_word: route.params.productItem.filter_word ? route.params.productItem.filter_word : route.params.filter_word ? route.params.filter_word : '',
         key_type: route.params.productItem.key_type,
         key_word: Keyword,
         start: '0',
         limit: limit + 20,
      }
      const data = params2
      setModalVisible(true)
      let response = await Request.post('get-product-list.php', data)
      setModalVisible(false)
      if (response.success == true) {
         if (response.data.result.length > 0) {
            setbannerImage(response.data.banner_image)
            setmorecategory(response.data.result)
            setnoproduct('')
            settotaldatalength(response.data.total)
         }
         else {
            setnoproduct('No product found')
         }

         setlimit(limit + 20)
      }
      else {
         if (response) {
            showSimpleAlert(response.message)
         }
      }
   }

   const getProductFilterListing = async () => {
      const KeyName = route.params.productItem.name
      const Keyword = route.params.productItem.key_word
      const mainCategory = route.params.productItem.main_cat_id ? route.params.productItem.main_cat_id : ''
      setKeyName(KeyName)
      setmaincategId(mainCategory)
      let params = {
         key_type: route.params.productItem.key_type,
         key_word: Keyword,
         main_category_id: mainCategory,
         category_id: '',
         filter_type: '',
         filter_word: '',
      }
      let response = await Request.post('get-product-filter-list.php', params)
      setfiltermodalvisible(false)
      if (response.success == true) {
         setfilterData(response.data.length > 0 ? response.data[0] : [])
         setArrToWhom(response.data.length > 0 ? response.data[0].title : [])
         setsubcategoryData(response.data.length > 0 ? response.data[0].sub_category : [])
         setfiltersegmentData(response.data.length > 0 ? response.data[0].filter_segment : [])
         setfiltersizeData(response.data.length > 0 ? response.data[0].filter_size : [])
         setfilterfragtype(response.data.length > 0 ? response.data[0].filter_fragtype : [])
         setcollectionData(response.data.length > 0 ? response.data[0].row_collections : [])
         setconcernData(response.data.length > 0 ? response.data[0].row_concerns : [])
         setbrandData(response.data.length > 0 ? response.data[0].filter_brand : [])
         setminprice(response.data.length > 0 ? response.data[0].min_price : '')
         setmaxprice(response.data.length > 0 ? response.data[0].max_price : '')
         setSliderValue(response.data[0].min_price + '' + "JOD - " + response.data[0].max_price + '' + "JOD")
         setminsliderprice(response.data.length > 0 ? response.data[0].min_price : '')
         setmaxsliderprice(response.data.length > 0 ? response.data[0].max_price : '')
      }
      else {
         if (response) {
            showSimpleAlert(response.message)
         }
      }
   }

   const sortDataArray = [
      { label: 'Relevancy', value: 'relevancy' },
      { label: 'Best Seller', value: 'best-seller' },
      { label: 'New Arrival', value: 'new-arrival' },
      { label: 'Price : Low to High', value: 'pasc' },
      { label: 'Price : High to Low', value: 'pdesc' },
      { label: 'Alphabetically : A-Z', value: 'aasc' },
      { label: 'Alphabetically :   Z-A', value: 'adesc' },
   ];
   const sortDataArray2 = [
      { label: 'Relevancy', value: 'relevancy' },
      { label: 'Price : Low to High', value: 'pasc' },
      { label: 'Price : High to Low', value: 'pdesc' },
      { label: 'Alphabetically : A-Z', value: 'aasc' },
      { label: 'Alphabetically :  Z-A', value: 'adesc' },
   ];


   const btnBackClick = () => {

      const categoryFlag = route.params.category ? route.params.category : 0
      if (categoryFlag == 1) {
         navigation.navigate('CategoryViewController')
      }
      else if (categoryFlag == 2) {
         navigation.navigate('CategoryViewController')
      }
      else {
         navigation.navigate('ShoppingHomveViewController')
      }
   }
   const onSpeechStart = () => {
      setStarted('True')
   };
   const onSpeechEnd = () => {
      setStarted(null);
      setEnd('True');
   };
   const onSpeechError = (e) => {
      setError(JSON.stringify(e.error));
   };
   const onSpeechResults = async (e) => {
      setResults(e.value)
      await Voice.destroy()
   };
   const onSpeechPartialResults = async (e) => {
      setPartialResults(e.value)
      setSearchText(e.value[0])
      const data = e.value[0] == '' ? searchText : e.value[0]
      if (data) {
         setStarted(null);
         setEnd('True');
         btnSearchIconClick(data)
         setMikeStop(!mikestop)
         await stopSpeechRecognizing()
      }
   };
   const onSpeechVolumeChanged = (e) => {
      setPitch(e.value)
   };
   const startSpeechRecognizing = async () => {
      setPitch('')
      setError('')
      setStarted('')
      setResults([])
      setPartialResults([])
      setSearchText('')
      setEnd('')
      try {
         await Voice.start('en-US')
         if (mikestop) {
            startSpeechRecognizing()
         }

      } catch (e) {
         console.error(e);
      }
   };
   const stopSpeechRecognizing = async () => {

      try {
         await Voice.stop();
      } catch (e) {
         console.error(e);
      }
   };
   Voice.onSpeechStart = onSpeechStart;
   Voice.onSpeechEnd = onSpeechEnd;
   Voice.onSpeechError = onSpeechError;
   Voice.onSpeechResults = onSpeechResults;
   Voice.onSpeechPartialResults = onSpeechPartialResults;
   Voice.onSpeechVolumeChanged = onSpeechVolumeChanged;

   const btnSearchIconClick = async (text) => {
      const textData = text ? text : ''
      setSearchText(textData)
      if (textData == '') {
         let params2 = {
            key_type: keytype,
            key_word: KeyWord,
            filter_type: newfiltertype,
            filter_word: maincatId,
            start: '0',
            limit: 20,
         }
         const data = params2
         let response = await Request.post('get-product-list.php', data)
         if (response.success == true) {

            if (response.data.result.length > 0) {
               setbannerImage(response.data.banner_image)
               setmorecategory(response.data.result)
               settotaldatalength(response.data.total)
               setnoproduct('')
            }
            else {
               setnoproduct('No product found')
            }

            setSortValue('')
         }
         else {
            if (response) {
               showSimpleAlert(response.message)
            }
         }
      }

      else {
         if (textData.length > 3) {
            setTimeout(async () => {
               let params = {
                  key_type: 'search',
                  key_word: textData,
                  filter_type: keytype,
                  filter_word: KeyWord,

               }
               let params2 = {
                  key_type: 'search',
                  key_word: textData,
                  filter_type: newfiltertype,
                  filter_word: maincatId,
                  segment_filter: [KeyWord],

               }
               const data = keytype == 'segment' ? params2 : params

               let response = await Request.post('get-product-list.php', data)
               if (response.success == true) {
                  if (response.data.result.length > 0) {
                     setbannerImage(response.data.banner_image)
                     setmorecategory(response.data.result)
                     settotaldatalength(response.data.total)
                     setnoproduct('')
                  }
                  else {
                     setnoproduct('No product found')
                  }

               }
               else {
                  if (response) {
                     showSimpleAlert(response.message)
                  }
               }
            }, 500);


         }
      }

   }
   const btnandroidClick = async () => {
      let speechToTextData = '';
      try {
         speechToTextData = await SpeechToText.startSpeech('Try saying something', 'en_IN');
         btnSearchIconClick(speechToTextData)
         setSearchText(speechToTextData)
      } catch (error) {
         console.log('error: ', error);
      }
   }
   const btnCamaraIconClick = () => {
      navigation.navigate('QrcodeProductScreen', { homescreen: 0, productItem: route.params.productItem })
   }
   const btnMikeIconClick = async () => {
      const checkPermission = await checkMultiple([PERMISSIONS.IOS.MICROPHONE, PERMISSIONS.IOS.SPEECH_RECOGNITION])
      if ((checkPermission[PERMISSIONS.IOS.MICROPHONE] == 'denied' || checkPermission[PERMISSIONS.IOS.MICROPHONE] == 'blocked') && (checkPermission[PERMISSIONS.IOS.SPEECH_RECOGNITION] == 'denied' || checkPermission[PERMISSIONS.IOS.SPEECH_RECOGNITION] == 'blocked')) {
         const resultPERMISSIONS = await requestMultiple([PERMISSIONS.IOS.MICROPHONE, PERMISSIONS.IOS.SPEECH_RECOGNITION])
         if (resultPERMISSIONS[PERMISSIONS.IOS.MICROPHONE] == 'granted' && resultPERMISSIONS[PERMISSIONS.IOS.SPEECH_RECOGNITION] == 'granted') {
            setMikeStop(!mikestop)

            if (mikestop) {
               await startSpeechRecognizing()
            } else {
               await stopSpeechRecognizing()
            }
         }
         else {
            Alert.alert(
               ConstantsText.appName,
               'Please allow microphone and speech recognition  permission to recognize voice ',
               [
                  { text: 'Cancel', onPress: () => console.log('cancel') },
                  { text: 'Okay', onPress: () => { Linking.openURL('app-settings:'); } },
               ]
            )
         }
      }
      else {
         setMikeStop(!mikestop)
         if (mikestop) {
            await startSpeechRecognizing()
         } else {
            await stopSpeechRecognizing()
         }
      }


   }
   const getProductcatid = async (item) => {
      setkeytype(item.key_type)
      setKeyword(item.key_word)
      setcategory_id(item.cat_id)
      setnewfiltertype(item.filter_type ? item.filter_type : '')
      setmaincatId(item.filter_word ? item.filter_word : '')
      setmaincategId(item.main_cat_id)
      let params = {
         key_type: item.key_type,
         key_word: item.key_word,
         main_category_id: item.main_cat_id,
         category_id: item.cat_id,
         filter_type: keytype == 'brand' || 'offer' ? item.filter_type : '',
         filter_word: keytype == 'brand' || 'offer' ? item.filter_word : ''
      }
      setmaincategoryId(item.main_cat_id)
      setfiltermodalvisible(true)
      let response = await Request.post('get-product-filter-list.php', params)
      setfiltermodalvisible(false)
      if (response.success == true) {
         setfilterData(response.data.length > 0 ? response.data[0] : [])
         setArrToWhom(response.data.length > 0 ? response.data[0].title : [])
         setsubcategoryData(response.data.length > 0 ? response.data[0].sub_category : [])
         setfiltersegmentData(response.data.length > 0 ? response.data[0].filter_segment : [])
         setfiltersizeData(response.data.length > 0 ? response.data[0].filter_size : [])
         setfilterfragtype(response.data.length > 0 ? response.data[0].filter_fragtype : [])
         setcollectionData(response.data.length > 0 ? response.data[0].row_collections : [])
         setconcernData(response.data.length > 0 ? response.data[0].row_concerns : [])
         setbrandData(response.data.length > 0 ? response.data[0].filter_brand : [])
         setminprice(response.data.length > 0 ? response.data[0].min_price : '')
         setmaxprice(response.data.length > 0 ? response.data[0].max_price : '')
         setminsliderprice(response.data.length > 0 ? response.data[0].min_price : '')
         setmaxsliderprice(response.data.length > 0 ? response.data[0].max_price : '')
         setSliderValue(response.data.length > 0 ? response.data[0].min_price : '' + "JOD - " + response.data.length > 0 ? response.data[0].max_price : '' + "JOD")
      }
      else {
         if (response) {
            showSimpleAlert(response.message)
         }
      }
   }

   const mainCateoriesClick = async (item, index) => {
      setarrwhomdata(item.cat_name)
      setnewfiltertype(item.filter_type ? item.filter_type : '')
      setmaincatId(item.filter_word ? item.filter_word : '')
      setkeytype(item.key_type)
      setKeyword(item.key_word)
      setcatName(item.cat_name)
      setlimit(0)
      settotaldatalength(0)
      setcategorymodal(false)
      setsubcategorymodal(true)
      setsubcatId('')
      setsubcatName('')
      setSortValue('')
      setnoproduct('')
      let params2 = {
         key_type: item.key_type,
         key_word: item.key_word,
         brand_filter: route.params?.brand ? [route.params.productItem?.key_word] : route.params.productItem?.key_type == 'brand' ? [route.params.productItem?.key_word] : [],
         filter_type: item.filter_type ? item.filter_type : '',
         filter_word: item.filter_word ? item.filter_word : '',
         start: '0',
         limit: 20,
      }
      const categorybrand = route.params?.brand ? [route.params.productItem?.key_word] : route.params.productItem?.key_type == 'brand' ? [route.params.productItem?.key_word] : []
      setcategorykeytype(item.key_type)
      setcategorykeyword(item.key_word)
      setcategorysortvalue(sortValue)
      setcategorybrandfilter(categorybrand)
      setcategoryfiltertype(item.filter_type ? item.filter_type : '')
      setcategoryfilterword(item.filter_word ? item.filter_word : '')
      setkeytype(item.key_type)
      setarrwhomdata(item.cat_name)
      setModalVisible(true)

      const data = params2
      let response = await Request.post('get-product-list.php', data)
      setModalVisible(false)
      if (response.success == true) {

         if (response.data.result.length > 0) {
            setbannerImage(response.data.banner_image)
            setmorecategory(response.data.result)
            settotaldatalength(response.data.total)
            setnoproduct('')
         }
         else {
            setnoproduct('No product found')
         }
         setloadervalue(true)
         getProductcatid(item)
      }
      else {
         if (response) {
            showSimpleAlert(response.message)
         }
      }
   }
   const subCateoriesClick = async (item, index) => {
      setkeytype(item.key_type)
      setKeyword(item.key_word)
      setsubcatId(item.sub_cat_id)
      setmaincatName(item.main_cat_name)
      setcatName(item.cat_name)
      setsubcatName(item.sub_cat_name)
      settotaldatalength(0)
      setlimit(0)
      setnoproduct('')
      if (filterflag == true) {
         if (route.params.isfilter == 1) {
            const brandData = route.params.brandData
            const filterbrand = brandData.map((item, index) => {
               return item.brand_id
            })
            const filterfragtype = route.params.fragrancetype.map((item, index) => {
               return item.key
            })
            const filtersegment = route.params.filtersegment.map((item, index) => {
               return item.in_segment
            })
            const filterSize = route.params.sizeData.map((item, index) => {
               return item.key
            })
            const concernData = route.params.concerndata.map((item, index) => {
               return item.id
            })
            const minprice = route.params.minprice
            const maxprice = route.params.maxprice
            const keytype2 = item.key_type

            let params2 = {
               key_type: item.key_type,
               key_word: item.key_word,
               max_price: maxprice,
               min_price: minprice,
               segment_filter: filtersegment,
               brand_filter: filterbrand,
               fragrance_filter: filterfragtype,
               size_filter: filterSize,
               concern_filter: concernData,
               sort: item.value,
               filter_type: item.filter_type,
               filter_word: item.filter_word,
               start: '0',
               limit: 20,
               category_id: item.cat_id

            }
            setSortValue(item.value)
            setModalVisible(true)
            setcategory_id(item.cat_id)
            const data = params2

            let response = await Request.post('get-product-list.php', data)
            setModalVisible(false)
            if (response.success == true) {

               if (response.data.result.length > 0) {
                  setbannerImage(response.data.banner_image)
                  setmorecategory(response.data.result)
                  settotaldatalength(response.data.total)
                  setnoproduct('')
               }
               else {
                  setnoproduct('No product found')
               }
               setloadervalue(true)
            }
            else {
               if (response) {
                  showSimpleAlert(response.message)
               }
            }
         }
         else {
            let params2 = {
               key_type: item.key_type,
               key_word: item.key_word,
               brand_filter: route.params?.brand ? [route.params?.productItem?.key_word] : route.params?.productItem?.key_type == 'brand' ? [route.params?.productItem?.key_word] : [],
               sort: sortValue,
               filter_type: item.filter_type,
               filter_word: item.filter_word,
               start: '0',
               limit: 20,
               category_id: item.cat_id
            }
            setcategory_id(item.cat_id)

            setkeytype(item.key_type)
            setarrwhomdata(item.cat_name)
            setModalVisible(true)
            const data = params2
            let response = await Request.post('get-product-list.php', data)
            setModalVisible(false)
            if (response.success == true) {

               if (response.data.result.length > 0) {
                  setbannerImage(response.data.banner_image)
                  setmorecategory(response.data.result)
                  settotaldatalength(response.data.total)
                  setnoproduct('')
               }
               else {
                  setnoproduct('No product found')
               }

               setloadervalue(true)
            }
            else {
               if (response) {
                  showSimpleAlert(response.message)
               }
            }
         }
      } else {
         let params2 = {
            key_type: item.key_type,
            key_word: item.key_word,
            brand_filter: route.params?.brand ? [route.params?.productItem?.key_word] : route.params?.productItem?.key_type == 'brand' ? [route.params?.productItem?.key_word] : [],
            sort: sortValue,
            filter_type: item.filter_type,
            filter_word: item.filter_word,
            start: '0',
            limit: 20,
            category_id: item.cat_id
         }
         setcategory_id(item.cat_id)

         setkeytype(item.key_type)
         setarrwhomdata(item.cat_name)
         setModalVisible(true)
         const data = params2
         let response = await Request.post('get-product-list.php', data)
         setModalVisible(false)
         if (response.success == true) {
            if (response.data.result.length > 0) {
               setbannerImage(response.data.banner_image)
               setmorecategory(response.data.result)
               settotaldatalength(response.data.total)
               setnoproduct('')
            }
            else {
               setnoproduct('No product found')
            }
            setloadervalue(true)
         }
         else {
            if (response) {
               showSimpleAlert(response.message)
            }
         }
      }
   }
   const favoriteUnFavoriteButtonClick = async (item, index, type) => {
      const favItem = item
      if (loginKey) {
         let params = {
            product_id: favItem.product_id,
         }
         setfavouritemodalvisible(true)
         let response = await Request.post('add-favorite-item.php', params)
         if (response.success == true) {
            if (type === 'newarrival') {
               morecategory[index].is_favourite = item.is_favourite == 0 ? 1 : 0
               setmorecategory(morecategory)
               setnoproduct('No product found')
            }
            setModalVisible(false)
            setfavouritemodalvisible(false)

         }
         else {
            showSimpleAlert(response.message)
         }
      } else {
         navigation.navigate('AuthSelection', { authFlag: 1, })
      }
   }

   const sendReviewButtonClick = (item, index) => {
      const shareItem = item
      const message = 'Product Name:' + shareItem.brand_name + '\n' + shareItem.family_name + '\n' + "Price:" + shareItem.main_price + "JD's"
      const shareOptions = {
         subject: 'Gift Center',
         message: message,
         url: shareItem.family_pic
      };
      const response = Share.open(shareOptions)
         .then(res => {
            console.log(res);
         })
         .catch(err => {
            err && console.log(err);
         });
      return response;
   }

   const newArrivalProductClick = (item) => {
      navigation.navigate('ProductDetailViewController', { seoUrl: item.seo_url, arrwhomdata: arrwhomdata, category: 1, filtertype: newfiltertype, maincatId: maincatId })

   }

   const renderVideoBannerItem = ({ item, index }) => {
      return (
         <View>
            <FastImage
               style={{ width: deviceWidth, height: scale(200) }}
               source={{
                  uri: item.banner,
                  priority: FastImage.priority.high,
               }}
            />
         </View>
      )
   }
   const rendermorecategory = (item, index, type) => {
      return (
         <View style={{}}>
            <ProductComponent
               item={item}
               isLoading={false}
               index={index}
               isFavourite={item.is_favourite == 0 ? false : true}
               mainCateoriesClick={(item, index) => newArrivalProductClick(item, index)}
               colorCombination={colorCombination}
               colorCodeChangedIndex={(i) => colorCodeChangedIndex(i)}
               colorSelectedIndex={colorSelectedIndex}
               favoriteUnFavoriteButtonClick={(item, index) => favoriteUnFavoriteButtonClick(item, index, type)}
               sendReviewButtonClick={(item, index) => sendReviewButtonClick(item, index)}
               isShowOffer={item.has_offer == '' ? false : true}
               isShowColorShade={false}
            />
         </View>
      )
   }
   const setsorting = async (item) => {
      const sortvalue = item.value ? item.value : sortValue
      setSortValue(sortvalue);
      setlimit(0)
      settotaldatalength(0)
      const mainCatId = keytype == 'segment' ? maincatId : route.params?.productItem?.main_cat_id ? route.params?.productItem?.main_cat_id : route.params?.maincatId ? route.params?.maincatId : ''
      setmaincatId(mainCatId)
      if (filterflag == true) {
         if (route.params.isfilter == 1) {
            const brandData = route.params.brandData
            const filterbrand = brandData.map((item, index) => {
               return item.brand_id
            })
            const filterfragtype = route.params.fragrancetype.map((item, index) => {
               return item.key
            })
            const filtersegment = route.params.filtersegment.map((item, index) => {
               return item.in_segment
            })
            const filterSize = route.params.sizeData.map((item, index) => {
               return item.key
            })
            const rowCollections = route.params.collectionData.map((item, index) => {
               return item.id
            })
            const concernData = route.params.concerndata.map((item, index) => {
               return item.id
            })
            const minprice = route.params.minprice
            const maxprice = route.params.maxprice

            let params2 = {
               key_type: keytype,
               key_word: KeyWord,
               max_price: maxprice,
               min_price: minprice,
               segment_filter: filtersegment,
               brand_filter: filterbrand,
               fragrance_filter: filterfragtype,
               size_filter: filterSize,
               concern_filter: concernData,
               sort: sortvalue,
               filter_type: newfiltertype,
               filter_word: keytype == 'segment' ? maincatId : mainCatId,
               start: '0',
               limit: 20,
            }
            setSortValue(item.value)
            setModalVisible(true)
            const data = params2
            let response = await Request.post('get-product-list.php', data)
            setModalVisible(false)
            if (response.success == true) {
               if (response.data.result.length > 0) {
                  setbannerImage(response.data.banner_image)
                  setmorecategory(response.data.result)
                  settotaldatalength(response.data.total)
                  setlimit(response.data.result.length)
                  setnoproduct('')
               }
               else {
                  setnoproduct('No product found')
               }
               setproductfilter()
            }
            else {
               if (response) {
                  showSimpleAlert(response.message)
               }
            }
         }
         else {
            let params2 = {
               key_type: keytype,
               key_word: KeyWord,
               sort: sortvalue,
               filter_type: newfiltertype,
               filter_word: keytype == 'segment' ? maincatId : mainCatId,
               start: '0',
               limit: 20,
            }
            const data = params2
            setModalVisible(true)
            let response = await Request.post('get-product-list.php', data)
            setModalVisible(false)
            if (response.success == true) {
               if (response.data.result.length > 0) {
                  setbannerImage(response.data.banner_image)
                  setmorecategory(response.data.result)
                  settotaldatalength(response.data.total)
                  setlimit(response.data.result.length)
                  setnoproduct('')
               }
               else {
                  setnoproduct('No product found')
               }
               setproductfilter()
            }
            else {
               if (response) {
                  showSimpleAlert(response.message)
               }
            }
         }
      } else {
         let params2 = {
            key_type: keytype,
            key_word: KeyWord,
            sort: sortvalue,
            filter_type: newfiltertype,
            filter_word: keytype == 'segment' ? maincatId : mainCatId,
            start: '0',
            limit: 20,
         }
         const data = params2
         setModalVisible(true)
         let response = await Request.post('get-product-list.php', data)
         setModalVisible(false)
         if (response.success == true) {
            if (response.data.result.length > 0) {
               setbannerImage(response.data.banner_image)
               setmorecategory(response.data.result)
               settotaldatalength(response.data.total)
               setnoproduct('')
               setlimit(response.data.result.length)
            }
            else {
               setnoproduct('No product found')
            }

            setproductfilter()
         }
         else {
            if (response) {
               showSimpleAlert(response.message)
            }
         }
      }
   }
   const setproductfilter = async () => {
      const KeyName = route.params.productItem.name
      const mainCategory = route.params.productItem.main_cat_id
      setmaincategId(mainCategory)
      setKeyName(KeyName)
      let params = {
         key_type: keytype,
         key_word: KeyWord,
         main_category_id: mainCategory,
         category_id: category_id
      }
      setfiltermodalvisible(true)
      let response = await Request.post('get-product-filter-list.php', params)
      setfiltermodalvisible(false)
      if (response.success == true) {
         setfilterData(response.data.length > 0 ? response.data[0] : [])
         setArrToWhom(response.data.length > 0 ? response.data[0].title : [])
         setsubcategoryData(response.data.length > 0 ? response.data[0].sub_category : [])
         setfiltersegmentData(response.data.length > 0 ? response.data[0].filter_segment : [])
         setfiltersizeData(response.data.length > 0 ? response.data[0].filter_size : [])
         setfilterfragtype(response.data.length > 0 ? response.data[0].filter_fragtype : [])
         setcollectionData(response.data.length > 0 ? response.data[0].row_collections : [])
         setconcernData(response.data.length > 0 ? response.data[0].row_concerns : [])
         setbrandData(response.data.length > 0 ? response.data[0].filter_brand : [])
         setminprice(response.data.length > 0 ? response.data[0].min_price : '')
         setmaxprice(response.data.length > 0 ? response.data[0].max_price : '')
         setminsliderprice(response.data.length > 0 ? response.data[0].min_price : '')
         setmaxsliderprice(response.data.length > 0 ? response.data[0].max_price : '')
         setSliderValue(response.data.length > 0 ? response.data[0].min_price : '' + "JOD - " + response.data.length > 0 ? response.data[0].max_price : '' + "JOD")
      }
      else {
         if (response) {
            showSimpleAlert(response.message)
         }
      }
   }
   const renderToWhomItem = ({ item, index }) => {
      return (
         <TouchableOpacity
            style={[styles.dropShadowStyle2, { backgroundColor: item.cat_id == category_id ? commonColors.Blue : commonColors.White, height: 40, marginTop: 4, marginBottom: 8, marginLeft: 4, justifyContent: 'center', marginRight: 8, paddingHorizontal: 12, borderRadius: 5 }]}
            onPress={() => mainCateoriesClick(item, index)}>
            <Text style={{ fontFamily: Fonts.montserratSemiBold, fontSize: 13, color: item.cat_id == category_id ? commonColors.White : commonColors.Blue }}>
               {item.cat_name}
            </Text>
         </TouchableOpacity>
      )
   }
   const rendersubcategoryData = ({ item, index }) => {
      return (
         <TouchableOpacity
            style={[styles.dropShadowStyle2, { backgroundColor: item.sub_cat_id == subcatId ? commonColors.Blue : commonColors.White, height: 40, marginTop: 4, marginBottom: 8, marginLeft: 4, justifyContent: 'center', marginRight: 8, paddingHorizontal: 12, borderRadius: 5 }]}
            onPress={() => subCateoriesClick(item, index)}>
            <Text style={{ fontFamily: Fonts.montserratSemiBold, fontSize: 13, color: item.sub_cat_id == subcatId ? commonColors.White : commonColors.Blue }}>
               {item.sub_cat_name}
            </Text>
         </TouchableOpacity>
      )
   }
   const qrDataRefresh = (data) => {
   }


   const filterByClick = () => {
      const categoryFlag = route.params.category ? route.params.category : 0
      if (filterflag == true) {
         if (route.params.isfilter == 1) {
            const brandDataArray = route.params.brandDataArray
            const filtersegmentData = route.params.filtersegmentData
            const sizeDataArray = route.params.sizeDataArray
            const filterfragtypeArray = route.params.filterfragtypeArray
            const collectionDataArray = route.params.collectionDataArray
            const concernDataArray = route.params.concernDataArray
            navigation.navigate('FilterOptionViewController', {
               filterData: filterData,
               isfilter: 1,
               brandDataArray: brandDataArray,
               filtersegmentData: filtersegmentData,
               sizeDataArray: sizeDataArray,
               collectionDataArray: collectionDataArray,
               concernDataArray: concernDataArray,
               filterfragtypeArray: filterfragtypeArray,
               minprice: route.params.minprice,
               maxprice: route.params.maxprice,
               category_id: category_id,
               subcatId: subcatId,
               productItem: route.params.productItem,
               sortingvalue: sortValue,
               category: categoryFlag,
               arrwhomdata: arrwhomdata,
               filtertype: newfiltertype,
               maincatId: maincatId, keyname: KeyName,
               keytype: keytype, Keyword: KeyWord, categoryID: category_id, segment_name: segmentName, cat_Name: catName, main_CatName: maincatName, sub_catName: subcatName
            })
         }
         else {
            navigation.navigate('FilterOptionViewController', {
               category_id: category_id,
               qrDataRefresh: (data) => qrDataRefresh(data),
               subcatId: subcatId, filterData: filterData, productItem: route.params.productItem, sortingvalue: sortValue, category: categoryFlag, arrwhomdata: arrwhomdata, filtertype: newfiltertype, maincatId: maincatId, keyname: KeyName, keytype: keytype, Keyword: KeyWord, categoryID: category_id, segment_name: segmentName, cat_Name: catName, main_CatName: maincatName, sub_catName: subcatName
            })

         }
      } else {
         navigation.push('FilterOptionViewController', {
            category_id: category_id,
            subcatId: subcatId, filterData: filterData, productItem: route.params.productItem, sortingvalue: sortValue, category: categoryFlag, arrwhomdata: arrwhomdata, filtertype: newfiltertype, maincatId: maincatId, keyname: KeyName, keytype: keytype, Keyword: KeyWord, categoryID: category_id, segment_name: segmentName, cat_Name: catName, main_CatName: maincatName, sub_catName: subcatName
         })
      }
   }
   let new_arrival = 'newarrival'
   const renderLabelItem = (item) => {
      return (
         <View style={{ height: 56, justifyContent: 'center', borderBottomColor: '#CDCECF', marginStart: 20, marginEnd: 20, borderBottomWidth: 1 }}>
            <View style={{}}>
               <Text style={styles.textItem}>{item.label}</Text>
            </View>
         </View>
      );
   };
   const renderFooter = () => {
      return (
         <View style={[styles.footer, { height: footermodalVisible == true ? hasNotch() ? 120 : 100 : null }]}>
            {footermodalVisible &&
               <BallIndicator visible={footermodalVisible} />
            }
         </View>
      );
   }

   const onViewableItemsChanged = React.useRef(({ viewableItems, changed }) => {
      if (changed && changed.length > 0) {
         if (changed.length > 1) {
            if (changed[1].index == 1 && changed[1].isViewable) {
               setSectionIndex(true)

            }
            else {
               setSectionIndex(false)

            }

         }
         else {
            setSectionIndex(true)

         }
      }
   })
   const viewabilityConfig = useRef({
      itemVisiblePercentThreshold: 50,
      waitForInteraction: true,
      minimumViewTime: 5,
   })

   const viewmore = async () => {

      if (filterflag == true) {

         if (route.params.isfilter == 1) {
            const brandData = route.params.brandData
            const filterbrand = brandData.map((item, index) => {
               return item.brand_id
            })
            const filterfragtype = route.params.fragrancetype.map((item, index) => {
               return item.key
            })
            const filtersegment = route.params.filtersegment.map((item, index) => {
               return item.in_segment
            })
            const filterSize = route.params.sizeData.map((item, index) => {
               return item.key
            })
            const rowCollections = route.params.collectionData.map((item, index) => {
               return item.id
            })
            const concernData = route.params.concerndata.map((item, index) => {
               return item.id
            })
            const minprice = route.params.minprice
            const maxprice = route.params.maxprice
            const mainCatId = keytype == 'segment' ? maincatId : route.params?.productItem?.main_cat_id ? route.params?.productItem?.main_cat_id : route.params?.maincatId ? route.params?.maincatId : ''

            let params2 = {
               max_price: maxprice,
               min_price: minprice,
               segment_filter: filtersegment,
               brand_filter: filterbrand,
               fragrance_filter: filterfragtype,
               size_filter: filterSize,
               concern_filter: concernData,
               filter_word: keytype == 'segment' ? maincatId : mainCatId,
               key_type: keytype,
               key_word: KeyWord,
               sort: sortValue ? sortValue : '',
               filter_type: newfiltertype ? newfiltertype : '',
               filter_word: maincatId ? maincatId : '',
               start: '0',
               limit: morecategory.length + 20,
            }
            setfootermodalVisible(true)
            setModalVisible(false)
            const data = params2
            let response = await Request.post('get-product-list.php', data)
            setfootermodalVisible(false)
            if (response.success == true) {

               if (response.data.result.length > 0) {
                  setbannerImage(response.data.banner_image)
                  setmorecategory(response.data.result)
                  settotaldatalength(response.data.total)
                  setnoproduct('')
                  setlimit(response.data.result.length)
               }
               else {
                  setnoproduct('No product found')
               }
               setproductfilter()
            }
            else {
               if (response) {
                  showSimpleAlert(response.message)
               }
            }
         } else {
            let params = {
               key_type: keytype,
               key_word: KeyWord,
               sort: sortValue ? sortValue : '',
               filter_type: newfiltertype ? newfiltertype : '',
               filter_word: maincatId ? maincatId : '',
               start: '0',
               limit: morecategory.length + 20,
            }
            setModalVisible(false)
            setfootermodalVisible(true)
            let response = await Request.post('get-product-list.php', params)
            setfootermodalVisible(false)
            if (response.success == true) {
               if (response.data.result.length > 0) {
                  setbannerImage(response.data.banner_image)
                  setmorecategory(response.data.result)
                  settotaldatalength(response.data.total)
                  setnoproduct('')
                  setlimit(response.data.result.length)
               }
               else {
                  setnoproduct('No product found')
               }
            }
            else {
               if (response) {
                  showSimpleAlert(response.message)
               }
            }
         }
      } else {
         let params = {
            key_type: keytype,
            key_word: KeyWord,
            sort: sortValue ? sortValue : '',
            filter_type: newfiltertype ? newfiltertype : '',
            filter_word: maincatId ? maincatId : '',
            start: '0',
            limit: morecategory.length + 20,
         }
         setModalVisible(false)
         setfootermodalVisible(true)
         let response = await Request.post('get-product-list.php', params)
         setfootermodalVisible(false)
         if (response.success == true) {
            // setbannerImage(response.data.banner_image)
            // setmorecategory(response.data.result)
            // settotaldatalength(response.data.total)
            // setnoproduct('No product found')
            if (response.data.result.length > 0) {
               setbannerImage(response.data.banner_image)
               setmorecategory(response.data.result)
               settotaldatalength(response.data.total)
               setnoproduct('')
            }
            else {
               setnoproduct('No product found')
            }

            setlimit(response.data.result.length)
         }
         else {
            if (response) {
               showSimpleAlert(response.message)
            }
         }
      }

   }
   const scrollEvent = (event) => {

      if (event.nativeEvent.contentOffset.y <= 0 && StickyHeaderHeightSet > 0) {
         setStickyHeaderHeightSet(0)
      }
      else if (event.nativeEvent.contentOffset.y > 0 && StickyHeaderHeightSet === 0) {
         setStickyHeaderHeightSet(subcategoryData.length > 0 ? bannerImage.length > 0 ? 320 : 270 : 230)

      }
   }

   const renderStickyHeader = () => {
      return (
         <View style={{}}>
            <View >
               <View style={[styles.headerLogoStyle]}>
                  <Image source={importImages.headerLogo} />
               </View>
               <TouchableOpacity onPress={() => btnBackClick()} style={{ marginLeft: 16, height: 44, alignItems: 'center', justifyContent: 'center', position: 'absolute' }}>
                  <Image source={importImages.backButtonArrow} style={{}} />
               </TouchableOpacity>
               {/* Search */}

               <View style={styles.searchViewStyle}>
                  <View style={{ flexDirection: 'row' }}>
                     <TouchableOpacity style={styles.searchClick} onPress={() => { btnSearchIconClick(searchText) }}>
                        <Image source={importImages.SearchIcon} style={styles.searchIconImage} />
                     </TouchableOpacity>
                     <View style={styles.searchBarView}>
                        <TextInput
                           style={styles.searchBarTextinput}
                           placeholder={'Search'}
                           placeholderTextColor={commonColors.Blue}
                           value={searchText}
                           multiline={false}
                           onChangeText={(searchText) => { btnSearchIconClick(searchText ? searchText : '') }}
                           underlineColorAndroid='transparent'
                        />
                     </View>
                     {searchText != '' ?
                        <TouchableOpacity style={{ marginLeft: scale(15), justifyContent: "center" }} onPress={() => {
                           crossfunct()
                        }}>
                           <Image source={importImages.crossImage} style={{
                              height: scale(12),
                              alignSelf: 'center',
                              width: scale(12),
                              tintColor: commonColors.Grey
                           }} />
                        </TouchableOpacity> : null}
                     <TouchableOpacity style={[styles.searchBarRightIconStyle, { width: scale(30) }]} onPress={Platform.OS == 'android' ? btnandroidClick : btnMikeIconClick}>
                        <Image source={importImages.mikeIcon} style={{
                           height: scale(20),
                           alignSelf: 'center',
                           width: scale(10),
                           tintColor: mikestop ? commonColors.Blue : 'green'
                        }} />
                     </TouchableOpacity>
                     <TouchableOpacity style={styles.searchBarRightIconStyle} onPress={btnCamaraIconClick}>
                        <Image source={importImages.cameraIcon} style={styles.cameraIcon} />
                     </TouchableOpacity>
                  </View>
               </View>
            </View>
            <View >

               {arrToWhom.length > 0 ?
                  <View style={{ marginLeft: 20, marginRight: 12, marginTop: scale(10) }}>
                     <FlatList
                        horizontal
                        bounces={false}
                        renderItem={renderToWhomItem}
                        data={arrToWhom}
                        style={{}}
                        showsVerticalScrollIndicator={false}
                        stickyHeaderIndices={[0]}
                        showsHorizontalScrollIndicator={false}
                        keyExtractor={(item, index) => `${index}-id`}
                     />
                  </View> : null}
               {subcategoryData.length > 0 ?
                  <View style={{ marginTop: 12, marginLeft: 20, marginRight: 12, }}>
                     <FlatList
                        horizontal
                        bounces={false}
                        renderItem={rendersubcategoryData}
                        data={subcategoryData}
                        style={{}}
                        showsVerticalScrollIndicator={false}
                        stickyHeaderIndices={[0]}

                        showsHorizontalScrollIndicator={false}
                        keyExtractor={(item, index) => `${index}-id`}
                     />
                  </View> : null}

               <View style={{ flexDirection: 'row', marginHorizontal: 12, marginTop: 8, justifyContent: "space-between" }}>
                  <TouchableOpacity style={styles.sortAndFilterButtonStyle}>
                     <Dropdown
                        style={styles.dropdown}
                        placeholderStyle={styles.placeholderStyle}
                        selectedTextStyle={styles.selectedTextStyle}
                        data={keytype == 'segment' ? sortDataArray2 : sortDataArray}
                        search={false}
                        maxHeight={300}
                        labelField="label"
                        activeColor={commonColors.White}
                        valueField="value"
                        placeholder={'Sort By'}
                        value={sortValue}
                        renderItem={renderLabelItem}
                        itemContainerStyle={{ color: commonColors.Blue }}
                        containerStyle={{ color: commonColors.Blue }}
                        showsVerticalScrollIndicator={false}
                        onChange={item => {
                           setsorting(item)
                        }}
                     />
                  </TouchableOpacity>
                  {/* <TouchableOpacity style={[styles.sortAndFilterButtonStyle,]} onPress={() => filterByClick()}> */}
                  <TouchableOpacity style={[styles.sortAndFilterButtonStyle,]} onPress={() => filteronClick()}>

                     <View style={styles.sortAndFilterTextViewStyle}>
                        <Text style={{ fontFamily: Fonts.montserratMedium, fontSize: scale(14), color: commonColors.Blue, textAlign: 'center' }}>Filter By</Text>
                     </View>
                     <View style={styles.sortAndFilterDropDownArrowViewStyle}>
                        <Image source={importImages.dropDownArrow} style={{ marginRight: 8 }} />
                     </View>
                  </TouchableOpacity>
               </View>
            </View>
         </View>
      )
   }
   const crossfunct = async () => {
      setSearchText('')
      let params2 = {
         key_type: keytype,
         key_word: KeyWord,
         filter_type: newfiltertype,
         filter_word: maincatId,
         start: '0',
         limit: 20,
      }
      const data = params2
      let response = await Request.post('get-product-list.php', data)
      if (response.success == true) {
         if (response.data.result.length > 0) {
            setbannerImage(response.data.banner_image)
            setmorecategory(response.data.result)
            settotaldatalength(response.data.total)
            setnoproduct('')
         }
         else {
            setnoproduct('No product found')
         }

         setSortValue('')
      }
      else {
         if (response) {
            showSimpleAlert(response.message)
         }
      }
   }
   const renderForeground = () => {
      return (
         <View style={{}}>
            <View style={styles.dropShadowStyle}>
               <View style={[styles.headerLogoStyle]}>
                  <Image source={importImages.headerLogo} />
               </View>
               <TouchableOpacity onPress={() => btnBackClick()} style={{ marginLeft: 16, height: 44, alignItems: 'center', justifyContent: 'center', position: 'absolute' }}>
                  <Image source={importImages.backButtonArrow} style={{}} />
               </TouchableOpacity>

               {/* Search */}

               <View style={styles.searchViewStyle}>
                  <View style={{ flexDirection: 'row' }}>
                     <TouchableOpacity style={styles.searchClick} onPress={() => { btnSearchIconClick(searchText) }}>
                        <Image source={importImages.SearchIcon} style={styles.searchIconImage} />
                     </TouchableOpacity>
                     <View style={styles.searchBarView}>
                        <TextInput
                           style={styles.searchBarTextinput}
                           placeholder={'Search'}
                           placeholderTextColor={commonColors.Blue}
                           value={searchText}
                           multiline={false}
                           onChangeText={(searchText) => { btnSearchIconClick(searchText ? searchText : '') }}
                           underlineColorAndroid='transparent'
                        />
                     </View>
                     {searchText != '' ?
                        <TouchableOpacity style={{ marginLeft: scale(15), justifyContent: "center" }} onPress={() => {
                           crossfunct()
                        }}>
                           <Image source={importImages.crossImage} style={{
                              height: scale(12),
                              alignSelf: 'center',
                              width: scale(12),
                              tintColor: commonColors.Grey
                           }} />
                        </TouchableOpacity> : null}
                     <TouchableOpacity style={[styles.searchBarRightIconStyle, { width: scale(30) }]} onPress={Platform.OS == 'android' ? btnandroidClick : btnMikeIconClick}>
                        <Image source={importImages.mikeIcon} style={{
                           height: scale(20),
                           alignSelf: 'center',
                           width: scale(10),
                           tintColor: mikestop ? commonColors.Blue : 'green'
                        }} />
                     </TouchableOpacity>
                     <TouchableOpacity style={styles.searchBarRightIconStyle} onPress={btnCamaraIconClick}>
                        <Image source={importImages.cameraIcon} style={styles.cameraIcon} />
                     </TouchableOpacity>
                  </View>
               </View>
            </View>
            <FlatList
               horizontal
               bounces={false}
               renderItem={renderVideoBannerItem}
               data={bannerImage}
               pagingEnabled={true}
               showsVerticalScrollIndicator={false}
               showsHorizontalScrollIndicator={false}
               keyExtractor={(item, index) => `${index}-id`}
            />

            <View style={[bannerImage.length > 0 ? styles.dropShadowStyle : {}, {}]}>
               {arrToWhom.length > 0 ?
                  <View style={{ marginLeft: 20, marginRight: 12, marginTop: scale(5) }}>
                     <FlatList
                        horizontal
                        bounces={false}
                        renderItem={renderToWhomItem}
                        data={arrToWhom}
                        showsVerticalScrollIndicator={false}
                        stickyHeaderIndices={[0]}
                        showsHorizontalScrollIndicator={false}
                        keyExtractor={(item, index) => `${index}-id`}
                     />
                  </View> : null}

               {subcategoryData.length > 0 ?
                  <View style={{ marginTop: 10, marginLeft: 20, marginRight: 12, }}>
                     <FlatList
                        horizontal
                        bounces={false}
                        renderItem={rendersubcategoryData}
                        data={subcategoryData}
                        style={{}}
                        showsVerticalScrollIndicator={false}
                        stickyHeaderIndices={[0]}
                        showsHorizontalScrollIndicator={false}
                        keyExtractor={(item, index) => `${index}-id`}
                     />
                  </View> : null}



               <View style={{ flexDirection: 'row', marginHorizontal: 12, marginTop: 8, justifyContent: "space-between" }}>
                  <TouchableOpacity style={styles.sortAndFilterButtonStyle}>
                     <Dropdown
                        style={styles.dropdown}
                        placeholderStyle={styles.placeholderStyle}
                        selectedTextStyle={styles.selectedTextStyle}
                        data={keytype == 'segment' ? sortDataArray2 : sortDataArray}
                        search={false}
                        maxHeight={300}
                        labelField="label"
                        activeColor={commonColors.White}
                        valueField="value"
                        placeholder={'Sort By'}
                        value={sortValue}
                        renderItem={renderLabelItem}
                        itemContainerStyle={{ color: commonColors.Blue }}
                        containerStyle={{ color: commonColors.Blue }}
                        showsVerticalScrollIndicator={false}
                        onChange={item => {
                           setsorting(item)
                        }}
                     />
                  </TouchableOpacity>
                  {/* <TouchableOpacity style={[styles.sortAndFilterButtonStyle,]} onPress={() => filterByClick()}> */}
                  <TouchableOpacity style={[styles.sortAndFilterButtonStyle,]} onPress={() => filteronClick()}>

                     <View style={styles.sortAndFilterTextViewStyle}>
                        <Text style={{ fontFamily: Fonts.montserratMedium, fontSize: scale(14), color: commonColors.Blue, textAlign: 'center' }}>Filter By</Text>
                     </View>
                     <View style={styles.sortAndFilterDropDownArrowViewStyle}>
                        <Image source={importImages.dropDownArrow} style={{ marginRight: 8 }} />
                     </View>
                  </TouchableOpacity>
               </View>
            </View>
         </View>
      )
   }

   //filter render
   const filteronClick = () => {
      refRBSheet.current.open()
   }
   const filterclose = () => {
      refRBSheet.current.close()
   }
   const bestsellersegmentClick = (item, index) => {
      console.log("item", item, index);
      var tempArry = filtersegmentData
      var tempItem = tempArry[index]
      console.log("tempItem", tempItem);
      if (tempItem.open_slug == 0) {
         tempItem.open_slug = 1
         console.log("tempArry", tempArry);
         setfiltersegmentData([...tempArry])

      } else {
         tempItem.open_slug = 0
         setfiltersegmentData([...tempArry])

      }


   }
   const checkMarkedClick = (item, index,) => {
      console.log("checkmarked", item, index);
      var tempArry = filtersegmentData
      var tempItem = tempArry[index].item
      tempArry[index].item.isSelected = !item.isSelected
      setfiltersegmentData([...tempArry])
   }

   const filtertypecheckMarkedClick = (item, index,) => {
      var tempArry = [...filterfragtype]
      tempArry[index].isSelected = !item.isSelected
      setfilterfragtype(tempArry)
   }
   const filtersegmentDatarenderItem = ({ item, index }) => {
      const data = item.item
      return (
         <View>
            <TouchableOpacity onPress={() => bestsellersegmentClick(item, index)} style={styles.optionsTextStyle}>
               <Text style={styles.categoryLabelTextStyle}>{item.title}</Text>
               <Image source={importImages.downArrow} style={{ marginRight: 36 }} />
            </TouchableOpacity>
            {
               item.open_slug == 1 ?
                  <View>
                     <TouchableOpacity onPress={() => { checkMarkedClick(data, index) }} style={styles.subCategoryItemStyle}>
                        <View style={{ marginRight: 8, }}>
                           <Image source={data.isSelected ? importImages.BlueTick : importImages.unCheckImage} />
                        </View>
                        <Text style={styles.subCategoryTextStyle}>{data.in_segment_title}</Text>
                     </TouchableOpacity>
                  </View> : null}
            <View style={styles.seperatoreStyle}></View>

         </View>
      )
   }

   const sizecheckMarkedClick = (item, index,) => {
      var tempArry = [...filtersizeData]
      tempArry[index].isSelected = !item.isSelected
      setfiltersizeData(tempArry)
   }
   const rendersizeData = ({ item, index }) => {
      return (

         <View>
            <TouchableOpacity onPress={() => { sizecheckMarkedClick(item, index) }} style={styles.subCategoryItemStyle}>
               <Image source={item.isSelected ? importImages.BlueTick : importImages.unCheckImage} style={{ marginRight: 8, }} />
               <Text style={styles.subCategoryTextStyle}>{item.key}</Text>
            </TouchableOpacity>
         </View>

      )
   }
   const renderbrandDataCategories = ({ item, index }) => {
      return (
         <View>
            <TouchableOpacity onPress={() => { brandcheckMarkedClick(item, index) }} style={styles.subCategoryItemStyle}>
               <Image source={item.isSelected ? importImages.BlueTick : importImages.unCheckImage} style={{ marginRight: 8, }} />
               <Text style={styles.subCategoryTextStyle}>{item.brand_name}</Text>
            </TouchableOpacity>
         </View>
      )
   }
   const filterfragtyperenderItem = ({ item, index }) => {
      return (
         <View>
            <TouchableOpacity onPress={() => { filtertypecheckMarkedClick(item, index) }} style={styles.subCategoryItemStyle}>
               <View style={{ marginRight: 8, }}>
                  <Image source={item.isSelected ? importImages.BlueTick : importImages.unCheckImage} />
               </View>
               <Text style={styles.subCategoryTextStyle}>{item.key}</Text>
            </TouchableOpacity>
         </View>
      )
   }
   const rendercollectionDataCategories = ({ item, index }) => {
      return (
         <View>
            <TouchableOpacity onPress={() => { collectioncheckMarkedClick(item, index) }} style={styles.subCategoryItemStyle}>
               <View style={{ marginRight: 8, }}>
                  <Image source={item.isSelected ? importImages.BlueTick : importImages.unCheckImage} />
               </View>
               <Text style={styles.subCategoryTextStyle}>{item.name}</Text>
            </TouchableOpacity>
         </View>
      )
   }
   const itemSeparatorComponent = ({ item }) => {
      return (
         <View style={styles.seperatoreStyle}></View>
      );
   }
   const collectioncheckMarkedClick = (item, index,) => {
      var tempArry = [...collectionData]
      tempArry[index].isSelected = !item.isSelected
      setcollectionData(tempArry)
   }
   const sliderValueChanged = (values) => {
      console.log("valuess:--", values);
      setminsliderprice(values[0])
      setmaxsliderprice(values[1])
      setSliderValue(values[0] + "JOD - " + values[1] + "JOD")

   }
   const brandcheckMarkedClick = (item, index,) => {
      let brandata = []
      var tempArry = [...brandData]
      tempArry[index].isSelected = !item.isSelected
      setbrandData(tempArry)

   }
   const sizeClick = () => {
      setsizeDataModal(!sizeDataModal)
   }
   const fragrancetypeClick = () => {
      setfilterfragtypeModal(!filterfragtypeModal)
   }
   const concernClick = () => {
      setconcernmodal(!concernmodal)
   }
   const priceClick = () => {
      setpriceModal(!priceModal)
   }
   const collectionClick = () => {
      setcollectionDataModal(!collectionDataModal)
   }
   const brandClick = () => {
      setbrandModal(!brandModal)
   }

   const getproductData = async () => {
      let selectedData = []
      let filterselectData = []
      const filterSegment = filtersegmentData ? filtersegmentData.map((item, index) => {
         var data = item.item
         console.log("route.params.data", data);
         if (data.isSelected == true) {
            selectedData.push(item)
            filterselectData.push(data)
         }
      }) : null
      let brandArray = brandData ? brandData.filter((item) => item.isSelected == true) : []
      let sizedata = filtersizeData ? filtersizeData.filter((item) => item.isSelected == true) : []
      let FragranceType = filterfragtype ? filterfragtype.filter((item) => item.isSelected == true) : []
      let CollectionData = collectionData ? collectionData.filter((item) => item.isSelected == true) : []
      let ConcernData = concernData ? concernData.filter((item) => item.isSelected == true) : []
      const filterbrand = brandArray.map((item, index) => {
         return item.brand_id
      })
      const filterfragtype = FragranceType.map((item, index) => {
         return item.key
      })
      const filtersegment = filterselectData.map((item, index) => {
         return item.in_segment
      })
      const filterSize = sizedata.map((item, index) => {
         return item.key
      })

      const concernData = ConcernData.map((item, index) => {
         return item.id
      })

      let params2 = {
         key_type: keytype,
         key_word: KeyWord,
         max_price: maxsliderprice,
         min_price: minsliderprice,
         segment_filter: filtersegment,
         brand_filter: filterbrand,
         fragrance_filter: filterfragtype,
         size_filter: filterSize,
         concern_filter: concernData,
         sort: sortValue ? sortValue : '',
         filter_type: newfiltertype ? newfiltertype : '',
         filter_word: maincatId ? maincatId : '',
         start: '0',
         limit: limit + 20,
      }
      const data = params2
      let response = await Request.post('get-product-list.php', data)
      refRBSheet.current.close()
      if (response.success == true) {
         if (response.data.result.length > 0) {
            setbannerImage(response.data.banner_image)
            setmorecategory(response.data.result)
            settotaldatalength(response.data.total)
            setnoproduct('')
            setisfilter(true)
         }
         else {
            setnoproduct('No product found')
         }

      }
      else {
         if (response) {
            showSimpleAlert(response.message)
         }
      }
   }
   const saveFilter = async () => {
      getproductData()
      getsaveproductfilter()
   }

   const clearFilter = async () => {
      setisfilter(false)
      refRBSheet.current.close()
   }
   const getsaveproductfilter = async () => {
      let params = {
         key_type: keytype,
         key_word: KeyWord,
         main_category_id: maincategId,
         category_id: category_id,
         filter_type: newfiltertype ? newfiltertype : '',
         filter_word: maincatId ? maincatId : '',
      }
      let response = await Request.post('get-product-filter-list.php', params)
      setfiltermodalvisible(false)
      if (response.success == true) {
         setfilterData(response.data.length > 0 ? response.data[0] : [])
         setArrToWhom(response.data.length > 0 ? response.data[0].title : [])
         setsubcategoryData(response.data.length > 0 ? response.data[0].sub_category : [])
         setfiltersegmentData(response.data.length > 0 ? response.data[0].filter_segment : [])
         setfiltersizeData(response.data.length > 0 ? response.data[0].filter_size : [])
         setfilterfragtype(response.data.length > 0 ? response.data[0].filter_fragtype : [])
         setcollectionData(response.data.length > 0 ? response.data[0].row_collections : [])
         setconcernData(response.data.length > 0 ? response.data[0].row_concerns : [])
         setbrandData(response.data.length > 0 ? response.data[0].filter_brand : [])
         setminprice(response.data.length > 0 ? response.data[0].min_price : '')
         setmaxprice(response.data.length > 0 ? response.data[0].max_price : '')
         setSliderValue(response.data[0].min_price + '' + "JOD - " + response.data[0].max_price + '' + "JOD")
         setminsliderprice(response.data.length > 0 ? response.data[0].min_price : '')
         setmaxsliderprice(response.data.length > 0 ? response.data[0].max_price : '')
      }
      else {
         if (response) {
            showSimpleAlert(response.message)
         }
      }
   }
   return (
      <View style={[styles.mainViewStyle, {}]}>

         <ParallaxScrollView
            bounces={false}
            stickyHeaderHeight={StickyHeaderHeightSet}
            parallaxHeaderHeight={bannerImage.length > 0 ? subcategoryData.length > 0 ? scale(455) : scale(390) : subcategoryData.length > 0 ? 300 : 240}
            showsVerticalScrollIndicator={false}
            backgroundColor={commonColors.White}
            contentBackgroundColor={commonColors.White}
            contentContainerStyle={{ flexGrow: 0.8, }}
            renderStickyHeader={renderStickyHeader}
            renderForeground={renderForeground}
            scrollEvent={scrollEvent}
            parallaxBackgroundScrollSpeed={5}
            parallaxForegroundScrollSpeed={2.5}
         >

            <View style={{ flex: 1, marginBottom: morecategory.length == 1 ? scale(160) : scale(60), }}>
               {
                  morecategory.length > 0 ?
                     <FlatList
                        ref={refContainer}
                        bounces={false}
                        renderItem={({ item, index }) => rendermorecategory(item, index, new_arrival)}
                        data={morecategory}
                        scrollEnabled={false}
                        numColumns={2}
                        viewabilityConfig={viewabilityConfig.current}
                        onViewableItemsChanged={onViewableItemsChanged.current}
                        pagingEnabled={true}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        ListFooterComponent={renderFooter}
                        keyExtractor={(item, index) => `${index}-id`}
                     /> :
                     <View style={styles.nodatafoundView}>
                        <Text style={styles.transactionText}>{noproduct}</Text>
                     </View>
               }
               {isModalVisible == false && (morecategory.length < totaldatalength) ?
                  <CustomButton title={'View More'} onPress={() => {
                     viewmore()
                  }}
                  /> : null}
            </View>



            {/* /  filter modal */}
            <View
               style={{
                  flex: 1,
               }}
            >
               <RBSheet
                  ref={refRBSheet}
                  closeOnDragDown={false}
                  closeOnPressMask={false}
                  customStyles={{
                     container: {
                        borderTopLeftRadius: scale(20),
                        borderTopRightRadius: scale(20),
                        height: "90%",
                        marginTop: scale(20)
                     },
                     wrapper: {

                     },
                     draggableIcon: {
                        backgroundColor: "#000"
                     }
                  }}
               >
                  <View style={styles.headerLogoStyle}>
                     <Text style={{ fontFamily: Fonts.montserratSemiBold, fontSize: 20, color: commonColors.Blue }}>Filter</Text>
                  </View>
                  <View style={{ width: 300, height: 1, backgroundColor: 'lightgrey', alignSelf: 'center', }}></View>
                  <TouchableOpacity onPress={() => filterclose()} style={{ marginLeft: 16, marginTop: 10, position: 'absolute' }}>
                     <Image source={importImages.downArrowButton} style={{}} />
                  </TouchableOpacity>
                  {/* NEW ARRIVALS */}
                  {filtersegmentData && filtersegmentData.length > 0 ?
                     <View>
                        <FlatList
                           bounces={false}
                           renderItem={filtersegmentDatarenderItem}
                           data={filtersegmentData}
                           showsVerticalScrollIndicator={false}
                           extraData={filtersegmentData}
                           nestedScrollEnabled={true}
                           scrollEnabled={true}
                           showsHorizontalScrollIndicator={false}
                           keyExtractor={(item, index) => `${index}-id`}
                        />
                     </View>
                     : null
                  }

                  {/*SIZE */}
                  {
                     filtersizeData && filtersizeData.length > 0 ?
                        <View>
                           <TouchableOpacity onPress={() => sizeClick()} style={styles.optionsTextStyle}>
                              <Text style={styles.categoryLabelTextStyle}>{'SIZE'}</Text>
                              <Image source={importImages.downArrow} style={{ marginRight: 36 }} />
                           </TouchableOpacity>
                           {
                              sizeDataModal == true ?
                                 <FlatList
                                    bounces={false}
                                    renderItem={rendersizeData}
                                    data={filtersizeData}
                                    ItemSeparatorComponent={itemSeparatorComponent}
                                    showsVerticalScrollIndicator={false}
                                    nestedScrollEnabled={true}
                                    scrollEnabled={true}
                                    showsHorizontalScrollIndicator={false}
                                    keyExtractor={(item, index) => `${index}-id`}
                                 /> : null}
                           <View style={styles.seperatoreStyle}></View>

                        </View> : null
                  }

                  {/*FRAGRANCE TYPE*/}
                  {
                     filterfragtype && filterfragtype.length > 0 ?
                        <View>
                           <TouchableOpacity onPress={() => fragrancetypeClick()} style={styles.optionsTextStyle}>
                              <Text style={styles.categoryLabelTextStyle}>{'FRAGRANCE TYPE'}</Text>
                              <Image source={importImages.downArrow} style={{ marginRight: 36 }} />
                           </TouchableOpacity>
                           {
                              filterfragtypeModal == true ?
                                 <FlatList
                                    bounces={false}
                                    renderItem={filterfragtyperenderItem}
                                    data={filterfragtype}
                                    ItemSeparatorComponent={itemSeparatorComponent}

                                    showsVerticalScrollIndicator={false}
                                    showsHorizontalScrollIndicator={false}
                                    keyExtractor={(item, index) => `${index}-id`}
                                 /> : null}
                           <View style={styles.seperatoreStyle}></View>

                        </View> : null
                  }

                  {/* COLLECTIONS */}
                  {
                     collectionData && collectionData.length > 0 ?
                        <View>
                           <TouchableOpacity onPress={() => collectionClick()} style={styles.optionsTextStyle}>
                              <Text style={styles.categoryLabelTextStyle}>{'COLLECTIONS'}</Text>
                              <Image source={importImages.downArrow} style={{ marginRight: 36 }} />
                           </TouchableOpacity>
                           {
                              collectionDataModal == true ?
                                 <FlatList
                                    bounces={false}
                                    renderItem={rendercollectionDataCategories}
                                    data={collectionData}
                                    ItemSeparatorComponent={itemSeparatorComponent}
                                    showsVerticalScrollIndicator={false}
                                    showsHorizontalScrollIndicator={false}
                                    keyExtractor={(item, index) => `${index}-id`}
                                 /> : null}
                           <View style={styles.seperatoreStyle}></View>

                        </View> : null
                  }


                  {
                     concernData && concernData.length > 0 ?
                        <View>
                           <TouchableOpacity onPress={() => concernClick()} style={styles.optionsTextStyle}>
                              <Text style={styles.categoryLabelTextStyle}>{'CONCERN'}</Text>
                              <Image source={importImages.downArrow} style={{ marginRight: 36 }} />
                           </TouchableOpacity>
                           {
                              concernmodal == true ?
                                 <FlatList
                                    bounces={false}
                                    renderItem={renderConcernData}
                                    ItemSeparatorComponent={itemSeparatorComponent}
                                    data={concernData}
                                    showsVerticalScrollIndicator={false}
                                    showsHorizontalScrollIndicator={false}
                                    keyExtractor={(item, index) => `${index}-id`}
                                 /> : null}
                           <View style={styles.seperatoreStyle}></View>

                        </View> : null
                  }

                  {/* BRANDS */}
                  {
                     brandData && brandData.length > 0 ?
                        <View>

                           <TouchableOpacity onPress={() => brandClick()} style={styles.optionsTextStyle}>
                              <Text style={styles.categoryLabelTextStyle}>{'BRANDS'}</Text>
                              <Image source={importImages.downArrow} style={{ marginRight: 36 }} />
                           </TouchableOpacity>
                           {
                              brandModal == true ?
                                 <FlatList
                                    bounces={false}
                                    renderItem={renderbrandDataCategories}
                                    data={brandData}
                                    ItemSeparatorComponent={itemSeparatorComponent}
                                    showsVerticalScrollIndicator={false}
                                    showsHorizontalScrollIndicator={false}
                                    keyExtractor={(item, index) => `${index}-id`}
                                 /> : null}
                           <View style={styles.seperatoreStyle}></View>

                        </View> : null
                  }
                  {maxprice > 0 ?
                     <View>
                        <TouchableOpacity onPress={() => priceClick()} style={styles.optionsTextStyle}>
                           <Text style={styles.categoryLabelTextStyle}>{'PRICE'}</Text>
                           <Image source={importImages.downArrow} style={{ marginRight: 36 }} />
                        </TouchableOpacity>
                        {
                           priceModal == true ?
                              <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                 <Text style={[styles.sliderTextStyle, { marginTop: 12 }]}>{sliderValue == '' ? (minprice + "JOD - " + maxprice + "JOD  ") : sliderValue}</Text>
                                 <MultiSlider
                                    enabledOne={parseInt(minprice) == parseInt(maxprice) ? false : true}
                                    enabledTwo={true}
                                    min={parseInt(minprice) == parseInt(maxprice) ? '' : parseInt(minprice)}
                                    max={parseInt(maxprice)}
                                    step={1}
                                    sliderLength={deviceWidth - 60}
                                    containerStyle={{ height: 30, width: deviceWidth - 60 }}
                                    values={[parseInt(minsliderprice), parseInt(maxsliderprice)]}
                                    onValuesChange={(values) => sliderValueChanged(values)}
                                    customMarker={(e) => {
                                       return (
                                          <View style={{ backgroundColor: commonColors.Blue, width: 24, height: 24, borderRadius: 12 }} />
                                       )
                                    }}
                                    trackStyle={{ backgroundColor: commonColors.Grey }}
                                    selectedStyle={{ backgroundColor: commonColors.Blue }}
                                 />
                              </View> : null}
                     </View> : null}
                  <View style={{ flexDirection: "row", marginHorizontal: scale(20), marginBottom: scale(60) }}>
                     <CustomButton
                        style={{ width: scale(135), }}
                        onPress={() => saveFilter()}
                        title={'Apply Filter'}
                     />
                     <CustomButton
                        style={{ width: scale(135) }}
                        onPress={() => clearFilter()}
                        title={'Clear Filter'}
                     />
                  </View>
               </RBSheet>
            </View>
         </ParallaxScrollView>

         {isModalVisible || filtermodalvisible ?


            <BallIndicator visible={true} /> : null
         }
      </View >
   );
}

const styles = StyleSheet.create({
   mainViewStyle: {
      flex: 1,
      backgroundColor: 'white'
   },
   footer: {
   },
   headerLogoStyle: {
      justifyContent: 'center',
      alignItems: 'center',
      height: 44
   },
   lineSeprator: {
      borderBottomWidth: 0.5,
      padding: 2,
      borderColor: commonColors.Grey
   },
   searchProductText: {
      fontSize: scale(10),
      color: commonColors.Black
   },
   searchProductView: {
      justifyContent: "space-between",
      flexDirection: "row"
   },
   searchItemText: {
      fontSize: scale(10),
      color: commonColors.Black
   },
   searchView: {
      marginHorizontal: scale(5),
   },
   modalView: {
      height: scale(100),
      width: deviceWidth - 50,
      marginHorizontal: scale(20),
      backgroundColor: commonColors.White,
      shadowColor: commonColors.Black,
      shadowOpacity: 1,
      shadowRadius: 10,
      shadowOffset: { width: 1, height: 3 },
      borderRadius: 8,
   },
   searchBarView: {
      flex: 1,
      justifyContent: 'center',
      alignSelf: 'center'
   },
   searchClick: {
      marginHorizontal: 8,
      justifyContent: 'center'
   },
   searchIconImage: {
      height: scale(20),
      width: scale(16),
   },
   cameraIcon: {
      height: scale(15),
      width: scale(18),
   },
   searchBarTextinput: {
      color: commonColors.Blue,
      fontFamily: Fonts.montserratRegular,
      fontSize: 12,
   },
   searchViewStyle: {
      height: scale(35),
      marginHorizontal: 16,
      alignContent: 'center',
      borderColor: commonColors.Blue,
      borderRadius: 15,
      borderWidth: 1,
      justifyContent: 'center'
   },
   nodatafoundView: {
      marginTop: scale(40),
      justifyContent: 'center',
      alignContent: 'center',
      alignSelf: 'center',
   },
   transactionText: {
      fontSize: scale(16),
      color: commonColors.Blue,
      fontFamily: Fonts.montserratMedium,
      alignSelf: "center"
   },
   searchBarRightIconStyle: {
      marginHorizontal: 8,
      alignContent: 'flex-end',
      justifyContent: 'center'
   },
   sortAndFilterButtonStyle: {
      flexDirection: 'row',
      height: 40,
      flex: 0.5,
      justifyContent: 'space-between',
      marginHorizontal: scale(10),
      borderColor: commonColors.Blue,
      borderWidth: 1,
      borderRadius: 10
   },
   sortAndFilterTextViewStyle: {
      justifyContent: 'center',
      marginLeft: 8,
      flex: 1
   },
   sortAndFilterDropDownArrowViewStyle: {
      width: '100%',
      height: "100%",
      position: 'absolute',
      justifyContent: 'center',
      alignItems: "flex-end"
   },
   dropShadowStyle: {
      shadowColor: commonColors.Blue,
      shadowOpacity: 0.5,
      shadowRadius: 2,
      elevation: 5,
      backgroundColor: 'white',
      marginTop: 0,
      marginBottom: 10
   },
   dropShadowStyle2: {
      shadowColor: commonColors.Blue,
      shadowOffset: { width: 0, height: 0 },

      shadowOpacity: 0.5,
      shadowRadius: 2,
      elevation: 5,
      backgroundColor: 'white',
      marginTop: 0
   },
   dropdown: {
      paddingHorizontal: 8,
      alignSelf: 'center',
      textAlign: 'center',
      color: commonColors.Blue,
      justifyContent: 'center',
      width: '100%',
      height: '100%',
   },
   placeholderStyle: {
      fontSize: scale(14),
      fontFamily: Fonts.montserratMedium,
      textAlign: 'center',
      color: commonColors.Blue
   },
   selectedTextStyle: {
      fontSize: scale(12),
      textAlign: 'center',
      fontFamily: Fonts.montserratMedium,
      color: commonColors.Blue
   },
   textItem: {
      color: commonColors.Blue,
      fontSize: scale(12),
      fontFamily: Fonts.montserratMedium
   },
   optionsTextStyle: {
      marginLeft: 36,
      marginTop: 8,
      height: 36,
      justifyContent: 'space-between',
      flexDirection: 'row',
      alignItems: 'center'
   },
   categoryLabelTextStyle: {
      fontFamily: Fonts.montserratMedium,
      fontSize: 16,
      color: commonColors.Blue
   },
   subCategoryTextStyle: {
      fontFamily: Fonts.montserratRegular,
      fontSize: 12,
      color: commonColors.Blue,
   },
   seperatoreStyle: {
      marginHorizontal: scale(30),
      borderColor: commonColors.Blue,
      borderBottomWidth: StyleSheet.hairlineWidth,
      marginVertical: scale(5),
   },
   subCategoryItemStyle: {
      marginLeft: 36,
      marginVertical: scale(10),
      flexDirection: 'row',
      alignItems: 'center',
      marginRight: 24
   },
   sliderTextStyle: {
      fontFamily: Fonts.montserratMedium,
      fontSize: 12,
      color: commonColors.Blue
   },
})
