import { React, importImages, TouchableOpacity, TextInput, useState, useEffect, showSimpleAlert, Request, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale } from '../../utils/importLibrary'

export default function GiftVoucherViewController({ route, navigation }) {
    const [physicalgiftVoucher, setphysicalgiftVoucher] = useState([])
    const [digitalgiftVoucher, setdigitalgiftVoucher] = useState([])
    const [topBannerSelectedIndex, setTopBannerSelectedIndex] = useState(0)
    const [ecardIndex, setecardIndex] = useState(0)

    const btnBackClick = () => {
        navigation.goBack()
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getgiftVoucher();
        });
        return unsubscribe;
    }, [navigation])


    const getgiftVoucher = async () => {
        const giftData = route.params.giftVoucher
        setdigitalgiftVoucher(giftData.e_gift_voucher)
        setphysicalgiftVoucher(giftData.physical_gift_voucher)
    }



    const renderPhysicalItem = ({ item, index }) => {
        console.log("item:->", item);

        return (
            <View style={{ marginHorizontal: scale(20), }}>
                <TouchableOpacity
                    onPress={() => {
                        navigation.navigate('ProductDetailViewController', { seoUrl: item.seo_url })
                    }}>
                    <Image source={{ uri: item.family_pic }} style={styles.imageView} resizeMode='contain' />
            
                </TouchableOpacity>
            </View>
        )
    }

    const renderDigitalItem = ({ item, index }) => {
        console.log("item:->", item);
        return (
            <View style={{ marginHorizontal: scale(20), }}>
                <TouchableOpacity onPress={() => {
                    navigation.navigate('DigitalGiftVoucherController', { giftData: item })
                }}>
                    <Image source={{ uri: item.family_pic }} style={styles.imageView} resizeMode='contain' />
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <View style={styles.mainViewStyle}>
            <View style={styles.headerLogoStyle}>
                <Text style={styles.headerLogoText}>{'Gift Voucher'}</Text>
                <TouchableOpacity onPress={() => btnBackClick()} style={{ marginLeft: 16 }}>
                    <Image source={importImages.backButtonArrow} style={{}} />
                </TouchableOpacity>
            </View>
            <View style={styles.line}></View>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ flex: 1, }}>
                    <Text style={styles.headerText}>{'Physical Gift Voucher'}</Text>
                    {physicalgiftVoucher ?
                        <FlatList
                            bounces={true}
                            renderItem={renderPhysicalItem}
                            data={physicalgiftVoucher}
                            style={{ marginTop: scale(12),height: scale(130), flex: 1, borderRadius: scale(5), borderWidth: 1, borderColor: commonColors.Blue, marginHorizontal: scale(20) }}
                            pagingEnabled={true}
                            horizontal
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={(item, index) => `${index}-id`}
                            onScroll={(event) => {
                                setTopBannerSelectedIndex(Math.round(event.nativeEvent.contentOffset.x / deviceWidth))
                            }}
                        />
                        : <View style={styles.nodatafoundView}>
                            <Text style={styles.transactionText}>{'No Physical Gift Voucher found'}</Text>
                        </View>}

                    {
                        (physicalgiftVoucher).length >= 1 ?
                            <View style={styles.dotView}>
                                {(physicalgiftVoucher).map((step, i) => {
                                    return (i % 2 == 0 ?
                                        <View key={i}
                                            style={[
                                                styles.stepSlides,
                                                { backgroundColor: topBannerSelectedIndex === i ? commonColors.Blue : commonColors.pageControlColor },
                                            ]}
                                        /> : null
                                    )
                                }
                                )}
                            </View> : null
                    }
                </View>
                <View style={{ flex: 1, marginTop: scale(15) }}>
                    <Text style={styles.headerText}>{'e-Gift Voucher'}</Text>
                    {digitalgiftVoucher ?
                        <FlatList
                            bounces={true}
                            renderItem={renderDigitalItem}
                            data={digitalgiftVoucher}
                            style={{ marginTop: scale(12), flex: 1,height: scale(130), borderRadius: scale(5), borderWidth: 1, borderColor: commonColors.Blue, marginHorizontal: scale(20) }}
                            pagingEnabled={true}
                            contentContainerStyle={{flexGrow:1}}
                            horizontal={true}
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={(item, index) => `${index}-id`}
                            onScroll={(event) => {
                                setecardIndex(Math.round(event.nativeEvent.contentOffset.x / deviceWidth))
                            }}
                        /> : <View style={styles.nodatafoundView}>
                            <Text style={styles.transactionText}>{'No  e-Gift Voucher found'}</Text>
                        </View>}
                    {
                        (digitalgiftVoucher).length >= 1 ?
                            <View style={styles.dotView}>
                                {(digitalgiftVoucher).map((step, i) => {
                                    return (i % 1 == 0 ?
                                        <View key={i}
                                            style={[
                                                styles.stepSlides,
                                                { backgroundColor: ecardIndex === i ? commonColors.Blue : commonColors.pageControlColor },
                                            ]}
                                        /> : null
                                    )
                                }
                                )}
                            </View> : null
                    }
                </View>
            </ScrollView>

        </View>
    )
}

const styles = StyleSheet.create({
    mainViewStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    headerLogoStyle: {
        alignItems: 'center',
        height: 44,
        flexDirection: 'row'
    },
    textStyle: {
        marginTop: 4,
        fontFamily: Fonts.montserratMedium,
        fontSize: 13,
        color: commonColors.Blue
    },
    dotView: {
        flexDirection: "row",
        alignSelf: 'center',
        marginTop: 16
    },
    stepSlides: {
        height: 8,
        width: 8,
        borderRadius: 4,
        marginRight: 8,
        marginTop: 0,
        backgroundColor: 'blue'
    },
    headerLogoText: { width: '100%', textAlign: 'center', position: 'absolute', fontFamily: Fonts.montserratSemiBold, fontSize: 20, color: commonColors.Blue },
    line: { width: 300, height: 1, backgroundColor: 'lightgrey', alignSelf: 'center', },
    headerText: { fontSize: scale(18), color: commonColors.Blue, alignSelf: "center", marginTop: scale(20), fontFamily: Fonts.montserratBold },
    nodatafoundView: { flex: 1, height: '100%', justifyContent: 'center', alignContent: 'center', alignSelf: 'center' },
    transactionText: { fontSize: scale(14), fontFamily: Fonts.montserratMedium, alignSelf: "center" },
    brandnameText: { fontSize: scale(14), color: commonColors.Blue, paddingVertical: scale(5) },
    familynameText: { fontSize: scale(14), color: commonColors.Blue, },
    pricerangeText: { fontSize: scale(14), color: commonColors.Blue, paddingTop: scale(15) },
    imageView: { height: scale(120), width: deviceWidth - 80, }
})