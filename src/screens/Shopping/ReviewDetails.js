import { React, importImages, moment, TouchableOpacity, TextInput, useState, useEffect, showSimpleAlert, Request, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale } from '../../utils/importLibrary'
import { AirbnbRating } from 'react-native-ratings';

export default function ReviewDetails({ route, navigation }) {
    const [reviewData, setreviewData] = useState([])
    const [isModalVisible, setModalVisible] = useState(false);

    const btnBackClick = () => {
        navigation.navigate('ProductDetailViewController', { seoUrl: route.params.seourl })
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getreviewData();
        });
        return unsubscribe;
    }, [navigation])


    const getreviewData = async () => {
        const review = route.params.productId
        setModalVisible(true)
        let params = { product_id: review, domain_id: 1 }
        let response = await Request.post('product-review.php', params)
        if (response.success == true) {
            setreviewData(response.data)
            setModalVisible(false)
        }
        else {

        }
    }

    const renderreviewDatalItem = ({ item, index }) => {
        return (
            <View style={styles.renderView}>
                <View style={{ flexDirection: "column" }}>
                    <Text style={styles.titleText}>{item.title}</Text>
                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                        <Text style={styles.customernameText}>{item.customer}  </Text>
                        <Text style={styles.datetext}>{moment(item.added_date).format('DD MMM YYYY')}</Text>
                    </View>
                </View>
                <View style={{ alignSelf: "flex-start", bottom: scale(25) }}>
                    <AirbnbRating
                        count={item.rating}
                        isDisabled={true}
                        defaultRating={11}
                        selectedColor={commonColors.Blue}
                        size={15}
                    /></View>
                <Text style={styles.descriptiontext}>{item.description}</Text>
            </View>
        )
    }

    return (
        <View style={styles.mainViewStyle}>

            <View style={styles.headerLogoStyle}>
                <Text style={styles.reviewHeader}>Reviews</Text>
                <TouchableOpacity onPress={() => btnBackClick()} style={{ marginLeft: 16 }}>
                    <Image source={importImages.backButtonArrow} style={{}} />
                </TouchableOpacity>
            </View>
            <View style={styles.line}></View>
            {reviewData.length > 0 ?
                < FlatList
                    bounces={true}
                    renderItem={renderreviewDatalItem}
                    data={reviewData}
                    style={{ marginTop: 8, flex: 1, marginHorizontal: scale(20) }}
                    contentContainerStyle={{ flexGrow: 1, }}
                    pagingEnabled={true}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={(item, index) => `${index}-id`}
                /> : <View style={styles.nolistView}>
                    <Text style={styles.transactionText}> {isModalVisible ? '' :  'No Reviews found'}</Text>
                </View>}
            {isModalVisible &&
                <BallIndicator visible={isModalVisible} />
            }
        </View>
    )
}

const styles = StyleSheet.create({
    mainViewStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    line: {
        width: 300,
        height: 1,
        backgroundColor: commonColors.White,
        alignSelf: 'center',
    },
    reviewHeader: {
        width: '100%',
        textAlign: 'center',
        position: 'absolute',
        fontFamily: Fonts.montserratSemiBold,
        fontSize: 20,
        color: commonColors.Blue
    },
    headerLogoStyle: {
        alignItems: 'center',
        height: 44,
        flexDirection: 'row'
    },
    textStyle: {
        marginTop: 4,
        fontFamily: Fonts.montserratMedium,
        fontSize: 13,
        color: commonColors.Blue
    },
    nolistView: {
        flex: 1,
        height: '100%',
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center'
    },
    titleText: {
        fontSize: scale(16),
        fontFamily: Fonts.montserratRegular,
        color: commonColors.Blue,
    },
    datetext: {
        fontSize: scale(14),
        fontFamily: Fonts.montserratRegular,
        color: commonColors.Blue
    },
    descriptiontext: {
        fontSize: scale(14),
        bottom: scale(10),
        fontFamily: Fonts.montserratRegular,
        color: commonColors.Blue,
    },
    renderView: {
        margin: scale(10),
        shadowRadius: 5,
        shadowOffset: { width: 2, height: 1 },
        backgroundColor: commonColors.White,
        padding: scale(10),
        elevation: 5,
        borderRadius: scale(10),
        borderColor: commonColors.Blue,
        shadowColor: commonColors.Blue,
        shadowOpacity: 1,
    },
    customernameText: {
        fontSize: scale(14),
        color: commonColors.Blue,
        fontFamily: Fonts.montserratBold,
        width: scale(80),
    },
    transactionText: {
        color: commonColors.Blue,
        fontSize: 20,
        fontFamily: Fonts.montserratMedium
    },
})