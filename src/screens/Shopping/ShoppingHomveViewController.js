import { React, PixelRatio, QRCodeScanner, useRef, useCallback, useWindowDimensions, WebView, RenderHtml, importImages, TouchableOpacity, TextInput, useState, showSimpleAlert, Request, useEffect, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale, Alert, Platform } from '../../utils/importLibrary'
import Voice from '@react-native-community/voice';
import ProductComponent from '../../components/ProductComponent'
import FastImage from 'react-native-fast-image';
import Share from 'react-native-share';
import SpeechAndroid from 'react-native-android-voice';
import RenderHTML from 'react-native-render-html';
import { defaultSystemFonts } from 'react-native-render-html';
import HTMLView from 'react-native-htmlview';
import SplashScreen from '../Auth/SplashScreen';
import SpeechToText from 'react-native-google-speech-to-text';
import { Linking, } from "react-native";
import { Dropdown } from 'react-native-element-dropdown';
import RBSheet from "react-native-raw-bottom-sheet";
import Video from "react-native-video";

import RNPermissions, { checkMultiple, RESULTS, NotificationOption, Permission, PERMISSIONS, requestMultiple } from 'react-native-permissions';
import CustomButton from '../../components/CustomButton';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import VideoPlayer from 'react-native-video-controls';
import RnCarousel from 'react-native-snap-carousel';
import Carousel from 'react-native-reanimated-carousel';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import dynamicLinks from '@react-native-firebase/dynamic-links';

let type = '1'


// Remember the latest callback.


export default function ShoppingHomveViewController({ route, navigation }) {
   const { ColorName, colors, icons, setScheme } = useTheme();
   const [userData, setUserDate] = useState('')
   const [searchText, setSearchText] = useState('')
   const [arrMainCategories, setArrMainCategories] = useState([])
   const [topBannerSelectedIndex, setTopBannerSelectedIndex] = useState(0)
   const [newArrivalSelectedIndex, setNewArrivalSelectedIndex] = useState(0)
   const [bestSellerSelectedIndex, setBestSellerSelectedIndex] = useState(0)
   const [setsIndex, setSetsIndex] = useState(0)
   const [colorCombination, setColorCombination] = useState(["#4B241C", "#DA749B", "#832325", "#A70B23", "#A10034", "#A6374A"])
   const [colorSelectedIndex, setColorSelectedIndex] = useState(0)
   const [salesItemSelectedIndex, setSalesItemSelectedIndex] = useState(0)
   const [shopByCategoryIndex, setShopByCategoryIndex] = useState(0)
   const [pitch, setPitch] = useState('');
   const [error, setError] = useState('');
   const [end, setEnd] = useState('');
   const [started, setStarted] = useState(false);
   const [results, setResults] = useState([]);
   const [mikestop, setMikeStop] = useState(true);
   const [isModalVisible, setModalVisible] = useState(false);
   const [loginKey, setloginKey] = useState('')
   const [partialResults, setPartialResults] = useState([]); setScheme('white')
   const [arrShopByCategories, setArrShopByCategories] = useState([])
   const [arrNewArrival, setArrNewArrival] = useState([])
   const [arrBestSeller, setArrBestSeller] = useState([])
   const [arrBanner, setArrBanner] = useState([])
   const [arrSETS, setArrSETS] = useState([])
   const [offerText, setOfferText] = useState('')
   const [arrAds, setArrAds] = useState([])
   const [arrSale, setArrSale] = useState([])
   const [modalvalues, modalVisible] = useState(false);
   const [searchData, setsearchData] = useState([]);
   const [brandData, setbrandData] = useState([]);
   const [segmentData, setsegmentData] = useState([]);
   const { width } = useWindowDimensions();
   const [canGoBack, setCanGoBack] = useState(false)
   const [canGoForward, setCanGoForward] = useState(false)
   const [currentUrl, setCurrentUrl] = useState('')
   const [bannerIndex, setbannerIndex] = useState(0)
   const [offerData, setofferData] = useState([])
   const [isreactive, setReactvie] = useState(false);
   const scanner = useRef(null);
   const webviewRef = useRef(null)
   const refRBSheet = useRef()
   const [paused, setpaused] = useState(true)
   const [videopaused, setvideopaused] = useState(false);
   const [autoplay, setautoplay] = useState(false);
   const [isPaused, setisPaused] = useState(-1)
   const carouselRef = useRef(null)
   const bannerRef = useRef(null)
   const segmentRef = useRef(null)
   const scrollRef = useRef(null)
   let currentslide = 0
   const videoref = useRef(null)
   useEffect(() => {
      const unsubscribe = navigation.addListener('focus', (e) => {
         setSearchText('')
         checkLogin()
         getHomeScreenData()
         setpaused(true)
         // setvideopaused(false)
         setautoplay(true)
      });

      return unsubscribe;
   }, [navigation,]);

   /**
   * Api Intigration
   */


   useEffect(() => {

      const unsubscribe = navigation.addListener('blur', (e) => {
         if (segmentData && segmentData.length > 0) {
            segmentData.map((item, index) => {
               console.log("item,index", item, index);
               if ((item.sg_advertisement) && (item.sg_advertisement).length > 0) {
                  (item.sg_advertisement).map((mainitem, mainindex) => {
                     segmentData[index].sg_advertisement[mainindex].pause = true
                     setsegmentData(segmentData)
                     setModalVisible(false)
                  })
               }
               else {

               }
            })
         }
      });

      return unsubscribe;
   }, [navigation]);


   const getHomeScreenData = async () => {
      console.log("getbannerData");
      const bannerData = await StorageService.getItem(StorageService.STORAGE_KEYS.BANNER_DETAILS)
      setArrBanner(bannerData)
      const categories = await StorageService.getItem(StorageService.STORAGE_KEYS.CATEGORY)
      setArrShopByCategories(categories)
      if (bannerData == null) {
         setModalVisible(true)
      }
      else {
         setModalVisible(false)
      }
      let response = await Request.post('home2.php')
      setModalVisible(false)
      if (response.success == true) {
         if (response.data) {
            if (response.data.categories.length > 0) {
               await StorageService.saveItem(StorageService.STORAGE_KEYS.CATEGORY, response.data.categories)
               setArrShopByCategories(response.data.categories)
            }
            if (response.data.banners.length > 0) {
               await StorageService.saveItem(StorageService.STORAGE_KEYS.BANNER_DETAILS, response.data.banners)
               setArrBanner(response.data.banners)
            }
            if (response.data.free_delivery.length > 0) {
               setOfferText(response.data.free_delivery[0].title)
            }
            if (response.data.menu.length > 0) {
               setArrMainCategories(response.data.menu)
               setbrandData(response.data.brand)
               setofferData(response.data.offer)
            }
            if (response.data.segment_list.length > 0) {
               var segmentData = response.data.segment_list
               segmentData.map((obj) => {
                  obj.activeIndex = 0
               })
               setsegmentData(segmentData)
            }

         }
      }
      else {
         if (response) {
            showSimpleAlert(response.message)
         }
      }

   }

   const checkLogin = async () => {
      let userData = await StorageService.getItem(StorageService.STORAGE_KEYS.USER_DETAILS)
      setloginKey(userData)
   }
   const onSpeechStart = () => {
      setStarted('True')
   };
   const onSpeechEnd = () => {
      setStarted(null);
      setEnd('True');
   };
   const onSpeechError = (e) => {
      setError(JSON.stringify(e.error));
   };
   const onSpeechResults = async (e) => {
      setResults(e.value)
      await Voice.destroy()
   };
   const onSpeechPartialResults = async (e) => {
      setPartialResults(e.value)
      setSearchText(e.value[0])
      const data = e.value[0] == '' ? searchText : e.value[0]
      console.log("data:---?>", data);
      if (data) {
         setStarted(null);
         setEnd('True');
         btnSearchIconClick(data)
         setMikeStop(!mikestop)
         await stopSpeechRecognizing()
      }
   };
   const onSpeechVolumeChanged = (e) => {
      setPitch(e.value)
   };
   const startSpeechRecognizing = async () => {
      console.log("start",);
      setPitch('')
      setError('')
      setStarted('')
      setResults([])
      setPartialResults([])
      setSearchText('')
      setEnd('')
      try {
         await Voice.start('en-US')
         if (mikestop) {
            startSpeechRecognizing()
         }

      } catch (e) {
         console.error(e);
      }
   };
   const stopSpeechRecognizing = async () => {
      try {
         await Voice.stop();
         await Voice.destroy()
      } catch (e) {
         console.error(e);
      }
   };
   Voice.onSpeechStart = onSpeechStart;
   Voice.onSpeechEnd = onSpeechEnd;
   Voice.onSpeechError = onSpeechError;
   Voice.onSpeechResults = onSpeechResults;
   Voice.onSpeechPartialResults = onSpeechPartialResults;
   Voice.onSpeechVolumeChanged = onSpeechVolumeChanged;


   const btnSearchIconClick = async (searchdata) => {
      setSearchText(searchdata)
      if (searchText == '' && searchdata == '') {
         modalVisible(false)
      }
      else {
         const value = searchdata ? searchdata : searchText
         if (value.length > 3) {
            setTimeout(async () => {
               let params = {
                  domain_id: 1,
                  query: value
               }
               let response = await Request.post('get-search-list.php', params)
               if (response.success == true) {
                  modalVisible(true);
                  setsearchData(response.data)
               }
               else {
                  modalVisible(false)
                  if (response) {
                  }
               }
            }, 500);

         }

      }
   }

   const btnCamaraIconClick = () => {
      navigation.navigate('QrcodeProductScreen', { homescreen: 1, qrDataRefresh: (data) => qrDataRefresh(data) })
   }
   const qrDataRefresh = (data) => {
      setTimeout(() => {
         setSearchText(data)
         btnSearchIconClick(data)
      }, 200);
   }
   const btnMikeIconClick = async () => {
      const checkPermission = await checkMultiple([PERMISSIONS.IOS.MICROPHONE, PERMISSIONS.IOS.SPEECH_RECOGNITION])
      if ((checkPermission[PERMISSIONS.IOS.MICROPHONE] == 'denied' || checkPermission[PERMISSIONS.IOS.MICROPHONE] == 'blocked') && (checkPermission[PERMISSIONS.IOS.SPEECH_RECOGNITION] == 'denied' || checkPermission[PERMISSIONS.IOS.SPEECH_RECOGNITION] == 'blocked')) {
         const resultPERMISSIONS = await requestMultiple([PERMISSIONS.IOS.MICROPHONE, PERMISSIONS.IOS.SPEECH_RECOGNITION])
         if (resultPERMISSIONS[PERMISSIONS.IOS.MICROPHONE] == 'granted' && resultPERMISSIONS[PERMISSIONS.IOS.SPEECH_RECOGNITION] == 'granted') {
            setMikeStop(!mikestop)
            if (mikestop) {
               await startSpeechRecognizing()
            } else {
               await stopSpeechRecognizing()
            }
         }
         else {
            Alert.alert(
               ConstantsText.appName,
               'Please allow microphone and speech recognition  permission to recognize voice ',
               [
                  { text: 'Cancel', onPress: () => console.log('cancel') },
                  { text: 'Okay', onPress: () => { Linking.openURL('app-settings:'); } },
               ]
            )
         }
      }
      else {
         setMikeStop(!mikestop)
         if (mikestop) {
            await startSpeechRecognizing()
         } else {
            await stopSpeechRecognizing()
         }
      }


   }

   const btnandroidClick = async () => {
      let speechToTextData = '';
      try {
         speechToTextData = await SpeechToText.startSpeech('Try saying something', 'en_IN');

         btnSearchIconClick(speechToTextData)
         setSearchText(speechToTextData)
      } catch (error) {
         console.log('error: ', error);
      }
   }

   const mainCateoriesClick = (item, index) => {

      if (item.type == 'brand') {
         navigation.navigate("BrandsViewController", { brandData: brandData, brandName: item.name, category: 3 })
      } else {
         navigation.navigate('ProductListingViewController', { productItem: item, })
      }
   }

   const newArrivalProductClick = ({ item }) => {

      navigation.navigate('ProductDetailViewController', { seoUrl: item.seo_url })
   }
   const renderLabelItemProduct = (item) => {
      return (
         <View>

            <View style={{ height: 56, justifyContent: 'center', borderBottomColor: '#CDCECF', marginStart: 20, marginEnd: 20, borderBottomWidth: 1 }}>
               <View style={{}}>
                  <Text style={styles.textItem}>{item.name}</Text>
               </View>
            </View>
         </View>

      );
   };
   const renderMainCategoriesItem = ({ item, index }) => {

      return (

         <View style={{}}>
            {/* <SkeletonContent
               isLoading={isModalVisible}
               layout={[
                  { key: 'someotherId', width: deviceWidth - 20, height: scale(40), },
               ]}
               animationDirection="horizontalLeft"
               animationType='shiver'> */}
               {item.slug != 'offer' ?
                  <TouchableOpacity style={{ justifyContent: 'center', marginLeft: scale(5), marginRight: scale(5) }} onPress={() => mainCateoriesClick(item, index)}>
                     <Text style={[styles.productnameText, {}]}>{item.name}</Text>
                  </TouchableOpacity> :
                  <TouchableOpacity style={{ justifyContent: 'center', marginLeft: scale(5), marginRight: scale(5) }} onPress={() => {
                     refRBSheet.current.open()
                  }}>
                     <Text style={[styles.productnameText, {}]}>{item.name}</Text>
                  </TouchableOpacity>


               }
            {/* </SkeletonContent> */}
         </View>
      )
   }

   const bannerSelect = (item) => {
      if (item.seo_url == '' && item.key_type == '' && item.key_word == '') {
      }
      else if (item.seo_url != '') {
         navigation.navigate('ProductDetailViewController', { seoUrl: item.seo_url })
      }
      else {
         navigation.navigate('ProductListingViewController', { productItem: item, })

      }
   }

   const ProductBannerSelect = (item) => {
      if (item.type == 'product') {
         navigation.navigate('ProductDetailViewController', { seoUrl: item.key_word })
      }
      else (
         navigation.navigate('ProductListingViewController', { productItem: item, })
      )
   }
   const renderTopBannerItem = ({ item, index }) => {
      return (
         <View>
            {
               item.mbanner ?
                  <View>
                     {item.seo_url == '' && item.key_type == '' && item.key_word == '' ?
                        <View>

                           <FastImage
                              style={styles.bannerImage}
                              source={{
                                 uri: item.mbanner,
                                 priority: FastImage.priority.high,
                              }}
                              resizeMode={FastImage.resizeMode.contain}
                           />

                        </View>
                        :
                        <TouchableOpacity onPress={() => bannerSelect(item)}>
                           <FastImage
                              style={styles.bannerImage}
                              source={{
                                 uri: item.mbanner,
                                 priority: FastImage.priority.high,
                              }}
                              resizeMode={FastImage.resizeMode.contain}
                           />

                        </TouchableOpacity>
                     }
                  </View>
                  :
                  <FastImage
                     style={styles.bannerImage}
                     source={importImages.defaultBannerImage}
                     resizeMode={FastImage.resizeMode.contain}
                  />
            }
         </View >
      )
   }
   const onvideoPlay = (mainItem, Index, mainIndex,) => {
      segmentData[mainIndex].sg_advertisement[Index].pause = (mainItem.pause == 'false' || mainItem.pause == false) ? true : false
      setsegmentData(segmentData)

      segmentData.map((item, index) => {
         if ((item.sg_advertisement) && (item.sg_advertisement).length > 0) {
            (item.sg_advertisement).map((mainitem, mainindex) => {
               if (index != mainIndex && Index != mainindex) {
                  segmentData[index].sg_advertisement[mainindex].pause = true
                  setsegmentData(segmentData)
               }
            })
         }
         else {
         }
      })

   }

   const onend = (item, index, mainIndex,) => {
      segmentData[mainIndex].sg_advertisement[index].pause = true
      setsegmentData(segmentData)
      setModalVisible(false)
   }

   const renderBannerItem = (item, index, mainIndex) => {
      return (
         <View style={{ backgroundColor: 'white' }}>
            {
               item.banner ?
                  <View>
                     {item.key_type == '' && item.key_word == '' ?
                        item.is_video == 1 ?
                           <View style={{ backgroundColor: commonColors.Black }}>
                              <Video
                                 style={styles.bannerImage}
                                 playInBackground={false}       // Audio continues to play when app entering background.
                                 playWhenInactive={false}
                                 repeat={true}
                                 rate={1.0}
                                 muted={item.pause == 'true' || item.pause == true ? true : false}
                                 onEnd={() => { onend(item, index, mainIndex) }}
                                 source={{
                                    uri: item.banner,
                                    priority: FastImage.priority.high,
                                 }}
                                 paused={item.pause == 'true' || item.pause == true ? true : false} // value updated with state variable 
                              />
                              <TouchableOpacity style={{ height: scale(25), width: scale(25), bottom: scale(10), left: scale(30), }} onPress={() => {
                                 onvideoPlay(item, index, mainIndex)
                              }}>
                                 <Image source={item.pause == "true" || item.pause == true ? importImages.PlayImage : importImages.PauseImage} style={{ alignSelf: "center", justifyContent: "center", alignItems: "center" }} />
                              </TouchableOpacity>
                           </View>


                           :
                           <View>
                              <FastImage
                                 style={styles.bannerImage}
                                 source={{
                                    uri: item.banner,
                                    priority: FastImage.priority.high,
                                 }}
                                 resizeMode={FastImage.resizeMode.contain}
                              />
                           </View>
                        :
                        item.is_video == 1 ?

                           <View style={{ backgroundColor: commonColors.Black }}>
                              <TouchableOpacity onPress={() => { ProductBannerSelect(item) }}>
                                 <Video
                                    style={styles.bannerImage}
                                    playInBackground={false}       // Audio continues to play when app entering background.
                                    playWhenInactive={false}
                                    repeat={true}
                                    rate={1.0}
                                    onEnd={() => { onend(item, index, mainIndex) }}
                                    source={{
                                       uri: item.banner,
                                       priority: FastImage.priority.high,
                                    }}
                                    muted={item.pause == 'true' || item.pause == true ? true : false}
                                    paused={item.pause == 'true' || item.pause == true ? true : false} // value updated with state variable 
                                 />


                              </TouchableOpacity>


                              <TouchableOpacity style={{ height: scale(25), width: scale(25), bottom: scale(10), left: scale(30), }}
                                 onPress={() => {
                                    onvideoPlay(item, index, mainIndex)
                                 }}
                              >
                                 <Image source={item.pause == "true" || item.pause == true ? importImages.PlayImage : importImages.PauseImage} style={{ alignSelf: "center", justifyContent: "center", alignItems: "center" }} />
                              </TouchableOpacity>
                           </View>

                           : <TouchableOpacity onPress={() => ProductBannerSelect(item)}>
                              <FastImage
                                 style={styles.bannerImage}
                                 source={{
                                    uri: item.banner,
                                    priority: FastImage.priority.high,
                                 }}
                                 resizeMode={FastImage.resizeMode.contain}
                              />
                           </TouchableOpacity>
                     }
                  </View>
                  :
                  <FastImage
                     style={styles.bannerImage}
                     source={importImages.defaultBannerImage}
                     resizeMode={FastImage.resizeMode.contain}
                  />
            }
         </View>
      )
   }



   const favoriteUnFavoriteButtonClick = async (item, index, mainIndex, type) => {
      const favItem = item
      if (loginKey) {
         let params = {
            product_id: favItem.product_id,
         }
         setModalVisible(true)
         let response = await Request.post('add-favorite-item.php', params)
       
         if (response.success == true) {
           
            segmentData[mainIndex].item[index].is_favourite = item.is_favourite == 0 ? 1 : 0
            setsegmentData(segmentData)
            setModalVisible(false)
         }
         else {
            setModalVisible(false)
            showSimpleAlert(response.message)
         }
      }
      else {
         navigation.navigate('AuthSelection', { authFlag: 1, })
      }

   }

   const generateLink = async (item) => {
      const shareItem = item.item
      const message = shareItem.brand_name + '\n' + shareItem.family_name + '\n' + shareItem.main_price + "JD's"

      console.log("message", message, shareItem.seo_url);
      const dynamicLink = await dynamicLinks().buildShortLink({
         link: `https://giftsshopping.page.link/?productId=${shareItem.seo_url}`,
         domainUriPrefix: 'https://giftsshopping.page.link',
         ios: {
            bundleId: 'com.giftsshopping',
            appStoreId: '6446405028'
         },
         android: {
            packageName: 'com.giftsshopping',
         },
         social: {
            title: shareItem.brand_name,
            descriptionText: message,
            imageUrl: shareItem.family_pic,
         },
         navigation: {
            forcedRedirectEnabled: true,
         }
      });
      return dynamicLink;

   }
   const sendReviewButtonClick = async (item, index) => {
      const appLink = await generateLink(item)
      console.log("appLink", appLink);
      const shareOption = {
         subject: 'Gift Center',
         message: appLink,
      }
      Share.open(shareOption)
         .then((res) => {
            console.log('res-> ', res);
         })
         .catch((err) => {
            err && console.log(err);
         });
   }
   // const sendReviewButtonClick = (item, index) => {
   //    const shareItem = item.item
   //    const message = 'Product Name:' + shareItem.brand_name + '\n' + shareItem.family_name + '\n' + "Price:" + shareItem.main_price + "JD's"
   //    const shareOptions = {
   //       subject: 'Gift Center',
   //       message: message,
   //       url: shareItem.family_pic
   //    };

   //    const response = Share.open(shareOptions)
   //       .then(res => {
   //          console.log(res);
   //       })
   //       .catch(err => {
   //          err && console.log(err);
   //       });
   //    return response;
   // }
   const renderNewArrivalItem = (item, index, mainIndex, type) => {
      return (
         <ProductComponent
            item={item}
            isLoading={false}
            index={index}
            isFavourite={item.is_favourite == 0 ? false : true}
            mainCateoriesClick={(item, index) => newArrivalProductClick({ item, index })}
            colorCombination={colorCombination}
            colorCodeChangedIndex={(i) => colorCodeChangedIndex(i)}
            colorSelectedIndex={colorSelectedIndex}
            disabled={isModalVisible == true ? true : false}
            favoriteUnFavoriteButtonClick={(item, index) => favoriteUnFavoriteButtonClick(item, index, mainIndex, type)}
            sendReviewButtonClick={(item, index) => sendReviewButtonClick({ item, index })}
            isShowOffer={item.has_offer == '' ? false : true}
            isShowColorShade={false}
         />
      )
   }

   const newArrivalPlusButtonClick = (item, title) => {
      navigation.navigate('ProductListingViewController', { productItem: item, })
      // navigation.navigate('SeeAllViewController', { sectionName: item, sectionTitle: title })
   }

   const renderVideoBannerItem = (item, index) => {
      return (
         <View>
            {
               item.video ?
                  <FastImage
                     style={styles.videoBanner}
                     source={{
                        uri: item.video,
                        priority: FastImage.priority.high,
                     }}
                     resizeMode={FastImage.resizeMode.contain}
                  /> : <FastImage
                     style={styles.videoBanner}
                     source={importImages.defaultsaleBannerImage}
                     resizeMode={FastImage.resizeMode.contain}
                  />
            }
         </View>
      )
   }

   const colorCodeChangedIndex = (i) => {
      setColorSelectedIndex(i)
   }

   const renderShopByCategory = ({ item, index }) => {
      return (
         <TouchableOpacity style={styles.shopCategoryView} onPress={() => shopCategoryClick({ item, index })}>
            <View style={styles.salesImageView}>
               {item.banner ? <FastImage
                  style={styles.shopCategoryImage}
                  source={{
                     uri: item.banner,
                     priority: FastImage.priority.high,
                  }}
               /> : <FastImage
                  style={styles.shopCategoryImage}
                  source={importImages.defaultcategoryBannerImage}
                  resizeMode={FastImage.resizeMode.contain}
               />}
            </View>
            <View style={styles.shopCategorynameView}>
               <Text style={styles.shopCategoryText}>{item.name}</Text>
            </View>
         </TouchableOpacity>
      )
   }

   const shopCategoryClick = ({ item, index }) => {

      navigation.navigate('ProductListingViewController', { productItem: item, })

   }

   const returnTitle = (str) => {
      return (
         <Text style={styles.sectionTitle}>{str}</Text>
      )
   }

   const returnFlatLists = (renderItem, renderData, horizontal, bounces, pagingEnabled) => {
      return (
         <FlatList
            horizontal={horizontal}
            bounces={bounces}
            pagingEnabled={pagingEnabled}
            renderItem={renderItem}
            data={renderData}
            style={{ marginTop: 16, }}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => `${index}-id`}
         />
      )
   }
   const searchItemRedirection = (item) => {
      modalVisible(false)
      navigation.navigate('ProductDetailViewController', { seoUrl: item.seo_url })

   }
   const brandRedirection = (item) => {
      modalVisible(false)
      navigation.navigate('ProductListingViewController', { productItem: item, })
   }
   const rendersearchItem = ({ item, index }) => {
      return (
         <View style={[styles.searchView, { marginHorizontal: scale(5), }]}>
            <View style={{}}>
               {item.type == 'product' ?
                  <View>
                     <TouchableOpacity onPress={() => {
                        searchItemRedirection(item)
                     }
                     }>
                        <View style={[styles.searchProductView, {}]}>
                           <Image source={{ uri: item.family_pic }} style={{ height: scale(30), width: scale(30) }} />
                           <Text style={[styles.searchProductText, { paddingHorizontal: scale(5), fontFamily: Fonts.montserratBold }]}>{item.brand_id ? item.brand_name : ' '}</Text>
                           <Text numberOfLines={1} style={[styles.searchProductText, { paddingVertical: scale(10), }]}>{item.family_name
                              ? item.family_name
                              : ' '}</Text>
                        </View>
                     </TouchableOpacity>
                  </View>
                  :
                  <View>
                     <TouchableOpacity onPress={() => { brandRedirection(item) }}>
                        <Text style={[styles.searchProductText, { fontFamily: Fonts.montserratBold, paddingVertical: scale(10), }]}>{item.brand_id ? item.brand_name : null}</Text>
                     </TouchableOpacity>
                  </View>
               }
               {item.term ?
                  <CustomButton title={'View More Results'} onPress={() => mainCateoriesClick(item, index)}
                     style={{ width: deviceWidth - 40, right: scale(10), bottom: scale(30) }}
                  /> : null}
            </View>

         </View>

      )

   }
   const itemSeparatorComponent = ({ item }) => {
      return (
         <View style={styles.lineSeprator}></View>
      );
   }
   const onscrollEvent = (event, item, index) => {
      item.activeIndex = Math.round(event.nativeEvent.contentOffset.x / deviceWidth) * 2
      let segmentdata = segmentData.map((obj, objInd) => {
         if (objInd == index) {
            item.activeIndex = Math.round(event.nativeEvent.contentOffset.x / deviceWidth) * 2
         }
         return { ...obj }
      })
      setsegmentData(segmentdata)
   }
   const onadvertiscroll = (event, item, index) => {
      let segmentdata = segmentData.map((obj, objInd) => {
         if (objInd == index) {
            item.activeIndex = Math.round(event.nativeEvent.contentOffset.x / deviceWidth) * 1
         }
         return { ...obj }
      })
      setsegmentData(segmentdata)
   }

   const backButtonHandler = () => {
      if (webviewRef.current) webviewRef.current.goBack()
   }

   const frontButtonHandler = () => {
      if (webviewRef.current) webviewRef.current.goForward()
   }
   const rendersegmentData = (item, index) => {
      let mainIndex = index
      return (

         <View style={{ flexDirection: 'column' }}>

            {item.type == 'advertisement' ?
               <View>
                  <FlatList
                     horizontal
                     bounces={false}
                     renderItem={({ item, index }) => renderVideoBannerItem(item, index, mainIndex, item.slug)}
                     extraData={item.item}
                     data={item.item}
                     style={{ marginTop: 16, }}
                     pagingEnabled={true}
                     showsVerticalScrollIndicator={false}
                     showsHorizontalScrollIndicator={false}
                     keyExtractor={(item, index) => `${index}-id`}
                     onScroll={(event) => {
                        onadvertiscroll(event, item, index)
                     }}
                  />
                  {
                     (item.item) && (item.item).length >= 1 ?
                        <View style={styles.dotView}>
                           {(item.item).map((step, i) =>
                              <View key={i}
                                 style={[
                                    styles.stepSlides,
                                    { backgroundColor: (item.activeIndex) === i ? commonColors.Blue : commonColors.pageControlColor },
                                 ]}
                              />
                           )}
                        </View> : null
                  }
               </View>
               :


               <View style={{}}>

                  <View style={{ flex: 1, }} >
                     <Text style={styles.sectionTitle}>{item.title}</Text>

                     {(item.sg_advertisement) && (item.sg_advertisement).length > 0 ?
                        <FlatList
                           horizontal
                           ref={bannerRef}
                           bounces={false}
                           renderItem={({ item, index }) => renderBannerItem(item, index, mainIndex)}
                           data={item.sg_advertisement}
                           pagingEnabled={true}
                           style={{ marginVertical: scale(10) }}
                           showsVerticalScrollIndicator={false}
                           showsHorizontalScrollIndicator={false}
                           extraData={item.sg_advertisement}
                           keyExtractor={(item, index) => `${index}-id`}
                           onScroll={(event) => {
                              setbannerIndex(Math.round(event.nativeEvent.contentOffset.x / deviceWidth))
                           }}
                        /> : null}
                     {/* <TouchableOpacity onPress={() => {
                                    console.log("bannerRedf", bannerRef.current.scrollToIndex)
                                    bannerRef.current.scrollToIndex({
                                       index: i,
                                       animated: true
                                    });
                                    setbannerIndex(i)

                                 }}> */}
                     {
                        (item.sg_advertisement) && (item.sg_advertisement).length > 1 ?
                           <View style={[styles.dotView, { marginVertical: scale(10) }]}>
                              {(item.sg_advertisement).map((step, i) =>

                                 <View key={i}
                                    style={[
                                       styles.stepSlides,
                                       { backgroundColor: bannerIndex === i ? commonColors.Blue : commonColors.pageControlColor },
                                    ]}
                                 />
                              )}

                           </View> : null
                     }
                     <FlatList
                        ref={segmentRef}
                        horizontal
                        bounces={false}
                        renderItem={({ item, index }) => renderNewArrivalItem(item, index, mainIndex, item.slug)}
                        extraData={item.item}
                        data={item.item}
                        pagingEnabled={true}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyExtractor={(item, index) => `${index}-id`}
                        onScroll={(event) => {
                           onscrollEvent(event, item, index)
                        }}
                     /></View>
                  {
                     (item.item) && (item.item).length > 1 ?
                        <View style={styles.dotView}>
                           {(item.item).map((step, i) => {
                              return (i % 2 == 0 ?
                                 <View key={i}
                                    style={[
                                       styles.stepSlides,
                                       { backgroundColor: (item.activeIndex) === i ? commonColors.Blue : commonColors.pageControlColor },
                                    ]}
                                 /> : null
                              )
                           }
                           )}
                           {
                              <TouchableOpacity style={{ marginRight: 8, marginTop: -2, }} onPress={() => newArrivalPlusButtonClick(item, item.title)}>
                                 <Image source={importImages.plusIcon} />
                              </TouchableOpacity>}
                        </View> : null
                  }
               </View>}


         </View>
      )


   }
   const renderofferData = ({ item, index }) => {
      return (
         <View style={{}}>

            <TouchableOpacity style={{ marginLeft: scale(20), marginRight: scale(5), flexDirection: "row", alignItems: "center" }} onPress={() => {
               refRBSheet.current.close()
               mainCateoriesClick(item, index)
            }}>
               <Image source={importImages.offerImage} style={{ height: scale(16), width: scale(16) }} />
               <Text style={[styles.productnameText, { fontFamily: Fonts.montserratRegular, fontWeight: "500", textAlign: "center", paddingHorizontal: scale(10) }]}>{item.name}</Text>
            </TouchableOpacity>
            <View style={[styles.lineSeprator, { marginVertical: scale(10) }]}></View>

         </View>
      )
   }
   const onSuccess = async (e) => {

   };

   const scan = () => {
      scanner.current.reactivate()
      setModalVisible(false)
   }
   const crossfunct = () => {
      setSearchText('')
   }
   const getItemLayout = (data, index) => {
      return {
         length: (502 * deviceWidth) / 780,
         offset: deviceWidth * index,
         index,
      };
   };



   const onScroll = (event) => {
      if (event) {
         if (segmentData && segmentData.length > 0) {
            segmentData.map((item, index) => {
               if ((item.sg_advertisement) && (item.sg_advertisement).length > 0) {
                  (item.sg_advertisement).map((mainitem, mainindex) => {
                     segmentData[index].sg_advertisement[mainindex].pause = true
                     setsegmentData(segmentData)
                     setModalVisible(false)
                  })
               }
               else {

               }
            })
         }
      }
   }

   return (
      <GestureHandlerRootView style={{ flex: 1 }}>

         <View style={styles.mainViewStyle}>
            <View style={styles.headerLogoStyle}>
               <Image source={importImages.headerLogo} style={{ width: scale(160), height: scale(23) }} />
            </View>
            {/* Search */}
            <View style={styles.searchViewStyle}>
               <View style={{ flexDirection: 'row' }}>
                  <TouchableOpacity style={styles.searchClick} disabled={searchText == '' ? true : false} onPress={() => { btnSearchIconClick(searchText) }}>
                     <Image source={importImages.SearchIcon} style={styles.searchIconImage} />
                  </TouchableOpacity>
                  <View style={styles.searchBarView}>
                     <TextInput
                        style={styles.searchBarTextinput}
                        placeholder={'Search'}
                        placeholderTextColor={commonColors.Blue}
                        value={searchText}
                        multiline={false}
                        onChangeText={(data) => btnSearchIconClick(data)}
                        underlineColorAndroid='transparent'
                     />
                  </View>
                  {searchText != '' ?
                     <TouchableOpacity style={{ marginLeft: scale(15), justifyContent: "center" }} onPress={() => {
                        crossfunct()
                     }}>
                        <Image source={importImages.crossImage} style={{
                           height: scale(12),
                           alignSelf: 'center',
                           width: scale(12),
                           tintColor: commonColors.Grey
                        }} />
                     </TouchableOpacity> : null}
                  <TouchableOpacity style={[styles.searchBarRightIconStyle, { width: scale(30) }]} onPress={Platform.OS == 'android' ? btnandroidClick : btnMikeIconClick}>
                     <Image source={importImages.mikeIcon} style={{
                        height: scale(20),
                        alignSelf: 'center',
                        width: scale(10),
                        tintColor: mikestop == true ? commonColors.Blue : 'green'
                     }} />
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.searchBarRightIconStyle} onPress={btnCamaraIconClick}>
                     <Image source={importImages.cameraIcon} style={styles.cameraIcon} />
                  </TouchableOpacity>
               </View>
            </View>

            <ScrollView style={{ marginTop: 8, }} nestedScrollEnabled={true} showsVerticalScrollIndicator={false}>
               <View>
                  {modalvalues == true && searchText != '' ?
                     <View style={[styles.modalView, {}]}>
                        <ScrollView>
                           <FlatList
                              renderItem={rendersearchItem}
                              nestedScrollEnabled={true}
                              style={{ flex: 1, marginBottom: scale(130), marginLeft: scale(10), marginRight: scale(15) }}
                              contentContainerStyle={{ paddingTop: scale(10), }}
                              data={searchData}
                           />
                        </ScrollView>
                     </View> : null}

                  {searchText == '' ?
                     <View style={{ zIndex: -100 }}>
                        {/* Main Category */}
                        <View style={{ flex: 1, marginHorizontal: scale(15) }}>
                           {returnFlatLists(renderMainCategoriesItem, arrMainCategories, true, true, false)}
                        </View>
                        <View style={styles.offerView}>
                           <Text style={styles.offerText}>{offerText}</Text>
                        </View>

                        {/* Top Banner */}


                        {
                           arrBanner && Platform.OS == 'android' ?
                              <Carousel
                                 ref={carouselRef}
                                 loop
                                 autoPlay={true}
                                 autoPlayInterval={3000}
                                 scrollAnimationDuration={500}
                                 firstItem={- arrBanner.length}
                                 data={arrBanner}
                                 height={(502 * deviceWidth) / 780}
                                 width={deviceWidth}
                                 getItemLayout={getItemLayout}
                                 onSnapToItem={(index) => { setTopBannerSelectedIndex(index) }}
                                 renderItem={renderTopBannerItem}
                              />
                              : null}


                        {arrBanner && Platform.OS == 'ios' ?
                           <RnCarousel
                              ref={carouselRef}
                              inactiveSlideScale={1}
                              autoplay
                              loop
                              autoplayDelay={3000}
                              enableMomentum={true}
                              decelerationRate={0.9}
                              autoplayInterval={3000}
                              firstItem={-arrBanner.length}
                              data={arrBanner}
                              itemHeight={(502 * deviceWidth) / 780}
                              sliderHeight={(502 * deviceWidth) / 780}
                              sliderWidth={deviceWidth}
                              itemWidth={deviceWidth}
                              getItemLayout={getItemLayout}
                              onSnapToItem={(index) => { setTopBannerSelectedIndex(index) }}
                              renderItem={renderTopBannerItem}
                           /> : null}
                        {/* <TouchableOpacity onPress={() => {
                                       carouselRef.current.scrollTo({
                                          index: i,
                                          animated: true
                                       });;
                                       setTopBannerSelectedIndex(i)

                                    }}> */}
                        {
                           arrBanner ? arrBanner.length > 1 ?
                              <View style={styles.dotView}>
                                 {arrBanner.map((step, i) =>

                                    <View key={i}
                                       style={[
                                          styles.stepSlides,
                                          { backgroundColor: topBannerSelectedIndex === i ? commonColors.Blue : commonColors.pageControlColor },
                                       ]}
                                    />
                                 )}
                              </View> : null : null
                        }
                        <FlatList
                           bounces={false}
                           renderItem={({ item, index }) => rendersegmentData(item, index,)}
                           data={segmentData}
                           style={{ marginTop: 16, marginBottom: 72 }}
                           extraData={segmentData}
                           pagingEnabled={true}
                           horizontal={false}
                           showsVerticalScrollIndicator={false}
                           showsHorizontalScrollIndicator={false}
                           keyExtractor={(item, index) => `${index}-id`}
                        />
                        <View
                           style={{
                              flex: 1,
                              justifyContent: "center",
                              alignItems: "center",
                              backgroundColor: "#000"
                           }}
                        >
                           <RBSheet
                              ref={refRBSheet}
                              closeOnDragDown={true}
                              closeOnPressMask={false}
                              customStyles={{
                                 container: {
                                    borderTopLeftRadius: scale(20),
                                    borderTopRightRadius: scale(20)
                                 },
                                 wrapper: {

                                 },
                                 draggableIcon: {
                                    backgroundColor: "#000"
                                 }
                              }}
                           >
                              <View style={{ flex: 1 }}>
                                 <Text style={{ textAlign: "center", fontSize: scale(14), color: commonColors.Blue, fontFamily: Fonts.montserratMedium }}>{'OFFERS'}</Text>
                                 <FlatList
                                    bounces={false}
                                    renderItem={renderofferData}
                                    nestedScrollEnabled={true}
                                    showsVerticalScrollIndicator={true}
                                    style={{ flex: 1, marginTop: scale(20) }}
                                    data={offerData}
                                 />
                              </View>
                           </RBSheet>
                        </View>
                     </View>

                     : null}
               </View>

            </ScrollView>
            {isModalVisible &&
               <BallIndicator visible={isModalVisible} />
            }
         </View>
      </GestureHandlerRootView>

   );
}

const styles = StyleSheet.create({
   mainViewStyle: {
      flex: 1,
      backgroundColor: 'white',

   },
   headerLogoStyle: {
      justifyContent: 'center',
      alignItems: 'center',
      height: 44,
   },

   barCodeImageStyle: {
      alignItems: 'center',
      justifyContent: 'center',
   },
   searchViewStyle: {
      height: scale(35),
      marginHorizontal: 16,
      alignContent: 'center',
      borderColor: commonColors.Blue,
      borderRadius: 15,
      borderWidth: 1,
      justifyContent: 'center'
   },
   searchBarRightIconStyle: {
      marginHorizontal: 8,
      alignContent: 'flex-end',
      justifyContent: 'center'
   },
   stepSlides: {
      height: 8,
      width: 8,
      borderRadius: 4,
      marginRight: 8,
      marginTop: 0,
      backgroundColor: 'blue'
   },
   stepSlides2: {
      height: 8,
      width: 8,
      borderRadius: 4,
      marginRight: 8,
      marginTop: 0,
      backgroundColor: 'blue'
   },
   sectionTitle: {
      fontFamily: Fonts.montserratSemiBold,
      fontSize: 20,
      color: commonColors.Blue,
      alignSelf: "center",
      paddingVertical: scale(10)
   },
   modalViewStyle: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: scale(120),
      opacity: 0.5,
      backgroundColor: commonColors.White
   },
   modalSubViewStyle: {
      backgroundColor: commonColors.White,
      shadowColor: commonColors.Black,
      shadowOpacity: 1,
      shadowRadius: 10,
      borderRadius: 8,
      width: deviceWidth - 50,
      height: scale(100),
      justifyContent: 'center',
      paddingHorizontal: 20,
      paddingTop: 20,
      marginBottom: scale(550),
   },
   lineSeprator: {
      borderBottomWidth: 0.3,
      padding: 2,
      borderColor: commonColors.Grey
   },
   productnameText: {
      fontFamily: Fonts.montserratMedium,
      fontSize: scale(12),
      color: commonColors.Blue
   },
   bannerImage: {
      width: deviceWidth,
      height: (502 * deviceWidth) / 780,
   },
   videoBanner: {
      width: deviceWidth,
      height: (279 * deviceWidth) / 375,
   },
   salesView: {
      flex: 1,
      marginHorizontal: 16,
      alignItems: 'center'
   },
   shopCategoryView: {
      justifyContent: 'center',
      width: (deviceWidth - 32) / 2,
      marginHorizontal: 8
   },
   salesImageView: {
      borderRadius: 10,
      shadowColor: commonColors.Blue,
      shadowOffset: { width: 0, height: 0 },
      shadowOpacity: 2,
      shadowRadius: 1,
      elevation: 5
   },
   searchIconImage: {
      height: scale(20),
      width: scale(16),
   },
   cameraIcon: {
      height: scale(15),
      width: scale(18),
   },
   modalView: {
      height: deviceHeight,
      width: deviceWidth,
      backgroundColor: commonColors.White,
      shadowColor: commonColors.Black,
      shadowOpacity: 1,
      shadowRadius: 10,
      position: Platform.OS == 'android' ? 'relative' : 'absolute',
      shadowOffset: { width: 1, height: 3 },
   },
   dotView: {
      flexDirection: "row",
      alignSelf: 'center',
      marginTop: 16
   },
   shopCategorynameView: {
      marginHorizontal: 12,
      marginTop: 8
   },
   offerView: {
      width: deviceWidth,
      height: 24,
      alignItems: 'center',
      opacity: 0.6,
      justifyContent: 'center',
      backgroundColor: commonColors.Blue,
      marginTop: scale(10),
      zIndex: 10000,
   },
   offerText: {
      alignSelf: 'center',
      color: 'white',
      fontFamily: Fonts.montserratSemiBold,
      fontSize: 13,
   },
   searchBarTextinput: {
      color: commonColors.Blue,
      fontFamily: Fonts.montserratRegular,
      fontSize: scale(12),
   },
   searchBarView: {
      flex: 1,
      justifyContent: 'center',
      alignSelf: 'center'
   },
   searchClick: {
      marginHorizontal: 8,
      justifyContent: 'center'
   },
   searchProductText: {
      fontSize: scale(12),
      color: commonColors.Black,

   },
   searchProductView: {
      alignItems: "center",
      flexDirection: "row"
   },
   searchItemText: {
      fontSize: scale(10),
      color: commonColors.Black,
      fontFamily: Fonts.montserratMedium
   },
   searchView: {
   },
   shopCategoryText: {
      alignSelf: 'center',
      fontFamily: Fonts.montserratBold,
      fontSize: 13,
      color: commonColors.Blue
   },
   shopCategoryImage: {
      alignSelf: 'center',
      width: 168,
      height: 113,
      borderRadius: 10,
   },
   webview: {
      flex: 1
   },
   sortAndFilterButtonStyle2: {
      flexDirection: 'row',
      height: scale(0),
      width: scale(80),
      justifyContent: 'space-between',
      alignSelf: 'center',
      borderColor: commonColors.Blue,
      borderWidth: 0,
      borderRadius: 5
   },
   dropdown: {
      paddingHorizontal: scale(4),
      marginTop: scale(15),
      alignSelf: 'center',
      textAlign: 'center',
      color: commonColors.Blue,
      justifyContent: 'center',
      width: '100%',
      height: '100%',
   },
   placeholderStyle: {
      fontSize: scale(12),
      fontFamily: Fonts.montserratMedium,
      alignSelf: 'center',
      textAlign: 'center',
      paddingRight: scale(120),
      color: commonColors.Blue
   },
   selectedTextStyle: {
      fontSize: scale(14),
      textAlign: 'center',
      fontFamily: Fonts.montserratMedium,
      color: commonColors.Blue
   },
   textItem: {
      color: commonColors.Blue,
      fontSize: scale(12),
      alignSelf: 'center',
      fontFamily: Fonts.montserratMedium
   },
})