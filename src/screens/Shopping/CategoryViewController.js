import { React, Request, Alert, showSimpleAlert, importImages, TouchableOpacity, TextInput, useState, useEffect, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale, JSFunctionUtils } from '../../utils/importLibrary'
import _ from 'lodash';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';

var teamp = []

export default function CategoryViewController({ route, navigation }) {
   const [arrMainCategories, setArrMainCategories] = useState([])
   const [arrSubCategories, setArrSubCategories] = useState([])
   const [isExpandCollaps, setIsExpandCollaps] = useState()
   const [loginKey, setloginKey] = useState('')
   const [isModalVisible, setModalVisible] = useState(false);
   const [giftVoucher, setgiftVoucher] = useState([])
   const [brandData, setbrandData] = useState([])

   useEffect(() => {
      const unsubscribe = navigation.addListener('focus', () => {
         checkLogin();
      });
      return unsubscribe;
   }, [navigation])

   const checkLogin = async () => {
      await getCategoriesList()
   }
   const getCategoriesList = async () => {
      let params = { domain_id: 8 }

      let response = await Request.post('get-category-list.php', params)
      setModalVisible(false)
      if (response.success == true) {

         if (teamp.length > 0) {
            var temp1 = []
            var dataarray = [...response.data.tab]
            for (let i = 0; i < dataarray.length; i++) {
               var index1 = teamp.findIndex(element => element.slug === dataarray[i].slug);
               if (teamp[index1].plus) {
                  dataarray[i].plus = true
                  dataarray[i].items.map((data) => {
                     var subindex1 = teamp[index1].items.findIndex(element => element.cat_id === data.cat_id);
                     if (teamp[index1].items[subindex1].plus) {
                        dataarray[i].items[subindex1].plus = true
                     }
                  })
                  temp1.push(dataarray[i])
               }
               else {
                  temp1.push(dataarray[i])
               }
            }

            setArrMainCategories(temp1)
         }
         else {
            teamp = [...response.data.tab]
            setArrMainCategories([...response.data.tab])
         }
      }
      else {
         if (response) {
            showSimpleAlert(response.message)
         }
      }
   }

   const expandCollapsClick = (name, type) => {
      if (isExpandCollaps == type) {
         setIsExpandCollaps('')
      }
      else {
         setIsExpandCollaps(type)
      }
      navigation.navigate('ProductListingViewController', { productItem: name, category: 1 })

   }


   const subcategoryExpand = (item, index, mainindex) => {
      var teamparray = [...arrMainCategories]
      teamparray[mainindex].items[index].plus = !item.plus
      teamp = teamparray
      setArrMainCategories([...teamparray])
      navigation.navigate('ProductListingViewController', { productItem: item, category: 1 })

   }
   const categoryExpand = (item, index, mainindex) => {
      var teamparray = [...arrMainCategories]
      teamparray[mainindex].items[index].plus = !item.plus
      teamp = teamparray
      setArrMainCategories([...teamparray])

   }
   const offerclick = (item, index, mainindex) => {
      navigation.navigate('ProductListingViewController', { productItem: item, category: 1 })

   }
   const rendermainCategories = (item, index, mainindex) => {

      return (
         <View>
            <View style={styles.seperatoreStyle}></View>
            <View style={styles.subCategoryItemStyle}>
               <TouchableOpacity onPress={() => expandCollapsClick(item, index)}>
                  <Text style={styles.subCategoryTextStyle}>{item.name ? (item.name).toUpperCase() : (item.cat_name).toUpperCase()}</Text>
               </TouchableOpacity>
               <TouchableOpacity onPress={() => item.subcategory == "[]" ? offerclick(item, index, mainindex) : item.subcategory && item.subcategory.length > 0 ? categoryExpand(item, index, mainindex) : subcategoryExpand(item, index, mainindex)}>
                  {item.type == 'brand' || item.type == 'gift_vocher' ? null : item.subcategory ? item.subcategory == "[]" ? null : item.subcategory.length > 0 ? item.plus == false ?
                     <Image source={importImages.plusImage} resizeMode='contain' style={{
                        width: scale(12),
                        height: scale(12),
                        alignSelf: 'center'
                     }} /> :
                     <View style={{ height: scale(12), width: scale(12) }}>
                        <Image source={importImages.minusImage}
                           resizeMode='contain'
                           style={{
                              height: scale(12), width: scale(12),
                              alignSelf: 'center'
                           }} /></View> : null : null}
               </TouchableOpacity>
            </View>
            {
               item.plus == true ?
                  <FlatList
                     data={item.subcategory}
                     renderItem={rendersubCategory}
                     nestedScrollEnabled={true}
                  /> : null
            }
         </View >
      )
   }

   const rendersubCategory = ({ item, index }) => {
      return (
         <View>
            <View style={styles.seperatoreStyle}></View>
            <View style={styles.subCategoryItemStyle2}>
               <TouchableOpacity onPress={() => expandCollapsClick(item, index)} style={styles.subCategoryItemStyle2}>
                  <Text style={styles.subCategorysubTextStyle}>{(item.sub_cat_name).toUpperCase()}</Text>
               </TouchableOpacity>
            </View>
         </View>
      )
   }




   const itemSeparatorComponent = ({ item }) => {
      return (
         <View style={styles.seperatoreStyle}></View>
      );
   }

   const expandBrand = (item) => {
      if (item.type == 'gift_vocher') {
         navigation.navigate("GiftVoucherViewController", { giftVoucher: item.items })
      }
      else {
         navigation.navigate("BrandsViewController", { brandData: item.items, brandName: item.name, category: 1 })
      }
   }
   const visibleItem = (item, index) => {
      var teamparray = [...arrMainCategories]
      teamparray[index].plus = !item.plus

      teamp = [...teamparray]
      setArrMainCategories([...teamparray])

   }
   const renderCategories = ({ item, index }) => {
      var mainindex = index
      return (
         <View style={{ flex: 1, }}>
            <View style={[styles.optionsTextStyle, { marginRight: scale(20) }]}>
               <TouchableOpacity onPress={() => item.type == 'brand' || item.type == 'gift_vocher' ? expandBrand(item) : item.type == 'offer' ? visibleItem(item, index) : expandCollapsClick(item, index)}>
                  <Text style={[styles.categoryLabelTextStyle, { color: commonColors.Blue }]}>{(item.name).toUpperCase()}</Text>
               </TouchableOpacity>
               <TouchableOpacity onPress={() => item.type == 'brand' || item.type == 'gift_vocher' ? expandBrand(item) : visibleItem(item, index)} >
                  <View style={{ marginRight: scale(10) }}>
                     {item.type == 'brand' || item.type == 'gift_vocher' ?
                        <Image source={importImages.leftArrowimage} style={{
                           alignSelf: 'center'
                        }} /> :
                        item.plus == false ? <Image source={importImages.plusImage} style={{
                           alignSelf: 'center'
                        }} /> : <Image source={importImages.minusImage} style={{
                           alignSelf: 'center'
                        }} />}
                  </View>
               </TouchableOpacity>
            </View>
            {item.type == 'brand' || item.type == 'gift_vocher' ? null :
               item.plus == true ?
                  <FlatList
                     data={item.items}
                     nestedScrollEnabled={true}
                     renderItem={({ item, index }) => rendermainCategories(item, index, mainindex)}
                  /> : null
            }

         </View>
      )
   }

   return (
      <View style={styles.mainViewStyle}>
         <View style={styles.headerLogoStyle}>
            <Text style={{ fontFamily: Fonts.montserratSemiBold, fontSize: 20, color: commonColors.Blue, marginBottom: 8 }}>CATEGORIES</Text>
         </View>
         <View style={styles.seperatoreStyle}></View>
         <View style={{ flex: 1, marginBottom: scale(80) }}>

            {arrMainCategories.length && !isModalVisible ?
               <FlatList
                  bounces={false}
                  renderItem={renderCategories}
                  data={arrMainCategories}
                  extraData={arrMainCategories}
                  ItemSeparatorComponent={itemSeparatorComponent}
                  contentContainerStyle={{ flexGrow: 1, }}
                  nestedScrollEnabled={true}
                  showsVerticalScrollIndicator={false}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item, index) => `${index}-id`}
               /> :
               <BallIndicator visible={true} />}
         </View>
         {/* {isModalVisible &&
            <BallIndicator visible={isModalVisible} />
         } */}

      </View>
   )
}

const styles = StyleSheet.create({
   mainViewStyle: {
      flex: 1,
      backgroundColor: 'white',
      overflow: 'hidden'
   },
   headerLogoStyle: {
      marginLeft: 24,
      marginTop: 24
   },
   optionsTextStyle: {
      marginHorizontal: scale(20),
      width: deviceWidth - 45,
      marginTop: 8,
      justifyContent: 'space-between',
      flexDirection: 'row',
      alignItems: 'center'
   },
   seperatoreStyle: {
      marginHorizontal: scale(30),
      borderColor: commonColors.Blue,
      opacity: 0.3,
      borderBottomWidth: StyleSheet.hairlineWidth,
      marginVertical: scale(10),
   },
   categoryLabelTextStyle: {
      fontFamily: Fonts.montserratMedium,
      fontSize: scale(16),
      textAlign: "center",
      paddingBottom: scale(7),
      paddingHorizontal: scale(25),
      color: commonColors.Blue
   },
   subCategoryItemStyle: {
      marginLeft: 65,
      height: 22,
      justifyContent: 'space-between',
      flexDirection: 'row',
      alignItems: 'center',
      marginRight: 35
   },
   subCategoryItemStyle2: {
      marginLeft: 42,
      height: 22,
      justifyContent: 'space-between',
      flexDirection: 'row',
      alignItems: 'center',
      marginRight: 35
   },
   subCategoryTextStyle: {
      fontFamily: Fonts.montserratMedium,
      fontSize: scale(12),
      color: commonColors.Blue,
   },
   subCategorysubTextStyle: {
      fontFamily: Fonts.montserratRegular,
      fontSize: scale(12),
      color: 'rgb(46, 64, 92)'

   }
})