import { React, importImages, TouchableOpacity, TextInput, useState, useEffect, showSimpleAlert, moment, Request, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale } from '../../utils/importLibrary'
import HTMLView from 'react-native-htmlview';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';

export default function NotificationViewController({ route, navigation }) {
    const [arrMainCategories, setArrMainCategories] = useState([])
    const [isModalVisible, setModalVisible] = useState(false);
    const [nostorefound, setnostorefound] = useState('')
    const btnBackClick = () => {
        navigation.goBack()
    }

    useEffect(() => {
        getNotification();
    }, [])
    const getNotification = async () => {
        setModalVisible(true)
        if (arrMainCategories.length > 0) {
            setTimeout(() => {
                setModalVisible(false)
            }, 500);
        }
        let response = await Request.post('notification-list.php')
        setModalVisible(false)
        if (response.success == true) {
            if (response.data.length > 0) {
                setArrMainCategories(response.data)
            }
            else {
                setnostorefound('No Notification found')
            }
        }
        else {
            if (response) {
                showSimpleAlert(response.message)
            }
        }
    }

    const renderNewArrivalItem = ({ item, index }) => {

        return (
            <View style={styles.transcationView}>
                <Text style={styles.dateStyle}>{moment(item.created_at, 'YYYY-MM-DD hh:mm:ss').format('ddd DD MMM hh:mm a').toString()}</Text>
                <View style={styles.databoxTextStyle}>
                    <Text style={styles.dataText}>{item.title}</Text>
                    <HTMLView
                        stylesheet={styles}
                        value={item.content}
                    />
                    {item.image != '' && item.image != null ?
                        <Image source={{ uri: item.image }} style={styles.iconStyle2} resizeMode='stretch' />
                        : null}
                    {item.visit != '' ? <Text style={styles.visitTextStyle}>{item.visit}</Text> : null}
                    {item.order_id ? <TouchableOpacity onPress={() => navigation.navigate('OrderDetailsViewController', { orderItem: item })}>
                        <Text style={styles.linkText}>{item.link}</Text>
                    </TouchableOpacity> :
                        <TouchableOpacity onPress={() => navigation.navigate('AboutScreen', { value: item.link })}>
                            <Text style={styles.linkText}>{item.link}</Text>
                        </TouchableOpacity>}
                </View>
            </View>
        )
    }

    return (
        <View style={styles.mainViewStyle}>

            <View style={styles.headerLogoStyle}>
                <Text style={{ width: '100%', textAlign: 'center', position: 'absolute', fontFamily: Fonts.montserratSemiBold, fontSize: 20, color: commonColors.Blue }}>{'Notifications'}</Text>
                <TouchableOpacity onPress={() => btnBackClick()} style={{ marginLeft: 16 }}>
                    <Image source={importImages.backButtonArrow} style={{}} />
                </TouchableOpacity>
            </View>
            <View style={{ width: 300, height: 1, backgroundColor: 'lightgrey', alignSelf: 'center', }}></View>
            {arrMainCategories.length > 0 ?
                < FlatList
                    bounces={false}
                    renderItem={renderNewArrivalItem}
                    data={arrMainCategories}
                    style={{ marginTop: 8, flex: 1, }}
                    contentContainerStyle={{ alignItems: 'center', }}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={(item, index) => `${index}-id`}
                /> : <View style={{ flex: 1, height: '100%', justifyContent: 'center', alignContent: 'center', alignSelf: 'center' }}>
                    <Text style={styles.transactionText}>{nostorefound}</Text>
                </View>}
          
            {/* {isModalVisible ?
                <View style={{ flexDirection: "column", }}>
                    <SkeletonContent
                        isLoading={true}
                        containerStyle={{ flexDirection: "column", marginHorizontal: scale(15), }}
                        layout={[
                            { key: 'someotherId4', width: (deviceWidth - 50), marginVertical: scale(5), height: scale(120), borderRadius: scale(10) },
                            { key: 'otherId5', width: (deviceWidth - 50), height: scale(120), marginVertical: scale(5), borderRadius: scale(10) },
                            { key: 'Id8', width: (deviceWidth - 50), height: scale(120), marginVertical: scale(5), borderRadius: scale(10) },
                            { key: 'id9', width: (deviceWidth - 50), height: scale(120), marginVertical: scale(5), borderRadius: scale(10) },
                            { key: 'id10', width: (deviceWidth - 50), height: scale(120), marginVertical: scale(5), borderRadius: scale(10) },
                            { key: 'id11', width: (deviceWidth - 50), height: scale(120), marginVertical: scale(5), borderRadius: scale(10) },

                        ]}
                        animationDirection="horizontalLeft"
                        animationType='shiver'
                    />

                </View> : null
            } */}
            {isModalVisible?
         <BallIndicator visible={isModalVisible} /> :null}
        </View>
    )
}
const styles = StyleSheet.create({
    mainViewStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    headerLogoStyle: {
        alignItems: 'center',
        height: scale(44),
        flexDirection: 'row'
    },
    textStyle: {
        marginTop: scale(4),
        fontFamily: Fonts.montserratMedium,
        fontSize: scale(13),
        color: commonColors.Blue
    },
    renderView: {
        width: (deviceWidth - 16),
        marginHorizontal: scale(8),
        marginTop: scale(4),
        backgroundColor: commonColors.White,
        borderRadius: 10,
        shadowColor: commonColors.Blue,
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 5,
        height: scale(150),
        marginVertical: scale(10),
        justifyContent: 'center'
    },
    transactionText: {
        color: commonColors.Blue,
        fontSize: scale(20),
        fontFamily: Fonts.montserratMedium
    },

    transcationView: {
        marginBottom: scale(20),
        alignSelf: 'center'
    },

    dateStyle: {
        marginLeft: scale(12),
        fontSize: scale(10),
        color: commonColors.Blue,
        fontFamily: Fonts.montserratRegular,
        textAlign: 'justify'
    },

    databoxTextStyle: {
        marginTop: scale(6),
        width: deviceWidth - 40,
        borderRadius: 12,
        shadowColor: 'gray',
        shadowOpacity: 1,
        shadowRadius: 6,
        backgroundColor: commonColors.White,
        elevation: 8,
        alignSelf: 'center'
    },
    dataText: {
        fontSize: scale(14),
        marginHorizontal: scale(10),
        marginTop: scale(10),
        fontFamily: Fonts.montserratSemiBold,
        color: commonColors.Blue,
    },

    discountText: {
        fontSize: scale(12),
        marginHorizontal: scale(10),
        fontFamily: Fonts.montserratRegular,
        textAlign: 'justify',
        color: commonColors.Black,
    },

    linkText: {
        marginHorizontal: scale(10),
        marginBottom: scale(10),
        fontSize: scale(12),
        color: "#0086FF",
        fontFamily: Fonts.montserratRegular
    },

    visitTextStyle: {
        marginHorizontal: scale(10),
        color: commonColors.Blue,
        fontSize: scale(10),
        marginTop: scale(10),
        fontFamily: Fonts.montserratSemiBold
    },
    dataText: {
        fontSize: scale(14),
        marginHorizontal: scale(10),
        marginTop: scale(10),
        fontFamily: Fonts.montserratSemiBold,
        color: commonColors.Blue,
    },

    iconStyle2: {
        height: 100,
        width: deviceWidth - 120,
        borderRadius: 5,
        marginHorizontal: 10,
        marginTop: 10,
    },


    visitTextStyle: {
        marginHorizontal: 10,
        color: commonColors.Blue,
        fontSize: 10,
        marginTop: 10,
        fontFamily: Fonts.montserratSemiBold
    },
    p:{
       
        fontSize: 12,
        marginHorizontal: 10,
        fontFamily: Fonts.montserratRegular,
        color: commonColors.Blue,
        textAlign: 'justify'
    }
})