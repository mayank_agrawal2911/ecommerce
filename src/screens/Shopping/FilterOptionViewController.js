import { React, ProductComponent, SectionList, useCallback, importImages, TouchableOpacity, TextInput, useState, useEffect, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale, showSimpleAlert, NavigationService } from '../../utils/importLibrary'
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { max } from 'moment';
import CustomButton from '../../components/CustomButton';

export default function FilterOptionViewController({ route, navigation }) {
   const [arrFilterOption, setArrFilterOption] = useState([
      { title: "BEST SELLER", subArray: [{ subtitle: "BEST SELLER", isSelected: 0 }] },
      { title: "NEW ARRIVAL", subArray: [{ subtitle: "NEW ARRIVAL", isSelected: 0 }] },
      { title: "FRAGRANCE TYPE", subArray: [{ subtitle: "EAU DE PARFUM", isSelected: 0 }, { subtitle: "EAU DE TOILETTE", isSelected: 0 }, { subtitle: "BATH LINE", isSelected: 0 }, { subtitle: "COFFRET", isSelected: 0 }, { subtitle: "DEODORANT", isSelected: 0 }, { subtitle: "BODY CARE", isSelected: 0 }, { subtitle: "HAIR MIST", isSelected: 0 }] },
      { title: "SIZE", subArray: [{ subtitle: "SET", isSelected: 0 }, { subtitle: "21", isSelected: 0 }, { subtitle: "50ML", isSelected: 0 }, { subtitle: "60ML", isSelected: 0 }, { subtitle: "75ML", isSelected: 0 }, { subtitle: "90ML", isSelected: 0 }, { subtitle: "100ML", isSelected: 0 }, { subtitle: "125ML", isSelected: 0 }, { subtitle: "150ML", isSelected: 0 }, { subtitle: "200ML", isSelected: 0 }] },
      { title: "BRAND", subArray: [{ subtitle: "ALAIA PARIS", isSelected: 0 }, { subtitle: "BLUMARINE", isSelected: 0 }, { subtitle: "CHLOE", isSelected: 0 }, { subtitle: "CLARINS", isSelected: 0 }, { subtitle: "CULTI MILANO", isSelected: 0 }, { subtitle: "DAVID BECKHAM", isSelected: 0 }, { subtitle: "ELIE SAAB", isSelected: 0 }] },
      { title: "PRICE", subArray: [{}] }])
   const [isExpandCollaps, setIsExpandCollaps] = useState()
   const [selectgiftwrap, setselectgiftwrap] = useState(false)
   const [sliderValue, setSliderValue] = useState('')
   const [filterData, setfilterData] = useState([])
   const [brandData, setbrandData] = useState([])
   const [brandModal, setbrandModal] = useState(false)
   const [filterfragtype, setfilterfragtype] = useState([])
   const [filterfragtypeModal, setfilterfragtypeModal] = useState(false)
   const [priceModal, setpriceModal] = useState(false)
   const [minprice, setminprice] = useState('')
   const [maxprice, setmaxprice] = useState('')
   const [sizeData, setsizeData] = useState([])
   const [sizeDataModal, setsizeDataModal] = useState(false)
   const [filtersegment, setfiltersegment] = useState([])
   const [filtersegmentmodal, setfiltersegmentmodal] = useState(false)
   const [newarrivalsegmentmodal, setnewarrivalsegmentmodal] = useState(false)
   const [bestsellersegmentmodal, setbestsellersegmentmodal] = useState(false)
   const [concernmodal, setconcernmodal] = useState(false)
   const [concernData, setconcernData] = useState([])
   const [collectionData, setcollectionData] = useState([])
   const [collectionDataModal, setcollectionDataModal] = useState(false)
   const [productfilter, setproductfilter] = useState([])
   const [arrData, setarrData] = useState([])
   const [filtertype, setfiltertype] = useState('')
   const [maincatId, setmaincatId] = useState('')
   const [minsliderprice, setminsliderprice] = useState('')
   const [maxsliderprice, setmaxsliderprice] = useState('')
   const [multiSliderValue, setMultiSliderValue] = React.useState([3, 7]);

   useEffect(() => {
      const unsubscribe = navigation.addListener('focus', () => {
         getProductFilterListing()
      });
      return unsubscribe;
   }, [navigation]);

   const getProductFilterListing = () => {
      if (route.params.isfilter == 1) {
         setfilterData(route.params.filterData)
         setbrandData(route.params.brandDataArray)
         setfiltersegment(route.params.filtersegmentData)
         setsizeData(route.params.sizeDataArray)
         setfilterfragtype(route.params.filterfragtypeArray)
         setcollectionData(route.params.collectionDataArray)
         setconcernData(route.params.concernDataArray)
         setfiltertype(route.params.filtertype)
         setmaincatId(route.params.maincatId)
         setminprice(route.params.filterData.min_price)
         setmaxprice(route.params.filterData.max_price)
         setminsliderprice(route.params.minprice)
         setmaxsliderprice(route.params.maxprice)
         setSliderValue(route.params.minprice + "JOD - " + route.params.maxprice + "JOD")
      }
      else {
         setfilterData(route.params.filterData)
         setbrandData(route.params.filterData?.filter_brand)
         setfiltersegment(route.params.filterData?.filter_segment)
         setsizeData(route.params.filterData?.filter_size)
         setfilterfragtype(route.params.filterData?.filter_fragtype)
         setcollectionData(route.params.filterData?.row_collections)
         setminprice(route.params.filterData?.min_price)
         setminsliderprice(route.params.filterData?.min_price)
         setmaxsliderprice(route.params.filterData?.max_price)
         setmaxprice(route.params.filterData?.max_price)
         setconcernData(route.params.filterData?.row_concerns)
         setfiltertype(route.params.filtertype)
         setmaincatId(route.params.maincatId)
      }
   }



   const btnBackClick = async () => {
      NavigationService.goBack()

   }
   const expandCollapsClick = (item, index) => {
      if (isExpandCollaps == index) {
         setIsExpandCollaps()
      }
      else {
         setIsExpandCollaps(index)
      }
   }
   const checkMarkedClick = (item, index,) => {
      console.log("checkmarked", item, index);
      var tempArry = filtersegment
      var tempItem = tempArry[index].item
      tempArry[index].item.isSelected = !item.isSelected
      setfiltersegment([...tempArry])
   }
   const collectioncheckMarkedClick = (item, index,) => {
      var tempArry = [...collectionData]
      tempArry[index].isSelected = !item.isSelected
      setcollectionData(tempArry)
   }
   const sizecheckMarkedClick = (item, index,) => {
      var tempArry = [...sizeData]
      tempArry[index].isSelected = !item.isSelected
      setsizeData(tempArry)
   }
   const concerncheckMarkedClick = (item, index) => {
      var tempArry = [...concernData]
      tempArry[index].isSelected = !item.isSelected
      setconcernData(tempArry)
   }
   const filtertypecheckMarkedClick = (item, index,) => {
      var tempArry = [...filterfragtype]
      tempArry[index].isSelected = !item.isSelected
      setfilterfragtype(tempArry)
   }
   const brandcheckMarkedClick = (item, index,) => {
      let brandata = []
      var tempArry = [...brandData]
      tempArry[index].isSelected = !item.isSelected
      setbrandData(tempArry)

   }

   const renderSUBCategories = (item, index, mainIndex) => {
      return (
         <View>
            <TouchableOpacity style={styles.subCategoryItemStyle}>
               <TouchableOpacity onPress={() => { checkMarkedClick(item, index, mainIndex) }} style={{ marginRight: 8, }}>
                  <Image source={item.isSelected ? importImages.BlueTick : importImages.unCheckImage} />
               </TouchableOpacity>
               <Text style={styles.subCategoryTextStyle}>{item.subtitle}</Text>
            </TouchableOpacity>
            <View style={styles.seperatoreStyle}></View>
         </View>
      )
   }
   const sliderValueChanged = (values) => {
      console.log("valuess:--", values);
      setminsliderprice(values[0])
      setmaxsliderprice(values[1])
      setSliderValue(values[0] + "JOD - " + values[1] + "JOD")

   }
   const sliderValueChangedFinish = (values) => {
      setSliderValue(values[0] + "JOD - " + values[1] + "JOD")
      setminsliderprice(values[0])
      setmaxsliderprice(values[1])
   }

   const renderbrandDataCategories = ({ item, index }) => {
      return (
         <View>
            <TouchableOpacity onPress={() => { brandcheckMarkedClick(item, index) }} style={styles.subCategoryItemStyle}>
               <Image source={item.isSelected ? importImages.BlueTick : importImages.unCheckImage} style={{ marginRight: 8, }} />
               <Text style={styles.subCategoryTextStyle}>{item.brand_name}</Text>
            </TouchableOpacity>
         </View>
      )
   }
   const brandClick = () => {
      setbrandModal(!brandModal)
   }
   const newArrivalsegmentClick = () => {
      setnewarrivalsegmentmodal(!newarrivalsegmentmodal)
   }
   const bestsellersegmentClick = (item, index) => {
      console.log("item", item, index);
      var tempArry = filtersegment
      var tempItem = tempArry[index]
      console.log("tempItem", tempItem);
      if (tempItem.open_slug == 0) {
         tempItem.open_slug = 1
         console.log("tempArry", tempArry);
         setfiltersegment([...tempArry])

      } else {
         tempItem.open_slug = 0
         setfiltersegment([...tempArry])



      }


   }
   const concernClick = () => {
      setconcernmodal(!concernmodal)
   }
   const fragrancetypeClick = () => {
      setfilterfragtypeModal(!filterfragtypeModal)
   }
   const sizeClick = () => {
      setsizeDataModal(!sizeDataModal)
   }
   const priceClick = () => {
      setpriceModal(!priceModal)
   }
   const collectionClick = () => {
      setcollectionDataModal(!collectionDataModal)
   }


   const newArrivalrenderItem = ({ item, index }) => {
      const data = item.item
      return (
         <View>
            <TouchableOpacity onPress={() => bestsellersegmentClick(item, index)} style={styles.optionsTextStyle}>
               <Text style={styles.categoryLabelTextStyle}>{item.title}</Text>
               <Image source={importImages.downArrow} style={{ marginRight: 36 }} />
            </TouchableOpacity>
            {
               item.open_slug == 1 ?
                  <View>
                     <TouchableOpacity onPress={() => { checkMarkedClick(data, index) }} style={styles.subCategoryItemStyle}>
                        <View style={{ marginRight: 8, }}>
                           <Image source={data.isSelected ? importImages.BlueTick : importImages.unCheckImage} />
                        </View>
                        <Text style={styles.subCategoryTextStyle}>{data.in_segment_title}</Text>
                     </TouchableOpacity>
                  </View> : null}
            <View style={styles.seperatoreStyle}></View>

         </View>
      )
   }
   const renderslug = ({ item, index }) => {
      console.log("item::-->", item);
      return (
         <View>

            <TouchableOpacity onPress={() => { checkMarkedClick(item, index) }} style={styles.subCategoryItemStyle}>
               <View style={{ marginRight: 8, }}>
                  <Image source={item.isSelected ? importImages.BlueTick : importImages.unCheckImage} />
               </View>
               <Text style={styles.subCategoryTextStyle}>{item.in_segment_title}</Text>
            </TouchableOpacity>
         </View>
      )
   }
   const rendercollectionDataCategories = ({ item, index }) => {
      return (
         <View>
            <TouchableOpacity onPress={() => { collectioncheckMarkedClick(item, index) }} style={styles.subCategoryItemStyle}>
               <View style={{ marginRight: 8, }}>
                  <Image source={item.isSelected ? importImages.BlueTick : importImages.unCheckImage} />
               </View>
               <Text style={styles.subCategoryTextStyle}>{item.name}</Text>
            </TouchableOpacity>
         </View>
      )
   }
   const bestsellerCategory = ({ item, index }) => {
      return (
         <View>
            {item.in_segment_slug == "best-seller" ?
               <TouchableOpacity onPress={() => { checkMarkedClick(item, index) }} style={styles.subCategoryItemStyle}>
                  <View style={{ marginRight: 8, }}>
                     <Image source={item.isSelected ? importImages.BlueTick : importImages.unCheckImage} />
                  </View>
                  <Text style={styles.subCategoryTextStyle}>{item.in_segment_title}</Text>
               </TouchableOpacity> : null}
         </View>
      )
   }

   const rendersizeData = ({ item, index }) => {
      return (
         <View>
            <TouchableOpacity onPress={() => { sizecheckMarkedClick(item, index) }} style={styles.subCategoryItemStyle}>
               <View style={{ marginRight: 8, }}>
                  <Image source={item.isSelected ? importImages.BlueTick : importImages.unCheckImage} />
               </View>
               <Text style={styles.subCategoryTextStyle}>{item.key}</Text>
            </TouchableOpacity>
         </View>
      )
   }
   const renderConcernData = ({ item, index }) => {
      return (
         <View>
            <TouchableOpacity onPress={() => { concerncheckMarkedClick(item, index) }} style={styles.subCategoryItemStyle}>
               <View style={{ marginRight: 8, }}>
                  <Image source={item.isSelected ? importImages.BlueTick : importImages.unCheckImage} />
               </View>
               <Text style={styles.subCategoryTextStyle}>{item.title}</Text>
            </TouchableOpacity>
         </View>
      )
   }
   const filterfragtyperenderItem = ({ item, index }) => {
      return (
         <View>
            <TouchableOpacity onPress={() => { filtertypecheckMarkedClick(item, index) }} style={styles.subCategoryItemStyle}>
               <View style={{ marginRight: 8, }}>
                  <Image source={item.isSelected ? importImages.BlueTick : importImages.unCheckImage} />
               </View>
               <Text style={styles.subCategoryTextStyle}>{item.key}</Text>
            </TouchableOpacity>
         </View>
      )
   }
   const itemSeparatorComponent = ({ item }) => {
      return (
         <View style={styles.seperatoreStyle}></View>
      );
   }
   const clearFilter = () => {
      let productItem = route.params.productItem
      navigation.push('ProductListingViewController', {
         isfilter: 0,
         brandData: [],
         filtersegment: [],
         sizeData: [],
         fragrancetype: [],
         collectionData: [],
         concerndata: [],
         pricerange: [],
         minprice: '',
         maxprice: '',
         productItem: productItem,
         sortvalue: route.params.sortingvalue,
         category: route.params.category,
         arrwhomData: route.params.arrwhomdata,
         maincatId: route.params.maincatId,
         filtertype: route.params.filtertype,
         keytype: route.params.keytype,
         keyword: route.params.Keyword,
         categoryID: route.params.categoryID,
         segment_name: route.params.segment_name,
         cat_Name: route.params.cat_Name,
         main_CatName: route.params.main_CatName,
         sub_catName: route.params.sub_catName,

      })
   }
   const saveFilter = () => {
      let selectedData = []
      let filterselectData = []
      const filterSegment = filtersegment ? filtersegment.map((item, index) => {
         var data = item.item
         console.log("route.params.data", data);
         if (data.isSelected == true) {
            selectedData.push(item)
            filterselectData.push(data)
         }
      }) : null
      let brandArray = brandData ? brandData.filter((item) => item.isSelected == true) : []
      let sizedata = sizeData ? sizeData.filter((item) => item.isSelected == true) : []
      let FragranceType = filterfragtype ? filterfragtype.filter((item) => item.isSelected == true) : []
      let CollectionData = collectionData ? collectionData.filter((item) => item.isSelected == true) : []
      let ConcernData = concernData ? concernData.filter((item) => item.isSelected == true) : []
      let productItem = route.params.productItem
      console.log("route.params.qrDataRefresh(e.data);route.params.qrDataRefresh(e.data);", route.params);
      navigation.push('ProductListingViewController', {
         isfilter: 1,
         brandData: brandArray,
         filtersegment: filterselectData,
         sizeData: sizedata,
         fragrancetype: FragranceType,
         collectionData: CollectionData,
         concerndata: ConcernData,
         pricerange: sliderValue,
         minprice: minsliderprice,
         maxprice: maxsliderprice,
         productItem: productItem,
         sortvalue: route.params.sortingvalue,
         category: route.params.category,
         arrwhomData: route.params.arrwhomdata,
         maincatId: route.params.maincatId,
         filtertype: route.params.filtertype,
         keytype: route.params.keytype,
         keyword: route.params.Keyword,
         categoryID: route.params.categoryID,
         segment_name: route.params.segment_name,
         cat_Name: route.params.cat_Name,
         main_CatName: route.params.main_CatName,
         sub_catName: route.params.sub_catName,
         subcatId: route.params.subcatId,
         catId: route.params.category_id,
         brandDataArray: brandData,
         filtersegmentData: filtersegment,
         sizeDataArray: sizeData,
         filterfragtypeArray: filterfragtype,
         collectionDataArray: collectionData,
         concernDataArray: concernData,
      })



   }
   return (
      <View style={styles.mainViewStyle}>
         <View style={styles.headerLogoStyle}>
            <Text style={{ fontFamily: Fonts.montserratSemiBold, fontSize: 20, color: commonColors.Blue }}>Filter</Text>
         </View>
         <View style={{ width: 300, height: 1, backgroundColor: 'lightgrey', alignSelf: 'center', }}></View>
         <TouchableOpacity onPress={() => btnBackClick()} style={{ marginLeft: 16, marginTop: 10, position: 'absolute' }}>
            <Image source={importImages.downArrowButton} style={{}} />
         </TouchableOpacity>

         <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
            {/* NEW ARRIVALS */}
            {filtersegment && filtersegment.length > 0 ?
               <View>
                  <FlatList
                     bounces={false}
                     renderItem={newArrivalrenderItem}
                     data={filtersegment}
                     showsVerticalScrollIndicator={false}
                     extraData={filtersegment}
                     showsHorizontalScrollIndicator={false}
                     keyExtractor={(item, index) => `${index}-id`}
                  />
               </View>
               : null
            }


            {/*SIZE */}
            {
               sizeData && sizeData.length > 0 ?
                  <View>
                     <TouchableOpacity onPress={() => sizeClick()} style={styles.optionsTextStyle}>
                        <Text style={styles.categoryLabelTextStyle}>{'SIZE'}</Text>
                        <Image source={importImages.downArrow} style={{ marginRight: 36 }} />
                     </TouchableOpacity>
                     {
                        sizeDataModal == true ?
                           <FlatList
                              bounces={false}
                              renderItem={rendersizeData}
                              data={sizeData}
                              ItemSeparatorComponent={itemSeparatorComponent}
                              showsVerticalScrollIndicator={false}
                              showsHorizontalScrollIndicator={false}
                              keyExtractor={(item, index) => `${index}-id`}
                           /> : null}
                     <View style={styles.seperatoreStyle}></View>

                  </View> : null
            }

            {/*FRAGRANCE TYPE*/}
            {
               filterfragtype && filterfragtype.length > 0 ?
                  <View>
                     <TouchableOpacity onPress={() => fragrancetypeClick()} style={styles.optionsTextStyle}>
                        <Text style={styles.categoryLabelTextStyle}>{'FRAGRANCE TYPE'}</Text>
                        <Image source={importImages.downArrow} style={{ marginRight: 36 }} />
                     </TouchableOpacity>
                     {
                        filterfragtypeModal == true ?
                           <FlatList
                              bounces={false}
                              renderItem={filterfragtyperenderItem}
                              data={filterfragtype}
                              ItemSeparatorComponent={itemSeparatorComponent}

                              showsVerticalScrollIndicator={false}
                              showsHorizontalScrollIndicator={false}
                              keyExtractor={(item, index) => `${index}-id`}
                           /> : null}
                     <View style={styles.seperatoreStyle}></View>

                  </View> : null
            }

            {/* COLLECTIONS */}
            {
               collectionData && collectionData.length > 0 ?
                  <View>
                     <TouchableOpacity onPress={() => collectionClick()} style={styles.optionsTextStyle}>
                        <Text style={styles.categoryLabelTextStyle}>{'COLLECTIONS'}</Text>
                        <Image source={importImages.downArrow} style={{ marginRight: 36 }} />
                     </TouchableOpacity>
                     {
                        collectionDataModal == true ?
                           <FlatList
                              bounces={false}
                              renderItem={rendercollectionDataCategories}
                              data={collectionData}
                              ItemSeparatorComponent={itemSeparatorComponent}
                              showsVerticalScrollIndicator={false}
                              showsHorizontalScrollIndicator={false}
                              keyExtractor={(item, index) => `${index}-id`}
                           /> : null}
                     <View style={styles.seperatoreStyle}></View>

                  </View> : null
            }

            {/* CONCERNS */}
            {
               concernData && concernData.length > 0 ?
                  <View>
                     <TouchableOpacity onPress={() => concernClick()} style={styles.optionsTextStyle}>
                        <Text style={styles.categoryLabelTextStyle}>{'CONCERN'}</Text>
                        <Image source={importImages.downArrow} style={{ marginRight: 36 }} />
                     </TouchableOpacity>
                     {
                        concernmodal == true ?
                           <FlatList
                              bounces={false}
                              renderItem={renderConcernData}
                              ItemSeparatorComponent={itemSeparatorComponent}
                              data={concernData}
                              showsVerticalScrollIndicator={false}
                              showsHorizontalScrollIndicator={false}
                              keyExtractor={(item, index) => `${index}-id`}
                           /> : null}
                     <View style={styles.seperatoreStyle}></View>

                  </View> : null
            }

            {/* BRANDS */}

            {
               brandData && brandData.length > 0 ?
                  <View>

                     <TouchableOpacity onPress={() => brandClick()} style={styles.optionsTextStyle}>
                        <Text style={styles.categoryLabelTextStyle}>{'BRANDS'}</Text>
                        <Image source={importImages.downArrow} style={{ marginRight: 36 }} />
                     </TouchableOpacity>
                     {
                        brandModal == true ?

                           <FlatList
                              bounces={false}
                              renderItem={renderbrandDataCategories}
                              data={brandData}
                              ItemSeparatorComponent={itemSeparatorComponent}
                              showsVerticalScrollIndicator={false}
                              showsHorizontalScrollIndicator={false}
                              keyExtractor={(item, index) => `${index}-id`}
                           /> : null}
                     <View style={styles.seperatoreStyle}></View>

                  </View> : null
            }

            {/* PRIZE */}

            {maxprice > 0 ?
               <View>
                  <TouchableOpacity onPress={() => priceClick()} style={styles.optionsTextStyle}>
                     <Text style={styles.categoryLabelTextStyle}>{'PRICE'}</Text>
                     <Image source={importImages.downArrow} style={{ marginRight: 36 }} />
                  </TouchableOpacity>
                  {
                     priceModal == true ?
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                           <Text style={[styles.sliderTextStyle, { marginTop: 12 }]}>{sliderValue == '' ? (minprice + "JOD - " + maxprice + "JOD  ") : sliderValue}</Text>
                           <MultiSlider
                              enabledOne={parseInt(minprice) == parseInt(maxprice) ? false : true}
                              enabledTwo={true}
                              min={parseInt(minprice) == parseInt(maxprice) ? '' : parseInt(minprice)}
                              max={parseInt(maxprice)}
                              step={1}
                              sliderLength={deviceWidth - 60}
                              containerStyle={{ height: 30, width: deviceWidth - 60 }}
                              values={[parseInt(minsliderprice), parseInt(maxsliderprice)]}
                              onValuesChange={(values) => sliderValueChanged(values)}
                              customMarker={(e) => {
                                 return (
                                    <View style={{ backgroundColor: commonColors.Blue, width: 24, height: 24, borderRadius: 12 }} />
                                 )
                              }}
                              trackStyle={{ backgroundColor: commonColors.Grey }}
                              selectedStyle={{ backgroundColor: commonColors.Blue }}
                           />
                        </View> : null}
               </View> : null}
            <View style={{ flexDirection: "row", marginHorizontal: scale(20), marginBottom: scale(60) }}>
               <CustomButton
                  style={{ width: scale(135), }}
                  onPress={() => saveFilter()}
                  title={'Apply Filter'}
               />
               <CustomButton
                  style={{ width: scale(135) }}
                  onPress={() => clearFilter()}
                  title={'Clear Filter'}
               />
            </View>
         </ScrollView >
      </View >
   )

}
const styles = StyleSheet.create({
   mainViewStyle: {
      flex: 1,
      backgroundColor: 'white'
   },
   headerLogoStyle: {
      justifyContent: 'center',
      alignItems: 'center',
      height: 44
   },
   transactionText: {
      color: commonColors.Blue,
      fontSize: 20,
      fontFamily: Fonts.montserratMedium,
   },
   optionsTextStyle: {
      marginLeft: 36,
      marginTop: 8,
      height: 36,
      justifyContent: 'space-between',
      flexDirection: 'row',
      alignItems: 'center'
   },
   seperatoreStyle: {
      marginHorizontal: scale(30),
      borderColor: commonColors.Blue,
      borderBottomWidth: StyleSheet.hairlineWidth,
      marginVertical: scale(5),
   },
   categoryLabelTextStyle: {
      fontFamily: Fonts.montserratMedium,
      fontSize: 16,
      color: commonColors.Blue
   },
   sliderTextStyle: {
      fontFamily: Fonts.montserratMedium,
      fontSize: 12,
      color: commonColors.Blue
   },
   subCategoryItemStyle: {
      marginLeft: 36,
      marginVertical: scale(10),
      flexDirection: 'row',
      alignItems: 'center',
      marginRight: 24
   },
   subCategoryTextStyle: {
      fontFamily: Fonts.montserratRegular,
      fontSize: 12,
      color: commonColors.Blue,
   }
})