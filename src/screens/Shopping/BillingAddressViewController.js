import CustomButton from '../../components/CustomButton';
import CustomHeader from '../../components/CustomHeader';
import { Dropdown } from 'react-native-element-dropdown';

import { React, Text, FlatList, useEffect, Modal, PhoneInput, isValidEmail, showSimpleAlert, useRef, TextInput, useState, ScrollView, View, StyleSheet, TouchableOpacity, Image, Fonts, commonColors, deviceWidth, useTheme, importImages, scale, deviceHeight, KeyboardAwareScrollView, Request, Platform, } from '../../utils/importLibrary';
export default function BillingAddressViewController({ route, navigation }) {
   const [phoneNumber, setPhoneNumber] = useState('')
   const [selectcheckBox, setselectcheckBox] = useState(true)
   const [customerName, setcustomerName] = useState('')
   const [emailAddress, setemailAddress] = useState('')
   const [TempValues, setTempValues] = useState('')
   const [TempValuesCountry, setTempValuesCountry] = useState('962')
   const [TempValuesCountryCall, setTempValuesCountryCall] = useState('')
   const [country, setCountry] = useState('962');
   const [City, setCity] = useState('')
   const [CityModal, setCityModal] = useState(false)
   const [cityData, setcityData] = useState([])
   const [AreaModal, setAreaModal] = useState(false)
   const [AreaData, setAreaData] = useState([])
   const [cityID, setcityID] = useState('')
   const [AreaId, setAreaId] = useState('')
   const [Area, setArea] = useState('')
   const [streetAddress, setstreetAddress] = useState('')
   const [buildingNumber, setbuildingNumber] = useState('')
   const [isModalVisible, setModalVisible] = useState(false);
   const [shippingData, setshippingData] = useState([]);


   const phoneInput = useRef();


   useEffect(() => {
      const unsubscribe = navigation.addListener('focus', () => {
         getBillingAddress()
      });
      return unsubscribe;
   }, [navigation,]);

   const getBillingAddress = async () => {
      console.log("shippingaddressData", route.params);
      const customerData = route.params.billingAddress
      let shippingAdd = []
      var shipData = route.params.shippingaddressData.map((obj, index) => {
         console.log("obj", obj);
         if (obj.is_selected == 1) {
            shippingAdd.push(obj)
         }
      })
      console.log("shipData", shippingAdd);
      setcustomerName(customerData.customer_name)
      setemailAddress(customerData.email)
      setPhoneNumber(customerData.phone)
      setCountry(customerData.country)
      setshippingData(shippingAdd[0])
      let response = await Request.post('get-city.php')
      setcityData(response.data)
   }

   const addcountrycode = (values) => {
      phoneInput.current.setState({ number: '' })
      setCountry(values.callingCode[0])
      setPhoneNumber('')
      setTempValuesCountryCall(values.cca2)
   }

   const saveBillingAddress = async () => {

      const ConfirmValid = validationOfField();
      if (ConfirmValid) {
         let params = {
            bill_address: selectcheckBox == true ? shippingData.street : streetAddress,
            bill_city: selectcheckBox == true ? shippingData.city : City,
            bill_state: selectcheckBox == true ? shippingData.state : Area,
            bill_street_number: selectcheckBox == true ? shippingData.street_number : buildingNumber
         }
         let response = await Request.post('add-billing-address.php', params)
         setModalVisible(false)
         if (response.success == true) {
            navigation.navigate("CheckOutViewController")

            showSimpleAlert(response.message)
         }
         else {
            if (response) {
               showSimpleAlert(response.message)

            }
         }

      }
 
   }
   const itemSeparator = () => {
      return (
         <View style={{ borderBottomWidth: 0.4, borderBottomColor: commonColors.Grey }}></View>
      )
   }
   const cityDatarenderItem = (item, index) => {
      return (
         <View>
            <View style={{ marginVertical: scale(10), alignSelf: "center" }}>
               <Text style={{ color: commonColors.Black }} >{item.name}</Text>
            </View>
            <View style={{
               borderColor: commonColors.Blue,
               opacity: 0.3,
               borderBottomWidth: StyleSheet.hairlineWidth,
               marginVertical: scale(2),
            }} />
         </View>
      )
   }
   const AreaDatarenderItem = (item, index) => {
      return (
         <View>
            <View style={{ marginVertical: scale(10), alignSelf: "center" }}>
               <Text style={{ color: commonColors.Black }} >{item.name}</Text>
            </View>
            <View style={{
               borderColor: commonColors.Blue,
               opacity: 0.3,
               borderBottomWidth: StyleSheet.hairlineWidth,
               marginVertical: scale(2),
            }} />
         </View>
      )
   }
   const selectArea = async (id) => {
 

      let params = {
         domain_id: 1,
         state_id: id
      }
      let response = await Request.post('get-area.php', params)
      if (response.data.length > 0) {
         setAreaData(response.data)
      } else {
      }
   }
   const validationOfField = () => {
      if (selectcheckBox == false) {
         if (City == '') {
            showSimpleAlert('Please enter city')
            return false;
         }
         else if (Area == '') {
            showSimpleAlert('Please enter area')
            return false;
         }

         else if (streetAddress == '') {
            showSimpleAlert('Please enter street address')
            return false;
         }
         else if (buildingNumber == '') {
            showSimpleAlert('Please enter building number')
            return false;
         }

         else {
            return true;
         }
      }
      else {
         return true;
      }
   }
   return (
      <View style={styles.container}>

         <KeyboardAwareScrollView
            contentContainerStyle={{ flexGrow: 1 }}
            bounces={false}
            keyboardShouldPersistTaps={'always'}
            showsVerticalScrollIndicator={false}
            extraScrollHeight={10}>
            <View style={{ marginLeft: scale(40) }}>
               <CustomHeader
                  leftBtnOnPress={() => navigation.goBack()}
                  leftBtn={<Image source={importImages.backButtonArrow}></Image>}
                  headerTitle={'BILLING ADDRESS'}
               />
               <View style={{
                  flexDirection: "row", right: scale(10),
               }}>
                  <View style={{ width: scale(5) }}>
                     <TouchableOpacity onPress={() => {
                        setselectcheckBox(!selectcheckBox)
                     }}>
                        <Image source={selectcheckBox ? importImages.BlueTick : importImages.BlueBox} style={{ alignSelf: "center", justifyContent: "center", marginTop: scale(2) }} />
                     </TouchableOpacity>
                  </View>
                  <Text style={[styles.sameShippingText,]}>Same as shipping address</Text>
               </View>
               <View style={styles.subContainer}>

                  <View style={styles.subListViewStyle}>
                     <Text style={styles.labelText}>{'Customer Name'}</Text>
                     <TextInput
                        style={styles.popUpTextInputStyle}
                        value={customerName}
                        placeholderTextColor={commonColors.Grey}
                        autoCapitalize={'none'}
                        disabled={true}
                        keyboardType='email-address'
                     ></TextInput>
                  </View>
                  <View style={styles.subListViewStyle}>
                     <Text style={styles.labelText}>{'Phone Number'}</Text>
                     <View >
                        <View style={styles.inputNumber}>
                           <PhoneInput
                              ref={phoneInput}
                              defaultValue={phoneNumber == '' ? route.params.billingAddress.phone : phoneNumber}
                              defaultCode={TempValuesCountryCall == '' ? route.params.billingAddress.country == '' ? 'JO' : route.params.billingAddress.country : TempValuesCountryCall}
                              layout="second"
                              placeholder={TempValuesCountryCall === '962' ? '7XXXXXXXX' : 'XXXXXX'}
                              value={phoneNumber == '' ? route.params.billingAddress.phone : phoneNumber}
                              textInputProps={{
                                 keyboardType: 'phone-pad',
                                 maxLength: country === '962' ? 9 : 13,
                                 placeholderTextColor: commonColors.Blue,
                                 style: {
                                    fontSize: scale(13),
                                    fontFamily: Fonts.montserratMedium,
                                    color: commonColors.Blue,
                                    borderColor: commonColors.Blue,
                                    borderLeftWidth: 1,
                                    borderTopWidth: Platform.OS == 'ios' ? 0.4 : 0,
                                    borderRightWidth: Platform.OS == 'ios' ? 1 : 1,
                                    borderBottomWidth: Platform.OS == 'ios' ? 0.4 : 1,
                                    backgroundColor: commonColors.White,
                                    right: scale(14),
                                    height: scale(39),
                                    paddingHorizontal: scale(5),
                                    width: scale(122)
                                 },
                                 selectionColor: commonColors.Grey

                              }}
                              textContainerStyle={{
                                 fontSize: scale(14),
                                 fontFamily: Fonts.montserratMedium,
                                 color: commonColors.Blue,
                              }}
                              countryPickerButtonStyle={{ marginStart: -10, marginEnd: -10, }}
                              codeTextStyle={{
                                 fontSize: scale(14),
                                 fontFamily: Fonts.montserratMedium,
                                 color: commonColors.Blue,
                              }}
                              filterProps={{
                                 fontSize: scale(14),
                                 placeholderTextColor: commonColors.Grey,
                                 fontFamily: Fonts.montserratMedium,
                                 color: commonColors.Blue,
                              }}
                              containerStyle={styles.textInputStyle}
                              disabled={true}

                           />
                        </View>
                     </View>
                  </View>
                  <View style={styles.subListViewStyle}>
                     <Text style={styles.labelText}>{'Email Address'}</Text>
                     <TextInput
                        style={styles.popUpTextInputStyle}
                        value={emailAddress}
                        keyboardType='email-address'
                        autoCapitalize='none'
                        placeholderTextColor={commonColors.Grey}
                     ></TextInput>
                  </View>
                  {selectcheckBox == false ?
                     <View>
                        <View style={styles.subListViewStyle}>

                           <Text style={styles.labelText}>{'City'}</Text>

                           <TouchableOpacity style={[styles.sortAndFilterButtonStyle2, { width: scale(185) }]}>
                              <Dropdown
                                 style={styles.dropdown}
                                 placeholderStyle={styles.placeholderStyle}
                                 selectedTextStyle={styles.selectedTextStyle}
                                 data={cityData}
                                 search={false}
                                 maxHeight={300}
                                 labelField="label"
                                 dropdownPosition={'bottom'}
                                 activeColor={commonColors.White}
                                 valueField='value'

                                 placeholder={'Select City'}
                                 value={City == '' ? 'Select City' : City}
                                 renderItem={(item) => cityDatarenderItem(item)}
                                 itemContainerStyle={{ color: commonColors.Blue }}
                                 containerStyle={{ color: commonColors.Blue }}
                                 showsVerticalScrollIndicator={false}
                                 onChange={item => {
                                    console.log("itemm:---->", item)
                                    setTimeout(async () => {
                                       setcityID(item.id)
                                       setCity(item.value)
                                       setArea('')
                                    }, 200);
                                    setCity(item.value)
                                    selectArea(item.id)

                                 }}
                              />
                           </TouchableOpacity>
                        </View>
                        {City != '' ?
                           <View style={styles.subListViewStyle}>
                              <Text style={styles.labelText}>{'Area'}</Text>
                              <TouchableOpacity style={[styles.sortAndFilterButtonStyle2, { width: scale(185) }]}>
                                 <Dropdown
                                    style={styles.dropdown}
                                    placeholderStyle={styles.placeholderStyle}
                                    selectedTextStyle={styles.selectedTextStyle}
                                    data={AreaData}
                                    search={false}
                                    maxHeight={300}
                                    labelField="label"
                                    dropdownPosition={'bottom'}
                                    activeColor={commonColors.White}
                                    valueField='value'
                                    placeholder={'Select Area'}
                                    value={Area == '' ? 'Select Area' : Area}
                                    renderItem={(item) => AreaDatarenderItem(item)}
                                    itemContainerStyle={{ color: commonColors.Blue }}
                                    containerStyle={{ color: commonColors.Blue }}
                                    showsVerticalScrollIndicator={false}
                                    onChange={item => {
                                       setAreaId(item.id)
                                       setArea(item.value)
                                    }}
                                 />
                              </TouchableOpacity>
                          

                           </View> : null}
                        <View style={styles.subListViewStyle}>
                           <Text style={styles.labelText}>{'Street Address'}</Text>
                           <TextInput
                              style={styles.popUpTextInputStyle}
                              value={streetAddress}
                              placeholderTextColor={commonColors.Grey}
                              keyboardType='email-address'
                              autoCapitalize={'none'}
                              onChangeText={(value) => { setstreetAddress(value) }}
                           ></TextInput>
                        </View>
                        <View style={styles.subListViewStyle}>
                           <Text style={styles.labelText}>{'Building Number'}</Text>
                           <TextInput
                              style={styles.popUpTextInputStyle}
                              value={buildingNumber}
                              placeholderTextColor={commonColors.Grey}
                              onChangeText={(value) => { setbuildingNumber(value) }}
                              keyboardType='number-pad'
                           ></TextInput>
                        </View>
                     </View> : null

                  }
               </View>

            </View>
         </KeyboardAwareScrollView >
         <CustomButton
            onPress={() => saveBillingAddress()}
            title={'SAVE'}
         />
       
         {
            isModalVisible &&
            <BallIndicator visible={isModalVisible} />
         }
      </View >
   );
}

const styles = StyleSheet.create({
   container: {
      backgroundColor: commonColors.White,
      flex: 1
   },
   headerLogoStyle: {
      flexDirection: 'row',
      justifyContent: "flex-start",
      marginVertical: scale(20)
   },
   headerText: {
      color: commonColors.Blue,
      fontSize: scale(18),
      alignSelf: "center",
      paddingLeft: scale(10),
      fontFamily: Fonts.montserratSemiBold,
   },
   borderline: {
      height: 2,
      backgroundColor: commonColors.LightGrey,
   },
   sameShippingText: {
      fontFamily: Fonts.montserratMedium,
      textAlign: "center",
      fontSize: scale(12),
      paddingHorizontal: scale(10),
      color: commonColors.Blue
   },
   ButtonStyle: {
      height: scale(50),
      borderWidth: 1,
      alignSelf: 'center',
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 5,
      borderColor: commonColors.Blue,
      width: deviceWidth - 30,
      marginTop: scale(250),
      backgroundColor: commonColors.Blue,
   },
   saveText: {
      fontSize: scale(20),
      fontFamily: Fonts.montserratMedium,
      color: commonColors.White,
   },
   subContainer: {
      marginVertical: scale(20),
      right: scale(10)
   },
   subListViewStyle: {
      flexDirection: "row",
      justifyContent: "space-between",
      right: scale(10),
      alignItems: "center",
      marginVertical: scale(10),
   },
   labelText: {
      fontSize: scale(13),
      fontFamily: Fonts.montserratMedium,
      color: commonColors.Blue
   },
   popUpTextInputStyle: {
      borderWidth: 0.8,
      borderColor: commonColors.Blue,
      borderRadius: scale(2),
      color: commonColors.Blue,
      fontSize: scale(14),
      justifyContent: "center",
      fontFamily: Fonts.montserratMedium,
      paddingHorizontal: scale(5),
      width: (deviceWidth + 20) / 2,
      height: scale(40),
   },
   popUpTextInputStyle2: {
      borderWidth: 0.8,
      borderColor: commonColors.Blue,
      borderRadius: scale(2),
      color: commonColors.Blue,
      fontSize: scale(14),
      fontFamily: Fonts.montserratMedium,
      width: (deviceWidth - 10) / 2,
      height: scale(25),
      paddingHorizontal: scale(5),
   },
   nextText: {
      fontSize: scale(20),
      fontFamily: Fonts.montserratMedium,
      color: commonColors.White,
   },
   inputNumber: {
      alignSelf: 'flex-start',
   },
   textInputStyle: {
      borderWidth: 1,
      borderColor: commonColors.Blue,
      width: (deviceWidth + 20) / 2,
      height: scale(40),
      fontSize: scale(14),
      fontFamily: Fonts.montserratMedium,
      color: commonColors.Blue,
   },
   outerViewModalStyle: {
      height: deviceHeight - 200,
      backgroundColor: commonColors.LightGrey,
      justifyContent: 'center',
      justifyContent: 'flex-end',
   },
   centerView: {
      flex: 1,
      marginHorizontal: scale(30),
      justifyContent: 'center',
      marginTop: 10,
   },
   modalView: {
      backgroundColor: 'white',
      borderRadius: 10,
      marginVertical: scale(60),
      paddingVertical: scale(20),
      paddingHorizontal: scale(20),
      shadowColor: '#000',
      shadowOffset: {
         width: 0,
         height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,
   },
   dropdown: {
      paddingHorizontal: scale(4),
      alignSelf: 'center',
      textAlign: 'center',
      color: commonColors.Blue,
      justifyContent: 'center',
      width: '100%',
      height: '100%',
   },
   sortAndFilterButtonStyle2: {
      marginTop: scale(8),
      flexDirection: 'row',
      height: scale(40),
      width: scale(90),
      justifyContent: 'space-between',
      alignSelf: 'center',
      borderWidth: 0.8,
      borderColor: commonColors.Blue,
      borderRadius: scale(2),
   },
   placeholderStyle: {
      fontSize: scale(12),
      fontFamily: Fonts.montserratMedium,
      alignSelf: 'center',
      textAlign: 'left',
      color: commonColors.Blue
   },
   selectedTextStyle: {
      fontSize: scale(14),
      textAlign: 'left',
      fontFamily: Fonts.montserratMedium,
      color: commonColors.Blue
   },
   textItem: {
      color: commonColors.Blue,
      fontSize: scale(12),
      alignSelf: 'center',

      fontFamily: Fonts.montserratMedium
   },
})



