import { React, importImages, TouchableOpacity, TextInput, useState, useEffect, showSimpleAlert, moment, Request, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale, Alert } from '../../utils/importLibrary'
import SkeletonContent from 'react-native-skeleton-content-nonexpo';

export default function MyOrderViewController({ route, navigation }) {
    const [arrMainCategories, setArrMainCategories] = useState([])
    const [isModalVisible, setModalVisible] = useState(false);
    const [nostorefound, setnostorefound] = useState('')
    const [loginKey, setloginKey] = useState('')

    const btnBackClick = () => {
        navigation.goBack()
    }



    useEffect(() => {
        getStoreList();
    }, [])
    const getStoreList = async () => {
        let userData = await StorageService.getItem(StorageService.STORAGE_KEYS.USER_DETAILS)
        setloginKey(userData)
        if (userData) {
            setModalVisible(true)
            let response = await Request.post('my-order.php')

            console.log("myorderresponse", response);
            setModalVisible(false)
            if (response.success == true) {
                if (response.data.length > 0) {
                    setArrMainCategories(response.data)
                }
                else {
                    setnostorefound('No order found')
                }
            }
            else {
                if (response) {
                    showSimpleAlert(response.message)
                }
            }
        } else {
            navigation.navigate('AuthSelection', { authFlag: 1, isfavourite: 1 })
        }
    }
    const renderNewArrivalItem = ({ item, index }) => {
        return (
            <TouchableOpacity style={styles.renderView} onPress={() => {
                navigation.navigate('OrderDetailsViewController', { orderItem: item })
            }} >
                <View style={{ marginHorizontal: 12, flex: 1, paddingTop: scale(5) }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={{ fontFamily: Fonts.montserratBold, fontSize: scale(14), color: commonColors.Blue }}>#{item.order_id}</Text>
                        <View>
                            <Text style={{ fontFamily: Fonts.montserratMedium, fontSize: scale(10), color: "#627B96" }}>{moment(item.sale_date_time).format('DD MMM YYYY')} | {item.sale_time}</Text>
                        </View>
                    </View>
                    <View style={{ paddingTop: scale(12) }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[styles.textStyle, { marginLeft: 4, color: "#627B96" }]}>Order Status : </Text>
                            <Text style={[styles.textStyle, {}]}>{item.order_status} </Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[styles.textStyle, { marginLeft: 4, color: "#627B96" }]}>Total Amount : </Text>
                            <Text style={[styles.textStyle, {}]}>{parseFloat(item.amount).toFixed(2)} JD's</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingVertical: scale(5) }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={[styles.textStyle, { marginLeft: 4, color: "#627B96" }]}>Payment : </Text>
                                <Text style={[styles.textStyle, {}]}>{item.payment_type}</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={[styles.textStyle, { marginLeft: 4, color: "#627B96" }]}>Qty : </Text>
                                <Text style={[styles.textStyle, { marginLeft: 4 }]}>{item.quantity}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    return (
        <View style={styles.mainViewStyle}>

            <View style={styles.headerLogoStyle}>
                <Text style={{ width: '100%', textAlign: 'center', position: 'absolute', fontFamily: Fonts.montserratSemiBold, fontSize: 20, color: commonColors.Blue }}>MY ORDERS</Text>
                <TouchableOpacity onPress={() => btnBackClick()} style={{ marginLeft: 16 }}>
                    <Image source={importImages.backButtonArrow} style={{}} />
                </TouchableOpacity>
            </View>
            <View style={{ width: 300, height: 1, backgroundColor: 'lightgrey', alignSelf: 'center', }}></View>
            {arrMainCategories && arrMainCategories.length > 0 ?
                < FlatList
                    bounces={true}
                    renderItem={renderNewArrivalItem}
                    data={arrMainCategories}
                    style={{ marginTop: 8, flex: 1, }}
                    contentContainerStyle={{ alignItems: 'center', flexGrow: 1 }}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={(item, index) => `${index}-id`}
                /> : <View style={[styles.nodatafoundView, { marginTop: isModalVisible ? 0 : scale(40) }]}>
                    <Text style={styles.transactionText}>{nostorefound}</Text>
                </View>}

            {/* {isModalVisible ?
                <View style={{}}>
                    <SkeletonContent
                        isLoading={true}
                        containerStyle={{ flexDirection: "column", marginHorizontal: scale(15), bottom: scale(5) }}
                        layout={[
                            { key: 'someotherId4', width: (deviceWidth - 50), marginVertical: scale(5), height: scale(120), borderRadius: scale(10) },
                            { key: 'otherId5', width: (deviceWidth - 50), height: scale(120), marginVertical: scale(5), borderRadius: scale(10) },
                            { key: 'Id8', width: (deviceWidth - 50), height: scale(120), marginVertical: scale(5), borderRadius: scale(10) },
                            { key: 'id9', width: (deviceWidth - 50), height: scale(120), marginVertical: scale(5), borderRadius: scale(10) },
                            { key: 'id10', width: (deviceWidth - 50), height: scale(120), marginVertical: scale(5), borderRadius: scale(10) },
                            { key: 'id11', width: (deviceWidth - 50), height: scale(120), marginVertical: scale(5), borderRadius: scale(10) },

                        ]}
                        animationDirection="horizontalLeft"
                        animationType='shiver'
                    />

                </View> : null
            } */}
            {isModalVisible?
         <BallIndicator visible={isModalVisible} /> :null}
        </View>
    )
}
const styles = StyleSheet.create({
    mainViewStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    headerLogoStyle: {
        alignItems: 'center',
        height: 44,
        flexDirection: 'row'
    },
    textStyle: {
        marginTop: 4,
        fontFamily: Fonts.montserratMedium,
        fontSize: 13,
        color: commonColors.Blue
    },
    renderView: {
        width: scale(300),
        marginVertical: 8,
        backgroundColor: commonColors.White,
        borderRadius: 10,
        shadowColor: commonColors.Blue,
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 5,
        paddingVertical: 12
    },
    imageView: {
        height: "100%",
        width: "30%",
        alignSelf: 'center',
        borderWidth: 2,
        borderRadius: scale(10),
        borderColor: '#BFCAD4',
        marginVertical: scale(10),
        justifyContent: 'center'
    },
    imageStyle: {
        alignSelf: 'center',
        height: scale(80),
        width: scale(80),
    },
    nodatafoundView: {
        marginTop: scale(40),
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center'
    },
    transactionText: {
        fontSize: scale(16),
        color: commonColors.Blue,
        fontFamily: Fonts.montserratMedium,
        alignSelf: "center"
    },
})