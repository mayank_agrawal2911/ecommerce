import { React, importImages, TouchableOpacity, TextInput, useState, useEffect, showSimpleAlert, Request, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale, ProductComponent } from '../../utils/importLibrary'
import Share from 'react-native-share';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import dynamicLinks from '@react-native-firebase/dynamic-links';

export default function SeeAllViewController({ route, navigation }) {
    const [arrMainCategories, setArrMainCategories] = useState([])
    const [isModalVisible, setModalVisible] = useState(false);
    const [headername, setheaderName] = useState('')
    const [colorCombination, setColorCombination] = useState(["#4B241C", "#DA749B", "#832325", "#A70B23", "#A10034", "#A6374A"])
    const [colorSelectedIndex, setColorSelectedIndex] = useState(0)
    const [loginKey, setloginKey] = useState('')
    const [nodatafound, setnodatafound] = useState('')
    const btnBackClick = () => {
        navigation.goBack()
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            checkLogin();
            getProductListing();
        });
        return unsubscribe;
    }, [navigation])

    const checkLogin = async () => {
        let userData = await StorageService.getItem(StorageService.STORAGE_KEYS.USER_DETAILS)
        setloginKey(userData)
    }
    const getProductListing = async () => {
        const segmentName = route.params.sectionName
        const sectionTitle = route.params.sectionTitle
        setheaderName(sectionTitle)
        setModalVisible(true)
        let params = {
            domain_id: 1,
            key_type: 'segment',
            key_word: segmentName,
        }
        let response = await Request.post('get-product-list.php', params)
        setModalVisible(false)
        if (response.success == true) {
            if (response.data.result.length > 0) {
                setArrMainCategories(response.data.result)
            }
            else {
                setnodatafound('No product found')
            }
        }
        else {
            if (response) {
                showSimpleAlert(response.message)
            }
        }
    }

    const colorCodeChangedIndex = (i) => {
        setColorSelectedIndex(i)
    }
    const favoriteUnFavoriteButtonClick = async (item, index, type) => {
        const favItem = item
        if (loginKey) {
            let params = {
                product_id: favItem.product_id,
            }
            setModalVisible(true)
            let response = await Request.post('add-favorite-item.php', params)
            if (response.success == true) {
                if (type === 'newarrival') {
                    arrMainCategories[index].is_favourite = item.is_favourite == 0 ? 1 : 0
                    setArrMainCategories(arrMainCategories)
                }
                setModalVisible(false)
            }
            else {
                showSimpleAlert(response.message)
            }
        } else {
            navigation.navigate('AuthSelection', { authFlag: 1, })
        }
    }
    // const sendReviewButtonClick = (item, index) => {
    //     const shareItem = item.item
    //     const message = 'Product Name:' + shareItem.brand_name + '\n' + shareItem.family_name + '\n' + "Price:" + shareItem.main_price + "JD's"
    //     const shareOptions = {
    //         subject: 'Gift Center',
    //         message: message,
    //         url: shareItem.family_pic
    //     };
    //     const response = Share.open(shareOptions)
    //         .then(res => {
    //             console.log(res);
    //         })
    //         .catch(err => {
    //             err && console.log(err);
    //         });
    //     return response;
    // }
    const generateLink = async (item) => {
        const shareItem = item.item
        const message =  shareItem.brand_name + '\n' + shareItem.family_name + '\n' +  shareItem.main_price + "JD's"
  
        console.log("message", message, shareItem.seo_url);
        const dynamicLink = await dynamicLinks().buildShortLink({
           link: `https://giftsshopping.page.link/?productId=${shareItem.seo_url}`,
           domainUriPrefix: 'https://giftsshopping.page.link',
           ios: {
              bundleId: 'com.giftsshopping',
              appStoreId: '6446405028'
           },
           android: {
              packageName: 'com.giftsshopping',
           },
           social: {
              title:  shareItem.brand_name,
              descriptionText: message,
              imageUrl: shareItem.family_pic,
           },
           navigation: {
              forcedRedirectEnabled: true,
           }
        });
        return dynamicLink;
  
     }
     const sendReviewButtonClick = async (item, index) => {
        const appLink = await generateLink(item)
        console.log("appLink", appLink);
        const shareOption = {
           subject: 'Gift Center',
           message: appLink,
        }
        Share.open(shareOption)
           .then((res) => {
              console.log('res-> ', res);
           })
           .catch((err) => {
              err && console.log(err);
           });
     }
    
    const newArrivalProductClick = ({ item }) => {
        navigation.navigate('ProductDetailViewController', { seoUrl: item.seo_url })
    }

    const renderNewArrivalItem = (item, index, type) => {
        return (
            <ProductComponent
                item={item}
                index={index}
                isLoading={false}
                isFavourite={item.is_favourite == 0 ? false : true}
                mainCateoriesClick={(item, index) => newArrivalProductClick({ item, index })}
                favoriteUnFavoriteButtonClick={(item, index) => favoriteUnFavoriteButtonClick(item, index, type)}
                sendReviewButtonClick={(item, index) => sendReviewButtonClick({ item, index })}
                isShowOffer={item.has_offer == '' ? false : true}
                isShowColorShade={false}
            />

        )

    }
    const renderFooter = () => {
        return (
            <View style={styles.footer}>
                <TouchableOpacity
                    activeOpacity={0.9}
                    onPress={() => { getProductListing() }}
                    style={styles.loadMoreBtn}>
                    <Text style={styles.btnText}>Load More</Text>
                    {isModalVisible &&
                        <BallIndicator visible={isModalVisible} />
                    }
                </TouchableOpacity>
            </View>
        );
    };
    let new_arrival = 'newarrival'
    return (
        <View style={styles.mainViewStyle}>

            <View style={styles.headerLogoStyle}>
                <Text style={{ width: '100%', textAlign: 'center', position: 'absolute', fontFamily: Fonts.montserratSemiBold, fontSize: 20, color: commonColors.Blue }}>{headername}</Text>
                <TouchableOpacity onPress={() => btnBackClick()} style={{ marginLeft: 16 }}>
                    <Image source={importImages.backButtonArrow} style={{}} />
                </TouchableOpacity>
            </View>

            <View style={{ width: 300, height: 1, backgroundColor: 'lightgrey', alignSelf: 'center', }}></View>
            <View style={{ flex: 1, }} bounces={false}>

                {arrMainCategories.length > 0 ?
                    <FlatList
                        bounces={false}
                        renderItem={({ item, index }) => renderNewArrivalItem(item, index, new_arrival)}
                        data={arrMainCategories}
                        style={{ flex: 1, marginBottom: scale(60) }}
                        contentContainerStyle={{ flexGrow: 1 }}
                        numColumns={2}
                        showsVerticalScrollIndicator={false}
                        keyExtractor={(item, index) => `${index}-id`}
                    /> : <View style={styles.noDataView}>
                        <Text style={styles.transactionText}>{nodatafound}</Text>
                    </View>}

                {/* {isModalVisible ?
                    <View style={{ flexDirection: "row", }}>

                        <SkeletonContent
                            isLoading={true}
                            containerStyle={{ flexDirection: "column", marginHorizontal: scale(10), }}
                            layout={[
                                { key: 'someotherId2', width: (deviceWidth - 32) / 2, marginVertical: scale(20), height: scale(310), borderRadius: scale(10) },
                                { key: 'otherId3', width: (deviceWidth - 32) / 2, height: scale(310), marginVertical: scale(10), borderRadius: scale(10) },
                                { key: 'Id4', width: (deviceWidth - 32) / 2, height: scale(310), marginVertical: scale(10), borderRadius: scale(10) },
                                { key: 'id5', width: (deviceWidth - 32) / 2, height: scale(310), marginVertical: scale(10), borderRadius: scale(10) },
                            ]}
                            animationDirection="horizontalLeft"
                            animationType='shiver'
                        />
                        <SkeletonContent
                            isLoading={true}
                            containerStyle={{ flexDirection: "column", marginRight: scale(5) }}
                            layout={[
                                { key: 'someotherId', width: (deviceWidth - 32) / 2, marginVertical: scale(20), height: scale(310), borderRadius: scale(10) },
                                { key: 'otherId', width: (deviceWidth - 32) / 2, height: scale(310), marginVertical: scale(10), borderRadius: scale(10) },
                                { key: 'Id', width: (deviceWidth - 32) / 2, height: scale(310), marginVertical: scale(10), borderRadius: scale(10) },
                                { key: 'id', width: (deviceWidth - 32) / 2, height: scale(310), marginVertical: scale(10), borderRadius: scale(10) },

                            ]}
                            animationDirection="horizontalLeft"
                            animationType='shiver'
                        />
                    </View> : null
                } */}
                {isModalVisible ?
                    <BallIndicator visible={isModalVisible} /> : null}
            </View>
        </View >
    )
}

const styles = StyleSheet.create({
    mainViewStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    noDataView: { flex: 1, height: '100%', justifyContent: 'center', alignContent: 'center', alignSelf: 'center' },
    headerLogoStyle: {
        alignItems: 'center',
        height: 44,
        flexDirection: 'row'
    },
    textStyle: {
        marginTop: 4,
        fontFamily: Fonts.montserratMedium,
        fontSize: 13,
        color: commonColors.Blue
    },

})