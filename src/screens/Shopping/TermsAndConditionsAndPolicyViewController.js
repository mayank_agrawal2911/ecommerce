import { React, Request, importImages, TouchableOpacity, TextInput, useState, useEffect, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale } from '../../utils/importLibrary'
import SkeletonContent from 'react-native-skeleton-content-nonexpo';

export default function TermsAndConditionsAndPolicyViewController({ route, navigation }) {
   const [isModalVisible, setModalVisible] = useState(false);
   const [termsData, settermsData] = useState([]);
   const [nopolicyfound, setnopolicyfound] = useState('')

   useEffect(() => {
      termandconditionPolicy();
   }, [])
   const btnBackClick = () => {
      navigation.goBack()
   }
   const termandconditionPolicy = async () => {
      setModalVisible(true)
      let params = { domain_id: 1 }
      let response = await Request.post('cms-pages.php', params)
      setModalVisible(false)
      if (response.success == true) {
         if (response.data.length > 0) {
            settermsData(response.data)
         }
         else {
            setnopolicyfound('No policy found')
         }
      }
      else {
         if (response) {
            showSimpleAlert(response.message)
         }
      }
   }
   const renderItem = ({ item, index }) => {
      return (
         <View>
            <TouchableOpacity style={styles.optionsTextStyle} onPress={() => termpageRedirection(item.slug)}>
               <Text style={styles.textStyle}>{item.article_name}</Text>
            </TouchableOpacity>
            <View style={styles.seperatoreStyle}></View>
         </View>
      );
   }

   const termpageRedirection = async (slug) => {
      setModalVisible(true)
      let params = { domain_id: 1, slug: slug }
      let response = await Request.post('cms-pages-detail.php', params)
      setModalVisible(false)
      if (response.success == true) {
         navigation.navigate("PolicyViewController", { slug: response.data })
      }
      else {
         if (response) {
            showSimpleAlert(response.message)
         }
      }
   }
   return (
      <View style={styles.mainViewStyle}>
         <TouchableOpacity onPress={() => btnBackClick()} style={styles.backbtnView}>
            <Image source={importImages.backButtonArrow} style={{}} />
         </TouchableOpacity>

         <View style={styles.headerLogoStyle}>
            <Text style={styles.headerText}>T&C & POLICY</Text>
         </View>
         <View style={styles.seperatoreStyle}></View>
         {/* {isModalVisible == true ?
            <View style={{ flexDirection: "column", }}>
               <SkeletonContent
                  isLoading={true}
                  containerStyle={{ flexDirection: "column", marginHorizontal: scale(15) }}
                  layout={[
                     { key: 'someotherId2', width: (deviceWidth - 30), height: scale(40), borderRadius: scale(10) },
                     { key: 'otherId3', width: (deviceWidth - 30), height: scale(40), marginVertical: scale(5), borderRadius: scale(10) },
                     { key: 'Id4', width: (deviceWidth - 30), height: scale(40), borderRadius: scale(10) },
                     { key: 'id5', width: (deviceWidth - 30), height: scale(40), marginVertical: scale(5), borderRadius: scale(10) },
                     { key: 'id7', width: (deviceWidth - 30), height: scale(40), marginVertical: scale(5), borderRadius: scale(10) },

                  ]}
                  animationDirection="horizontalLeft"
                  animationType='shiver'
               />

            </View> : */}
             <FlatList
               data={termsData}
               renderItem={renderItem}
               keyExtractor={(item, index) => index.toString()}
               showsVerticalScrollIndicator={false}
               ListEmptyComponent={() => (
                  <Text style={styles.transactionText}>{nopolicyfound}</Text>
               )}
            >
            </FlatList>
         {/* } */}

{isModalVisible?
         <BallIndicator visible={isModalVisible} /> :null}
      </View>
   )
}
const styles = StyleSheet.create({
   mainViewStyle: {
      flex: 1,
      backgroundColor: 'white'
   },
   backbtnView: {
      marginLeft: scale(16),
      marginTop: scale(16)
   },
   headerLogoStyle: {
      marginLeft: scale(40),
      marginTop: scale(24)
   },
   headerText: {
      fontFamily: Fonts.montserratSemiBold,
      fontSize: scale(24),
      color: commonColors.Blue,
      marginBottom: scale(8)
   },
   optionsTextStyle: {
      marginLeft: scale(52),
      marginTop: scale(8),
      height: scale(30),
      justifyContent: 'center'
   },
   seperatoreStyle: {
      marginHorizontal: scale(30),
      borderColor: commonColors.Blue,
      borderBottomWidth: StyleSheet.hairlineWidth,
      marginVertical: scale(5),
   },
   textStyle: {
      fontFamily: Fonts.montserratRegular,
      fontSize: scale(15),
      color: commonColors.lightBlue
   },
   transactionText: {
      color: commonColors.Blue,
      marginTop: scale(200),
      fontSize: scale(20),
      justifyContent: "center",
      alignSelf: 'center',
      fontFamily: Fonts.montserratMedium
   },
   policyView: {
      marginBottom: scale(20),
      alignSelf: 'center'
   },
})