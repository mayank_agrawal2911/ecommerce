import { React, showSimpleAlert, useRef, ProductComponent, Request, Dimensions, importImages, TouchableOpacity, TextInput, useState, useEffect, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale, Alert, NavigationService } from '../../utils/importLibrary'
import Share from 'react-native-share';
import FastImage from 'react-native-fast-image'
import ImageZoom from 'react-native-image-pan-zoom';
import { Dropdown } from 'react-native-element-dropdown';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import dynamicLinks from '@react-native-firebase/dynamic-links';
import { useRoute, useNavigation } from '@react-navigation/native';
import { NavigationEvents } from '@react-navigation/compat';

export default function ProductDetailViewController({ route, navigation }) {
   let listViewRef;
   let colorRef
   const [arrMainCategories, setArrMainCategories] = useState([])
   const [colorCombination, setColorCombination] = useState([])
   const [colorSelectedIndex, setColorSelectedIndex] = useState(0)
   const [seoUrl, setseoUrl] = useState('')
   const [isModalVisible, setModalVisible] = useState(false);
   const [productIndex, setproductIndex] = useState(0);
   const [productDetailImage, setProductDetailImage] = useState('')
   const [modalvisible, setmodalvisible] = useState(false)
   const [arrData, setarrData] = useState([])
   const [productName, setproductName] = useState('')
   const [productId, setproductId] = useState()
   const [familyName, setFamilyName] = useState('')
   const [familyDescription, setfamilyDescription] = useState('')
   const [metaBrand, setmetaBrand] = useState('')
   const [price, setprice] = useState('')
   const [isFavourite, setisFavourite] = useState(0)
   const [wishId, setwishId] = useState('')
   const [quantity, setquantity] = useState('1')
   const [quantityData, setquantityData] = useState([])
   const [points, setpoints] = useState('')
   const [descModal, setdescModal] = useState(false)
   const [reviewModal, setreviewModal] = useState(false)
   const [reviewData, setreviewData] = useState([])
   const [productmoreImages, setproductmoreImages] = useState([])
   const [loginKey, setloginKey] = useState('')
   const [quantitymodalVisible, setquantitymodalVisible] = useState(false)
   const [successModal, setsuccessModal] = useState(false)
   const [successMessage, setsuccessMessage] = useState('')
   const [productArray, setproductArray] = useState([])
   const [isvoucher, setisvoucher] = useState('')
   const [viewmoremodal, setviewmoremodal] = useState()
   const [famname, setfamname] = useState('')
   const [offerprice, setofferprice] = useState('')
   const [offername, setoffername] = useState('')
   const [favouritemodalVisible, setfavouritemodalVisible] = useState(false)
   const [imageloader, setimageloader] = useState(false)
   const [contentloader, setcontentloader] = useState(false)
   const [addbasketmodal, setaddbasketmodal] = useState(false)
   const scrollRef = useRef(null)
   // const Routes = useRoute();
   // const navigates = useNavigation()
   useEffect(() => {
      // console.log("productdetails", Routes);
      checkLogin();
   }, [seoUrl, route.params.seoUrl, navigation])


   const checkLogin = async () => {
      let userData = await StorageService.getItem(StorageService.STORAGE_KEYS.USER_DETAILS)
      setloginKey(userData)
      const seoUrlData = seoUrl ? seoUrl : route.params.seoUrl
      setseoUrl(seoUrlData)
      if (seoUrlData) {
         await callProductListing(seoUrlData)
      }
   }
   const renderProductImages = ({ item, index }) => {
      return (

         <View style={styles.productImageView}>
            <TouchableOpacity style={styles.productImageView} onPress={() => { productSelect(item) }}>
               <Image source={{ uri: item.product_pic }} style={styles.productimage} />
            </TouchableOpacity>
         </View>

      )
   }
   const productSelect = (item) => {
      setProductDetailImage(item.product_pic)
   }
   const btnBackClick = () => {
      navigation.goBack()

      // NavigationService.goBack()

   }
   const bottomScrollClick = () => {
      listViewRef.scrollToEnd({ animated: true });
   }
   const colourBottomScroll = () => {
      colorRef.scrollToEnd({ animated: true });

   }
   const callProductListing = async (seoUrlData) => {
      let params = { seo_url: seoUrlData ? seoUrlData : seoUrl, domain_id: 1 }
      setModalVisible(true)
      setcontentloader(true)
      setimageloader(true)
      let response = await Request.post('product.php', params)
      setModalVisible(false)
      if (response.success == true) {
         if (response.data) {
            setarrData(response.data)
            setproductName(response.data[0].value)
            setproductId(response.data[0].product_id)
            setmetaBrand(response.data[0].meta_brand)
            setFamilyName(response.data[0].family_name)
            setfamilyDescription(response.data[0].desc)
            setprice(response.data[0].main_price)
            setpoints(response.data[0].lylty_pnts)
            setquantity('1')
            setofferprice(response.data[0].dmain_price)
            setoffername(response.data[0].has_offer_name)
            setisFavourite(response.data[0].is_favourite)
            setwishId(response.data[0].wish_id)
            setisvoucher(response.data[0].is_voucher)
            var sub_array = [];
            console.log("response.data[0].stock", response.data[0].stock);
            for (var i = 1; i <= response.data[0].stock; i++) {
               if (i > 0) {
                  sub_array.push({
                     label: JSON.stringify(i),
                     value: JSON.stringify(i)
                  });
               }
            }
            setquantityData(sub_array)
            setModalVisible(false)
            getproductImage(response.data[0].product_id)
         } else {
            setimageloader(false)
            setcontentloader(false)
         }
      } else {
         setimageloader(false)
         setcontentloader(false)
      }
   }

   const getproductImage = async (imageData) => {
      let params = { product_id: imageData, domain_id: 1 }
      setimageloader(true)
      let response = await Request.post('product-img.php', params)
      setimageloader(false)
      setcontentloader(false)
      if (response.success == true) {
         setArrMainCategories(response.data)
         setProductDetailImage(response.data.length > 0 ? response.data[0].product_pic : '')
         getmoreproduct(imageData)
      }
      else {

      }
   }
   const getmoreproduct = async (imageData) => {
      let params = { product_id: imageData, domain_id: 1 }
      // setcontentloader(true)
      let response = await Request.post('product-more.php', params)

      if (response.success == true) {
         setproductmoreImages(response.data)
      }
      else {
      }
   }

   const colorCodeChangedIndex = async (item) => {
      console.log("item", item);
      setproductName(item.value)
      setmetaBrand(item.meta_brand)
      setFamilyName(item.family_name)
      setfamilyDescription(item.desc)
      setprice(item.main_price)
      setseoUrl(item.seo_url)
      setproductId(item.product_id)
      setofferprice(item.dmain_price)
      setoffername(item.has_offer_name)
      setpoints(item.lylty_pnts)
      setisFavourite(item.is_favourite)
      setwishId(item.wish_id)
      setquantity('1')
      var sub_array = [];
      for (var i = 1; i <= item.stock; i++) {
         if (i > 0) {
            sub_array.push({
               label: JSON.stringify(i),
               value: JSON.stringify(i)
            });
         }
      }
      setquantityData(sub_array)
      await getproductImage(item.product_id)
   }
   const renderColorItems = ({ item, index }) => {
      return (
         <View>
            {item.photo2 ?
               <TouchableOpacity
                  key={index}
                  style={styles.renderColorView}
                  onPress={() => colorCodeChangedIndex(item)}
               >
                  <Image style={styles.colorImage} source={{ uri: item.photo2 }} />
               </TouchableOpacity> : null}
         </View>
      )
   }
   const productRenderItem = ({ item, index }) => {
      return (
         <ProductComponent
            isLoading={isModalVisible}
            item={item}
            index={index}
            isFavourite={item.is_favourite == 0 ? false : true}
            mainCateoriesClick={(item, index) => newArrivalProductClick({ item, index })}
            favoriteUnFavoriteButtonClick={(item, index) => favoriteUnFavoriteButtonClick(item, index)}
            sendReviewButtonClick={(item, index) => sendReviewButtonClick({ item, index })}
            isShowOffer={item.has_offer == '' ? false : true}
            isShowColorShade={false}
         />
      )
   }
   const renderproductmoreImagesItem = ({ item, index }) => {
      return (
         <View>
            <ProductComponent
               isLoading={isModalVisible}
               item={item}
               index={index}
               isFavourite={item.is_favourite == 0 ? false : true}
               mainCateoriesClick={(item, index) => newArrivalProductClick({ item, index })}
               favoriteUnFavoriteButtonClick={(item, index) => favoriteUnFavoriteButtonClick(item, index)}
               sendReviewButtonClick={(item, index) => sendReviewButtonClick({ item, index })}
               isShowOffer={item.has_offer ? item.has_offer == '' ? false : true : false}
               isShowColorShade={false}
            />
         </View>
      )
   }
   const selectModalItem = async (item) => {
      console.log("selectModalItemitem", item);
      setproductName(item.value)
      setmetaBrand(item.meta_brand)
      setFamilyName(item.family_name)
      setfamilyDescription(item.desc)
      setprice(item.main_price)
      setofferprice(item.dmain_price)
      setoffername(item.has_offer_name)
      setisFavourite(item.is_favourite)
      setwishId(item.wish_id)
      // setreviewData(item.reviews)
      setproductId(item.product_id)
      setseoUrl(item.seo_url)
      setpoints(item.lylty_pnts)
      var sub_array = [];
      for (var i = 1; i <= item.stock; i++) {
         if (i > 0) {
            sub_array.push({
               label: JSON.stringify(i),
               value: JSON.stringify(i)
            });
         }
      }
      setquantityData(sub_array)
      await getproductImage(item.product_id)

   }
   const arrDatarenderItem = ({ item, index }) => {
      return (
         <View style={{ marginVertical: scale(10) }}>
            <TouchableOpacity onPress={() => {
               selectModalItem(item)
            }}>
               <Text style={{ color: commonColors.Blue }}>{item.fam_name}</Text>
            </TouchableOpacity>
         </View>
      )
   }
   const quantityrenderItem = ({ item, index }) => {
      return (
         <View style={{ marginVertical: scale(10) }}>
            <TouchableOpacity onPress={() => {
               setquantity(item)
               setquantitymodalVisible(!quantitymodalVisible)
            }}>
               <Text style={{ color: commonColors.Blue }}>{item}</Text>
            </TouchableOpacity>
         </View >
      )
   }
   const favoriteUnFavoriteButtonClick = async (item, index) => {
      console.log("itemitemitem", item, seoUrl);
      const favItem = item
      if (loginKey) {
         let params = {
            product_id: favItem.product_id,
         }
         setfavouritemodalVisible(true)
         let response = await Request.post('add-favorite-item.php', params)
         if (response.success == true) {
            productmoreImages[index].is_favourite = item.is_favourite == 0 ? 1 : 0
            setproductmoreImages(productmoreImages)

            setfavouritemodalVisible(false)
         }
      } else {
         navigation.navigate('AuthSelection', { authFlag: 1, isfavourite: 1 })
      }
   }
   // const sendReviewButtonClick = (item, index) => {
   //    console.log("itemmmmm", item);

   //    const shareItem = item.item
   //    console.log("shareItemitemmmmm", shareItem);
   //    const message = 'Product Name:' + shareItem.brand_name + '\n' + shareItem.family_name + '\n' + "Price:" + shareItem.main_price + "JD's"


   //    const shareOptions = {
   //       subject: 'Gift Center',
   //       message: message,
   //       url: shareItem.family_pic
   //    };
   //    const response = Share.open(shareOptions)
   //       .then(res => {
   //          console.log(res);
   //       })
   //       .catch(err => {
   //          err && console.log(err);
   //       });
   //    return response;
   // }



   const generateLink = async (item) => {
      const shareItem = item.item
      const message = shareItem.brand_name + '\n' + shareItem.family_name + '\n' + shareItem.main_price + "JD's"

      console.log("message", message, shareItem.seo_url);
      const dynamicLink = await dynamicLinks().buildShortLink({
         link: `https://giftsshopping.page.link/?productId=${shareItem.seo_url}`,
         domainUriPrefix: 'https://giftsshopping.page.link',
         ios: {
            bundleId: 'com.giftsshopping',
            appStoreId: '6446405028'
         },
         android: {
            packageName: 'com.giftsshopping',
         },
         social: {
            title: shareItem.brand_name,
            descriptionText: message,
            imageUrl: shareItem.family_pic,
         },
         navigation: {
            forcedRedirectEnabled: true,
         }
      });
      return dynamicLink;

   }
   const sendReviewButtonClick = async (item, index) => {
      const appLink = await generateLink(item)
      console.log("appLink", appLink);
      const shareOption = {
         subject: 'Gift Center',
         message: appLink,
      }
      Share.open(shareOption)
         .then((res) => {
            console.log('res-> ', res);
         })
         .catch((err) => {
            err && console.log(err);
         });
   }

   const newArrivalProductClick = ({ item }) => {
      setseoUrl(item.seo_url)
      callProductListing(item.seo_url)
      console.log("scrollRefscrollRef", scrollRef);
      scrollRef.current?.scrollTo({
         y: 0,
         animated: true,
      });
   }
   const productDetailClick = () => {
      navigation.navigate("ProductDetails", { productDetails: familyDescription })
   }
   const reviewModalClick = () => {
      navigation.navigate("ReviewDetails", { productId: productId })
   }
   const addtoBasket = async () => {
      console.log("quantity", quantity, productId);
      if (loginKey) {
         if (quantity == '') {
            showSimpleAlert('Please select quantity')
         } else {
            if (productId != '' && productId != undefined) {
               let params = { func: 'add', product_id: productId, domain_id: 1, quantity: quantity, product_type: isvoucher == 1 ? 'voucher' : '' }
               // setModalVisible(true)
               setaddbasketmodal(true)
               let response = await Request.post('manage-cart.php', params)
               setModalVisible(false)
               setaddbasketmodal(false)

               if (response.success == true) {
                  setsuccessMessage(response.message)
                  setsuccessModal(true)
               } else {
                  showSimpleAlert(response.message)
               }
            }
         }
      } else {
         navigation.push('AuthSelection', { authFlag: 1, isfavourite: 1 })
      }
   }
   const itemSeparator = () => {
      return (
         <View style={styles.itemSeperatorView}></View>
      )
   }
   const writeReview = () => {
      if (loginKey) {
         navigation.navigate('WriteAReviewViewController', { productId: productId })

      } else {
         navigation.push('AuthSelection', { authFlag: 1, isfavourite: 1 })
      }
   }
   const openModal = () => {
      setmodalvisible(!modalvisible)
   }

   const callproductData = async (seoUrlData) => {
      let params = { seo_url: seoUrlData }

      let response = await Request.post('product.php', params)
      setModalVisible(false)
      if (response.success == true) {
         if (response.data) {
            setarrData(response.data)

         } else {
            setimageloader(false)
            setcontentloader(false)
         }
      } else {
         setimageloader(false)
         setcontentloader(false)
      }
   }

   const favItem = async () => {
      if (loginKey) {
         if (isFavourite == 1) {
            let params = {
               product_id: productId
            }
            setimageloader(true)
            let response = await Request.post('add-favorite-item.php', params)
            setimageloader(false)

            if (response.success == true) {
               setisFavourite(0)
               callproductData(seoUrl)

            }
            else {
               if (response) {
                  showSimpleAlert(response.message)
               }
            }
         }
         else {
            let params = {
               product_id: productId
            }
            setimageloader(true)
            let response = await Request.post('add-favorite-item.php', params)
            setimageloader(false)

            if (response.success == true) {
               setisFavourite(1)
               callproductData(seoUrl)
            }
            else {
               if (response) {
                  showSimpleAlert(response.message)
               }
            }

         }
      } else {
         navigation.push('AuthSelection', { authFlag: 1, isfavourite: 1 })

      }
   }
   // const sendReview = () => {
   //    const message = 'Product Name:' + familyName + '\n' + productName + '\n' + "Price:" + price + "JD's"
   //    const shareOptions = {
   //       subject: 'Gift Center',
   //       message: message,
   //       url: productDetailImage
   //    };
   //    const response = Share.open(shareOptions)
   //       .then(res => {
   //          console.log(res);
   //       })
   //       .catch(err => {
   //          err && console.log(err);
   //       });
   //    return response;
   // }

   const generateLinkShare = async () => {
      const message = familyName + '\n' + productName + '\n' + price + "JD's"


      console.log("message", productDetailImage);
      const dynamicLink = await dynamicLinks().buildShortLink({
         link: `https://giftsshopping.page.link/?productId=${seoUrl}`,
         domainUriPrefix: 'https://giftsshopping.page.link',
         ios: {
            bundleId: 'com.giftsshopping',
            appStoreId: '6446405028'
         },
         android: {
            packageName: 'com.giftsshopping',
         },
         social: {
            title: metaBrand,
            descriptionText: message,
            imageUrl: productDetailImage,
         },
         navigation: {
            forcedRedirectEnabled: true,
         }
      });
      return dynamicLink;

   }
   const sendReview = async () => {
      const appLink = await generateLinkShare()
      console.log("appLink", appLink);
      console.log("appLink2", productDetailImage);

      const shareOption = {
         subject: 'Gift Center',
         message: appLink,
      }
      Share.open(shareOption)
         .then((res) => {
            console.log('res-> ', res);
         })
         .catch((err) => {
            err && console.log(err);
         });
   }

   const renderLabelItem = (item) => {
      return (
         <View>

            <View style={{ height: 56, justifyContent: 'center', borderBottomColor: '#CDCECF', marginStart: 20, marginEnd: 20, borderBottomWidth: 1 }}>
               <View style={{}}>
                  <Text style={styles.textItem}>{item.label}</Text>
               </View>
            </View>
         </View>

      );
   };
   const renderLabelItemProduct = (item) => {
      return (
         <View>
            <View style={{ height: 56, justifyContent: 'center', borderBottomColor: '#CDCECF', marginStart: 20, marginEnd: 20, borderBottomWidth: 1 }}>
               <View style={{}}>
                  <Text style={styles.textItem}>{item.fam_name}</Text>
               </View>
            </View>

         </View>
      );
   };
   const sortDataArray2 = [
      { label: 'Relevancy', value: 'relevancy' },
      { label: 'Price : Low to High', value: 'pasc' },
      { label: 'Price : High to Low', value: 'pdesc' },
      { label: 'Alphabetically : A-Z', value: 'aasc' },
      { label: 'Alphabetically :  Z-A', value: 'adesc' },
   ];

   return (
      <View style={styles.mainViewStyle}>
         <View style={styles.headerLogoStyle}>
            <TouchableOpacity onPress={() => btnBackClick()} style={{ marginLeft: 16 }}>
               <Image source={importImages.backButtonArrow} style={{}} />
            </TouchableOpacity>
         </View>
         <NavigationEvents
            onWillFocus={async () => {
               checkLogin()
            }}
         />
         <ScrollView showsVerticalScrollIndicator={false} ref={scrollRef} bounces={false} nestedScrollEnabled={true} >

            <View style={{ marginHorizontal: 16, }}>
               <View style={styles.productDetailsView}>
                  <View style={styles.renderProductDetailView}>

                     <FlatList
                        bounces={false}
                        renderItem={renderProductImages}
                        nestedScrollEnabled={true}
                        data={arrMainCategories}
                        style={{ flex: 1 }}
                        contentContainerStyle={{ flexGrow: 1 }}
                        ref={(ref) => {
                           listViewRef = ref;
                        }}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        keyExtractor={(item, index) => `${index}-id`}
                     />
                     {arrMainCategories.length > 4 ?
                        <TouchableOpacity onPress={() => bottomScrollClick()} style={styles.dropdownImage}>
                           <Image source={importImages.dropDownProductImage} style={{}} />
                        </TouchableOpacity> : null}
                  </View>




                  <View style={styles.imageView}>

                     <View style={{ flexDirection: 'row', }}>


                        <ImageZoom
                           cropWidth={scale(200)}
                           cropHeight={scale(240)}
                           imageWidth={scale(200)}
                           imageHeight={scale(240)}>
                           <FastImage
                              style={styles.defaultImageView}
                              source={{
                                 uri: productDetailImage,
                                 priority: FastImage.priority.high,
                              }}
                              resizeMode={FastImage.resizeMode.contain}
                           />
                        </ImageZoom>

                        <View style={styles.reviewView}>
                           <TouchableOpacity onPress={() => sendReview()} disabled={imageloader == true ? true : false}>
                              <Image source={importImages.sendReview} />
                           </TouchableOpacity>
                           <TouchableOpacity style={{ marginTop: scale(10) }} onPress={() => { favItem() }} disabled={imageloader == true ? true : false}>
                              <Image source={isFavourite == 0 ? importImages.favUnselected : importImages.favSelected} />
                           </TouchableOpacity>
                        </View>

                     </View>

                  </View>

               </View>

               <View style={styles.mainProductView}>


                  <View style={[styles.colorView, { flexDirection: "row", }]}>
                     <FlatList
                        horizontal={false}
                        bounces={false}
                        scrollEnabled={false}
                        renderItem={renderColorItems}
                        numColumns={6}
                        style={{ width: scale(210), flexWrap: 'wrap', marginTop: scale(10) }}
                        contentContainerStyle={{ flexGrow: 1, flexWrap: viewmoremodal == true ? 'wrap' : 'nowrap', flexDirection: 'row' }}
                        data={arrData}
                        ref={(ref) => {
                           colorRef = ref;
                        }}
                        keyExtractor={(item, index) => `${index}-id`}
                     />
                     {arrData ? arrData.length > 6 ?
                        <TouchableOpacity
                           style={{ alignSelf: "flex-end", bottom: scale(5), }} onPress={() => { setviewmoremodal(!viewmoremodal) }} >
                           <Image source={importImages.dropDownProductImage} style={{ width: 24, height: 20, transform: [{ rotate: viewmoremodal == true ? '180deg' : '0deg' }] }} />

                        </TouchableOpacity> : null : null}
                  </View>

               </View>

               <View style={{ marginLeft: 16, flex: 1, }}>


                  <View>
                     <Text style={styles.metaBrandtext}>{metaBrand}</Text>
                     <Text numberOfLines={2} style={styles.famnameText}>{familyName}</Text>
                     <TouchableOpacity style={[styles.sortAndFilterButtonStyle2, { width: deviceWidth - 40 }]}>
                        <Dropdown
                           style={styles.dropdown}
                           placeholderStyle={styles.placeholderStyle}
                           selectedTextStyle={styles.selectedTextStyle}
                           data={arrData}
                           search={false}
                           maxHeight={300}
                           labelField="label"
                           dropdownPosition={'bottom'}
                           activeColor={commonColors.White}
                           valueField='value'
                           placeholder={productName}
                           value={productName}
                           renderItem={(item) => renderLabelItemProduct(item)}
                           itemContainerStyle={{ color: commonColors.Blue }}
                           containerStyle={{ color: commonColors.Blue }}
                           showsVerticalScrollIndicator={false}
                           onChange={item => {
                              console.log("itemm:---->", item)
                              selectModalItem(item)
                           }}
                        />

                     </TouchableOpacity>
                  </View>
                  <View style={{ flexDirection: 'row', marginVertical: scale(10), justifyContent: "space-between" }}>

                     <View style={{ flexDirection: "column" }}>

                        <View style={{ flexDirection: "row", width: scale(200) }}>
                           <Text style={[styles.priceText2]}>{"Price: "}</Text>
                           <Text style={[styles.priceText, { textAlign: "left", width: offerprice ? scale(80) : scale(120), fontFamily: offerprice ? Fonts.montserratRegular : Fonts.montserratMedium, fontSize: offerprice ? scale(16) : scale(18), textDecorationLine: offerprice ? 'line-through' : '' }]}> {price} JD's</Text>
                           {offerprice ? <Text style={[styles.priceText, { width: scale(80), textAlign: "left", }]}>{offerprice} JD's</Text> : null}
                        </View>

                     </View>


                     <View style={{ flexDirection: 'row', marginHorizontal: scale(10), alignSelf: "center", justifyContent: "center" }} >
                        <TouchableOpacity style={styles.sortAndFilterButtonStyle3}>
                           <Dropdown
                              style={styles.dropdown}
                              placeholderStyle={styles.placeholderStyle}
                              selectedTextStyle={styles.selectedTextStyle}
                              data={quantityData}
                              search={false}
                              maxHeight={300}
                              labelField="label"
                              dropdownPosition={'bottom'}
                              activeColor={commonColors.White}
                              valueField='value'
                              placeholder={1}
                              value={quantity == '' ? 'Select quantity' : quantity}
                              renderItem={renderLabelItem}
                              itemContainerStyle={{ color: commonColors.Blue }}
                              containerStyle={{ color: commonColors.Blue }}
                              showsVerticalScrollIndicator={false}
                              onChange={item => {
                                 setquantity(item.value)
                              }}
                           />
                        </TouchableOpacity>

                     </View>


                  </View>

                  <Text style={[styles.pointText,]}>Earn {points} Points</Text>


               </View>
            </View>

            <TouchableOpacity onPress={() => addtoBasket()} style={styles.basketView} disabled={addbasketmodal == true || isModalVisible == true ? true : false}>
               <Image source={importImages.cartImage} style={{ marginRight: 8 }} />
               <Text style={styles.addtoBasketText}>ADD TO BASKET</Text>
            </TouchableOpacity>


            <View style={{ marginHorizontal: 16, }}>

               <TouchableOpacity style={styles.expandButtonClick} onPress={() => productDetailClick()} disabled={isModalVisible == true ? true : false}>
                  <Text style={styles.optionTextStyle}>PRODUCT DETAILS</Text>
                  <Image source={importImages.leftArrow} style={{ marginRight: 8 }} />
               </TouchableOpacity>

               <TouchableOpacity style={styles.expandButtonClick} onPress={() => reviewModalClick()} disabled={isModalVisible == true ? true : false}>
                  <Text style={styles.optionTextStyle}>REVIEWS</Text>
                  <Image source={importImages.leftArrow} style={{ marginRight: 8 }} />
               </TouchableOpacity>

               <TouchableOpacity style={styles.writeReviewView} onPress={() => { writeReview() }} disabled={isModalVisible == true ? true : false}>
                  <Image source={importImages.pencilEdiIcon} style={{ marginRight: 4 }} />
                  <Text style={styles.writeReviewText}>WRITE REVIEW</Text>
               </TouchableOpacity>

               {/* YOU MAY ALSO LIKE */}

               <View>
                  {productmoreImages && productmoreImages.length > 0 ?
                     <View>
                        <Text style={styles.sectionTitle}>YOU MAY ALSO LIKE</Text>
                        <FlatList
                           horizontal
                           bounces={false}
                           renderItem={renderproductmoreImagesItem}
                           data={productmoreImages}
                           // pagingEnabled={true}
                           style={{ marginVertical: scale(20), marginBottom: scale(60) }}
                           contentContainerStyle={{ flexGrow: 1 }}
                           showsVerticalScrollIndicator={false}
                           showsHorizontalScrollIndicator={false}
                           keyExtractor={(item, index) => `${index}-id`}
                        /></View> : null}
               </View>

            </View>
         </ScrollView>
         <Modal
            style={{ bottom: 0, backgroundColor: commonColors.Grey }}
            animationType="slide"
            transparent={true}
            visible={modalvisible}
            onRequestClose={() => {
               setmodalvisible(!modalvisible)
            }}>
            <TouchableOpacity
               underlayColor={'transparent'}
               onPress={() => {
                  setmodalvisible(!modalvisible)
               }

               }
               activeOpacity={1}
               style={styles.outerViewModalStyle}>
               <View style={styles.centerView}>
                  <View style={styles.modalView}>
                     <FlatList
                        data={arrData}
                        renderItem={arrDatarenderItem}
                        ItemSeparatorComponent={itemSeparator}
                        keyExtractor={(item, index) => `${index}-id`}
                     />
                  </View>
               </View>
            </TouchableOpacity>
         </Modal>
         <Modal
            style={{ bottom: 0, backgroundColor: commonColors.Grey }}
            animationType="slide"
            transparent={true}
            visible={quantitymodalVisible}
            onRequestClose={() => {
               setmodalvisible(!quantitymodalVisible)
            }}>
            <TouchableOpacity
               underlayColor={'transparent'}
               onPress={() => {
                  setmodalvisible(!quantitymodalVisible)
               }
               }
               activeOpacity={1}
               style={styles.outerViewModalStyle}>
               <View style={styles.centerView}>
                  <View style={styles.modalView}>
                     <FlatList
                        data={quantityData}
                        nestedScrollEnabled={true}
                        renderItem={quantityrenderItem}
                        ItemSeparatorComponent={itemSeparator}
                        keyExtractor={(item, index) => `${index}-id`}
                     />
                  </View>
               </View>
            </TouchableOpacity>
         </Modal>
         <Modal
            style={{ bottom: 0, backgroundColor: commonColors.Grey }}
            animationType="slide"
            transparent={true}
            visible={successModal}
            onRequestClose={() => {
               setsuccessModal(!successModal)
            }}>
            <TouchableOpacity
               underlayColor={'transparent'}
               activeOpacity={1}
               style={styles.schoolouterViewModalStyle}>
               <View style={styles.schoolcenterView}>
                  <View style={styles.schoolmodalView}>
                     <Text style={{ alignSelf: "center", fontFamily: Fonts.montserratMedium, color: commonColors.Blue, }}>{successMessage}</Text>
                     <View style={{ flexDirection: "row", justifyContent: "space-around", marginVertical: scale(20) }}>
                        <TouchableOpacity onPress={() => { navigation.navigate('MyBasketViewController') }}>
                           <View style={{ borderWidth: 1, borderColor: commonColors.Blue }}><Text style={{ padding: scale(10), color: commonColors.Blue, fontSize: scale(10), fontFamily: Fonts.montserratRegular }}>{'Show my Basket'}</Text></View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {
                           setsuccessModal(!successModal)
                        }}>
                           <View style={{ borderWidth: 1, borderColor: commonColors.Blue, backgroundColor: commonColors.Blue }}>
                              <Text style={{ padding: scale(10), color: commonColors.White, fontSize: scale(10), fontFamily: Fonts.montserratMedium }}>{'Continue Shopping'}</Text></View>
                        </TouchableOpacity>
                     </View>
                  </View>
               </View>
            </TouchableOpacity>
         </Modal>

         {
            favouritemodalVisible || contentloader || isModalVisible ?
               <BallIndicator visible={true} /> : null
         }
      </View >
   )
}

const styles = StyleSheet.create({
   mainViewStyle: {
      flex: 1,
      backgroundColor: 'white'
   },
   headerLogoStyle: {
      alignItems: 'center',
      height: scale(44),
      flexDirection: 'row'
   },
   textStyle: {
      marginTop: scale(4),
      fontFamily: Fonts.montserratMedium,
      fontSize: scale(13),
      color: commonColors.Blue
   },
   colorSection: {
      height: scale(24),
      width: scale(24),
      borderRadius: 5,
      marginRight: scale(10),
      marginTop: scale(2),
   },
   selectedColorSection: {
      height: scale(24),
      width: scale(24),
      borderRadius: 5,
      marginRight: scale(10),
      marginTop: 0,
   },
   sortAndFilterButtonStyle: {
      marginTop: scale(8),
      flexDirection: 'row',
      height: scale(40),
      justifyContent: 'space-between',
      borderColor: commonColors.Blue,
      borderWidth: 1,
      borderRadius: 5
   },
   sortAndFilterTextViewStyle: {
      justifyContent: 'center',
   },
   sortAndFilterDropDownArrowViewStyle: {
      width: '100%',
      height: "100%",
      position: 'absolute',
      justifyContent: 'center',
      alignItems: "flex-end"
   },
   sectionTitle: {
      fontFamily: Fonts.montserratSemiBold,
      fontSize: scale(20),
      color: commonColors.Blue,
      alignSelf: "center",
      marginTop: scale(16)
   },
   expandButtonClick: {
      marginTop: scale(8),
      marginLeft: scale(16),
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      height: scale(30)
   },
   optionTextStyle: {
      fontFamily: Fonts.montserratMedium,
      fontSize: scale(15),
      color: commonColors.qtySizeMlText
   },
   outerViewModalStyle: {
      height: deviceHeight - 200,
      backgroundColor: commonColors.LightGrey,
      justifyContent: 'center',
      justifyContent: 'flex-end',
   },
   centerView: {
      flex: 1,
      marginHorizontal: scale(30),
      justifyContent: 'center',
      marginTop: scale(10),
   },
   modalView: {
      backgroundColor: 'white',
      borderRadius: 10,
      marginVertical: scale(60),
      paddingVertical: scale(20),
      paddingHorizontal: scale(20),
      shadowColor: '#000',
      shadowOffset: {
         width: 0,
         height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,
   },
   productImageView: {
      justifyContent: 'center',
      alignItems: 'center',
      marginHorizontal: scale(10),
      marginVertical: scale(4),
      height: scale(60),
      width: scale(60),
      borderRadius: 10,
      borderWidth: 2,
      borderColor: '#B2B2B2'
   },
   productimage: {
      width: scale(50),
      height: scale(50),
   },
   renderColorView: {
      height: scale(30),
      borderRadius: scale(5),
      marginHorizontal: scale(5)
   },
   colorImage: {
      height: scale(25),
      borderRadius: scale(5),
      width: scale(25),
      resizeMode: 'contain'
   },
   itemSeperatorView: {
      borderWidth: 0.3,
      height: scale(1),
      borderBottomColor: commonColors.Blue
   },
   writeReviewView: {
      flexDirection: 'row',
      marginLeft: scale(16),
      marginVertical: scale(12),
      borderBottomWidth: 2,
      borderBottomColor: commonColors.Blue,
      height: scale(18),
      width: scale(110),
      backgroundColor: commonColors.White,
      alignItems: 'center',
   },
   writeReviewText: {
      fontFamily: Fonts.montserratSemiBold,
      fontSize: scale(12),
      color: commonColors.Blue,
   },
   addtoBasketText: {
      fontFamily: Fonts.montserratMedium,
      fontSize: scale(12),
      color: commonColors.White
   },
   basketView: {
      flexDirection: 'row',
      width: deviceWidth,
      marginVertical: scale(10),
      height: scale(50),
      backgroundColor: commonColors.Blue,
      justifyContent: 'center',
      alignItems: 'center'
   },
   pointText: {
      marginTop: 4,
      width: '70%',
      fontFamily: Fonts.montserratMedium,
      fontSize: 18,
      color: commonColors.Blue
   },
   priceText: {
      marginVertical: scale(5),
      fontFamily: Fonts.montserratMedium,
      fontSize: scale(18),
      color: commonColors.Blue,
      textAlign: "center"
   },
   priceText2: {
      marginVertical: scale(5),
      fontFamily: Fonts.montserratMedium,
      fontSize: scale(16),
      color: commonColors.Blue,
      textAlign: "left"
   },
   offernamestyle: {
      width: '100%',
      fontFamily: Fonts.montserratMedium,
      fontSize: scale(18),
      color: commonColors.Blue
   },
   productNameText: {
      marginLeft: scale(8),
      fontFamily: Fonts.montserratMedium,
      fontSize: scale(14),
      color: commonColors.qtySizeMlText
   },
   famnameText: {
      marginTop: scale(8),
      fontFamily: Fonts.montserratMedium,
      fontSize: scale(14),
      color: commonColors.Blue
   },
   metaBrandtext: {
      marginTop: scale(8),
      fontFamily: Fonts.montserratBold,
      fontSize: scale(21),
      color: commonColors.Blue
   },
   colorView: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center'
   },
   mainProductView: {
      flexDirection: 'row',
      flex: 1,
      width: (deviceWidth - 100),
      justifyContent: 'space-between',
      marginLeft: scale(80)
   },
   reviewView: {
      marginVertical: scale(10),
      left: scale(10)
   },
   defaultImageView: {
      alignSelf: 'center',
      width: scale(200),
      height: scale(240),
      margin: scale(10)
   },
   imageView: {
      width: '75%',
      height: scale(280),
      borderWidth: 2,
      borderColor: '#B2B2B2',
      borderRadius: scale(10)
   },
   dropdownImage: {
      width: "25%",
      marginLeft: scale(10),
      top: scale(10),
      left: scale(5)
   },
   productDetailsView: {
      flexDirection: 'row',
      flex: 1,
      height: scale(280),
      width: (deviceWidth - 32),
      justifyContent: 'space-between'
   },
   renderProductDetailView: {
      width: '25%',
      height: scale(280),
      flexDirection: "column"
   },
   dropdown: {
      paddingHorizontal: scale(4),
      alignSelf: 'center',
      textAlign: 'center',
      color: commonColors.Blue,
      justifyContent: 'center',
      width: '100%',
      height: '100%',
   },
   placeholderStyle: {
      fontSize: scale(12),
      fontFamily: Fonts.montserratMedium,
      alignSelf: 'center',
      textAlign: 'center',
      color: commonColors.Blue
   },
   selectedTextStyle: {
      fontSize: scale(14),
      textAlign: 'center',
      fontFamily: Fonts.montserratMedium,
      color: commonColors.Blue
   },
   textItem: {
      color: commonColors.Blue,
      fontSize: scale(12),
      alignSelf: 'center',

      fontFamily: Fonts.montserratMedium
   },
   sortAndFilterButtonStyle2: {
      marginTop: scale(8),
      flexDirection: 'row',
      height: scale(40),
      width: scale(90),
      justifyContent: 'space-between',
      alignSelf: 'center',
      borderColor: commonColors.Blue,
      borderWidth: 1,
      borderRadius: 5
   },
   sortAndFilterButtonStyle3: {
      flexDirection: 'row',
      height: scale(40),
      width: scale(90),
      justifyContent: 'space-between',
      alignSelf: 'center',
      borderColor: commonColors.Blue,
      borderWidth: 1,
      borderRadius: 5
   },
   schoolouterViewModalStyle: {
      height: deviceHeight,
      backgroundColor: commonColors.modalBackground,
      width: deviceWidth,
      justifyContent: 'flex-end',
   },
   schoolcenterView: {
      flex: 1,
      justifyContent: 'flex-end',
      marginTop: 10,
   },
   schoolmodalView: {
      backgroundColor: 'white',
      width: deviceWidth,
      borderRadius: 10,
      paddingVertical: scale(20),
      paddingHorizontal: scale(20),
      shadowColor: '#000',
      shadowOffset: {
         width: 0,
         height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,
   },
})