import { React, importImages, moment, TouchableOpacity, TextInput, useState, useEffect, showSimpleAlert, Request, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale } from '../../utils/importLibrary'
import Share from 'react-native-share';
import CustomButton from '../../components/CustomButton';
import { BackHandler, Alert } from 'react-native';

export default function Shareyourpurchase({ route, navigation }) {
    const [sharelink, setsharelink] = useState('');
    const [points, setpoints] = useState('');

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', async () => {
            let params = {
                type: 2
            }
            let response = await Request.post('get-reward-point.php', params)
            console.log("response", response);
            if (response) {
                setpoints(response.data.points)
            }
        });
        return unsubscribe;
    }, [navigation])
    useEffect(() => {
        const backAction = () => {
            navigation.navigate('ShopStackNavigator')
            return true
        };

        const backHandler = BackHandler.addEventListener(
            'hardwareBackPress',
            backAction,
        );

        return () => backHandler.remove();
    }, []);

    const sharepurchase = async () => {

        let params = {
            order_id: route.params.orderId,
        }
        let response = await Request.post('add-share.php', params)
        console.log("response", response);
        if (response.success == true) {
            setsharelink(response.data.link)
            const shareItem = response.data.link
            const shareOptions = {
                subject: 'Gift Center',
                url: shareItem
            };
            const response2 = Share.open(shareOptions)
                .then(res => {
                    console.log('res', res);
                })
                .catch(err => {
                    navigation.navigate('ShopStackNavigator')
                    err && console.log(err);
                });
            return response2;
        }
    }

    return (
        <View style={styles.mainViewStyle}>
            <TouchableOpacity style={{ height: scale(30), width: scale(30), alignSelf: "flex-end", marginRight: scale(15), marginTop: scale(15) }} onPress={() => { navigation.navigate('ShopStackNavigator') }}>
                <View style={{}}>
                    <Image source={importImages.thankyoucross} style={{ height: scale(25), width: scale(25), }} />
                </View>
            </TouchableOpacity>
            <View style={{ justifyContent: "center", marginTop: 20, alignSelf: 'center' }}>

                <Text style={{ fontSize: scale(20), color: commonColors.Blue, fontFamily: Fonts.montserratMedium }}>{'Share Your Purchase'}</Text>
                <Text style={{ fontSize: scale(16), color: commonColors.Grey, fontFamily: Fonts.montserratMedium, textAlign: 'center' }}>{'+' + points + ' ' + 'points'}</Text>
            </View>
            <CustomButton
                onPress={() => sharepurchase()}
                title={'Share your purchase'}
                style={styles.submitButton}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    mainViewStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    submitButton: {
        marginTop: scale(40),
        width: deviceWidth - 50,
        alignSelf: "center",
    },
})