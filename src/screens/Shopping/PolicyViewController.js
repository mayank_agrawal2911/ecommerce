import { React, TouchableOpacity, WebView, FlatList, useState, CommonActions, useEffect, StyleSheet, Text, View, Header, Image, importImages, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, StorageService, scale } from '../../utils/importLibrary'

export default function PolicyViewController({ route, navigation }) {
    const { slug } = route.params
    /**
     * Render Method
     */
 
    return (
        <View style={styles.container}>
            <Header
                leftBtnOnPress={() => navigation.goBack()}
                leftBtn={<Image source={importImages.blueBackArrowImage}></Image>}
                style={{ backgroundColor: commonColors.White, justifyContent: 'center' }}
                headerTitle={slug.article_name}
                titleStyle={styles.fontStyle}
            />
            <WebView
                originWhitelist={['*']}
                scalesPageToFit
                bounces={false}
                showsVerticalScrollIndicator={false}
                style={{ marginHorizontal: scale(25), }}
                source={{ html: slug.article_content }}
            >
            </WebView>
        </View>
    )
}

/**
 * UI of About Screen
 */
const styles = StyleSheet.create({
    fontStyle: {
        alignSelf: 'center',
        fontSize: 18,
        fontFamily: Fonts.montserratBold,
        color: commonColors.Blue,
        textTransform: 'capitalize'
    },
    container: {
        backgroundColor: commonColors.White,
        flex: 1
    }
})