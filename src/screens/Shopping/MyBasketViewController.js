import { React, KeyboardAvoidingView, Request, KeyboardAwareScrollView, importImages, useRef, showSimpleAlert, TouchableOpacity, TextInput, useState, useEffect, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale, Alert } from '../../utils/importLibrary'
import RBSheet from "react-native-raw-bottom-sheet";
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import HTMLView from 'react-native-htmlview';
import HTML from 'react-native-render-html';

export default function MyBasketViewController({ route, navigation }) {
   const [arrMainCategories, setArrMainCategories] = useState([])
   const [giftOptionClick, setGiftOptionClick] = useState(false)
   const [promoCodeClick, setPromoCodeClick] = useState(false)
   const [chooseOfferClick, setChooseOfferClick] = useState(false)
   const [promoCodeText, setPromoCodeText] = useState('')
   const [selectgiftwrap, setselectgiftwrap] = useState(false)
   const [selectgiftmessage, setselectgiftmessage] = useState(false)
   const [motherDay, setmotherDay] = useState(false)
   const [bogofree, setbogofree] = useState(false)
   const [giftMessage, setGiftMessage] = useState('')
   const [isModalVisible, setModalVisible] = useState(false);
   const [totalScore, settotalScore] = useState('')
   const [subtotal, setsubtotal] = useState('')
   const [discount, setdiscount] = useState('')
   const [pointgained, setpointgained] = useState('')
   const [offerData, setofferData] = useState([])
   const [loginKey, setloginKey] = useState('')
   const [promocodeamt, setpromocodeamt] = useState('')
   const [noproduct, setnoproduct] = useState('');
   const [coupon_id, setcoupon_id] = useState('');
   const [applyoffer, setapplyoffer] = useState('');
   const [promocodeapplyoffer, setpromocodeapplyoffer] = useState(false);
   const [hasgiftoffer, sethasgiftoffer] = useState(false);
   const [hasofferData, sethasofferData] = useState([]);
   const [applyofferquanity, setapplyofferquanity] = useState('');
   const [offerID, setofferID] = useState('');
   const [productgiftId, setproductgiftId] = useState('');
   const [loadervisible, setloadervisible] = useState(false)
   const [showsample, setshowsample] = useState('')

   const sheetRef = useRef()
   useEffect(() => {
      const unsubscribe = navigation.addListener('focus', async () => {
         await checkLogin();
         getBasketData();
         getofferData()
      });
      return unsubscribe;
   }, [navigation,])

   const checkLogin = async () => {
      console.log("giftMessage", giftMessage);
      let userData = await StorageService.getItem(StorageService.STORAGE_KEYS.USER_DETAILS)
      setloginKey(userData)
   }
   const getBasketData = async () => {
      let params = { func: 'fetch', domain_id: 1, type: 'refresh' }
      setModalVisible(true)
      let response = await Request.post('manage-cart.php', params)
      setModalVisible(false)
      if (response.success == true) {
         setArrMainCategories(response.data.products)
         settotalScore(response.data.grand_total)
         setpointgained(response.data.gained_loyalty)
         setdiscount(response.data.discount)
         setsubtotal(response.data.sub_total)
         setshowsample(response.data.has_sample)
         setPromoCodeText('')
         if (response.data.products.length == 0) {
            setnoproduct('Your cart is empty')
         }
      } else {
         showSimpleAlert(response.message)
      }
   }

   const getofferData = async () => {
      let response = await Request.post('fetch_offer.php')
      if (response.success == true) {
         setofferData(response.data)
         setapplyoffer('')
      } else {
         showSimpleAlert(response.message)
      }
   }

   const favoriteUnFavoriteButtonClick = (item, index) => {
   }

   const btnGiftOptionClick = () => {
      setGiftOptionClick(!giftOptionClick)
   }

   const btnPromoCodeClick = () => {
      setPromoCodeClick(!promoCodeClick)
   }

   const btnChooseOfferClick = () => {
      setChooseOfferClick(!chooseOfferClick)
   }

   const checkOutNavigation = async () => {
      var arrTimingArray = []
      var dict = {
         selectgiftwrap: selectgiftwrap,
         giftMessage: giftMessage,
         discount: discount,
         pointgained: pointgained,
         promoCodeText: promoCodeText,
         coupon_code: coupon_id
      }
      arrTimingArray.push(dict)
      if (loginKey) {
         const response = await Request.post('check-cart-stock.php')
         console.log("responseresponse:-->", response);

         if (response.success == true) {
            StorageService.saveItem(StorageService.STORAGE_KEYS.BASKET_DATA, arrTimingArray)
            setselectgiftwrap(false)
            setGiftMessage('')
            setselectgiftmessage(false)
            if (offerData.length > 0 && applyoffer == '') {
               Alert.alert(
                  ConstantsText.appName,
                  'Are you sure to proceed without applying offer?',
                  [{ text: "No", onPress: () => { } },
                  {
                     text: "Yes, Proceed", onPress: () => {
                        navigation.navigate('CheckOutViewController')
                     }
                  },

                  ],
                  { cancelable: false }
               );
            }
            else {

               navigation.navigate('CheckOutViewController')

            }
         } else {
            showSimpleAlert(response.message.replace(/<[^>]+>/g, ''))
         }

      }
      else {
         navigation.navigate('AuthSelection', { authFlag: 1 })

      }
   }

   const onpromocodeApply = async () => {
      const ConfirmValid = validationOfField();
      if (ConfirmValid) {
         let params = {
            func: 'coupon',
            coupon_code: promoCodeText
         }
         setpromocodeapplyoffer(true)
         let response = await Request.post('manage-cart.php', params)
         setModalVisible(false)
         setpromocodeapplyoffer(false)
         if (response.success == true) {
            settotalScore(response.data.grand_total)
            setpointgained(response.data.gained_loyalty)
            setdiscount(response.data.discount)
            setshowsample(response.data.has_sample)
            setsubtotal(response.data.sub_total)
            setcoupon_id(response.data[0].coupon_id)
            showSimpleAlert(response.message)
            let params = {
               func: 'fetch',
               coupon_code: response.data[0].coupon_id
            }
            let response2 = await Request.post('manage-cart.php', params)
            if (response2.success == true) {
               settotalScore(response2.data.grand_total)
               setpointgained(response2.data.gained_loyalty)
               setdiscount(response2.data.discount)
               setshowsample(response2.data.has_sample)
               setsubtotal(response2.data.sub_total)
            }
            else {
               setPromoCodeText('')
               showSimpleAlert(response2.message)
            }
         } else {
            showSimpleAlert(response.message)
         }
      }
   }
   const validationOfField = () => {

      if (promoCodeText == '') {
         showSimpleAlert('Please enter promocode')
         return false;
      }

      else {
         return true;
      }
   }

   const productDecrease = async (item) => {
      let params = {
         func: 'update', domain_id: 1, product_id: item.product_id, quantity: item.product_qty - 1
      }
      let response = await Request.post('manage-cart.php', params)
      if (response.success == true) {
         setArrMainCategories(response.data.products)
         settotalScore(response.data.grand_total)
         setpointgained(response.data.gained_loyalty)
         setdiscount(response.data.discount)
         setshowsample(response.data.has_sample)
         setsubtotal(response.data.sub_total)
      } else {
         showSimpleAlert(response.message)
      }
   }
   const productIncrease = async (item) => {
      let params = {
         func: 'update', domain_id: 1, product_id: item.product_id, quantity: item.product_qty + 1
      }
      let response = await Request.post('manage-cart.php', params)
      setModalVisible(false)
      if (response.success == true) {
         setArrMainCategories(response.data.products)
         settotalScore(response.data.grand_total)
         setpointgained(response.data.gained_loyalty)
         setdiscount(response.data.discount)
         setshowsample(response.data.has_sample)
         setsubtotal(response.data.sub_total)
      } else {
         showSimpleAlert(response.message)
      }
   }
   const deleteProduct = async (item) => {
      let params = { func: 'delete', domain_id: 1, product_id: item.product_id }
      // setModalVisible(true)
      let response = await Request.post('manage-cart.php', params)
      // setModalVisible(false)
      if (response.success == true) {
         setArrMainCategories(response.data.products)
         settotalScore(response.data.grand_total)
         setpointgained(response.data.gained_loyalty)
         setdiscount(response.data.discount)
         setshowsample(response.data.has_sample)
         setsubtotal(response.data.sub_total)
         if (response.data.products.length == 0) {
            setnoproduct('Your cart is empty')
         }
      } else {
         showSimpleAlert(response.message)
      }
   }
   const renderNewArrivalItem = ({ item, index }) => {
      const productTotalPrice = item.product_price * item.product_qty
      return (
         <View style={{
            width: (deviceWidth - 16),
            marginHorizontal: 8,
            marginVertical: 8,
            backgroundColor: commonColors.White,
            borderRadius: 10,
            shadowColor: commonColors.Blue,
            shadowOffset: { width: 0, height: 0 },
            shadowOpacity: 0.5,
            shadowRadius: 2,
            elevation: 5,
            height: scale(195),
            justifyContent: 'center'
         }} >
            <View style={{}}>
               <View style={{ flexDirection: 'row', marginLeft: 12, marginVertical: scale(10), }}>
                  <View style={styles.productImageView}>
                     {item.product_img ? <Image source={{ uri: item.product_img }} style={styles.productImage} />
                        : <Image source={importImages.defaultProductImage} style={styles.productImage} />}
                  </View>
                  <View style={styles.productView}>
                     <View style={{}}>
                        <Text numberOfLines={2} style={[styles.nameText, {}]}>{item.brand_name}</Text>
                        <Text numberOfLines={2} style={[styles.familynameText, { width: scale(210), paddingRight: scale(5), }]}>{item.family_name}</Text>
                        <Text numberOfLines={1} style={[styles.productNameText, { width: scale(210), paddingRight: scale(5) }]}>{item.fam_name}</Text>
                        <Text numberOfLines={1} style={[styles.productNoText, {}]}>SKU: {item.barcode}</Text>

                     </View>
                     <View style={{ flexDirection: 'row', marginTop: scale(10) }}>
                        <Text style={styles.priceText}>Price : </Text>
                        <Text style={styles.priceText}>{item.product_price} JD's</Text>
                     </View>

                     <View style={styles.quantityView}>
                        <Text style={styles.quantityText}>QTY </Text>
                        {item.is_voucher == 1 ? null :
                           item.is_gift == '1' ? null :
                              <TouchableOpacity
                                 disabled={item.product_qty <= 1 ? true : false}
                                 style={{ height: scale(15), width: scale(15), alignSelf: 'center', justifyContent: 'center', }}
                                 onPress={() => {
                                    productDecrease(item)
                                 }}>
                                 <Image source={importImages.QTYMinus} style={{ alignSelf: 'center', height: scale(15), width: scale(15), }} />
                              </TouchableOpacity>}
                        <Text style={styles.product_qtytext}>{item.product_qty}</Text>
                        {item.is_voucher == 1 ? null :
                           item.is_gift == '1' ? null :
                              <TouchableOpacity onPress={() => { productIncrease(item) }} style={{ height: scale(15), width: scale(15), alignSelf: 'center', justifyContent: 'center', }}>
                                 <Image source={importImages.QTYPlus} style={{ alignSelf: 'center', height: scale(15), width: scale(15), }} />
                              </TouchableOpacity>}
                     </View>
                  </View>
                  <TouchableOpacity style={{ justifyContent: "center", marginRight: scale(13), marginTop: scale(10), width: scale(25), height: scale(25), alignItems: 'center' }} onPress={() => deleteProduct(item, index)}>
                     <Image source={importImages.deleteItem} style={{ height: scale(12), width: scale(10), }} resizeMode='contain' />
                  </TouchableOpacity>
               </View>
            </View>
         </View >


      )
   }

   const calloffer = async (item) => {
      let params = {
         offer_id: item.offer_id,
         product_id: item.offer_item
      }
      setofferID(item.offer_id)
      setproductgiftId(item.offer_item)
      setloadervisible(true)
      let response = await Request.post('fetch_offer_gifts.php', params)
      console.log("calloffercalloffer", response);
      setModalVisible(false)
      setloadervisible(false)

      if (response.success == true) {
         sethasofferData(response.data)
         sheetRef.current.open()
      } else {
         showSimpleAlert(response.message)
      }
   }

   const apply = async () => {
      let offerID = offerData ? offerData.filter((item) => {
         if (item.isSelected == 1) {
            return item
         }
      }
      ) : ''
      if (offerID == '') {
         showSimpleAlert('Please select any offer')
      } else {
         if (hasgiftoffer == true) {
            offerData.map((item) => {
               if (item.isSelected == 1) {
                  calloffer(item)
               }
            })
         } else {
            console.log("elsee:-->>>>>");
            let params = {
               offer_id: offerID[0].offer_id, offer_item: offerID[0].offer_item ? offerID[0].offer_item : ''
            }
            setloadervisible(true)
            let response2 = await Request.post('apply_offer.php', params)
            setloadervisible(false)
            console.log("response222", response2);
            if (response2.success == true) {
               let params = { func: 'fetch', domain_id: 1, }
               let response = await Request.post('manage-cart.php', params)
               console.log("response22234", response);

               if (response.success == true) {
                  setArrMainCategories(response.data.products)
                  settotalScore(response.data.grand_total)
                  setpointgained(response.data.gained_loyalty)
                  setdiscount(response.data.discount)
                  setshowsample(response.data.has_sample)
                  setsubtotal(response.data.sub_total)
               } else {
                  showSimpleAlert(response.message)
               }
            } else {
               showSimpleAlert(response2.message)
            }
         }
      }
   }

   const selectoffer = async ({ item, index }) => {
      let params = {
         offer_id: item.offer_id
      }
      setapplyoffer(item.offer_id)
      setloadervisible(true)
      let response = await Request.post('fetch_offer.php', params)
      console.log("getofferDataresponse", response);
      let offerData = response.data.map((item) => {
         if (item.isSelected == 1 && item.gift_quantity != '') {
            sethasgiftoffer(true)
         }
         else {
         }
      })
      setModalVisible(false)
      setloadervisible(false)

      if (response.success == true) {
         setofferData(response.data)
      } else {
         showSimpleAlert(response.message)
      }


   }
   const offerrenderItem = ({ item, index }) => {
      return (
         <View style={{ marginTop: 12, marginHorizontal: 20, flexDirection: 'row', alignItems: 'center' }}>
            <TouchableOpacity style={{ marginLeft: 8, flexDirection: 'row' }} onPress={() => selectoffer({ item, index })}>
               <Image source={item.isSelected == 1 ? importImages.Checkbox : importImages.radioUnCheckImage} />
               <Text style={styles.offerTextStyle}>{item.offer_name}</Text>
            </TouchableOpacity>
         </View>
      )
   }

   const selectItem = (mainitem, index) => {
      var tempArry = hasofferData
      var tempData = tempArry[index].isSelected == 1 ? 0 : 1
      tempArry[index].isSelected = tempData
      if (tempData == 1) {
         let tempofferData = hasofferData.map((item) => {
            if (item.product_id != mainitem.product_id) {
               item.isSelected = 0
               sethasgiftoffer([tempofferData])

            }
            else {
               sethasgiftoffer([...tempArry])

            }
            console.log("tempofferData", tempofferData);
         })
      }
      else {
         sethasgiftoffer([...tempArry])
      }

   }
   const renderofferData = ({ item, index }) => {
      console.log("item", item);
      return (
         <TouchableWithoutFeedback >
            <View style={{
               backgroundColor: commonColors.White,
               borderRadius: 10,
               shadowColor: commonColors.Blue,
               shadowOffset: { width: 0, height: 0 },
               shadowOpacity: 0.5,
               shadowRadius: 5,
               elevation: 5,
               justifyContent: 'center',
               marginVertical: scale(10),
               height: scale(100),
               width: deviceWidth - 40,
               marginHorizontal: scale(10),
            }}>

               <View style={{ marginHorizontal: scale(10), flexDirection: "row", alignItems: "center" }}>
                  <TouchableOpacity onPress={() => selectItem(item, index)}  >
                     <Image source={item.isSelected == 1 ? importImages.Checkbox : importImages.radioUnCheckImage} style={{}} />
                  </TouchableOpacity>
                  <View style={{ marginHorizontal: scale(5) }}>
                     {item.product_img != '' ?
                        <Image source={{ uri: item.product_img }} resizeMode='contain' style={{ height: scale(70), width: scale(70), borderRadius: scale(10) }} />
                        : <Image source={importImages.defaultProductImage} resizeMode='contain' style={{ height: scale(70), width: scale(70), borderRadius: scale(10) }} />
                     }
                  </View>
                  <View style={{ flexDirection: "column", width: scale(140), }}>
                     <Text style={{ paddingHorizontal: scale(5), fontSize: scale(16), fontFamily: Fonts.montserratMedium, color: commonColors.Blue }}>{item.brand_name}</Text>
                     <Text numberOfLines={3} style={{ paddingHorizontal: scale(5), fontSize: scale(12), fontFamily: Fonts.montserratRegular, color: commonColors.Blue }}>{item.family_name
                     }</Text>
                  </View>
                  <View style={{ justifyContent: "center" }}>
                     <Text style={{ fontSize: scale(16), fontFamily: Fonts.montserratMedium, color: commonColors.Blue, textAlign: "center" }}>{item.main_price + " JD's"}</Text>
                  </View>
               </View>
            </View>
         </TouchableWithoutFeedback>
      )
   }

   const addtogiftoffer = async () => {
      let unselectedData = []
      const data = hasofferData.map((item) => {
         if (item.isSelected == 0) {
            unselectedData.push(item)
         }
      })
      if (unselectedData.length == hasofferData.length) {
         showSimpleAlert('Please select any product')
      }
      else {
         const data = hasofferData.map((item) => {
            if (item.isSelected == 1) {
               applycheckoffer(item)
            }

         })

      }
   }
   const applycheckoffer = async (item) => {
      let params = {
         offer_id: offerID,
         offer_item: productgiftId
      }
      setModalVisible(true)
      let response = await Request.post('apply_offer.php', params)
      if (response.success == true) {
         setModalVisible(false)
         setapplyoffer(response.data.offer_prdct_qty)
         applygiftoffer(item, response.data.offer_prdct_qty)

      } else {
         showSimpleAlert(response.message)
      }
   }
   const applygiftoffer = async (item, qty) => {
      console.log("qtyqtyqtyqtyqty", qty);
      let params = {
         offer_id: offerID,
         func: 'add-gift',
         quantity: qty,
         product_id: item.product_id
      }
      setModalVisible(true)
      let response = await Request.post('manage-cart.php', params)
      if (response.success == true) {
         sheetRef.current.close()
         setModalVisible(false)
         if (response.data) {
            setArrMainCategories(response.data.products)
            settotalScore(response.data.grand_total)
            setpointgained(response.data.gained_loyalty)
            setdiscount(response.data.discount)
            setshowsample(response.data.has_sample)
            setsubtotal(response.data.sub_total)
         }

      } else {
         showSimpleAlert(response.message)
      }
   }
   return (
      <View style={styles.mainViewStyle}>
         <View style={styles.headerLogoStyle}>
            <Text style={styles.headerText}>MY BASKET</Text>
         </View>
         <View style={styles.lineView}></View>
         <KeyboardAwareScrollView
            contentContainerStyle={{ flexGrow: 1, }}
            bounces={false}
            keyboardShouldPersistTaps={'always'}
            showsVerticalScrollIndicator={false}
            extraScrollHeight={10}>
            <ScrollView style={{ marginVertical: 8, }} contentContainerStyle={{ flexGrow: 1 }} scrollEnabled={true} showsVerticalScrollIndicator={false} nestedScrollEnabled={true}>

               {arrMainCategories && !isModalVisible && arrMainCategories.length > 0 ? <FlatList
                  bounces={false}
                  renderItem={renderNewArrivalItem}
                  data={arrMainCategories}
                  extraData={arrMainCategories}
                  style={{ marginTop: 0, }}
                  pagingEnabled={true}
                  showsVerticalScrollIndicator={false}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item, index) => `${index}-id`}
               /> : null}
               {arrMainCategories && arrMainCategories.length > 0 && !isModalVisible ?
                  <View>
                     <View>
                        <TouchableOpacity style={styles.expandButtonClick} onPress={() => btnGiftOptionClick()}>
                           <Text style={styles.optionTextStyle}>GIFT OPTIONS</Text>
                           <Image source={importImages.dropDownArrow} style={{ marginRight: 4 }} />
                        </TouchableOpacity>
                        {
                           giftOptionClick ? <View>
                              <View style={{ marginTop: 12, marginHorizontal: 20, flexDirection: 'row', alignItems: 'center' }}>
                                 <TouchableOpacity onPress={() => { setselectgiftwrap(!selectgiftwrap) }} style={{ marginLeft: 8, flexDirection: 'row' }}>
                                    <Image source={selectgiftwrap ? importImages.BlueTick : importImages.unCheckImage} />
                                    <Text style={{ marginLeft: 8, fontFamily: Fonts.montserratMedium, fontSize: 13, color: commonColors.Blue }}>Add a gift wrap.</Text>

                                 </TouchableOpacity>
                              </View>
                              <View style={{ marginTop: 12, marginHorizontal: 20, flexDirection: 'row', alignItems: 'center' }}>
                                 <TouchableOpacity onPress={() => { setselectgiftmessage(!selectgiftmessage) }} style={{ marginLeft: 8, flexDirection: 'row' }}>
                                    <Image source={selectgiftmessage ? importImages.BlueTick : importImages.unCheckImage} />
                                    <Text style={{ marginLeft: 8, fontFamily: Fonts.montserratMedium, fontSize: 13, color: commonColors.Blue }}>Add a gift message.</Text>

                                 </TouchableOpacity>
                              </View>
                              {selectgiftmessage ?
                                 <View style={styles.emailIdText}>
                                    <TextInput
                                       placeholder="gift message"
                                       style={{
                                          color: commonColors.Blue,
                                          fontSize: scale(12),
                                          fontFamily: Fonts.montserratMedium,
                                       }}
                                       placeholderTextColor={commonColors.Grey}
                                       keyboardType='email-address'
                                       multiline={true}
                                       value={giftMessage}
                                       autoCapitalize='none'
                                       onChangeText={(value) => setGiftMessage(value)}
                                    />
                                 </View> : null}

                           </View> : null
                        }
                     </View>

                     <View style={{ marginVertical: scale(10) }}>
                        <TouchableOpacity style={styles.expandButtonClick} onPress={() => btnPromoCodeClick()}>
                           <Text style={styles.optionTextStyle}>PROMO CODE</Text>
                           <Image source={importImages.dropDownArrow} style={{ marginRight: 4 }} />
                        </TouchableOpacity>
                        {
                           promoCodeClick ? <View>
                              <View style={{ marginTop: 12, marginHorizontal: 20, flexDirection: 'row', alignItems: 'center' }}>
                                 <TextInput
                                    style={{
                                       marginLeft: 8, alignSelf: 'center', height: scale(35), paddingHorizontal: 8, width: 190, color: commonColors.Blue, fontFamily: Fonts.montserratRegular, fontSize: 12, borderWidth: 1, borderColor: commonColors.Blue,
                                    }}
                                    placeholder={''}
                                    placeholderTextColor={commonColors.Blue}
                                    value={promoCodeText}
                                    multiline={false}
                                    onChangeText={(dummyvehicleType) => setPromoCodeText(dummyvehicleType)}
                                    underlineColorAndroid='transparent'
                                 />
                                 {promocodeapplyoffer == false ?
                                    <TouchableOpacity onPress={() => onpromocodeApply()} style={{ marginLeft: 12, height: scale(35), width: 85, backgroundColor: commonColors.Blue, justifyContent: 'center', alignItems: 'center' }}>
                                       <Text style={{ fontFamily: Fonts.montserratMedium, fontSize: 18, color: commonColors.White, textAlign: "center" }}>Apply</Text>
                                    </TouchableOpacity> :
                                    <View style={{ marginLeft: scale(25), justifyContent: 'center', alignItems: 'center' }}>
                                       <BallIndicator visible={true} size={30} />
                                    </View>}
                              </View>
                           </View> : null
                        }
                     </View>
                     {offerData.length > 0 ?
                        <View>
                           <TouchableOpacity style={styles.expandButtonClick} onPress={() => btnChooseOfferClick()}>
                              <Text style={styles.optionTextStyle}>CHOOSE OFFER</Text>
                              <Image source={importImages.dropDownArrow} style={{ marginRight: 4 }} />
                           </TouchableOpacity>
                           {
                              chooseOfferClick ?
                                 <View>
                                    <FlatList
                                       data={offerData}
                                       extraData={offerData}
                                       renderItem={offerrenderItem}
                                    />
                                    <TouchableOpacity style={styles.applySmallButtonStyle} onPress={apply} >
                                       <Text style={styles.applySmallTextStyle}>Apply</Text>
                                    </TouchableOpacity>

                                 </View> : null
                           }
                        </View> : null}
                     {showsample != 0 ?
                        <View>
                           <View style={styles.expandButtonClick}>
                              <Text style={styles.optionTextStyle}>SAMPLE</Text>

                           </View>
                           <Text style={styles.sampleText}>You will get {showsample}  free sample based on your order</Text>
                        </View>
                        : null}
                     <View>
                        <View style={styles.expandButtonClick}>
                           <Text style={styles.optionTextStyle}>ORDER SUMMARY</Text>
                        </View>
                        <View style={styles.orderSummaryViewStyle}>
                           <Text style={styles.orderSummaryTextStyle}>Sub Total</Text>
                           <Text style={styles.orderSummaryValueStyle}>{subtotal ? parseFloat(subtotal).toFixed(2) : 0} JD's</Text>
                        </View>
                        <View style={styles.orderSummaryViewStyle}>
                           <Text style={styles.orderSummaryTextStyle}>Discount</Text>
                           <Text style={styles.orderSummaryValueStyle}>{discount ? parseFloat(discount).toFixed(2) : 0} JD's</Text>
                        </View>
                        <View style={styles.orderSummaryViewStyle}>
                           <Text style={styles.orderSummaryTextStyle}>Gift Wrap</Text>
                           <Text style={styles.orderSummaryValueStyle}>{selectgiftwrap ? 'Yes' : 'No'}</Text>
                        </View>
                        <View style={styles.orderSummaryViewStyle}>
                           <Text style={styles.orderSummaryTextStyle}>Point Gained</Text>
                           <Text style={styles.orderSummaryValueStyle}>{pointgained ? pointgained : 0}</Text>
                        </View>
                        <View style={styles.orderSummaryViewStyle}>
                           <Text style={styles.orderSummaryTextStyle}>Total</Text>

                           <Text style={styles.orderSummaryValueStyle}>{totalScore ? parseFloat(totalScore).toFixed(2) : 0} JD's</Text>
                        </View>
                     </View>
                  </View> : null}

               {arrMainCategories && arrMainCategories.length == 0 && !isModalVisible ? <View style={styles.nodatafoundView}>
                  <Text style={styles.transactionText}>{noproduct}</Text>
               </View> : null}
             
            </ScrollView>
         </KeyboardAwareScrollView>

         {arrMainCategories && arrMainCategories.length > 0 && !isModalVisible ?
            <TouchableOpacity onPress={() => checkOutNavigation()} style={{ height: scale(50), backgroundColor: commonColors.Blue, marginBottom: scale(70), marginHorizontal: 20, justifyContent: 'center', alignItems: 'center' }}>
               <Text style={{ fontFamily: Fonts.montserratBold, fontSize: 15, color: commonColors.White }}>Checkout</Text>
            </TouchableOpacity> : null}
         <View
            style={{
               flex: 1,
               justifyContent: "center",
               alignItems: "center",
            }}
         >

            <RBSheet
               ref={sheetRef}
               closeOnDragDown={true}
               height={scale(500)}
               closeOnPressMask={false}
               customStyles={{
                  container: {
                     borderTopLeftRadius: scale(20),

                     borderTopRightRadius: scale(20)
                  },
                  wrapper: {
                     borderTopLeftRadius: scale(10),
                  },
                  draggableIcon: {
                     backgroundColor: "#000"
                  }
               }}
            >
               <View style={{ flexDirection: "row", justifyContent: "space-evenly" }}>
                  <TouchableOpacity onPress={addtogiftoffer}>
                     <View style={{ justifyContent: "center", alignItems: "center", backgroundColor: 'green', height: scale(30), width: scale(100), borderRadius: scale(5) }}><Text style={{ color: commonColors.White, fontSize: scale(14), fontFamily: Fonts.montserratMedium }}>{'add gift'}</Text></View>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => {
                     sheetRef.current.close()
                  }}>
                     <View style={{ justifyContent: "center", alignItems: "center", backgroundColor: '#CC5454', height: scale(30), width: scale(100), borderRadius: scale(5) }}><Text style={{ color: commonColors.White, fontSize: scale(14), fontFamily: Fonts.montserratMedium }}>{'back to offer'}</Text></View>
                  </TouchableOpacity>
               </View>
               <FlatList
                  bounces={false}
                  renderItem={renderofferData}
                  nestedScrollEnabled={true}
                  scrollEnabled={true}
                  extraData={hasofferData}
                  showsVerticalScrollIndicator={false}
                  contentContainerStyle={{ justifyContent: "center", alignItems: "center" }}
                  data={hasofferData}
               />
            </RBSheet>
         </View>
         {(loadervisible || isModalVisible) && loginKey ?
            <BallIndicator visible={true} /> : null
         }

      </View>
   )
}

const styles = StyleSheet.create({
   mainViewStyle: {
      flex: 1,
      backgroundColor: 'white'
   },
   headerLogoStyle: {
      justifyContent: 'center',
      alignItems: 'center',
      height: 44
   },
   expandButtonClick: {
      marginTop: 16,
      marginHorizontal: 20,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center'
   },
   orderSummaryViewStyle: {
      marginTop: 12,
      marginHorizontal: 20,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center'
   },
   nodatafoundView: {
      marginTop: scale(40),
      justifyContent: 'center',
      alignContent: 'center',
      alignSelf: 'center',
   },
   transactionText: {
      fontSize: scale(16),
      color: commonColors.Blue,
      fontFamily: Fonts.montserratMedium,
      alignSelf: "center"
   },
   orderSummaryTextStyle: {
      marginLeft: 4,
      fontFamily: Fonts.montserratSemiBold,
      fontSize: 13,
      color: commonColors.Blue
   },
   orderSummaryValueStyle: {
      fontFamily: Fonts.montserratSemiBold,
      fontSize: 13,
      color: commonColors.Blue
   },
   sampleText: {
      fontSize: scale(15),
      color: commonColors.Blue,
      paddingTop: scale(5),
      paddingHorizontal: scale(20)
   },
   optionTextStyle: {
      fontFamily: Fonts.montserratBold,
      fontSize: 15,
      color: commonColors.Blue
   },
   applySmallButtonStyle: {
      marginLeft: scale(20),
      marginTop: scale(10),
      height: scale(27),
      width: scale(63),
      backgroundColor: commonColors.Blue,
      justifyContent: 'center',
      alignItems: 'center'
   },
   applySmallTextStyle: {
      fontFamily: Fonts.montserratMedium,
      fontSize: 13,
      color: commonColors.White
   },
   headerText: {
      fontFamily: Fonts.montserratSemiBold,
      fontSize: 20,
      color: commonColors.Blue
   },
   offerTextStyle: {
      marginLeft: 8,
      fontFamily: Fonts.montserratMedium,
      fontSize: 13,
      color: commonColors.Blue,
      width: scale(150)
   },
   emailIdText: {
      borderWidth: 0.8,
      marginTop: scale(20),
      borderColor: commonColors.Blue,
      borderRadius: scale(5),
      color: commonColors.Blue,
      fontSize: scale(12),
      fontFamily: Fonts.montserratMedium,
      paddingLeft: scale(10),
      width: deviceWidth - 40,
      marginHorizontal: scale(20),
      height: scale(100),
   },
   quantityText: {
      fontFamily: Fonts.montserratMedium,
      fontSize: 12,
      color: commonColors.Blue,
      marginRight: 16
   },
   product_qtytext: {
      fontFamily: Fonts.montserratMedium,
      fontSize: 15,
      color: commonColors.Blue,
      marginHorizontal: 8
   },
   productPrice: {
      fontFamily: Fonts.montserratMedium,
      fontSize: scale(12),
      color: commonColors.Blue
   },
   productNoText: {
      fontFamily: Fonts.montserratMedium,
      fontSize: scale(12),
      color: commonColors.qtySizeMlText
   },
   familyNameText: {
      paddingTop: scale(10),
      fontFamily: Fonts.montserratRegular,
      fontSize: scale(14),
      color: commonColors.Blue
   },
   productNameText: {
      fontFamily: Fonts.montserratSemiBold,
      fontSize: 16,
      color: commonColors.Blue
   },
   quantityView: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: scale(5)
   },
   productView: {
      marginLeft: 8,
      flex: 1,
      marginTop: scale(10)
   },
   productImageView: {
      height: scale(140),
      width: scale(100),
      alignSelf: 'center',
      borderWidth: 2,
      borderRadius: scale(10),
      borderColor: '#BFCAD4',
      marginVertical: scale(10)
   },
   productImage: {
      height: scale(120),
      width: scale(80),
      alignSelf: 'center',
      marginVertical: scale(10)
   },
   lineView: {
      width: 300,
      height: 1,
      backgroundColor: 'lightgrey',
      alignSelf: 'center',
   },
   nameText: {
      fontFamily: Fonts.montserratMedium,
      fontSize: scale(18),
      color: commonColors.Blue
   },
   familynameText: {
      paddingVertical: scale(5),
      fontFamily: Fonts.montserratRegular,
      fontSize: 13,
      color: commonColors.Blue
   },
   productNameText: {
      paddingVertical: scale(5),
      fontFamily: Fonts.montserratRegular,
      fontSize: 13,
      color: commonColors.qtySizeMlText
   },
   priceText: {
      fontFamily: Fonts.montserratRegular,
      fontSize: scale(15),
      color: commonColors.Blue
   }
})