import { React, importImages, useRef, TouchableOpacity, useState, useEffect, StyleSheet, Text, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, WebView, apiConfigs, Request } from '../../utils/importLibrary'
export default function CreditCardViewController({ route, navigation }) {
    const [urlData, seturlData] = useState('')
    const [paramData, setparamsData] = useState([])
    const [isModalVisible, setModalVisible] = useState(true);
    const [customerId, setcustomerId] = useState('')
    const webviewRef = useRef();

    /** 
     * Life cycle method 
     */
    useEffect(async () => {

        // The screen is focused
        // Call any action
        await getUrlDeatils()
    }, []);

    /**
     * getUserDetails function get details form storageStorage and set data 
     */
    const getUrlDeatils = async () => {
        console.log("route", route.params.params,);
        let userdetails = await StorageService.getItem(StorageService.STORAGE_KEYS.USER_DETAILS)
        console.log("userdetails:-->", userdetails);
        setcustomerId(userdetails.customer_id)
        const paramsData = route.params.params
        setparamsData(paramsData)


    }
    const onMessage = async (event, eventData) => {
        let data = route.params.params
        let eventorder = JSON.parse(event)
        data.order_id = eventorder.order_id
        // console.log("data", event,eventorder.payment, data);
        if (eventorder.payment == false) {
            navigation.navigate("MyBasketViewController")
        } else {
            let response = await Request.post('payment1.php', data)
            if (response.success == true) {
                console.log("response", response);
                navigation.navigate('ThankyouViewController', { orderId: response.data.order_id })
            }

        }
    }
    /**
    * Render Method
    */
    const IndicatorLoadingView = () => {
        return (
            <BallIndicator visible={true} />
        )
    }
    return (
        <View style={{ backgroundColor: commonColors.White, flex: 1, justifyContent: 'space-between' }}>
            <Header
                // leftBtnOnPress={() => navigation.goBack()}
                // leftBtn={<Image source={importImages.blueBackArrowImage}></Image>}
                style={{ backgroundColor: commonColors.White, justifyContent: 'center' }}
                headerTitle={"Credit Card Payment"}
                titleStyle={styles.fontStyle}
            />
            <WebView
                originWhitelist={['*']}
                scalesPageToFit
                ref={webviewRef}
                bounces={false}
                showsVerticalScrollIndicator={false}
                renderLoading={IndicatorLoadingView}
                startInLoadingState={true}
                domStorageEnabled={true}
                javaScriptEnabled={true}
                onMessage={(event) => {
                    console.log("Event,data", event)
                    const value = JSON.parse(event.nativeEvent.data);
                    onMessage(value, event.nativeEvent.data)
                }}
                source={{ uri: `${apiConfigs.SERVER_API_URL}${'checkout-page.php'}?customer_id=${customerId}` }}
            >

            </WebView>

        </View>
    );
}

/**
 * UI of card screen
 */
const styles = StyleSheet.create({
    container: {
        backgroundColor: commonColors.Blue,
        justifyContent: 'flex-end',
        flex: 1,
    },
    fontStyle: {
        alignSelf: 'center',
        fontSize: 18,
        fontFamily: Fonts.montserratBold,
        color: commonColors.Blue,
        textTransform: 'capitalize'
    },
    Subcontainer: {
        flex: 0.9,
        backgroundColor: commonColors.White,
        shadowColor: commonColors.Black,
        shadowOpacity: 0.4,
        shadowRadius: 5,
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
        elevation: 8,
        paddingTop: 40,
    },

    backButtonImage: {
        marginLeft: 30,
    },

    cardImageStyle: {
        marginTop: 50,
        width: deviceWidth - 115,
    },

    addToWalletStyle: {
        height: 28,
        marginTop: 50,
        borderWidth: 0.67,
        borderColor: commonColors.Blue,
        justifyContent: 'center',
        alignContent: 'center',
        borderRadius: 3
    },

    addToWalletText: {
        fontSize: 14,
        marginStart: 16,
        marginEnd: 16,
        color: commonColors.Blue,
        alignSelf: 'center',
        fontFamily: Fonts.montserratRegular
    },

    activeTextColor: {
        fontFamily: Fonts.montserratSemiBold,
        fontSize: 12,
        width: (deviceWidth - 20) / 2,
    },

    InactiveTextColor: {
        fontFamily: Fonts.montserratMedium,
        fontSize: 12,
    },

})