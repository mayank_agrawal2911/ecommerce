// import { React, Request, showSimpleAlert, ProductComponent, importImages, TouchableOpacity, TextInput, useState, useEffect, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale, Platform } from '../../utils/importLibrary'
// import { Dropdown } from 'react-native-element-dropdown';
// import Share from 'react-native-share';
// import Voice from '@react-native-community/voice';
// import SpeechToText from 'react-native-google-speech-to-text';
// import CustomButton from '../../components/CustomButton';
// import { hasNotch } from 'react-native-device-info';

// export default function ProductListingViewController({ route, navigation }) {
//    const { ColorName, colors, icons, setScheme } = useTheme();
//    const [userData, setUserDate] = useState('')
//    const [searchText, setSearchText] = useState('')
//    const [arrMainCategories, setArrMainCategories] = useState(["NEW", "BRANDS", "FRAGRANCES", "MAKEUP", "SKINCARE", "WATCHES", "WATCHES1"])
//    const [colorCombination, setColorCombination] = useState(["#4B241C", "#DA749B", "#832325", "#A70B23", "#A10034", "#A6374A"])
//    const [colorSelectedIndex, setColorSelectedIndex] = useState(0)
//    const [arrToWhom, setArrToWhom] = useState([])
//    const [sortValue, setSortValue] = useState('');
//    const [filterValue, setFilterValue] = useState(null);
//    const [KeyName, setKeyName] = useState('')
//    const [bannerImage, setbannerImage] = useState([])
//    const [morecategory, setmorecategory] = useState([])
//    const [isModalVisible, setModalVisible] = useState(false);
//    const [loginKey, setloginKey] = useState('')
//    const [filterData, setfilterData] = useState([])
//    const [loadervalue, setloadervalue] = useState(false)
//    const [modalvalues, modalVisible] = useState(false);
//    const [searchData, setsearchData] = useState([]);
//    const [pitch, setPitch] = useState('');
//    const [error, setError] = useState('');
//    const [end, setEnd] = useState('');
//    const [started, setStarted] = useState(false);
//    const [results, setResults] = useState([]);
//    const [mikestop, setMikeStop] = useState(true);
//    const [partialResults, setPartialResults] = useState([]);
//    const [noproduct, setnoproduct] = useState('');
//    const [arrwhomdata, setarrwhomdata] = useState('');
//    const [newfiltertype, setnewfiltertype] = useState('')
//    const [maincatId, setmaincatId] = useState('')
//    const [keytype, setkeytype] = useState('')
//    const [sortfiltertype, setsortfiltertype] = useState('')
//    const [sortfilterword, setsortfilterword] = useState('')
//    const [KeyWord, setKeyword] = useState('')
//    const [pageNumber, setpageNumber] = useState('1')
//    const [limit, setlimit] = useState(0)
//    const [totaldatalength, settotaldatalength] = useState(0)
//    const [subcategoryData, setsubcategoryData] = useState([])
//    const [footermodalVisible, setfootermodalVisible] = useState(false)
//    const [category_id, setcategory_id] = useState('')
//    const [segmentName, setsegmentName] = useState('')
//    const [maincatName, setmaincatName] = useState('')
//    const [subcatName, setsubcatName] = useState('')
//    const [catName, setcatName] = useState('')

//    useEffect(() => {
//       const unsubscribe = navigation.addListener('focus', async () => {
//          await getProductListing()
//       });
//       return unsubscribe;
//    }, [navigation, loadervalue]);
//    const getProductListing = async () => {
//       let userData = await StorageService.getItem(StorageService.STORAGE_KEYS.USER_DETAILS)
//       setloginKey(userData)
//       console.log("route.params", route.params);
//       const keyType = route.params.productItem.type
//       const Keyword = route.params.productItem.key_word
//       const KeyName = route.params.productItem.name ? route.params.productItem.name : route.params.name ? route.params.name : null
//       const arrdata = route.params.arrwhomData ? route.params.arrwhomdata : ''
//       const filterype = route.params.filtertype ? route.params.filtertype : route.params.productItem.filter_type ? route.params.productItem.filter_type : ''
//       const mainCatId = route.params.productItem.main_cat_id ? route.params.productItem.main_cat_id : route.params.maincatId ? route.params.maincatId : ''
//       const filterWord = route.params.maincatId ? route.params.maincatId : route.params.productItem.filter_word ? route.params.productItem.filter_word : route.params.filter_word ? route.params.filter_word : ''
//       const searchData = route.params.qrData ? route.params.qrData : ''
//       const segmentData = route.params.segment_name ? route.params.segment_name : ''
//       const mainCatname = route.params.main_CatName ? route.params.main_CatName : route.params.productItem.name ? route.params.productItem.name : route.params.name ? route.params.name : null
//       const subCatname = route.params.sub_catName ? route.params.sub_catName : ''
//       const catName = route.params.cat_Name ? route.params.cat_Name : ''

//       setnewfiltertype(filterype)
//       setmaincatId(filterWord)
//       setKeyword(Keyword)
//       setkeytype(keyType)
//       setarrwhomdata(route.params.arrwhomData)
//       setKeyName(KeyName)
//       setSearchText(searchData)
//       setsegmentName(segmentData)
//       setmaincatName(mainCatname)
//       setsubcatName(subCatname)
//       setcatName(catName)
//       var brandId = []
//       if (route.params.isfilter == 1) {
//          const brandData = route.params.brandData
//          const keytypeData = route.params.keytype
//          const keywordData = route.params.keyword ? route.params.keyword : route.params.productItem.key_word

//          const filterbrand = brandData.map((item, index) => {
//             return item.brand_id
//          })
//          const filterfragtype = route.params.fragrancetype.map((item, index) => {
//             return item.key
//          })
//          const filtersegment = route.params.filtersegment.map((item, index) => {
//             return item.in_segment
//          })
//          const filterSize = route.params.sizeData.map((item, index) => {
//             return item.key
//          })
//          const rowCollections = route.params.collectionData.map((item, index) => {
//             return item.id
//          })
//          const concernData = route.params.concerndata.map((item, index) => {
//             return item.id
//          })
//          const minprice = route.params.minprice
//          const maxprice = route.params.maxprice

//          let params2 = {
//             key_type: keytypeData ? keytypeData : keytype,
//             key_word: keywordData ? keywordData : KeyWord,
//             max_price: maxprice,
//             min_price: minprice,
//             segment_filter: filtersegment,
//             brand_filter: filterbrand,
//             fragrance_filter: filterfragtype,
//             size_filter: filterSize,
//             concern_filter: concernData,
//             sort: route.params.sortvalue,
//             filter_type: filterype,
//             filter_word: filterWord,
//             start: '0',
//             limit: limit + 20,

//          }
//          setSortValue(route.params.sortvalue)
//          setModalVisible(true)
//          const data = params2
//          let response = await Request.post('get-product-list.php', data)
//          setModalVisible(false)
//          if (response.success == true) {
//             setbannerImage(response.data.banner_image)
//             setmorecategory(response.data.result)
//             setnoproduct('No product found')
//             settotaldatalength(response.data.total)
//             setlimit(limit + 20)
//             await getProductFilterListing()
//          }
//          else {
//             if (response) {
//                showSimpleAlert(response.message)
//             }
//          }
//       } else {
//          let params2 = {
//             filter_type: route.params.productItem.filter_type ? route.params.productItem.filter_type : route.params.filtertype ? route.params.filtertype : '',
//             filter_word: route.params.productItem.filter_word ? route.params.productItem.filter_word : route.params.filter_word ? route.params.filter_word : '',
//             key_type: route.params.productItem.key_type,
//             key_word: Keyword,
//             start: '0',
//             limit: limit + 20,
//          }
//          const data = params2
//          setModalVisible(true)
//          let response = await Request.post('get-product-list.php', data)
//          setModalVisible(false)
//          if (response.success == true) {
//             setbannerImage(response.data.banner_image)
//             setmorecategory(response.data.result)
//             settotaldatalength(response.data.total)

//             setnoproduct('No product found')
//             setSortValue('')
//             setlimit(limit + 20)

//             await getProductFilterListing()
//          }
//          else {
//             if (response) {
//                showSimpleAlert(response.message)
//             }
//          }
//       }
//    }

//    const getProductFilterListing = async () => {
//       const keyType = route.params.productItem.type
//       const KeyName = route.params.productItem.name
//       const Keyword = route.params.productItem.key_word
//       const mainCategory = route.params.productItem.main_cat_id ? route.params.productItem.main_cat_id : ''
//       setKeyName(KeyName)
//       let params = {
//          key_type: route.params.isfilter == 1 ? route.params.keytype : keyType,
//          key_word: route.params.isfilter == 1 ? route.params.keyword ? route.params.keyword : route.params.productItem.key_word : Keyword,
//          main_category_id: mainCategory,
//          category_id: route.params.isfilter == 1 ? route.params.categoryID : ''
//       }
//       setModalVisible(true)
//       let response = await Request.post('get-product-filter-list.php', params)
//       setModalVisible(false)
//       if (response.success == true) {
//          setfilterData(response.data.length > 0 ? response.data[0] : [])
//          setArrToWhom(response.data.length > 0 ? response.data[0].title : [])
//          setsubcategoryData(response.data.length > 0 ? response.data[0].sub_category : [])
//       }
//       else {
//          if (response) {
//             showSimpleAlert(response.message)
//          }
//       }
//    }
//    const sortDataArray = [
//       { label: 'Relevancy', value: 'relevancy' },
//       { label: 'Best Seller', value: 'best-seller' },
//       { label: 'New Arrival', value: 'new-arrival' },
//       { label: 'Price : Low to High', value: 'pasc' },
//       { label: 'Price : High to Low', value: 'pdesc' },
//       { label: 'Alphabetically : A-Z', value: 'aasc' },
//       { label: 'Alphabetically :   Z-A', value: 'adesc' },
//    ];
//    const sortDataArray2 = [
//       { label: 'Relevancy', value: 'relevancy' },
//       { label: 'Price : Low to High', value: 'pasc' },
//       { label: 'Price : High to Low', value: 'pdesc' },
//       { label: 'Alphabetically : A-Z', value: 'aasc' },
//       { label: 'Alphabetically :  Z-A', value: 'adesc' },
//    ];

//    const data = [
//       { label: 'BEST SELLER', value: '1' },
//       { label: 'NEW ARRIVAL', value: '2' },
//       { label: 'FRAGRANCE TYPE', value: '3' },
//       { label: 'SIZE', value: '4' },
//       { label: 'BRAND', value: '5' },
//       { label: 'PRICE', value: '6' }
//    ];

//    const btnBackClick = () => {
//       const categoryFlag = route.params.category ? route.params.category : 0

//       if (categoryFlag == 1) {
//          navigation.navigate('CategoryViewController')
//       }
//       else if (categoryFlag == 2) {
//          navigation.navigate('CategoryViewController')
//       }
//       else {
//          navigation.navigate('ShoppingHomveViewController')
//       }
//    }
//    const onSpeechStart = () => {
//       setStarted('True')
//    };
//    const onSpeechEnd = () => {
//       setStarted(null);
//       setEnd('True');
//    };
//    const onSpeechError = (e) => {
//       setError(JSON.stringify(e.error));
//    };
//    const onSpeechResults = async (e) => {
//       setResults(e.value)
//       await Voice.destroy()
//    };
//    const onSpeechPartialResults = async (e) => {
//       console.log("eeee", e);
//       setPartialResults(e.value)
//       setSearchText(e.value[0])
//       const data = e.value[0] == '' ? searchText : e.value[0]
//       btnSearchIconClick(data)
//    };
//    const onSpeechVolumeChanged = (e) => {
//       setPitch(e.value)
//    };
//    const startSpeechRecognizing = async () => {
//       console.log("start");
//       setPitch('')
//       setError('')
//       setStarted('')
//       setResults([])
//       setPartialResults([])
//       setSearchText('')
//       setEnd('')
//       try {
//          await Voice.start('en-US')
//          if (mikestop) {
//             startSpeechRecognizing()
//          }

//       } catch (e) {
//          console.error(e);
//       }
//    };
//    const stopSpeechRecognizing = async () => {
//       console.log("stop");

//       try {
//          await Voice.stop();
//       } catch (e) {
//          console.error(e);
//       }
//    };
//    Voice.onSpeechStart = onSpeechStart;
//    Voice.onSpeechEnd = onSpeechEnd;
//    Voice.onSpeechError = onSpeechError;
//    Voice.onSpeechResults = onSpeechResults;
//    Voice.onSpeechPartialResults = onSpeechPartialResults;
//    Voice.onSpeechVolumeChanged = onSpeechVolumeChanged;

//    const btnSearchIconClick = async (text) => {
//       const textData = text ? text : route.params.qrData ? route.params.qrData : ''
//       setSearchText(textData)
//       const keyType = route.params.productItem.type
//       const Keyword = route.params.productItem.key_word
//       const filterword = route.params.productItem.filter_word
//       const filterType = route.params.productItem.filter_type
//       const keytypeData = route.params.productItem.key_type
//       if (searchText.length >= 4) {
//          let params = {
//             key_type: 'search',
//             key_word: searchText,
//             filter_type: keyType,
//             filter_word: Keyword,
//             start: '0',
//             limit: limit + 20,
//          }
//          let params2 = {
//             key_type: 'search',
//             key_word: searchText,
//             filter_type: filterType,
//             filter_word: filterword,
//             segment_filter: [Keyword],
//             start: '0',
//             limit: limit + 20,
//          }
//          const data = keytypeData == 'segment' ? params2 : params
//          let response = await Request.post('get-product-list.php', data)
//          if (response.success == true) {
//             setbannerImage(response.data.banner_image)
//             setmorecategory(response.data.result)
//             settotaldatalength(response.data.total)

//             setlimit(response.data.result.length + 20)
//             setnoproduct('No product found')

//          }
//          else {
//             if (response) {
//                showSimpleAlert(response.message)
//             }
//          }
//       }
//       else if (textData < 4) {
//          let params2 = {
//             key_type: keyType,
//             key_word: Keyword,
//             filter_type: newfiltertype,
//             filter_word: maincatId,
//             start: '0',
//             limit: limit + 20,
//          }
//          const data = params2

//          let response = await Request.post('get-product-list.php', data)
//          if (response.success == true) {
//             setbannerImage(response.data.banner_image)
//             setmorecategory(response.data.result)
//             setlimit(response.data.result.length + 20)
//             settotaldatalength(response.data.total)
//             setnoproduct('No product found')
//             setSortValue('')
//          }
//          else {
//             if (response) {
//                showSimpleAlert(response.message)
//             }
//          }
//       }
//       else if (searchText == '') {
//          let params2 = {
//             key_type: keyType,
//             key_word: Keyword,
//             filter_type: newfiltertype,
//             filter_word: maincatId,
//             start: '0',
//             limit: limit + 20,
//          }
//          const data = params2
//          let response = await Request.post('get-product-list.php', data)
//          if (response.success == true) {
//             setbannerImage(response.data.banner_image)
//             setlimit(response.data.result.length + 20)
//             setmorecategory(response.data.result)
//             settotaldatalength(response.data.total)

//             setnoproduct('No product found')
//             setSortValue('')
//             setSearchText('')
//          }
//          else {
//             if (response) {
//                showSimpleAlert(response.message)
//             }
//          }
//       }

//    }
//    const btnandroidClick = async () => {
//       let speechToTextData = '';
//       try {
//          speechToTextData = await SpeechToText.startSpeech('Try saying something', 'en_IN');
//          btnSearchIconClick(speechToTextData)
//          setSearchText(speechToTextData)
//       } catch (error) {
//          console.log('error: ', error);
//       }
//    }
//    const btnCamaraIconClick = () => {
//       console.log("QrcodeProductScreenroute.params", route.params);
//       navigation.navigate('QrcodeProductScreen', { homescreen: 0, productItem: route.params.productItem })
//    }
//    const btnMikeIconClick = async () => {
//       setMikeStop(!mikestop)
//       if (mikestop) {
//          await startSpeechRecognizing()
//       } else {
//          await stopSpeechRecognizing()
//       }
//    }
//    const getProductcatid = async (item) => {

//       console.log("getProductcatiditem", item);
//       const keyType = route.params.productItem.type
//       const KeyName = route.params.productItem.name
//       const Keyword = route.params.productItem.key_word
//       const mainCategory = route.params.productItem.main_cat_id
//       setKeyName(KeyName)
//       setcategory_id(item.cat_id)
//       let params = {
//          key_type: item.key_type,
//          key_word: item.key_word,
//          main_category_id: mainCategory,
//          category_id: item.cat_id
//       }
//       setModalVisible(true)
//       let response = await Request.post('get-product-filter-list.php', params)
//       setModalVisible(false)
//       if (response.success == true) {
//          setfilterData(response.data.length > 0 ? response.data[0] : [])
//          setArrToWhom(response.data.length > 0 ? response.data[0].title : [])
//          setsubcategoryData(response.data.length > 0 ? response.data[0].sub_category : [])
//       }
//       else {
//          if (response) {
//             showSimpleAlert(response.message)
//          }
//       }
//    }



//    const mainCateoriesClick = async (item, index) => {
//       console.log("mainCateoriesClickitem", item);
//       setarrwhomdata(item.cat_name)
//       setnewfiltertype(item.filter_type)
//       setmaincatId(item.filter_word)
//       setkeytype(item.key_type)
//       setKeyword(item.key_word)
//       setcatName(item.cat_name)
//       setsubcatName('')
//       if (route.params.isfilter == 1) {
//          const brandData = route.params.brandData
//          const filterbrand = brandData.map((item, index) => {
//             return item.brand_id
//          })
//          const filterfragtype = route.params.fragrancetype.map((item, index) => {
//             return item.key
//          })
//          const filtersegment = route.params.filtersegment.map((item, index) => {
//             return item.in_segment
//          })
//          const filterSize = route.params.sizeData.map((item, index) => {
//             return item.key
//          })
//          const concernData = route.params.concerndata.map((item, index) => {
//             return item.id
//          })
//          const minprice = route.params.minprice
//          const maxprice = route.params.maxprice
//          const keytype2 = item.key_type

//          let params2 = {
//             key_type: item.key_type,
//             key_word: item.key_word,
//             max_price: maxprice,
//             min_price: minprice,
//             segment_filter: filtersegment,
//             brand_filter: filterbrand,
//             fragrance_filter: filterfragtype,
//             size_filter: filterSize,
//             concern_filter: concernData,
//             sort: item.value,
//             filter_type: item.filter_type,
//             filter_word: item.filter_word,
//             start: '0',
//             limit: limit + 20,
//          }
//          setSortValue(sortValue)
//          setModalVisible(true)
//          const data = params2

//          let response = await Request.post('get-product-list.php', data)
//          setModalVisible(false)
//          if (response.success == true) {
//             setbannerImage(response.data.banner_image)
//             setmorecategory(response.data.result)
//             setlimit(response.data.result.length + 20)
//             settotaldatalength(response.data.total)
//             setnoproduct('No product found')
//             getProductcatid(item)
//             setloadervalue(true)
//          }
//          else {
//             if (response) {
//                showSimpleAlert(response.message)
//             }
//          }
//       }
//       else {
//          let params2 = {
//             key_type: item.key_type,
//             key_word: item.key_word,
//             brand_filter: route.params.brand ? [route.params.productItem.key_word] : route.params.productItem.key_type == 'brand' ? [route.params.productItem.key_word] : [],
//             sort: sortValue,
//             filter_type: item.filter_type,
//             filter_word: item.filter_word,
//             start: '0',
//             limit: limit + 20,
//          }
//          setkeytype(item.key_type)
//          setarrwhomdata(item.cat_name)
//          setModalVisible(true)
//          const data = params2
//          let response = await Request.post('get-product-list.php', data)
//          setModalVisible(false)
//          if (response.success == true) {
//             setbannerImage(response.data.banner_image)
//             setmorecategory(response.data.result)
//             setlimit(response.data.result.length + 20)
//             settotaldatalength(response.data.total)
//             setnoproduct('No product found')
//             setloadervalue(true)
//             getProductcatid(item)

//          }
//          else {
//             if (response) {
//                showSimpleAlert(response.message)
//             }
//          }
//       }
//    }
//    const subCateoriesClick = async (item, index) => {
//       console.log("subCateoriesClickitem", item);

//       setkeytype(item.key_type)
//       setKeyword(item.key_word)
//       setmaincatName(item.main_cat_name)
//       setcatName(item.cat_name)
//       setsubcatName(item.sub_cat_name)
//       if (route.params.isfilter == 1) {
//          const brandData = route.params.brandData
//          const filterbrand = brandData.map((item, index) => {
//             return item.brand_id
//          })
//          const filterfragtype = route.params.fragrancetype.map((item, index) => {
//             return item.key
//          })
//          const filtersegment = route.params.filtersegment.map((item, index) => {
//             return item.in_segment
//          })
//          const filterSize = route.params.sizeData.map((item, index) => {
//             return item.key
//          })
//          const concernData = route.params.concerndata.map((item, index) => {
//             return item.id
//          })
//          const minprice = route.params.minprice
//          const maxprice = route.params.maxprice
//          const keytype2 = item.key_type

//          let params2 = {
//             key_type: item.key_type,
//             key_word: item.key_word,
//             max_price: maxprice,
//             min_price: minprice,
//             segment_filter: filtersegment,
//             brand_filter: filterbrand,
//             fragrance_filter: filterfragtype,
//             size_filter: filterSize,
//             concern_filter: concernData,
//             sort: item.value,
//             filter_type: item.filter_type,
//             filter_word: item.filter_word,
//             start: '0',
//             limit: limit + 20,
//             category_id: item.cat_id

//          }
//          setSortValue(sortValue)
//          setModalVisible(true)
//          setcategory_id(item.cat_id)
//          const data = params2

//          let response = await Request.post('get-product-list.php', data)
//          setModalVisible(false)
//          if (response.success == true) {
//             setbannerImage(response.data.banner_image)
//             setmorecategory(response.data.result)
//             setlimit(response.data.result.length + 20)
//             settotaldatalength(response.data.total)
//             setnoproduct('No product found')
//             setloadervalue(true)
//          }
//          else {
//             if (response) {
//                showSimpleAlert(response.message)
//             }
//          }
//       }
//       else {
//          let params2 = {
//             key_type: item.key_type,
//             key_word: item.key_word,
//             brand_filter: route.params.brand ? [route.params.productItem.key_word] : route.params.productItem.key_type == 'brand' ? [route.params.productItem.key_word] : [],
//             sort: sortValue,
//             filter_type: item.filter_type,
//             filter_word: item.filter_word,
//             start: '0',
//             limit: limit + 20,
//             category_id: item.cat_id
//          }
//          setcategory_id(item.cat_id)

//          setkeytype(item.key_type)
//          setarrwhomdata(item.cat_name)
//          setModalVisible(true)
//          const data = params2
//          let response = await Request.post('get-product-list.php', data)
//          setModalVisible(false)
//          if (response.success == true) {
//             setbannerImage(response.data.banner_image)
//             setmorecategory(response.data.result)
//             setlimit(response.data.result.length + 20)
//             settotaldatalength(response.data.total)
//             setnoproduct('No product found')
//             setloadervalue(true)

//          }
//          else {
//             if (response) {
//                showSimpleAlert(response.message)
//             }
//          }
//       }
//    }
//    const favoriteUnFavoriteButtonClick = async (item, index, type) => {
//       const favItem = item
//       if (loginKey) {
//          let params = {
//             product_id: favItem.product_id,
//          }
//          setModalVisible(true)
//          let response = await Request.post('add-favorite-item.php', params)
//          if (response.success == true) {
//             if (type === 'newarrival') {
//                morecategory[index].is_favourite = item.is_favourite == 0 ? 1 : 0
//                setmorecategory(morecategory)
//                setnoproduct('No product found')

//             }
//             setModalVisible(false)
//          }
//          else {
//             showSimpleAlert(response.message)
//          }
//       } else {
//          navigation.navigate('AuthSelection', { authFlag: 1 })
//       }
//    }

//    const sendReviewButtonClick = (item, index) => {
//       const shareItem = item
//       const message = 'Product Name:' + shareItem.brand_name + '\n' + shareItem.family_name + '\n' + "Price:" + shareItem.main_price + "JD's"
//       const shareOptions = {
//          subject: 'Gift Center',
//          message: message,
//          url: shareItem.family_pic
//       };
//       const response = Share.open(shareOptions)
//          .then(res => {
//             console.log(res);
//          })
//          .catch(err => {
//             err && console.log(err);
//          });
//       return response;
//    }

//    const newArrivalProductClick = (item) => {
//       navigation.navigate('ProductDetailViewController', { seoUrl: item.seo_url, arrwhomdata: arrwhomdata, category: 1, filtertype: newfiltertype, maincatId: maincatId })

//    }

//    const renderVideoBannerItem = ({ item, index }) => {
//       return (
//          <Image source={{ uri: item.banner }} style={{ width: deviceWidth, height: scale(200), marginTop: scale(10) }} />
//       )
//    }
//    const rendermorecategory = (item, index, type) => {
//       return (
//          <View>
//             <ProductComponent
//                item={item}
//                index={index}
//                isFavourite={item.is_favourite == 0 ? false : true}
//                mainCateoriesClick={(item, index) => newArrivalProductClick(item, index)}
//                colorCombination={colorCombination}
//                colorCodeChangedIndex={(i) => colorCodeChangedIndex(i)}
//                colorSelectedIndex={colorSelectedIndex}
//                favoriteUnFavoriteButtonClick={(item, index) => favoriteUnFavoriteButtonClick(item, index, type)}
//                sendReviewButtonClick={(item, index) => sendReviewButtonClick(item, index)}
//                isShowOffer={item.has_offer == '' ? false : true}
//                isShowColorShade={false}
//             />
//          </View>
//       )
//    }

//    const renderNewArrivalItem = (item, index) => {
//       return (
//          <ProductComponent
//             item={item}
//             index={index}
//             mainCateoriesClick={(item, index) => newArrivalProductClick(item, index)}
//             colorCombination={colorCombination}
//             colorCodeChangedIndex={(i) => colorCodeChangedIndex(i)}
//             colorSelectedIndex={colorSelectedIndex}
//             favoriteUnFavoriteButtonClick={(item, index) => favoriteUnFavoriteButtonClick(item, index)}
//             sendReviewButtonClick={(item, index) => sendReviewButtonClick(item, index)}
//             isShowOffer={item.has_offer == '' ? false : true}
//             isShowColorShade={false}
//          />
//       )
//    }

//    const setsorting = async (item) => {
//       console.log("route.params.productItem.type", route.params.productItem);
//       const sortvalue = item.value ? item.value : sortValue
//       console.log("sortvalue", item.value, sortvalue);
//       setSortValue(sortvalue);
     
//       const keyType = route.params.productItem.type
//       const Keyword = route.params.productItem.key_word
//       const KeyName = route.params.productItem.name
//       const mainCatId = route.params.productItem.main_cat_id ? route.params.productItem.main_cat_id : route.params.maincatId ? route.params.maincatId : ''
//       const filterype = route.params.productItem.filter_type ? route.params.productItem.filter_type : route.params.filtertype ? route.params.filtertype : ''
//       const filterWord = route.params.productItem.filter_word ? route.params.productItem.filter_word : route.params.filter_word ? route.params.filter_word : ''
//       setKeyName(KeyName)
//       if (route.params.isfilter == 1) {
//          const brandData = route.params.brandData
//          const filterbrand = brandData.map((item, index) => {
//             return item.brand_id
//          })
//          const filterfragtype = route.params.fragrancetype.map((item, index) => {
//             return item.key
//          })
//          const filtersegment = route.params.filtersegment.map((item, index) => {
//             return item.in_segment
//          })
//          const filterSize = route.params.sizeData.map((item, index) => {
//             return item.key
//          })
//          const rowCollections = route.params.collectionData.map((item, index) => {
//             return item.id
//          })
//          const concernData = route.params.concerndata.map((item, index) => {
//             return item.id
//          })
//          const minprice = route.params.minprice
//          const maxprice = route.params.maxprice

//          let params2 = {
//             key_type: item.value ? keyType : keytype,
//             key_word: Keyword,
//             max_price: maxprice,
//             min_price: minprice,
//             segment_filter: filtersegment,
//             brand_filter: filterbrand,
//             fragrance_filter: filterfragtype,
//             size_filter: filterSize,
//             concern_filter: concernData,
//             sort: sortvalue,
//             filter_type: newfiltertype,
//             filter_word: keyType == 'segment' ? maincatId : mainCatId,
//          }
//          setSortValue(item.value)
//          setModalVisible(true)
//          const data = params2
//          let response = await Request.post('get-product-list.php', data)
//          setModalVisible(false)
//          if (response.success == true) {
//             setbannerImage(response.data.banner_image)
//             settotaldatalength(response.data.total)

//             setmorecategory(response.data.result)
//             setnoproduct('No product found')
//             setproductfilter()
//          }
//          else {
//             if (response) {
//                showSimpleAlert(response.message)
//             }
//          }
//       }
//       else {
//          let params2 = {
//             key_type: keytype,
//             key_word: KeyWord,
//             sort: sortvalue,
//             filter_type: newfiltertype,
//             filter_word: keyType == 'segment' ? maincatId : mainCatId,
//          }
//          const data = params2
//          console.log("Keyword1", data);
//          setModalVisible(true)
//          let response = await Request.post('get-product-list.php', data)
//          setModalVisible(false)

//          if (response.success == true) {
//             setbannerImage(response.data.banner_image)
//             settotaldatalength(response.data.total)
//             setmorecategory(response.data.result)
//             setnoproduct('No product found')
//             setproductfilter()
//          }
//          else {
//             if (response) {
//                showSimpleAlert(response.message)
//             }
//          }
//       }
//    }
//    const setproductfilter = async () => {
//       const keyType = route.params.productItem.type
//       const KeyName = route.params.productItem.name
//       const Keyword = route.params.productItem.key_word
//       const mainCategory = route.params.productItem.main_cat_id
//       setKeyName(KeyName)
//       let params = {
//          key_type: keytype,
//          key_word: KeyWord,
//          main_category_id: mainCategory,
//          category_id: category_id
//       }
//       setModalVisible(true)
//       let response = await Request.post('get-product-filter-list.php', params)
//       setModalVisible(false)
//       if (response.success == true) {
//          setfilterData(response.data.length > 0 ? response.data[0] : [])
//          setArrToWhom(response.data.length > 0 ? response.data[0].title : [])
//          setsubcategoryData(response.data.length > 0 ? response.data[0].sub_category : [])
//       }
//       else {
//          if (response) {
//             showSimpleAlert(response.message)
//          }
//       }
//    }
//    const renderToWhomItem = ({ item, index }) => {
//       return (
//          <TouchableOpacity
//             style={[styles.dropShadowStyle2, { height: 40, marginTop: 4, marginBottom: 8, marginLeft: 4, justifyContent: 'center', marginRight: 8, paddingHorizontal: 12, borderRadius: 5 }]}
//             onPress={() => mainCateoriesClick(item, index)}>
//             <Text style={{ fontFamily: Fonts.montserratSemiBold, fontSize: 13, color: commonColors.Blue }}>
//                {item.cat_name}
//             </Text>
//          </TouchableOpacity>
//       )
//    }
//    const rendersubcategoryData = ({ item, index }) => {
//       return (
//          <TouchableOpacity
//             style={[styles.dropShadowStyle2, { height: 40, marginTop: 4, marginBottom: 8, marginLeft: 4, justifyContent: 'center', marginRight: 8, paddingHorizontal: 12, borderRadius: 5 }]}
//             onPress={() => subCateoriesClick(item, index)}>
//             <Text style={{ fontFamily: Fonts.montserratSemiBold, fontSize: 13, color: commonColors.Blue }}>
//                {item.sub_cat_name}
//             </Text>
//          </TouchableOpacity>
//       )
//    }
//    const filterByClick = () => {
//       console.log("arrwhomdata:-->", arrwhomdata);

//       const categoryFlag = route.params.category ? route.params.category : 0
//       navigation.navigate('FilterOptionViewController', { filterData: filterData, productItem: route.params.productItem, sortingvalue: sortValue, category: categoryFlag, arrwhomdata: arrwhomdata, filtertype: newfiltertype, maincatId: maincatId, keyname: KeyName, keytype: keytype, Keyword: KeyWord, categoryID: category_id, segment_name: segmentName, cat_Name: catName, main_CatName: maincatName, sub_catName: subcatName })
//    }
//    const rendersearchItem = ({ item, }) => {
//       return (
//          <View style={styles.searchView}>

//          </View>

//       )
//    }
//    const itemSeparatorComponent = ({ item }) => {
//       return (
//          <View style={styles.lineSeprator}></View>
//       );
//    }
//    let new_arrival = 'newarrival'
//    const renderLabelItem = (item) => {
//       console.log("item:-->", item);
//       return (
//          <View style={{ height: 56, justifyContent: 'center', borderBottomColor: '#CDCECF', marginStart: 20, marginEnd: 20, borderBottomWidth: 1 }}>
//             <View style={{}}>
//                <Text style={styles.textItem}>{item.label}</Text>
//             </View>
//          </View>
//       );
//    };
//    const renderFooter = () => {
//       return (
//          <View style={[styles.footer, { height: footermodalVisible == true ? hasNotch() ? 120 : 100 : null }]}>
//             {footermodalVisible &&
//                <BallIndicator visible={footermodalVisible} />
//             }
//          </View>
//       );
//    }
//    const viewmore = async () => {
//       let userData = await StorageService.getItem(StorageService.STORAGE_KEYS.USER_DETAILS)
//       setloginKey(userData)
//       console.log("route.params", route.params);
//       const keyType = route.params.productItem.type
//       const Keyword = route.params.productItem.key_word
//       const KeyName = route.params.productItem.name ? route.params.productItem.name : route.params.name ? route.params.name : null
//       const arrdata = route.params.arrwhomData ? route.params.arrwhomdata : ''
//       const filterype = route.params.filtertype ? route.params.filtertype : route.params.productItem.filter_type ? route.params.productItem.filter_type : ''
//       const mainCatId = route.params.productItem.main_cat_id ? route.params.productItem.main_cat_id : route.params.maincatId ? route.params.maincatId : ''
//       const filterWord = route.params.maincatId ? route.params.maincatId : route.params.productItem.filter_word ? route.params.productItem.filter_word : route.params.filter_word ? route.params.filter_word : ''
//       const searchData = route.params.qrData ? route.params.qrData : ''
//       setnewfiltertype(filterype)
//       setmaincatId(filterWord)
//       setkeytype(keyType)
//       setarrwhomdata(route.params.arrwhomData)
//       setKeyName(KeyName)
//       setSearchText(searchData)
//       var brandId = []
//       if (route.params.isfilter == 1) {
//          const brandData = route.params.brandData
//          const keytypeData = route.params.keytype
//          const keywordData = route.params.keyword ? route.params.keyword : route.params.productItem.key_word

//          const filterbrand = brandData.map((item, index) => {
//             return item.brand_id
//          })
//          const filterfragtype = route.params.fragrancetype.map((item, index) => {
//             return item.key
//          })
//          const filtersegment = route.params.filtersegment.map((item, index) => {
//             return item.in_segment
//          })
//          const filterSize = route.params.sizeData.map((item, index) => {
//             return item.key
//          })
//          const rowCollections = route.params.collectionData.map((item, index) => {
//             return item.id
//          })
//          const concernData = route.params.concerndata.map((item, index) => {
//             return item.id
//          })
//          const minprice = route.params.minprice
//          const maxprice = route.params.maxprice
//          let params2 = {
//             key_type: keytypeData ? keytypeData : keytype,
//             key_word: keywordData ? keywordData : KeyWord,
//             max_price: maxprice,
//             min_price: minprice,
//             segment_filter: filtersegment,
//             brand_filter: filterbrand,
//             fragrance_filter: filterfragtype,
//             size_filter: filterSize,
//             concern_filter: concernData,
//             sort: route.params.sortvalue,
//             filter_type: filterype,
//             filter_word: filterWord,
//             start: '0',
//             limit: limit + 20,
//          }
//          setSortValue(route.params.sortvalue)
//          const data = params2
//          setfootermodalVisible(true)
//          let response = await Request.post('get-product-list.php', data)
//          setfootermodalVisible(false)
//          if (response.success == true) {
//             setbannerImage(response.data.banner_image)
//             setmorecategory(response.data.result)
//             setnoproduct('No product found')
//             settotaldatalength(response.data.total)
//             setlimit(limit + 20)
//             await getProductFilterListing()
//          }
//          else {
//             if (response) {
//                showSimpleAlert(response.message)
//             }
//          }
//       } else {
//          let params2 = {
//             filter_type: route.params.productItem.filter_type ? route.params.productItem.filter_type : route.params.filtertype ? route.params.filtertype : '',
//             filter_word: route.params.productItem.filter_word ? route.params.productItem.filter_word : route.params.filter_word ? route.params.filter_word : '',
//             key_type: route.params.productItem.key_type,
//             key_word: Keyword,
//             start: '0',
//             limit: limit + 20,
//          }
//          const data = params2
//          setfootermodalVisible(true)
//          let response = await Request.post('get-product-list.php', data)
//          setfootermodalVisible(false)
//          if (response.success == true) {
//             setbannerImage(response.data.banner_image)
//             setmorecategory(response.data.result)
//             settotaldatalength(response.data.total)
//             setnoproduct('No product found')
//             setSortValue('')
//             setlimit(limit + 20)
//             await getProductFilterListing()
//          }
//          else {
//             if (response) {
//                showSimpleAlert(response.message)
//             }
//          }
//       }
//    }
//    return (
//       <View style={styles.mainViewStyle}>
//          <View style={styles.dropShadowStyle}>
//             <View style={[styles.headerLogoStyle]}>
//                <Image source={importImages.headerLogo} />
//             </View>
//             <TouchableOpacity onPress={() => btnBackClick()} style={{ marginLeft: 16, height: 44, alignItems: 'center', justifyContent: 'center', position: 'absolute' }}>
//                <Image source={importImages.backButtonArrow} style={{}} />
//             </TouchableOpacity>
//             {/* Search */}
//             <View style={styles.searchViewStyle}>
//                <View style={{ flexDirection: 'row' }}>
//                   <TouchableOpacity style={styles.searchClick} onPress={() => { btnSearchIconClick(searchText) }}>
//                      <Image source={importImages.SearchIcon} style={styles.searchIconImage} />
//                   </TouchableOpacity>
//                   <View style={styles.searchBarView}>
//                      <TextInput
//                         style={styles.searchBarTextinput}
//                         placeholder={'Search'}
//                         placeholderTextColor={commonColors.Blue}
//                         value={searchText}
//                         multiline={false}
//                         onChangeText={(searchText) => { btnSearchIconClick(searchText) }}
//                         underlineColorAndroid='transparent'
//                      />
//                   </View>
//                   <TouchableOpacity style={[styles.searchBarRightIconStyle, { width: scale(30) }]} onPress={Platform.OS == 'android' ? btnandroidClick : btnMikeIconClick}>
//                      <Image source={importImages.mikeIcon} style={{
//                         height: scale(20),
//                         alignSelf: 'center',
//                         width: scale(10),
//                      }} />
//                   </TouchableOpacity>
//                   <TouchableOpacity style={styles.searchBarRightIconStyle} onPress={btnCamaraIconClick}>
//                      <Image source={importImages.cameraIcon} style={styles.cameraIcon} />
//                   </TouchableOpacity>
//                </View>
//             </View>
//          </View>
//          <ScrollView nestedScrollEnabled={true} showsVerticalScrollIndicator={false} bounces={false}>
//             <View>
//                <View style={{ zIndex: -100, }}>
//                   <FlatList
//                      horizontal
//                      bounces={false}
//                      renderItem={renderVideoBannerItem}
//                      data={bannerImage}
//                      style={{ zIndex: 1000, }}
//                      pagingEnabled={true}
//                      showsVerticalScrollIndicator={false}
//                      showsHorizontalScrollIndicator={false}
//                      keyExtractor={(item, index) => `${index}-id`}
//                   />
//                   <View style={[styles.dropShadowStyle, {}]}>
//                      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
//                         <View style={{ flexDirection: 'row', marginLeft: 24, marginTop: 16 }}>
//                            <Text style={{ fontFamily: Fonts.montserratMedium, fontSize: 15, color: commonColors.Blue }}>{route.params.category == 1 ? 'CATEGORY >' : `HOME >`}</Text>
//                            <Text style={{ marginLeft: 4, fontFamily: Fonts.montserratMedium, fontSize: 15, color: commonColors.Grey }}>{maincatName ? maincatName : ''}</Text>

//                            <Text style={{ marginLeft: 4, fontFamily: Fonts.montserratMedium, fontSize: 15, color: commonColors.Grey }}>{catName ? ' > ' + catName : ''}</Text>
//                            <Text style={{ marginLeft: 4, fontFamily: Fonts.montserratMedium, fontSize: 15, color: commonColors.Grey }}>{subcatName ? ' > ' + subcatName : ''}</Text>

//                         </View>
//                      </ScrollView>
//                      {arrToWhom.length > 0 ?
//                         <View style={{ marginTop: 12, marginLeft: 20, marginRight: 12, }}>
//                            <FlatList
//                               horizontal
//                               bounces={false}
//                               renderItem={renderToWhomItem}
//                               data={arrToWhom}
//                               style={{}}
//                               showsVerticalScrollIndicator={false}
//                               showsHorizontalScrollIndicator={false}
//                               keyExtractor={(item, index) => `${index}-id`}
//                            />
//                         </View> : null}
//                      {subcategoryData.length > 0 ?
//                         <View style={{ marginTop: 12, marginLeft: 20, marginRight: 12, }}>
//                            <FlatList
//                               horizontal
//                               bounces={false}
//                               renderItem={rendersubcategoryData}
//                               data={subcategoryData}
//                               style={{}}
//                               showsVerticalScrollIndicator={false}
//                               showsHorizontalScrollIndicator={false}
//                               keyExtractor={(item, index) => `${index}-id`}
//                            />
//                         </View> : null}

//                      <View style={{ flexDirection: 'row', marginHorizontal: 12, marginTop: 8, justifyContent: "space-between" }}>
//                         <TouchableOpacity style={styles.sortAndFilterButtonStyle}>
//                            <Dropdown
//                               style={styles.dropdown}
//                               placeholderStyle={styles.placeholderStyle}
//                               selectedTextStyle={styles.selectedTextStyle}
//                               data={keytype == 'segment' ? sortDataArray2 : sortDataArray}
//                               search={false}
//                               maxHeight={300}
//                               labelField="label"
//                               activeColor={commonColors.White}
//                               valueField="value"
//                               placeholder={'Sort By'}
//                               value={sortValue}
//                               renderItem={renderLabelItem}
//                               itemContainerStyle={{ color: commonColors.Blue }}
//                               containerStyle={{ color: commonColors.Blue }}
//                               showsVerticalScrollIndicator={false}
//                               onChange={item => {
//                                  setsorting(item)
//                               }}
//                            />
//                         </TouchableOpacity>
//                         <TouchableOpacity style={[styles.sortAndFilterButtonStyle,]} onPress={() => filterByClick()}>
//                            <View style={styles.sortAndFilterTextViewStyle}>
//                               <Text style={{ fontFamily: Fonts.montserratMedium, fontSize: scale(14), color: commonColors.Blue, textAlign: 'center' }}>Filter By</Text>
//                            </View>
//                            <View style={styles.sortAndFilterDropDownArrowViewStyle}>
//                               <Image source={importImages.dropDownArrow} style={{ marginRight: 8 }} />
//                            </View>
//                         </TouchableOpacity>
//                      </View>

//                      {morecategory.length > 0 ?
//                         <FlatList
//                            bounces={false}
//                            renderItem={({ item, index }) => rendermorecategory(item, index, new_arrival)}
//                            data={morecategory}
//                            style={{ marginTop: 12, }}
//                            numColumns={2}
//                            pagingEnabled={true}
//                            showsVerticalScrollIndicator={false}
//                            showsHorizontalScrollIndicator={false}
//                            ListFooterComponent={renderFooter}
//                            keyExtractor={(item, index) => `${index}-id`}
//                         /> :
//                         <View style={styles.nodatafoundView}>
//                            <Text style={styles.transactionText}>{noproduct}</Text>
//                         </View>}
//                      {isModalVisible == false && (morecategory.length < totaldatalength) ?
//                         <CustomButton title={'View More'} onPress={() => {
//                            viewmore()
//                         }}
//                         /> : null}
//                   </View>
//                </View>
//             </View>
//          </ScrollView>
         
//          {isModalVisible &&
//             <BallIndicator visible={isModalVisible} />
//          }
//       </View>
//    );
// }

// const styles = StyleSheet.create({
//    mainViewStyle: {
//       flex: 1,
//       backgroundColor: 'white'
//    },
//    footer: {
//    },
//    headerLogoStyle: {
//       justifyContent: 'center',
//       alignItems: 'center',
//       height: 44
//    },
//    lineSeprator: {
//       borderBottomWidth: 0.5,
//       padding: 2,
//       borderColor: commonColors.Grey
//    },
//    searchProductText: {
//       fontSize: scale(10),
//       color: commonColors.Black
//    },
//    searchProductView: {
//       justifyContent: "space-between",
//       flexDirection: "row"
//    },
//    searchItemText: {
//       fontSize: scale(10),
//       color: commonColors.Black
//    },
//    searchView: {
//       marginHorizontal: scale(5),
//    },
//    modalView: {
//       height: scale(100),
//       width: deviceWidth - 50,
//       marginHorizontal: scale(20),
//       backgroundColor: commonColors.White,
//       shadowColor: commonColors.Black,
//       shadowOpacity: 1,
//       shadowRadius: 10,
//       shadowOffset: { width: 1, height: 3 },
//       borderRadius: 8,
//    },
//    searchBarView: {
//       flex: 1,
//       justifyContent: 'center',
//       alignSelf: 'center'
//    },
//    searchClick: {
//       marginHorizontal: 8,
//       justifyContent: 'center'
//    },
//    searchIconImage: {
//       height: scale(20),
//       width: scale(16),
//    },
//    cameraIcon: {
//       height: scale(15),
//       width: scale(18),
//    },
//    searchBarTextinput: {
//       color: commonColors.Blue,
//       fontFamily: Fonts.montserratRegular,
//       fontSize: 12,
//    },
//    searchViewStyle: {
//       height: scale(35),
//       marginHorizontal: 16,
//       alignContent: 'center',
//       borderColor: commonColors.Blue,
//       borderRadius: 15,
//       borderWidth: 1,
//       justifyContent: 'center'
//    },
//    nodatafoundView: {
//       marginTop: scale(40),
//       justifyContent: 'center',
//       alignContent: 'center',
//       alignSelf: 'center',
//    },
//    transactionText: {
//       fontSize: scale(16),
//       color: commonColors.Blue,
//       fontFamily: Fonts.montserratMedium,
//       alignSelf: "center"
//    },
//    searchBarRightIconStyle: {
//       marginHorizontal: 8,
//       alignContent: 'flex-end',
//       justifyContent: 'center'
//    },
//    sortAndFilterButtonStyle: {
//       flexDirection: 'row',
//       height: 40,
//       flex: 0.5,
//       justifyContent: 'space-between',
//       marginHorizontal: scale(10),
//       borderColor: commonColors.Blue,
//       borderWidth: 1,
//       borderRadius: 10
//    },
//    sortAndFilterTextViewStyle: {
//       justifyContent: 'center',
//       marginLeft: 8,
//       flex: 1
//    },
//    sortAndFilterDropDownArrowViewStyle: {
//       width: '100%',
//       height: "100%",
//       position: 'absolute',
//       justifyContent: 'center',
//       alignItems: "flex-end"
//    },
//    colorSection: {
//       height: 10,
//       width: 10,
//       borderRadius: 5,
//       marginRight: 10,
//       marginTop: 2,
//       backgroundColor: 'blue'
//    },
//    selectedColorSection: {
//       height: 14,
//       width: 14,
//       borderRadius: 7,
//       marginRight: 10,
//       marginTop: 0,
//    },
//    dropShadowStyle: {
//       shadowColor: commonColors.Blue,
//       shadowOpacity: 0.5,
//       shadowRadius: 2,
//       elevation: 5,
//       backgroundColor: 'white',
//       marginTop: 0
//    },
//    dropShadowStyle2: {
//       shadowColor: commonColors.Blue,
//       shadowOffset: { width: 0, height: 0 },

//       shadowOpacity: 0.5,
//       shadowRadius: 2,
//       elevation: 5,
//       backgroundColor: 'white',
//       marginTop: 0
//    },
//    dropdown: {
//       paddingHorizontal: 8,
//       alignSelf: 'center',
//       textAlign: 'center',
//       color: commonColors.Blue,
//       justifyContent: 'center',
//       width: '100%',
//       height: '100%',
//    },
//    placeholderStyle: {
//       fontSize: scale(14),
//       fontFamily: Fonts.montserratMedium,
//       textAlign: 'center',
//       color: commonColors.Blue
//    },
//    selectedTextStyle: {
//       fontSize: scale(12),
//       textAlign: 'center',
//       fontFamily: Fonts.montserratMedium,
//       color: commonColors.Blue
//    },
//    textItem: {
//       color: commonColors.Blue,
//       fontSize: scale(12),
//       fontFamily: Fonts.montserratMedium
//    }
// })

