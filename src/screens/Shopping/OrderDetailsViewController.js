import { React, importImages, TouchableOpacity, TextInput, useState, useEffect, showSimpleAlert, moment, Request, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale, Alert } from '../../utils/importLibrary'
import SkeletonContent from 'react-native-skeleton-content-nonexpo';

export default function OrderDetailsViewController({ route, navigation }) {
    const [arrMainCategories, setArrMainCategories] = useState([])
    const [customerDetails, setcustomerDetails] = useState([])
    const [isModalVisible, setModalVisible] = useState(false);
    const [nostorefound, setnostorefound] = useState('')
    const [loginKey, setloginKey] = useState('')

    const btnBackClick = () => {
        navigation.goBack()
    }

    useEffect(async () => {
        await getNotification();
    }, [])
    const getNotification = async () => {
        console.log("route.params:-->", route.params.orderItem.quantity);
        let userData = await StorageService.getItem(StorageService.STORAGE_KEYS.USER_DETAILS)
        setloginKey(userData)
        if (userData) {
            let orderId = route.params.orderItem.order_id
            let params = {
                order: orderId,
                limit: 2
            }
            setModalVisible(true)
            let response = await Request.post('get-my-order-detail.php', params)
            setModalVisible(false)
            console.log("response34", response.data);
            if (response.success == true) {
                if (response.data) {
                    setArrMainCategories(response.data.item)
                    setcustomerDetails(response.data)
                }
                else {
                    setnostorefound('No Details found')
                }
            }
            else {
                if (response) {
                    showSimpleAlert(response.message)
                }
            }
        }
        else {
            navigation.navigate('AuthSelection', { authFlag: 1, isfavourite: 1 })

        }
    }
    const renderNewArrivalItem = ({ item, index }) => {
        console.log("itemm", item);
        return (
            <View style={styles.renderView} >
                <View style={{ marginBottom: scale(10) }}>
                    <View style={{ flexDirection: 'row', marginLeft: 12, }}>
                        <View style={styles.imageView}>
                            {item.product_img
                                ?
                                <Image source={{
                                    uri: item.product_img
                                }} resizeMode='contain' style={styles.imageStyle} />
                                :
                                <Image source={importImages.defaultFavImage} resizeMode='contain' style={styles.imageStyle} />
                            }
                        </View>

                        <View style={styles.nameView}>
                            <Text numberOfLines={1} style={[styles.nameText, { fontWeight: "700" }]}>{item.product_no}</Text>
                            <Text numberOfLines={1} style={[styles.nameText, { color: "#627B96", paddingVertical: scale(3) }]}>{item.brand}</Text>
                            <Text numberOfLines={2} style={[styles.nameText, {
                                fontFamily: Fonts.montserratMedium,
                            }]}>{item.family_name} </Text>
                            <Text numberOfLines={2} style={[styles.nameText, {
                                fontFamily: Fonts.montserratRegular,
                                fontSize: 13,
                                color: commonColors.qtySizeMlText
                            }]}>{item.fam_name} </Text>
                            {/* <Text numberOfLines={2} style={[styles.nameText, {}]}>{item.brand} - {item.old_id}</Text> */}
                            {/* <Text numberOfLines={2} style={[styles.nameText, {}]}>{item.family_name} </Text> */}

                            <View style={{ flexDirection: 'row', paddingVertical: scale(3) }}>
                                <Text style={[styles.textStyle, { color: "#627B96", }]}>Sales :</Text>
                                <Text style={[styles.textStyle, {}]}>{item.sales_person} </Text>
                            </View>
                        </View>
                    </View>
                    <View style={{
                        borderColor: "#627B96",
                        marginHorizontal: scale(10),
                        borderBottomWidth: scale(0.5),
                    }} />
                    <View style={{ flexDirection: "row", width: "100%", }}>
                        <View style={{ flexDirection: 'column', paddingVertical: scale(3), alignItems: "center", justifyContent: "center", width: '15%', }}>
                            <Text style={[styles.labeltextStyle, { color: "#627B96", }]}>Tax% </Text>
                            <Text style={[styles.textinputStyle, { fontWeight: "bold", }]}>{item.tax ? item.tax : 0}  </Text>
                        </View>
                        <View style={{ flexDirection: 'column', paddingVertical: scale(3), alignItems: "center", justifyContent: "center", width: '15%', }}>
                            <Text style={[styles.labeltextStyle, { color: "#627B96", }]}>Disc% </Text>
                            <Text style={[styles.textinputStyle, { fontWeight: "bold" }]}>{item.discount ? item.discount : 0}</Text>
                        </View>
                        <View style={{ flexDirection: 'column', paddingVertical: scale(3), alignItems: "center", justifyContent: "center", width: '15%', }}>
                            <Text style={[styles.labeltextStyle, { color: "#627B96", }]}>Qty </Text>
                            <Text style={[styles.textinputStyle, { fontWeight: "bold" }]}>{item.qty ? item.qty : 0}  </Text>
                        </View>
                        <View style={{ flexDirection: 'column', paddingVertical: scale(3), alignItems: "center", justifyContent: "center", width: '15%', }}>
                            <Text style={[styles.labeltextStyle, { color: "#627B96", }]}>Adv </Text>
                            <Text style={[styles.textinputStyle, { fontWeight: "bold" }]}>{item.adv ? item.adv : 0} </Text>
                        </View>
                        <View style={{ flexDirection: 'column', paddingVertical: scale(3), alignItems: "center", justifyContent: "center", width: '18%', }}>
                            <Text style={[styles.labeltextStyle, { color: "#627B96", }]}>Total </Text>
                            <Text style={[styles.textinputStyle, { fontWeight: "bold" }]}>{item.total ? item.total : 0} </Text>
                        </View>
                        <View style={{ flexDirection: 'column', paddingVertical: scale(3), alignItems: "center", justifyContent: "center", width: '20%', }}>
                            <Text style={[styles.labeltextStyle, { color: "#627B96", }]}>Retail Price </Text>
                            <Text style={[styles.textinputStyle, { fontWeight: "bold" }]}>{item.retail_price ? item.retail_price : 0} </Text>
                        </View>

                    </View>
                </View>
            </View>
        )
    }
    const viewAll = () => {
        navigation.navigate('OrderListViewController', { orderId: route.params.orderItem.order_id })
    }
    return (
        <View style={styles.mainViewStyle}>

            <View style={styles.headerLogoStyle}>
                <Text style={{ width: '100%', textAlign: 'center', position: 'absolute', fontFamily: Fonts.montserratSemiBold, fontSize: 20, color: commonColors.Blue }}>{'Order Details'}</Text>
                <TouchableOpacity onPress={() => btnBackClick()} style={{ marginLeft: 16 }}>
                    <Image source={importImages.backButtonArrow} style={{}} />
                </TouchableOpacity>
            </View>
            <View style={{ width: 300, height: 1, backgroundColor: 'lightgrey', alignSelf: 'center', }}></View>

            <View style={{ marginTop: scale(8) }}>
                <Text style={{ fontWeight: "bold", padding: scale(10), fontSize: scale(14), color: commonColors.Blue, }}>{'ORDER DETAILS '}</Text>
                <View style={[styles.renderView2, { padding: scale(10) }]}>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '40%' }]}>Customer ID </Text>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '10%' }]}> {':'}</Text>
                        <Text style={[styles.textStyle, { width: '60%' }]}>{customerDetails.customer_id ? customerDetails.customer_id : '-'}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingVertical: scale(3) }}>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '40%' }]}>Customer Name </Text>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '10%' }]}> {':'}</Text>
                        <Text style={[styles.textStyle, { width: '60%' }]}>{customerDetails.customer_name ? customerDetails.customer_name : '-'}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingVertical: scale(3) }}>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '40%' }]}>Order ID </Text>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '10%' }]}> {':'}</Text>
                        <Text style={[styles.textStyle, { width: '60%' }]}>{customerDetails.order_id ? customerDetails.order_id : '-'}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingVertical: scale(3) }}>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '40%' }]}>Date & Time </Text>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '10%' }]}> {':'}</Text>
                        <Text style={[styles.textStyle, { width: '60%' }]}>{moment(customerDetails.sale_date_time).format('DD MMM YYYY')} | {moment(customerDetails.sale_date_time).format('hh:mm A')}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingVertical: scale(3) }}>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '40%' }]}>Store Name & ID </Text>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '10%' }]}> {':'}</Text>
                        <Text numberOfLines={2} style={[styles.textStyle, { width: '50%' }]}>{customerDetails.store_name ? customerDetails.store_name : '-'} {customerDetails.store_no} </Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingVertical: scale(3) }}>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '40%' }]}>Sub Total</Text>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '10%' }]}> {':'}</Text>
                        <Text style={[styles.textStyle, { width: '60%' }]}>{customerDetails.sub_total ? parseFloat(customerDetails.sub_total).toFixed(2) : 0} JD`s</Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingVertical: scale(3) }}>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '40%' }]}>Discount </Text>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '10%' }]}> {':'}</Text>
                        <Text style={[styles.textStyle, { width: '60%' }]}>{customerDetails.discount ? parseFloat(customerDetails.discount).toFixed(2) : 0} JD`s </Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingVertical: scale(3) }}>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '40%' }]}>Shipping </Text>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '10%' }]}> {':'}</Text>
                        <Text style={[styles.textStyle, { width: '60%' }]}>{customerDetails.shipping_charge ? parseFloat(customerDetails.shipping_charge).toFixed(2) : 0} JD`s</Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingVertical: scale(3) }}>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '40%' }]}>Redeem</Text>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '10%' }]}> {':'}</Text>
                        <Text style={[styles.textStyle, { width: '60%' }]}>{customerDetails.order_redeem_points ? customerDetails.order_redeem_points : 0}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingVertical: scale(3) }}>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '40%' }]}>Points Gained</Text>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '10%' }]}> {':'}</Text>
                        <Text style={[styles.textStyle, { width: '60%' }]}>{customerDetails.order_gained ? customerDetails.order_gained : 0}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingVertical: scale(3) }}>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '40%' }]}>Total</Text>
                        <Text style={[styles.textStyle, { color: "#627B96", width: '10%' }]}> {':'}</Text>
                        <Text style={[styles.textStyle, { width: '60%' }]}>{customerDetails.total ? parseFloat(customerDetails.total).toFixed(2) : 0} JD`s</Text>
                    </View>
                </View>
            </View>
            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                <Text style={{ fontWeight: "bold", padding: scale(10), fontSize: scale(14), color: commonColors.Blue, }}>{'PRODUCTS '}</Text>
                {route.params.orderItem.quantity >= 3 ?
                    <TouchableOpacity onPress={() => viewAll()}>
                        <Text style={{ fontWeight: "bold", padding: scale(10), fontSize: scale(14), color: commonColors.Blue, }}>{'VIEW All '}</Text>
                    </TouchableOpacity> : null}
            </View>
            {arrMainCategories.length > 0 ?

                <FlatList
                    bounces={false}
                    renderItem={renderNewArrivalItem}
                    data={arrMainCategories}
                    style={{ marginTop: 8, flex: 1, }}
                    contentContainerStyle={{ alignItems: 'center', flexGrow: 1 }}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={(item, index) => `${index}-id`}
                /> : <View style={{ flex: 1, height: '100%', justifyContent: 'center', alignContent: 'center', alignSelf: 'center' }}>
                    <Text style={styles.transactionText}>{nostorefound}</Text>
                </View>}

            {isModalVisible ?
                <BallIndicator visible={isModalVisible} /> : null}
        </View>
    )
}
const styles = StyleSheet.create({
    mainViewStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    headerLogoStyle: {
        alignItems: 'center',
        height: 44,
        flexDirection: 'row'
    },
    textStyle: {
        marginTop: 4,
        fontFamily: Fonts.montserratMedium,
        fontSize: 13,
        color: commonColors.Blue
    },
    labeltextStyle: {
        marginTop: 4,
        fontFamily: Fonts.montserratMedium,
        fontSize: scale(10),
        color: commonColors.Blue
    },
    textStyle2: {
        marginTop: 4,
        fontFamily: Fonts.montserratMedium,
        fontSize: 13,
        color: commonColors.Blue
    },

    transactionText: {
        color: commonColors.Blue,
        fontSize: 20,
        fontFamily: Fonts.montserratMedium
    },
    textinputStyle: {
        marginTop: 4,
        fontFamily: Fonts.montserratMedium,
        fontSize: scale(11),
        color: commonColors.Blue
    },
    transcationView: {
        marginBottom: 20,
        alignSelf: 'center'
    },

    dateStyle: {
        marginLeft: 12,
        fontSize: 10,
        color: commonColors.Blue,
        fontFamily: Fonts.montserratRegular,
    },

    databoxTextStyle: {
        marginTop: 6,
        width: deviceWidth - 40,
        borderRadius: 12,
        shadowColor: 'gray',
        shadowOpacity: 1,
        shadowRadius: 6,
        backgroundColor: commonColors.White,
        elevation: 8,
        alignSelf: 'center'
    },
    dataText: {
        fontSize: 14,
        marginHorizontal: 10,
        marginTop: 10,
        fontFamily: Fonts.montserratSemiBold,
        color: commonColors.Blue,
    },

    discountText: {
        fontSize: 12,
        marginHorizontal: 10,
        fontFamily: Fonts.montserratRegular,
        color: commonColors.Blue,
        textAlign: 'justify'
    },

    linkText: {
        marginHorizontal: 10,
        marginBottom: 10,
        fontSize: 12,
        color: "#0086FF",
        fontFamily: Fonts.montserratRegular
    },

    visitTextStyle: {
        marginHorizontal: 10,
        color: commonColors.Blue,
        fontSize: 10,
        marginTop: 10,
        fontFamily: Fonts.montserratSemiBold
    },
    dataText: {
        fontSize: 14,
        marginHorizontal: 10,
        marginTop: 10,
        fontFamily: Fonts.montserratSemiBold,
        color: commonColors.Blue,
    },
    renderView: {
        width: (deviceWidth - 30),
        marginHorizontal: 8,
        marginTop: scale(4),
        backgroundColor: commonColors.White,
        borderRadius: 10,
        shadowColor: commonColors.Blue,
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 5,
        marginVertical: scale(10),
    },
    renderView2: {
        width: (deviceWidth - 30),
        marginHorizontal: 10,
        marginTop: scale(4),
        backgroundColor: commonColors.White,
        borderRadius: 10,
        shadowColor: commonColors.Blue,
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 5,
        // height: scale(320),
        marginVertical: scale(10),
    },
    imageView: {
        height: scale(100),
        width: "20%",
        borderWidth: 0.8,
        borderRadius: scale(8),
        borderColor: '#BFCAD4',
        marginTop: scale(10),
        justifyContent: 'center'
    },
    imageStyle: {
        alignSelf: 'center',
        height: scale(50),
        width: scale(40),
    },
    nameView: {
        marginHorizontal: 8,
        marginLeft: scale(20),
        marginTop: scale(10),
        flex: 1,
    },
    nameText: {
        fontFamily: Fonts.montserratMedium,
        fontSize: scale(14),
        color: commonColors.Blue
    },
    familynameText: {
        marginTop: 4,
        fontFamily: Fonts.montserratRegular,
        fontSize: scale(12),
        color: commonColors.Blue
    },
    productNameText: {
        marginTop: 4,
        fontFamily: Fonts.montserratRegular,
        fontSize: scale(12),
        color: commonColors.qtySizeMlText
    },
    priceText: {
        marginTop: 8,
        fontFamily: Fonts.montserratRegular,
        fontSize: scale(12),
        color: commonColors.Blue
    },
    labelText: {
        marginTop: 8,
        fontFamily: Fonts.montserratMedium,
        fontSize: scale(12),
        color: '#627B96'
    },

})