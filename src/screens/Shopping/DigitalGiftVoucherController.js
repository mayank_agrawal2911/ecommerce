import CustomButton from '../../components/CustomButton';
import { React, Request, DatePicker, moment, importImages, Keyboard, TouchableOpacity, TextInput, useState, useEffect, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale, showSimpleAlert, isValidEmail } from '../../utils/importLibrary'
import { Calendar, CalendarList, LocaleConfig, Agenda } from 'react-native-calendars';

export default function DigitalGiftVoucherController({ route, navigation }) {
    const [isModalVisible, setModalVisible] = useState(false);
    const [giftData, setgiftData] = useState([]);
    const [giftmessage, setgiftmessage] = useState('');
    const [smsactive, setsmsactive] = useState(false);
    const [emailactive, setemailactive] = useState(true);
    const [selfreceiptentbox, setselfreceiptentbox] = useState(true);
    const [otherreceiptentbox, setotherreceiptentbox] = useState(false);
    const [fromemailaddress, setfromemailaddress] = useState('');
    const [toemailaddress, settoemailaddress] = useState('');
    const [receiptentemailAddress, setreceiptentemailAddress] = useState('');
    const [asapactive, setasapactive] = useState(true);
    const [dateselection, setdateselection] = useState(false);
    const [futuredateactive, setfuturedateactive] = useState(false);
    const [open, setOpen] = useState(false)
    const [dateOfBirth, setDateOfBirth] = useState('')
    const [productId, setproductId] = useState('')
    const [amountactive, setamountactive] = useState(false)
    const [date, setDate] = useState(moment(new Date()).format('YYYY-MM-DD'))
    const [amtData, setamtData] = useState([])
    const [Index, setIndex] = useState(null)
    const [amtprice, setamtprice] = useState('')
    const [familypic, setfamilypic] = useState('')
    const [loginKey, setloginKey] = useState('')
    const [useremail, setuseremail] = useState('')
    const [userphone, setuserphone] = useState('')
    const [monthChange, setmonthChange] = useState('');
    const [yearChange, setyearChange] = useState('');
    const [day, setDay] = useState('');
    const [month, setMonth] = useState('');
    const [Year, setYear] = useState('');
    const [markedDate, setmarkedDate] = useState('');
    const [selectedDate, setselectedDate] = useState('')
    const [futureselection, setfutureselection] = useState(false)

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            digitalgiftvoucher();
        });
        return unsubscribe;
    }, [navigation])


    const btnBackClick = () => {
        navigation.goBack()
    }
    const digitalgiftvoucher = async () => {
        let userData = await StorageService.getItem(StorageService.STORAGE_KEYS.USER_DETAILS)
        console.log("userData", userData);
        if (userData) {
            setuseremail(userData.email)
            setuserphone(userData.phone)
            setloginKey(userData)
        }

        setgiftData([route.params.giftData])
        setproductId(route.params.giftData.product_id)
        setamtData(route.params.giftData.item)
        setfamilypic(route.params.giftData.item[0].family_pic)
    }
    const outClickModalDisable = () => {
        setfuturedateactive(false)
        setasapactive(false)
        setdateselection(true)
        setmonthChange('')
    }
    const actionclick = (type) => {
        setDateOfBirth(moment(date).format('YYYY-MM-DD'))
        setfuturedateactive(false)
        setasapactive(false)
    }
    const validationofField = () => {
        const checkEmail = isValidEmail(receiptentemailAddress)
        console.log("Index:--?", Index, amtprice);
        if (amtprice == '') {
            showSimpleAlert('Please select a amount')
            return false;
        }

        else if (giftmessage == '') {
            showSimpleAlert('Please enter message')
            return false;
        }
        else if (otherreceiptentbox == true) {
            if (fromemailaddress == '') {
                showSimpleAlert('Please enter from field')
                return false;
            }
            else if (toemailaddress == '') {
                showSimpleAlert('Please enter to field')
                return false;
            }
            if (emailactive == true) {
                if (receiptentemailAddress == '') {
                    showSimpleAlert('Please enter receiptent email address')
                    return false;
                }
                else if (!isValidEmail(receiptentemailAddress)) {
                    showSimpleAlert('Please enter valid receiptent email address')
                    return false;
                }
                else {
                    return true
                }
            }
            else if (smsactive == true) {
                if (receiptentemailAddress == '') {
                    showSimpleAlert('Please enter receiptent phone number')
                    return false;
                }
                else {
                    return true
                }
            }


            else {
                return true
            }
        }
        else if (dateselection == true) {
            if (dateOfBirth == '') {
                showSimpleAlert('Please select future date')
                return false;
            }
            else {
                return true;

            }
        }

        else {
            return true;
        }
    }
    const addtobag = () => {
        if (loginKey) {
            const ConfirmValid = validationofField();
            if (ConfirmValid) {
                addtobagApicall()
            }
        } else {
            navigation.navigate('AuthSelection', { authFlag: 1 })
        }

    }
    const addtobagApicall = async () => {
        let params = {
            func: 'add',
            product_type: 'voucher',
            quantity: '1',
            product_id: productId,
            send_message: giftmessage,
            send_type: selfreceiptentbox == true ? 'yourself' : 'recipient',
            send_to: toemailaddress,
            send_from: fromemailaddress,
            send_email: emailactive == true ? selfreceiptentbox == true ? useremail : receiptentemailAddress : '',
            send_phone: smsactive == true ? selfreceiptentbox == true ? userphone : receiptentemailAddress : '',
            send_medium: smsactive == true ? 'sms' : 'email',
            send_on: asapactive == true ? 'now' : 'later',
            send_date: dateselection == true ? dateOfBirth : ''
        }
        setModalVisible(true)
        let response = await Request.post('manage-cart.php', params)
        console.log("response", response);
        setModalVisible(false)
        if (response.success == true) {
            showSimpleAlert(response.message)
            navigation.goBack()
        }
        else {
            showSimpleAlert(response.message)
        }

    }
    const selectamt = (item, index) => {
        console.log("item:1233", item, index);
        var tempdata = amtData
        tempdata[index].is_active = item.is_active == 0 ? 1 : 0
        setIndex(index)
        setamtData(tempdata)
        setamtprice(item.main_price)
        setfamilypic(item.family_pic)
        setproductId(item.product_id)
    }
    const getSelectedDayEvents = date => {
        let markedDates = {};
        setselectedDate(date)
        markedDates[date] = { selected: true, color: '#00B0BF', textColor: '#FFFFFF' };
        setmarkedDate(markedDates)
    };
    const amtrenderItem = ({ item, index }) => {
        index == Index ? setamtprice(item.main_price) : ''

        console.log(" index == Index", index, Index);
        return (
            <TouchableOpacity onPress={() => {
                selectamt(item, index)
            }}>
                <View style={{ justifyContent: 'center', width: scale(80), height: scale(30), alignItems: 'center', borderWidth: 1, borderColor: index == Index ? commonColors.Blue : '#B9C4D0', borderRadius: scale(5), marginHorizontal: scale(10) }}>
                    <Text style={{ textAlign: 'center', alignSelf: 'center', color: commonColors.Blue }}>{item.main_price} JD`s</Text>
                </View>
            </TouchableOpacity>
        )
    }
    const renderItem = ({ item, index }) => {


        var today = new Date();
        var dd = today.getDate() + 1;
        var mm = today.getMonth() + 1; //January is 0 so need to add 1 to make it 1!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        const minimumDate = yyyy + '-' + mm + '-' + dd;
        var maxDate = new Date()
        var maxtodayDate = maxDate.getDate()

        var maxMonth = new Date().getMonth() + 1
        var maxYear = new Date().getFullYear() + 1


        if (maxtodayDate < 10) {
            maxtodayDate = '0' + maxtodayDate
        }
        if (maxMonth < 10) {
            maxMonth = '0' + maxMonth
        }
        var maximumDate = maxYear + '-' + maxMonth + '-' + maxtodayDate

        console.log("minimumDate", minimumDate);
        return (
            <View style={{ flex: 1 }}>
                <View style={{ borderWidth: 1, borderColor: '#B9C4D0', borderRadius: scale(10), marginHorizontal: scale(20) }}>
                    <Image source={{ uri: familypic }} resizeMode='contain' style={styles.imageView} />
                </View>
                <Text style={styles.amtText}>{'Choose the amount'}</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: scale(10) }}>
                    <FlatList
                        data={amtData}
                        renderItem={amtrenderItem}
                        extraData={amtData}
                        style={{ flex: 1, marginHorizontal: scale(10), }}
                        contentContainerStyle={{ flexGrow: 1, alignSelf: 'center', justifyContent: 'center' }}
                        horizontal
                    />

                </View>
                <View style={styles.lineStyle}></View>
                <View style={[styles.emailIdText, { alignSelf: 'flex-start', justifyContent: "flex-start" }]}>
                    <TextInput
                        placeholder="Write a personal message"
                        placeholderTextColor={commonColors.Grey}
                        keyboardType='email-address'
                        multiline={true}
                        value={giftmessage}
                        autoCapitalize='none'
                        onChangeText={(value) => setgiftmessage(value)}
                    />
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: scale(10), marginHorizontal: scale(20) }}>
                    <TouchableOpacity onPress={() => {
                        setemailactive(true)
                        setsmsactive(false)
                    }}>
                        <View style={{ justifyContent: 'center', width: scale(80), height: scale(30), alignItems: 'center', borderWidth: 1, borderColor: emailactive == true ? commonColors.Blue : '#B9C4D0', borderRadius: scale(5) }}>
                            <Text style={{ textAlign: 'center', alignSelf: 'center', color: commonColors.Blue }}>{'Email'} </Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        setsmsactive(true)
                        setemailactive(false)
                    }}>
                        <View style={{ marginHorizontal: scale(10), justifyContent: 'center', width: scale(80), height: scale(30), alignItems: 'center', borderWidth: 1, borderColor: smsactive == true ? commonColors.Blue : '#B9C4D0', borderRadius: scale(5) }}>
                            <Text style={{ textAlign: 'center', alignSelf: 'center', color: commonColors.Blue }}>{'SMS'} </Text>
                        </View>
                    </TouchableOpacity>

                </View>
                <View style={{ flexDirection: 'row', marginTop: scale(25), marginHorizontal: scale(20) }}>
                    <TouchableOpacity onPress={() => {
                        setselfreceiptentbox(true)
                        setotherreceiptentbox(false)
                    }} style={{ flexDirection: 'row' }}>
                        <Image source={selfreceiptentbox == false ? importImages.radioUnCheckImage : importImages.Checkbox} />
                        <Text style={{ paddingHorizontal: scale(5), fontFamily: Fonts.montserratMedium, fontSize: scale(12), color: commonColors.Blue }}>Send to Yourself</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flexDirection: 'row', marginHorizontal: scale(15) }} onPress={() => {
                        setotherreceiptentbox(true)
                        setselfreceiptentbox(false)
                    }}>
                        <Image source={otherreceiptentbox == false ? importImages.radioUnCheckImage : importImages.Checkbox} />
                        <Text style={{ paddingHorizontal: scale(5), fontFamily: Fonts.montserratMedium, fontSize: scale(12), color: commonColors.Blue }}>Send to Receipient</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ marginTop: scale(15) }}></View>
                {otherreceiptentbox == true ?
                    <View >
                        <View style={styles.subListViewStyle}>
                            <Text style={styles.labelText}>{'From*'}</Text>
                            <TextInput
                                style={styles.popUpTextInputStyle}
                                value={fromemailaddress}
                                placeholder={'From*'}
                                placeholderTextColor={commonColors.Grey}
                                keyboardType='email-address'
                                autoCapitalize={'none'}
                                onChangeText={(value) => { setfromemailaddress(value) }}
                            ></TextInput>
                        </View>
                        <View style={styles.subListViewStyle}>
                            <Text style={styles.labelText}>{'To*'}</Text>
                            <TextInput
                                style={styles.popUpTextInputStyle}
                                value={toemailaddress}
                                placeholder={'To*'}
                                placeholderTextColor={commonColors.Grey}
                                keyboardType='email-address'
                                autoCapitalize={'none'}
                                onChangeText={(value) => { settoemailaddress(value) }}
                            ></TextInput>
                        </View>

                        <View style={styles.subListViewStyle}>
                            <Text style={styles.labelText}>{smsactive == true ? "Receiptent's Phone number " : "Receiptent's email "}</Text>
                            <TextInput
                                style={styles.popUpTextInputStyle}
                                value={receiptentemailAddress}
                                placeholder={smsactive == true ? "9627xxxxxxx" : "Receiptent's email "}
                                placeholderTextColor={commonColors.Grey}
                                keyboardType={smsactive == true ? "number-pad" : "email-address"}
                                autoCapitalize={'none'}
                                onChangeText={(value) => { setreceiptentemailAddress(value) }}
                            ></TextInput>
                        </View>
                    </View> : <View style={styles.subListViewStyle}>
                        <Text style={styles.labelText}>{smsactive == true ? "Receiptent's Phone number" : "Receiptent's email "}</Text>

                        <View style={styles.popUpTextInputStyle}>
                            <Text style={{
                                color: commonColors.Blue,
                                fontSize: scale(12),
                                fontFamily: Fonts.montserratMedium,
                            }}>{smsactive == true ? userphone : useremail}</Text>
                        </View>
                    </View>}

                <View style={{ flexDirection: 'row', marginHorizontal: scale(20), marginTop: scale(15) }}>
                    <TouchableOpacity onPress={() => {
                        setasapactive(true)
                        setdateselection(false)
                        setfuturedateactive(false)
                    }}>
                        <View style={{ borderRadius: scale(10), borderColor: commonColors.Blue, flexDirection: 'column', borderWidth: 1, width: scale(150), height: scale(90), alignSelf: 'center', justifyContent: "center" }}>
                            <View style={{ flexDirection: 'row', alignSelf: 'center', }}>
                                <Image source={asapactive == true ? importImages.Checkbox : importImages.radioUnCheckImage} />
                                <Image source={importImages.sendsms} style={{ marginHorizontal: scale(10) }} />
                            </View>
                            <Text style={{ textAlign: 'center', paddingTop: scale(10), color: commonColors.DarkGrey }}>{'Send ASAP'}</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            setfuturedateactive(true)
                            setdateselection(true)
                            setasapactive(false)
                        }} >
                        <View style={{ borderRadius: scale(10), borderColor: commonColors.Blue, marginLeft: scale(10), flexDirection: 'column', borderWidth: 1, width: scale(150), height: scale(90), alignSelf: 'center', justifyContent: "center" }}>
                            <View style={{ flexDirection: 'row', alignSelf: 'center', }}>
                                <Image source={dateselection == true ? importImages.Checkbox : importImages.radioUnCheckImage} />
                                <Image source={importImages.futurecalendar} style={{ marginHorizontal: scale(10) }} />
                            </View>
                            <Text style={{ textAlign: 'center', paddingTop: scale(10), color: commonColors.DarkGrey }}>{'Send on Future date'}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <Text style={{ paddingVertical: scale(15), textAlign: 'center', color: commonColors.Blue, fontSize: scale(10), fontFamily: Fonts.montserratMedium }}>{'Choose to send it as soon as possible,or on a future date of your choice'}</Text>
                <View style={{ padding: scale(10), flexDirection: 'row', height: scale(70), width: deviceWidth - 40, borderWidth: 1, borderColor: commonColors.Blue, marginHorizontal: scale(20) }}>
                    <View style={{ flexDirection: 'row', alignSelf: 'center', }}>
                        <Image source={importImages.giftbox} style={{ height: scale(40), width: scale(40), resizeMode: 'contain' }} />
                        <Text style={{ alignSelf: 'center', paddingHorizontal: scale(20), width: scale(130), fontSize: scale(12), color: commonColors.DarkGrey }}>{'Amount of your e-gift voucher'}</Text>
                        <Text style={{ alignSelf: 'center', paddingHorizontal: scale(20), width: scale(130), textAlign: 'center', fontSize: scale(18), color: commonColors.Blue }}>{amtprice ? amtprice : '0'} JD`s</Text>

                    </View>
                </View>
                {futuredateactive == true ?
                    <Modal
                        animationType='slide'
                        transparent={true}
                        visible={futuredateactive == true ? true : false} >
                        <TouchableWithoutFeedback onPress={() => { outClickModalDisable() }}>

                            <View style={styles.modalViewStyle}>
                                <TouchableWithoutFeedback onPress={null}>
                                    <View style={styles.modalSubViewStyle}>
                                        <View >
                                            <Calendar
                                                style={[{
                                                }]}
                                                current={minimumDate}
                                                maxDate={maximumDate}
                                                disabledByDefault={false}
                                                minDate={minimumDate}
                                                theme={{
                                                    calendarBackground: commonColors.White,
                                                    textSectionTitleColor: '#b6c1cd',
                                                    selectedDayBackgroundColor: commonColors.Blue,
                                                    selectedDayTextColor: commonColors.White,
                                                    todayTextColor: commonColors.Blue,
                                                    textInactiveColor: commonColors.White,
                                                    textDisabledColor: commonColors.Grey,
                                                    dayTextColor: commonColors.Blue,
                                                    dotColor: '#00adf5',
                                                    selectedDotColor: 'red',
                                                    arrowColor: commonColors.Blue,
                                                    disabledArrowColor: commonColors.Grey,
                                                    monthTextColor: commonColors.Blue,
                                                    textDayFontWeight: '400',
                                                    textMonthFontWeight: 'bold',
                                                    textDayHeaderFontWeight: '300',
                                                    textDayFontSize: scale(14),
                                                    textMonthFontSize: scale(14),
                                                    textDayHeaderFontSize: scale(12)
                                                }}
                                                onDayPress={day => {
                                                    getSelectedDayEvents(day.dateString);
                                                    setselectedDate(day.dateString)
                                                    setDateOfBirth(day.dateString)
                                                    setDay(day.day)
                                                    setMonth(day.month)
                                                    setYear(day.year)
                                                }}
                                                onDayLongPress={day => {
                                                }}
                                                monthFormat={'MMMM yyyy'}
                                                onMonthChange={month => {
                                                    console.log("monthmonth", month.month, new Date().getMonth() + 1)
                                                    console.log("monthmonth", month.year, new Date().getFullYear() + 1)
                                                    setmonthChange(month.month)
                                                    setyearChange(month.year)

                                                }}
                                                markedDates={markedDate}
                                                hideArrows={false}
                                                hideDayNames={false}
                                                hideExtraDays={true}
                                                showWeekNumbers={false}
                                                onPressArrowLeft={monthChange ? monthChange > new Date().getMonth() + 1 ? subtractMonth => subtractMonth() : null : null}
                                                onPressArrowRight={addMonth => addMonth()}
                                                disableArrowRight={monthChange ? (monthChange == new Date().getMonth() + 1 && yearChange == new Date().getFullYear() + 1) ? true : false : false}

                                                disableArrowLeft={monthChange ? (monthChange == new Date().getMonth() + 1) && (yearChange == new Date().getFullYear()) ? true : false : true}

                                                disableAllTouchEventsForDisabledDays={true}
                                                disableAllTouchEventsForInactiveDays={true}

                                            />




                                        </View>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                        </TouchableWithoutFeedback>
                    </Modal> : null}
                <CustomButton title='Add to Bag' onPress={() => { addtobag() }} />

            </View >
        )
    }
    return (
        <View style={styles.mainViewStyle}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.headerLogoStyle}>
                    <TouchableOpacity onPress={() => btnBackClick()} style={{ marginLeft: 16 }}>
                        <Image source={importImages.backButtonArrow} style={{}} />
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1 }}>
                    <FlatList
                        data={giftData}
                        extraData={giftData}
                        renderItem={renderItem}
                        style={{ marginTop: 8, flex: 1, }}
                    />
                </View>
            </ScrollView>
            {isModalVisible &&
                <BallIndicator visible={isModalVisible} />
            }
        </View>
    )
}

const styles = StyleSheet.create({
    mainViewStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    headerLogoStyle: {
        alignItems: 'center',
        height: 44,
        flexDirection: 'row'
    },
    imageView: {
        height: scale(140),
        width: deviceWidth - 60,
    },
    amtText: {
        textAlign: 'center',
        fontSize: scale(14),
        color: commonColors.Blue,
        fontFamily: Fonts.montserratSemiBold,
        paddingTop: scale(25)
    },
    emailIdText: {
        borderWidth: 0.8,
        marginHorizontal: scale(20),
        borderColor: commonColors.Blue,
        borderRadius: scale(5),
        color: commonColors.Blue,
        fontSize: scale(12),
        fontFamily: Fonts.montserratMedium,
        paddingLeft: scale(10),
        width: deviceWidth - 50,
        height: scale(100),
    },
    lineStyle: {
        marginVertical: scale(20),
        borderBottomColor: commonColors.Blue,
        borderBottomWidth: 1,
        width: deviceWidth,
    },

    subListViewStyle: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginBottom: scale(10),
        marginHorizontal: scale(20)
    },
    labelText: {
        fontSize: scale(13),
        textAlign: 'left',
        alignSelf: 'center',
        fontFamily: Fonts.montserratMedium,
        color: commonColors.Blue,
        width: scale(100)
    },
    popUpTextInputStyle: {
        borderWidth: 0.8,
        borderColor: commonColors.Blue,
        borderRadius: scale(2),
        color: commonColors.Blue,
        fontSize: scale(12),
        justifyContent: "center",
        fontFamily: Fonts.montserratMedium,
        paddingHorizontal: scale(5),
        width: (deviceWidth + 20) / 2,
        height: scale(40),
    },
    modalViewStyle: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: commonColors.Transparent
    },

    modalSubViewStyle: {
        backgroundColor: commonColors.White,
        shadowColor: commonColors.Black,
        shadowOpacity: 1,
        shadowRadius: 10,
        shadowOffset: { width: 1, height: 3 },
        borderRadius: 8,
        width: deviceWidth - 50,
        justifyContent: 'center',
        paddingHorizontal: 20,
        paddingTop: 20, paddingBottom: 20

    },

})