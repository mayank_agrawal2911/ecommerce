import { Calendar, CalendarList, LocaleConfig, Agenda } from 'react-native-calendars';
import CustomButton from '../../components/CustomButton';
import CustomHeader from '../../components/CustomHeader';
import { React, Text, useEffect, useState, moment, Request, ScrollView, View, StyleSheet, TouchableOpacity, Image, Fonts, commonColors, deviceWidth, useTheme, importImages, scale, showSimpleAlert, StorageService, } from '../../utils/importLibrary';
import { Dropdown } from 'react-native-element-dropdown';

export default function ShippingDateViewController({ route, navigation }) {
    const [markedDate, setmarkedDate] = useState(route.params.shippingDate ? route.params.shippingDate : '');
    const [day, setDay] = useState('');
    const [month, setMonth] = useState('');
    const [Year, setYear] = useState('');
    const [minDate, setminDate] = useState(route.params.minDate)
    const [extraDays, setextraDays] = useState()
    const [shippingData, setshippingData] = useState([])
    const [selectedDate, setselectedDate] = useState('')
    const [selectcheckBox, setselectcheckBox] = useState(false)
    const [arrMainCategories, setArrMainCategories] = useState([])
    const [isModalVisible, setModalVisible] = useState(false);
    const [store, setstore] = useState('');
    const [storeId, setstoreId] = useState('');
    const [monthChange, setmonthChange] = useState('');
    const [yearChange, setyearChange] = useState('');
    const [isDeliverable, setisDeliverable] = useState('');
    const [isDeliverableInfo, setisDeliverableInfo] = useState('');

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', async () => {
            console.log("minDate", route.params);
            await getShippingDateStatus()
            getStoreData()
            getShippingDate()

        });
        return unsubscribe;
    }, [navigation]);
    const getShippingDateStatus = async () => {

        const response = await Request.post('check-delivery-date.php')
        console.log("responseresponse", response);
        if (response.success == true) {
            setisDeliverable(response.data.is_deliverable)
            setisDeliverableInfo(response.data.deliverable_info.replace('<br>', ' '))
        } else {

        }

    }
    const getStoreData = async () => {
        setModalVisible(true)
        let params = { domain_id: 8 }

        let response = await Request.post('get-store.php', params)
        setModalVisible(false)
        if (response.success == true) {
            setArrMainCategories(response.data)
        }
        else {
            if (response) {
                showSimpleAlert(response.message)
            }
        }
    }
    const getShippingDate = async () => {

        let markedDates = {};
        let shippingdata = route.params.shippData
        if (shippingdata.length == 1) {
            setshippingData(shippingdata)
            if (shippingdata[0].city == 'Amman') {
                var today = new Date();
                var dd = today.getDate() + 1;
                var mm = today.getMonth() + 1; //January is 0 so need to add 1 to make it 1!
                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }
                today = yyyy + '-' + mm + '-' + dd;
                console.log("todayAmman", today);
                console.log("mm2", yyyy, mm);

                setminDate(today)
            }
            else {
                var today = new Date();
                var dd = today.getDate() + 2;
                var mm = today.getMonth() + 1; //January is 0 so need to add 1 to make it 1!
                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }
                today = yyyy + '-' + mm + '-' + dd;
                console.log("today", today);
                console.log("mm3", yyyy, mm);

                setminDate(today)
            }

        }
        else {
            const filterData = shippingdata.filter((item) => item.is_selected == 1)
            if (filterData[0].city == 'Amman') {
                var today = new Date();
                var dd = today.getDate() + 1;
                var mm = today.getMonth() + 1; //January is 0 so need to add 1 to make it 1!
                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }
                today = yyyy + '-' + mm + '-' + dd;
                console.log("mm4", yyyy, mm);

                setminDate(today)
            }
            else {
                var today = new Date();
                var dd = today.getDate() + 2;
                var mm = today.getMonth() + 1; //January is 0 so need to add 1 to make it 1!
                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }

                today = yyyy + '-' + mm + '-' + dd;
                console.log("mm5", yyyy, mm);

                setminDate(today)
            }
            setshippingData(filterData)
            console.log("filterData", filterData);

        }
        markedDates[route.params.shippingDate] = { selected: true, color: '#00B0BF', textColor: '#FFFFFF' };
        const Day = route.params.shippingDate ? moment(route.params.shippingDate).format('DD') : ''
        const month = route.params.shippingDate ? moment(route.params.shippingDate).format('MM') : ''
        const year = route.params.shippingDate ? moment(route.params.shippingDate).format('YYYY') : ''
        setmarkedDate(markedDates)
        setDay(Day ? Day : '')
        setMonth(month ? month : '')
        setYear(year ? year : '')
        var shippingData = await StorageService.getItem(StorageService.STORAGE_KEYS.SHIPPING_DATE)
        if (shippingData) {
            setselectcheckBox(shippingData.storeId ? true : false)
            setstore(shippingData.storeName)
            setstoreId(shippingData.storeId)
            setselectedDate(shippingData.shippingselecteddate)
            setselectcheckBox(shippingData.selectcheckBox)
        }
        console.log("shippingDatashippingData:-->", shippingData);
        console.log("markedDatesmarkedDates", markedDates, Day + '-' + month + '-' + year);
        console.log("DayDay", Day, month, year);
    }

    const getSelectedDayEvents = date => {
        let markedDates = {};
        setselectedDate(date)
        markedDates[date] = { selected: true, color: '#00B0BF', textColor: '#FFFFFF' };
        setmarkedDate(markedDates)
    };
    const saveDate = () => {
        console.log("markeddate", markedDate);
        if (selectedDate == '') {
            showSimpleAlert('Please select shipping date')
        }
        else if (selectcheckBox == true) {
            if (store == '') {
                showSimpleAlert('Please select store')
            }
            else {
                StorageService.saveItem(StorageService.STORAGE_KEYS.SHIPPING_DATE, { shippingselecteddate: selectedDate, storeId: storeId, storeName: store, selectcheckBox: selectcheckBox })
                navigation.replace("CheckOutViewController", { shippingselecteddate: selectedDate, storeId: storeId })
            }
        }

        else {
            StorageService.saveItem(StorageService.STORAGE_KEYS.SHIPPING_DATE, { shippingselecteddate: selectedDate, storeId: storeId, storeName: store, selectcheckBox: selectcheckBox })
            navigation.replace("CheckOutViewController", { shippingselecteddate: selectedDate, storeId: storeId })

        }

    }
    const renderLabelItem = (item) => {
        return (
            <View>

                <View style={{ height: 56, justifyContent: 'center', borderBottomColor: '#CDCECF', marginStart: 20, marginEnd: 20, borderBottomWidth: 1 }}>
                    <View style={{}}>
                        <Text style={styles.textItem}>{item.store_name}</Text>
                    </View>
                </View>
            </View>

        );
    };
    console.log("markedDate", markedDate, new Date(minDate).getMonth() + 1, monthChange);
    return (
        <View style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.mainView}>
                    <View style={{ marginHorizontal: scale(40) }}>
                        <CustomHeader
                            leftBtnOnPress={() => navigation.goBack()}
                            leftBtn={<Image source={importImages.backButtonArrow}></Image>}
                            headerTitle={'SHIPPING DATE'}
                        />

                        {isDeliverable == 0 ? null :
                            <Calendar
                                style={[{
                                    justifyContent: "center", alignSelf: 'center', height: scale(250), width: deviceWidth - 20, marginTop: scale(20)
                                }]}
                                minDate={minDate}
                                current={minDate}
                                theme={{
                                    calendarBackground: commonColors.White,
                                    textSectionTitleColor: '#b6c1cd',
                                    selectedDayBackgroundColor: commonColors.Blue,
                                    selectedDayTextColor: commonColors.White,
                                    todayTextColor: commonColors.Blue,
                                    textInactiveColor: commonColors.White,
                                    textDisabledColor: commonColors.Grey,
                                    dayTextColor: commonColors.Blue,
                                    dotColor: '#00adf5',
                                    selectedDotColor: 'red',
                                    arrowColor: commonColors.Blue,
                                    disabledArrowColor: commonColors.Grey,
                                    monthTextColor: commonColors.Blue,
                                    textDayFontWeight: '400',
                                    textMonthFontWeight: 'bold',
                                    textDayHeaderFontWeight: '300',
                                    textDayFontSize: scale(14),
                                    textMonthFontSize: scale(14),
                                    textDayHeaderFontSize: scale(12)
                                }}
                                markedDates={markedDate}
                                onDayPress={day => {
                                    getSelectedDayEvents(day.dateString);
                                    setselectedDate(day.dateString)
                                    setDay(day.day)
                                    setMonth(day.month)
                                    setYear(day.year)
                                }}
                                onDayLongPress={day => {
                                }}
                                monthFormat={'MMMM yyyy'}
                                onMonthChange={month => {
                                    console.log("monthmonth", month)
                                    setmonthChange(month.month)
                                    setyearChange(month.year)
                                }}

                                hideArrows={false}
                                hideDayNames={false}
                                hideExtraDays={true}
                                showWeekNumbers={false}
                                onPressArrowLeft={monthChange ? monthChange > new Date(minDate).getMonth() + 1 ? subtractMonth => subtractMonth() : null : null}
                                // onPressArrowLeft={!minDate ? subtractMonth => subtractMonth() : null}
                                onPressArrowRight={addMonth => addMonth()}
                                // disableArrowLeft={new Date(minDate).getMonth() + 1 ? true : monthChange ? monthChange >= new Date(minDate).getMonth() + 1 ? false : true : false}
                                disableArrowRight={false}
                                disableArrowLeft={monthChange ? monthChange > new Date(minDate).getMonth() + 1 ? false : yearChange ? yearChange > new Date(minDate).getFullYear() ? false : true : true : true}
                                // disableArrowLeft={false}
                                disableAllTouchEventsForDisabledDays={true}
                                disableAllTouchEventsForInactiveDays={true}

                                enableSwipeMonths={monthChange ? monthChange > new Date(minDate).getMonth() + 1 ? true : yearChange ? yearChange > new Date(minDate).getFullYear() ? true : false : false : false}

                            // enableSwipeMonths={new Date(minDate).getMonth() + 1 ? true : monthChange ? monthChange >= new Date(minDate).getMonth() + 1 ? false : true : false}
                            />}

                        {isDeliverable == 0 ? null :
                            <View style={styles.subView}>
                                <View style={styles.yearView}>
                                    <Text style={styles.headerText}>Day</Text>
                                    <Text style={styles.yearText}>{day}</Text>
                                </View>
                                <View style={styles.borderLine} />
                                <View style={styles.yearView}>
                                    <Text style={styles.headerText}>Month</Text>
                                    <Text style={styles.yearText}>{month}</Text>
                                </View>
                                <View style={styles.borderLine} />

                                <View style={styles.yearView}>
                                    <Text style={styles.headerText}>Year</Text>
                                    <Text style={styles.yearTextYear}>{Year}</Text>
                                </View>
                            </View>
                        }
                        {isDeliverable == 0 ? null :
                            <TouchableOpacity onPress={() => { setselectcheckBox(!selectcheckBox) }}>
                                <View style={{ flexDirection: "row", marginTop: scale(40), }}>
                                    <View style={{ width: scale(5) }}>
                                        <Image source={selectcheckBox ? importImages.BlueTick : importImages.BlueBox} style={{ width: scale(15), height: scale(15), alignSelf: "center", justifyContent: "center", marginTop: scale(2) }} />
                                    </View>
                                    <Text style={styles.sameShippingText}>{'Pickup from store'}</Text>
                                </View>
                            </TouchableOpacity>
                        }
                  
                    {
                        selectcheckBox ?
                            <View style={{ flexDirection: 'row', marginTop: scale(20) }} >
                                <TouchableOpacity style={styles.sortAndFilterButtonStyle2}>
                                    <Dropdown
                                        style={styles.dropdown}
                                        placeholderStyle={styles.placeholderStyle}
                                        selectedTextStyle={styles.selectedTextStyle}
                                        data={arrMainCategories}
                                        search={false}
                                        maxHeight={200}
                                        labelField="label"
                                        dropdownPosition={'bottom'}
                                        activeColor={commonColors.White}
                                        valueField='value'
                                        placeholder={store == '' ? 'Select Store' : store}
                                        value={store}
                                        renderItem={renderLabelItem}
                                        itemContainerStyle={{ color: commonColors.Blue }}
                                        containerStyle={{ color: commonColors.Blue }}
                                        showsVerticalScrollIndicator={false}
                                        onChange={item => {
                                            setstore(item.store_name)
                                            setstoreId(item.store_id)

                                        }}
                                    />
                                </TouchableOpacity>
                            </View> : null
                    }
                    </View>
                    <View style={styles.itemSeperatorView}></View>

                    <Text style={[styles.sameShippingText, {
                        paddingHorizontal: scale(40)
                        , textAlign: 'left', paddingTop: 10, fontSize: scale(16), fontFamily: Fonts.montserratRegular,
                    }]}>{isDeliverableInfo.replace('<br>', ' ').trim()}</Text>

                </View>

            </ScrollView>
            {isDeliverable == 0 ? null :
                <CustomButton
                    onPress={() => { saveDate() }}
                    title={'SAVE'}
                />}
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: commonColors.White,
    },
    labelText: {
        color: commonColors.Blue,
        fontSize: scale(18),
        alignSelf: "center",
        paddingLeft: scale(10),
        fontFamily: Fonts.montserratSemiBold,
    },
    mainView: {
    },
    subView: {
        flexDirection: "row",
        justifyContent: "center",
        marginTop: scale(60)
    },
    yearView: {
        flexDirection: "column",
        width: scale(50),
    },
    headerText: {
        fontFamily: Fonts.montserratSemiBold,
        fontSize: scale(8),
        color: commonColors.Blue,
        textAlign: "center"

    },
    itemSeperatorView: {
        borderColor: commonColors.Blue,
        opacity: 0.3,
        borderBottomWidth: StyleSheet.hairlineWidth,
        marginVertical: scale(10),
    },
    yearText: {
        fontFamily: Fonts.montserratSemiBold,
        fontSize: scale(16),
        color: commonColors.Blue,
        paddingTop: scale(10),
        textAlign: "center"
    },
    yearTextYear: {
        fontFamily: Fonts.montserratSemiBold,
        fontSize: scale(16),
        color: commonColors.Blue,
        paddingTop: scale(10),
        textAlign: "center"
    },
    borderLine: {
        height: scale(33),
        borderWidth: 0.4,
        borderColor: commonColors.Blue,
        top: scale(5),
        marginHorizontal: scale(5)
    },
    sameShippingText: {
        fontFamily: Fonts.montserratMedium,
        fontSize: scale(16),
        paddingHorizontal: scale(10),
        color: commonColors.Blue
    },
    dropdown: {
        paddingHorizontal: scale(4),
        alignSelf: 'center',
        color: commonColors.Blue,
        width: '100%',
        height: '100%',
    },
    placeholderStyle: {
        fontSize: scale(12),
        fontFamily: Fonts.montserratMedium,
        color: commonColors.Blue
    },
    selectedTextStyle: {
        fontSize: scale(14),
        textAlign: 'center',
        fontFamily: Fonts.montserratMedium,
        color: commonColors.Blue
    },
    textItem: {
        color: commonColors.Blue,
        fontSize: scale(12),
        alignSelf: 'flex-start',

        fontFamily: Fonts.montserratMedium
    },
    sortAndFilterButtonStyle2: {
        marginTop: scale(8),
        flexDirection: 'row',
        height: scale(40),
        justifyContent: 'space-between',
        alignSelf: 'center',
        borderColor: commonColors.Blue,
        borderWidth: 1,
        borderRadius: 5
    },
})



