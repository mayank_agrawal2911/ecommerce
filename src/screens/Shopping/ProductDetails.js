import { React, importImages, TouchableOpacity, WebView, TextInput, useState, useEffect, showSimpleAlert, Request, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale, useWindowDimensions } from '../../utils/importLibrary'
import HTMLView from 'react-native-htmlview';
import { defaultSystemFonts } from 'react-native-render-html';
import RenderHtml, { HTMLElementModel, HTMLContentModel, defaultHTMLElementModels } from 'react-native-render-html';

export default function ProductDetails({ route, navigation }) {
    const [productData, setproductData] = useState('')
    const [isModalVisible, setModalVisible] = useState(false);
    const [noproduct, setnoproduct] = useState('')
    const btnBackClick = () => {
        navigation.goBack()
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getproductData();
        });
        return unsubscribe;
    }, [navigation])

    const getproductData = async () => {
        const productDetails = route.params.productDetails
        if (productDetails) {
            setproductData(productDetails)
        }
        else {
            setnoproduct('No Product Description Found')
        }
    }

    const { width } = useWindowDimensions();
    {/* <HTMLView
                        stylesheet={styles}
                        value={productData}
                    /> */}
    const renderersProps = {
        img: {
            enableExperimentalPercentWidth: true
        }
    };
    const customHTMLElementModels = {
        img: defaultHTMLElementModels.img.extend({
            contentModel: HTMLContentModel.mixed
        })
    };

    return (
        <View style={styles.mainViewStyle}>
            <View style={styles.headerLogoStyle}>
                <Text style={styles.headerText}>{'Product Description'}</Text>
                <TouchableOpacity onPress={() => btnBackClick()} style={{ marginLeft: 16 }}>
                    <Image source={importImages.backButtonArrow} style={{}} />
                </TouchableOpacity>
            </View>
            <View style={styles.lineView}></View>
            <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
                {productData ?
                    <View style={{ marginHorizontal: scale(20) }}>
                        <RenderHtml
                            contentWidth={width}
                            renderersProps={renderersProps}
                            source={{ html: productData }}
                            systemFonts={[...defaultSystemFonts, 'Montserrat-Bold', 'Montserrat-Regular']}
                            tagsStyles={{
                                p: { fontSize: scale(14), fontFamily: Fonts.montserratRegular, color: commonColors.Blue, },
                            }}
                        />
                    </View>
                    : <Text>{noproduct}</Text>}
            </ScrollView>
            {isModalVisible &&
                <BallIndicator visible={isModalVisible} />
            }
        </View>
    )
}

const styles = StyleSheet.create({
    mainViewStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    headerLogoStyle: {
        alignItems: 'center',
        height: scale(44),
        flexDirection: 'row'
    },
    headerText: {
        width: '100%',
        textAlign: 'center',
        position: 'absolute',
        fontFamily: Fonts.montserratSemiBold,
        fontSize: scale(20),
        color: commonColors.Blue
    },
    productText: {
        justifyContent: "center",
        alignSelf: 'center',
        marginTop: scale(10),
        marginHorizontal: scale(20),
        fontSize: scale(14),
        fontFamily: Fonts.montserratRegular,
        color: commonColors.Blue,
        textAlign: 'justify'
    },
    lineView: {
        width: 300,
        height: 1,
        backgroundColor: 'lightgrey',
        alignSelf: 'center',
    },
    textStyle: {
        marginTop: 4,
        fontFamily: Fonts.montserratMedium,
        fontSize: scale(13),
        color: commonColors.Blue
    },
    p: {
        marginTop: scale(10),
        marginHorizontal: scale(20),
        fontSize: scale(14),
        fontFamily: Fonts.montserratRegular,
        color: commonColors.Blue,
    }
})