import CustomButton from '../../components/CustomButton';
import CustomHeader from '../../components/CustomHeader';
import MonthPicker from 'react-native-month-year-picker';
import { React, Text, Modal, moment, useEffect, BallIndicator, KeyboardAwareScrollView, PhoneInput, useCallback, useRef, TextInput, useState, ScrollView, View, StyleSheet, TouchableOpacity, Image, Fonts, commonColors, deviceWidth, useTheme, importImages, scale, deviceHeight, showSimpleAlert, Request, StorageService, } from '../../utils/importLibrary';
import { Linking } from 'react-native';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import { FlatList } from 'react-native-gesture-handler';

export default function PlacedOrderViewController({ route, navigation }) {
   const [TempValues, setTempValues] = useState('')
   const [cashBox, setcashBox] = useState(true);
   const [cardBox, setcardBox] = useState(false);
   const [creditBox, setcreditBox] = useState(false);
   const [redeemPoint, setredeemPoint] = useState(false);
   const [redeemVoucher, setredeemVoucher] = useState(false);
   const [paymentMethod, setpaymentMethod] = useState(true);
   const [pointBalance, setpointBalance] = useState('')
   const [voucherCode, setvoucherCode] = useState('')
   const [cardNumber, setcardNumber] = useState('')
   const [expiryDate, setexpiryDate] = useState('')
   const [cvv, setcvv] = useState('')
   const [date, setDate] = useState(new Date());
   const [show, setShow] = useState(false);
   const [isModalVisible, setModalVisible] = useState(false);
   const [redeemModal, setredeemModal] = useState(false);
   const [verifycodeModal, setverifycodeModal] = useState(false);
   const [verifyCode, setverifyCode] = useState('');
   const [discount, setdiscount] = useState('');
   const [total, settotal] = useState('');
   const [subtotal, setsubtotal] = useState('')
   const [pointgained, setpointgained] = useState('')
   const [shippingData, setshippingData] = useState([])
   const [discountype, setdiscountype] = useState('')
   const [discountcode, setdiscountcode] = useState('')
   const [isredeem, setisredeem] = useState('')
   const [voucher_amt, setvoucher_amt] = useState('')
   const [verifyData, setverifyData] = useState('')
   const [paymentArray, setpaymentArray] = useState([])
   const [epaymentData, setepaymentData] = useState(false)
   const [shipCharge, setshipCharge] = useState('');
   const [loyaltyPoint, setloyaltyPoint] = useState('');
   const [voucherRedeem, setvoucherRedeem] = useState('');
   const [totalpointbalance, settotalpointbalance] = useState('');
   const [giftwrap, setgiftwrap] = useState('');
   const [productArray, setproductArray] = useState([])
   const [voucherProductData, setvoucherProductData] = useState([])
   const [isdelivereable, setisdelivereable] = useState(route.params.isdelivereable ? route.params.isdelivereable : '')
   const [deliveryDate, setdeliveryDate] = useState('')

   let voucherData = []

   const showPicker = useCallback((value) => setShow(value), []);

   useEffect(() => {
      const unsubscribe = navigation.addListener('focus', () => {
         placeorder();
         getvoucherApi();
      });
      return unsubscribe;
   }, [navigation])
   const getvoucherApi = async () => {
      let response = await Request.post('get-home-data.php')
      if (response.success == true) {
         console.log("response", response);
         settotalpointbalance(response.data.point_total)
      }
   }
   const placeorder = async () => {
      console.log("route.params.shippingId", route.params.shippingId);
      var basketData = await StorageService.getItem(StorageService.STORAGE_KEYS.BASKET_DATA)
      let params = { func: 'fetch', coupon_code: basketData[0].coupon_code, city_name: route.params.shippingId.state, state_name: route.params.shippingId.city, store_id: route.params.storeId ? route.params.storeId : '' }
      const giftwrap = basketData[0].selectgiftwrap == false ? 'No' : 'Yes'
      setgiftwrap(giftwrap)
      setModalVisible(true)
      setshippingData(basketData)

      let response = await Request.post('manage-cart.php', params)
      console.log("responseresponse", response);
      setModalVisible(false)
      if (response.success == true) {
         console.log("response", response);
         setproductArray(response.data.products)

         setdiscount(response.data.discount)
         settotal(response.data.grand_total)
         setsubtotal(response.data.sub_total)
         setpointgained(response.data.gained_loyalty)
         getpaymentType(response.data.products)
         setdeliveryDate(isdelivereable == 1 ? route.params.shippingDate : response.data.shipping_date)
         setshipCharge(response.data.ship_charge)
      }
   }
   const getpaymentType = async (productData) => {
      let response = await Request.post('get-payment-type.php')
      console.log("getpaymenttype", response);
      if (response.success == true) {
         setpaymentArray(response.data)
         productData.map((item) => {
            console.log("productDataitemmmm", item);
            if (item.evoucher_det != null && item.evoucher_det != '') {
               voucherData.push(item)

            }
         })
         console.log("voucherData", productData);
         if (voucherData) {
            console.log("voucherDataiff", voucherData, response.data.length);
            if (voucherData.length < productData.length) {
               console.log("voucherDatavoucherDataepayment1");
               setepaymentData(false)
               setcreditBox(false)
               setcashBox(true)
            }
            else {
               console.log("voucherDatavoucherDataepayment2");
               setepaymentData(true)
               setcreditBox(true)
               setcashBox(false)
            }
         }
         else {
            console.log("voucherDatai3lse", voucherData);

            setepaymentData(false)
         }
      }
   }
   const onValueChange = useCallback(
      (event, newDate) => {
         const selectedDate = newDate || date;
         showPicker(false);
         setDate(moment(selectedDate, "MM/YYYY")
         );
      },
      [date, showPicker],
   );

   const CreditFunction = () => {
      setcreditBox(true)
      setcardBox(false)
      setcashBox(false)
   }
   const CardFunction = () => {
      setcardBox(true)
      setcreditBox(false)
      setcashBox(false)
   }
   const cashFunction = () => {
      setcashBox(true)
      setcreditBox(false)
      setcardBox(false)
   }

   const savePlaceOrder = async () => {
      var basketData = await StorageService.getItem(StorageService.STORAGE_KEYS.BASKET_DATA)

      await StorageService.saveItem(StorageService.STORAGE_KEYS.SHIPPING_DATE, '')
      if (creditBox == true) {
         let params = {
            gift_message: shippingData[0].giftMessage,
            gift_box: shippingData[0].giftMessage == '' ? 0 : 1,
            gift_wrap: shippingData[0].selectgiftwrap == true ? 1 : 0,
            payment_type: cashBox == true ? '1' : cardBox == true ? '2' : '0',
            ship_address_id: route.params.shippingId.id,
            delivery_date: route.params.shippingDate,
            coupon_code: shippingData[0].coupon_code,
            coupon_discount: discount,
            voucher_code: discountcode,
            voucher_type: discountype,
            is_redeemed: isredeem == '' ? 0 : 1,
            redeemed_amount: pointBalance,
            store_id: route.params.storeId ? route.params.storeId : ''

         }
         console.log("paramsparams", params);
         setModalVisible(true)

         setvoucherCode('')
         setpointBalance('')
         navigation.navigate('CreditCardViewController', { params })

      } else {

         let params = {
            gift_message: shippingData[0].giftMessage,
            gift_box: shippingData[0].giftMessage == '' ? 0 : 1,
            gift_wrap: shippingData[0].selectgiftwrap == true ? 1 : 0,
            payment_type: cashBox == true ? '1' : cardBox == true ? '2' : '0',
            ship_address_id: route.params.shippingId.id,
            delivery_date: route.params.shippingDate,
            coupon_code: shippingData[0].coupon_code,
            coupon_discount: discount,
            voucher_code: discountcode,
            voucher_type: discountype,
            is_redeemed: isredeem == '' ? 0 : 1,
            redeemed_amount: pointBalance,
            store_id: route.params.storeId ? route.params.storeId : ''

         }
         console.log("paramsparams", params);
         setModalVisible(true)
         let response = await Request.post('payment1.php', params)
         setModalVisible(false)
         if (response.success == true) {
            setvoucherCode('')
            setpointBalance('')
            StorageService.saveItem(StorageService.STORAGE_KEYS.BASKET_DATA, '')
            showSimpleAlert(response.message)
            navigation.navigate('ThankyouViewController', { orderId: response.data.order_id })
         }
         else {
            if (response) {
               showSimpleAlert(response.message)
            }
         }
      }
   }

   const verifycode = async () => {
      if (verifyCode == '') {
         showSimpleAlert('Please enter verification code')
      }

      else {
         let params = {
            otp: verifyCode,
            points: pointBalance,
            total: total
         }
         setModalVisible(true)
         let response = await Request.post('verify-redeem-otp.php', params)
         setModalVisible(false)
         setverifycodeModal(false)

         if (response.success == true) {

            let params = { func: 'fetch', domain_id: 1, coupon_code: shippingData[0].coupon_code, redeem_amount: pointBalance, voucher_amount: voucher_amt, city_name: route.params.shippingId.state, store_id: route.params.storeId ? route.params.storeId : '' }
            let response = await Request.post('manage-cart.php', params)
            if (response.success == true) {
               setdiscount(response.data.discount)
               settotal(response.data.grand_total)
               setsubtotal(response.data.sub_total)
               setpointgained(response.data.gained_loyalty)
               setshipCharge(response.data.ship_charge)
               setdeliveryDate(isdelivereable == 1 ? route.params.shippingDate : response.data.shipping_date)
               setisredeem('1')
               setloyaltyPoint(pointBalance)
            }
         }
         else {
            if (response) {
               showSimpleAlert(response.message)
            }
         }
      }

   }

   const validationOfField = () => {

   }
   const sendCode = async () => {
      let params = {
         points: pointBalance,
         total: total,
         city_name: route.params.shippingId.state
      }
      setModalVisible(true)
      let response = await Request.post('send-redeem-otp.php', params)
      setModalVisible(false)
      setredeemModal(false)
      if (response.success == true) {
         setverifycodeModal(true)
         setverifyCode('')
      }
      else {
         if (response) {
            showSimpleAlert(response.message)
         }
      }
   }
   const redeempoint = async () => {
      if (pointBalance == '') {
         showSimpleAlert('Please enter points')
      }
      else {
         setredeemModal(true)
      }
   }

   const redeemvoucherData = async () => {
      console.log("verifyData", verifyData);
      if (voucherCode == '') {
         showSimpleAlert('Please enter voucher code')
      }
      else if (verifyData == voucherCode) {
         showSimpleAlert('Voucher Already Applied')

      }
      else {
         let params = {
            code: voucherCode,
            discount: JSON.stringify(discount),
            grand_total: JSON.stringify(total)
         }
         setModalVisible(true)
         let response = await Request.post('manage-voucher.php', params)
         setModalVisible(false)
         if (response.success == true) {
            setverifyData(response.data.code)
            showSimpleAlert(response.message)
            setdiscountcode(response.data.code)
            setdiscountype(response.data.type)
            setvoucherRedeem(response.data.amount)
            var basketData = await StorageService.getItem(StorageService.STORAGE_KEYS.BASKET_DATA)
            console.log("basketData23", basketData);
            console.log(" shippingData[0].coupon_code", shippingData[0].coupon_code);
            setvoucher_amt(response.data.amount)
            let params = { func: 'fetch', domain_id: 1, coupon_code: shippingData[0].coupon_code, redeem_amount: pointBalance, voucher_amount: response.data.amount, city_name: route.params.shippingId.state, store_id: route.params.storeId ? route.params.storeId : '' }
            let response2 = await Request.post('manage-cart.php', params)
            if (response2.success == true) {
               setdiscount(response2.data.discount)
               settotal(response2.data.grand_total)
               setsubtotal(response2.data.sub_total)
               setpointgained(response2.data.gained_loyalty)
               setshipCharge(response2.data.ship_charge)
               setdeliveryDate(isdelivereable == 1 ? route.params.shippingDate : response2.data.shipping_date)
            }
         }
         else {
            if (response) {
               showSimpleAlert(response.message)
            }
         }
      }
   }
   const paymentrenderItem = ({ item, index }) => {


      console.log("productDatatatataitem", item,);
      console.log("productDatatatatavoucherProductData", epaymentData, voucherProductData, productArray);
      // voucherProductData.length != productArray.length ? setcashBox(true) : setcashBox(false)

      return (
         <View>
            {item.pay_type == 'Cash App' && epaymentData == false ?
               <View style={styles.cardView}>
                  <TouchableOpacity style={{ alignSelf: 'center' }} onPress={() => cashFunction()} disabled={isModalVisible == true ? true : false}>
                     {cashBox ? <Image source={importImages.Checkbox} /> :
                        <Image source={importImages.blankCheckbox} />}
                  </TouchableOpacity>
                  <Text style={[styles.labelText, { marginHorizontal: scale(10) }]}>Cash On Delivery</Text>
               </View> : null}

            {item.pay_type == 'Card App' ?
               <View style={styles.cardView}>
                  <TouchableOpacity style={{ alignSelf: 'center' }} onPress={() => CardFunction()} disabled={isModalVisible == true ? true : false}>
                     {cardBox ? <Image source={importImages.Checkbox} /> :
                        <Image source={importImages.blankCheckbox} />}
                  </TouchableOpacity>
                  <Text style={[styles.labelText, { marginHorizontal: scale(10) }]}>Card On Delivery</Text>
               </View> : null}
            {item.pay_type == 'Credit App' ?
               <View style={styles.cardView}>
                  <TouchableOpacity style={{ alignSelf: 'center' }} onPress={CreditFunction} disabled={isModalVisible == true ? true : false}>
                     {creditBox ? <Image source={importImages.Checkbox} style={{ alignSelf: 'center' }} /> :
                        <Image source={importImages.blankCheckbox} style={{ alignSelf: 'center' }} />}
                  </TouchableOpacity>
                  <Text style={[styles.labelText, { marginHorizontal: scale(10) }]}>Credit</Text>
               </View> : null}
         </View>
      )

   }

   return (
      <View style={styles.container}>
         <ScrollView bounces={false}>
            <KeyboardAwareScrollView
               contentContainerStyle={{ flexGrow: 1 }}
               bounces={false}
               keyboardShouldPersistTaps={'always'}
               showsVerticalScrollIndicator={false}
               extraScrollHeight={20}>
               <View style={{ marginHorizontal: scale(40) }}>

                  <CustomHeader
                     style={{ marginLeft: scale(5) }}
                     leftBtnOnPress={() => navigation.goBack()}
                     leftBtn={<Image source={importImages.backButtonArrow}></Image>}
                     headerTitle={'PAYMENT'}
                  />
                  <View style={styles.subContainer}>
                     <TouchableOpacity style={styles.subView} disabled={isModalVisible == true ? true : false} onPress={() => {
                        setredeemPoint(!redeemPoint)
                     }}>
                        <Text style={styles.redeemPointText}>REDEEM POINT</Text>
                        <View style={styles.dropDownImage} >
                           <Image source={importImages.dropDown} />
                        </View>
                     </TouchableOpacity>
                     {redeemPoint ?
                        <View style={styles.borderline} /> : null}

                     {redeemPoint ?
                        <View style={styles.subListViewStyle}>
                           <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
                              <Text style={styles.labelText}>{'Loyalty Point'} </Text>
                              <Text style={{ color: 'red', fontSize: scale(12), fontFamily: Fonts.montserratMedium, textAlign: "center", }}> {parseInt(totalpointbalance)}</Text>
                           </View>
                           <TextInput
                              style={[styles.popUpTextInputStyle, { marginLeft: scale(20), padding: 0 }]}
                              value={pointBalance}
                              keyboardType='number-pad'
                              placeholderTextColor={commonColors.Grey}
                              onChangeText={(value) => { setpointBalance(value) }}
                           ></TextInput>
                           <TouchableOpacity disabled={isModalVisible == true ? true : false} onPress={() => {
                              redeempoint()
                           }}>
                              <View style={styles.redeemView}>
                                 <Text style={styles.redeemText}>Redeem</Text>
                              </View>
                           </TouchableOpacity>
                        </View> : null}


                     <View style={styles.borderline} />

                     <TouchableOpacity style={styles.subView} onPress={() => {
                        setredeemVoucher(!redeemVoucher)
                     }} disabled={isModalVisible == true ? true : false}>
                        <Text style={styles.redeemPointText}>REDEEM VOUCHER</Text>
                        <View style={styles.dropDownImage} >
                           <Image source={importImages.dropDown} />
                        </View>
                     </TouchableOpacity>
                     {redeemVoucher ?
                        <View style={styles.borderline} /> : null}
                     {redeemVoucher ?
                        <View style={styles.subListViewStyle}>
                           <Text style={styles.labelText}>{'Voucher Code'}</Text>
                           <TextInput
                              style={[styles.popUpTextInputStyle, { marginLeft: scale(40), padding: 0 }]}
                              value={voucherCode}
                              placeholderTextColor={commonColors.Grey}
                              onChangeText={(value) => { setvoucherCode(value) }}
                           ></TextInput>
                           <TouchableOpacity disabled={isModalVisible == true ? true : false} style={styles.redeemView} onPress={() => { redeemvoucherData() }}>
                              <View style={styles.redeemView}>
                                 <Text style={styles.redeemText}>Redeem</Text>
                              </View>
                           </TouchableOpacity>
                        </View> : null}

                     <View style={{ marginHorizontal: scale(10) }}>
                        <Text style={styles.headerText}>ORDER SUMMARY</Text>
                        <View style={styles.subView2}>
                           <Text style={styles.orderSummaryText}>Sub Total</Text>
                           <Text style={styles.orderSummaryText}> {subtotal ? parseFloat(subtotal).toFixed(2) : 0}JD`S</Text>
                        </View>
                        <View style={styles.subView2}>
                           <Text style={styles.orderSummaryText}>Discount</Text>
                           <Text style={styles.orderSummaryText}>{discount ? parseFloat(discount).toFixed(2) : 0} JD`S</Text>
                        </View>
                        <View style={styles.subView2}>
                           <Text style={styles.orderSummaryText}>Gift Wrap</Text>
                           <Text style={styles.orderSummaryText}>{giftwrap}</Text>
                        </View>
                        <View style={styles.subView2}>
                           <Text style={styles.orderSummaryText}>Points Redeemed </Text>
                           <Text style={styles.orderSummaryText}>{loyaltyPoint ? parseFloat(loyaltyPoint).toFixed(2) : 0} JD`S</Text>
                        </View>
                        <View style={styles.subView2}>
                           <Text style={styles.orderSummaryText}>Point Gained</Text>
                           <Text style={styles.orderSummaryText}>{pointgained}</Text>
                        </View>
                        <View style={styles.subView2}>
                           <Text style={styles.orderSummaryText}>Voucher Redeemed</Text>
                           <Text style={styles.orderSummaryText}>{voucherRedeem ? parseFloat(voucherRedeem).toFixed(2) : 0} JD`s</Text>
                        </View>
                        {route.params.storeId ? null :
                           <View style={styles.subView2}>
                              <Text style={styles.orderSummaryText}>Delivery</Text>
                              <Text style={styles.orderSummaryText}>{shipCharge ? parseFloat(shipCharge).toFixed(2) : 0}  JD`S</Text>
                           </View>}
                        <View style={styles.subView2}>
                           <Text style={styles.orderSummaryText}>Total</Text>
                           <Text style={styles.orderSummaryText}>{total ? parseFloat(total).toFixed(2) : 0}  JD`S</Text>
                        </View>

                        <View style={styles.subView2}>
                           <Text style={styles.orderSummaryText}>Expected Delivery Date</Text>
                           <Text style={styles.orderSummaryText}>{deliveryDate ? moment(deliveryDate).format('DD MMM YYYY') : null} </Text>
                        </View>
                     </View>

                     <View style={styles.borderline} />

                     <TouchableOpacity style={[styles.subView, { alignItems: "center" }]} disabled={isModalVisible == true ? true : false} onPress={() => {
                        setpaymentMethod(!paymentMethod)
                     }}>
                        <Text style={styles.redeemPointText}>PAYMENT METHOD</Text>
                        <View style={styles.dropDownImage}>
                           <Image source={importImages.dropDown} />
                        </View>
                     </TouchableOpacity>
                     {paymentMethod ?
                        <FlatList
                           data={paymentArray}
                           extraData={paymentArray} s
                           scrollEnabled={false}
                           renderItem={paymentrenderItem}
                        /> : null}



                     <Modal
                        style={{ bottom: 0, backgroundColor: commonColors.Grey }}
                        animationType="slide"
                        transparent={true}
                        visible={redeemModal}
                        onRequestClose={() => {
                           setredeemModal(!redeemModal)
                        }}>
                        <TouchableOpacity
                           underlayColor={'transparent'}
                           onPress={() => {
                              setredeemModal(!redeemModal)
                           }
                           }
                           activeOpacity={1}
                           style={styles.outerViewModalStyle}>
                           <View style={styles.centerView}>
                              <View style={styles.modalView}>
                                 <Image source={importImages.crossImage} style={{ alignSelf: 'flex-end', tintColor: 'gray' }} />
                                 <Text style={styles.loyaltyText}>{'Redeem loyalty points?'}</Text>
                                 <Text style={styles.confirmationCodeText}>{'A confirmation code will be send to your email and phone to redeem loyalty points'}</Text>
                                 <View style={styles.buttonView}>
                                    <TouchableOpacity onPress={() => {
                                       sendCode()
                                    }}>
                                       <View style={styles.sendView}>
                                          <Text style={[styles.sendText, { color: commonColors.White }]}>{'Send Code'}</Text>
                                       </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => {
                                       setredeemModal(!redeemModal)
                                    }}>
                                       <View style={[styles.sendView, { backgroundColor: commonColors.White, borderWidth: 1, borderColor: commonColors.Blue }]}>
                                          <Text style={styles.sendText}>{'No'}</Text>
                                       </View>
                                    </TouchableOpacity>
                                 </View>
                              </View>
                           </View>
                        </TouchableOpacity>
                     </Modal>
                     <Modal
                        style={{ bottom: 0, backgroundColor: commonColors.Grey }}
                        animationType="slide"
                        transparent={true}
                        visible={verifycodeModal}
                        onRequestClose={() => {
                           setverifycodeModal(!verifycodeModal)
                        }}
                        >
                        <TouchableOpacity
                           underlayColor={'transparent'}
                           onPress={() => {
                              setverifycodeModal(!verifycodeModal)
                           }
                           }
                           activeOpacity={1}
                           style={styles.outerViewModalStyle}>
                           <View style={styles.centerView}>
                              <View style={styles.modalView}>
                                 <Image source={importImages.crossImage} style={{ alignSelf: 'flex-end', tintColor: 'gray' }} />
                                 <Text style={styles.loyaltyText}>{'Verify Code'}</Text>
                                 <Text style={styles.confirmationCodeText}>{'Please provide the confirmation code send to your email or phone'}</Text>
                                 <TextInput
                                    style={styles.emailIdText}
                                    placeholder="Your Verification Code"
                                    placeholderTextColor={commonColors.Grey}
                                    keyboardType='email-address'
                                    value={verifyCode}
                                    autoCapitalize='none'
                                    onChangeText={(value) => setverifyCode(value)}
                                 />
                                 <View style={styles.buttonView}>
                                    <TouchableOpacity disabled={isModalVisible == true ? true : false} onPress={() => {
                                       verifycode()
                                    }}>
                                       <View style={styles.sendView}>
                                          <Text style={[styles.sendText, { color: commonColors.White }]}>{'Verify Code'}</Text>
                                       </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity disabled={isModalVisible == true ? true : false} onPress={() => {
                                       setverifycodeModal(!verifycodeModal)
                                    }}>
                                       <View style={[styles.sendView, { backgroundColor: commonColors.White, borderWidth: 1, borderColor: commonColors.Blue }]}>
                                          <Text style={styles.sendText}>{'No'}</Text>
                                       </View>
                                    </TouchableOpacity>
                                 </View>
                              </View>
                           </View>
                        </TouchableOpacity>
                     </Modal>
                  </View>
               </View>

            </KeyboardAwareScrollView>

         </ScrollView>

         {isModalVisible ? null : <CustomButton
            disabled={isModalVisible == true ? true : false}
            onPress={() => savePlaceOrder()}
            title={'PLACE ORDER'}
         />}
         {show && (
            <MonthPicker
               onChange={onValueChange}
               value={date}
               minimumDate={new Date()}
               maximumDate={new Date(2025, 5)}
               locale="en"
            />
         )}
         {
            isModalVisible &&
            <BallIndicator visible={isModalVisible} />
         }

      </View>
   );
}



const styles = StyleSheet.create({
   container: {
      backgroundColor: commonColors.White,
      flex: 1
   },
   headerLogoStyle: {
      flexDirection: 'row',
      justifyContent: "flex-start",
      marginVertical: scale(20)
   },
   headerText2: {
      color: commonColors.Blue,
      fontSize: scale(18),
      alignSelf: "center",
      paddingLeft: scale(10),
      fontFamily: Fonts.montserratSemiBold,
   },
   headerText: {
      color: commonColors.Blue,
      fontSize: scale(16),
      paddingVertical: scale(20),
      fontFamily: Fonts.montserratSemiBold,
   },
   buttonView: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginHorizontal: scale(20)
   },
   subView: {
      flexDirection: "row",
      justifyContent: "space-between"
   },
   subView2: {
      flexDirection: "row",
      justifyContent: "space-between",
      marginVertical: scale(5)
   },
   borderline: {
      borderWidth: 1,
      marginVertical: scale(15),
      borderColor: commonColors.LightGrey,
      width: deviceWidth - 60
   },
   redeemPointText: {
      fontFamily: Fonts.montserratSemiBold,
      textAlign: "center",
      fontSize: scale(16),
      paddingHorizontal: scale(10),
      color: commonColors.Blue
   },
   ButtonStyle: {
      height: scale(50),
      borderWidth: 1,
      alignSelf: 'center',
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 5,
      borderColor: commonColors.Blue,
      width: deviceWidth - 30,
      marginVertical: scale(10),
      backgroundColor: commonColors.Blue,
   },
   subContainer: {
      marginVertical: scale(20),
   },
   subListViewStyle: {
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      marginVertical: scale(15),
      marginHorizontal: scale(12)
   },
   labelText: {
      fontSize: scale(12),
      fontFamily: Fonts.montserratMedium,
      color: commonColors.Blue,
      textAlign: "center"

   },
   popUpTextInputStyle: {
      marginHorizontal: scale(5),
      borderWidth: 0.8,
      borderColor: commonColors.Blue,
      borderRadius: scale(2),
      color: commonColors.Blue,
      fontSize: 12,
      justifyContent: "center",
      fontFamily: Fonts.montserratMedium,
      paddingHorizontal: scale(5),
      width: (deviceWidth - 20) / 4,
      height: scale(25),
      // height: '60%'
   },
   cardnumberInput: {
      borderWidth: 0.8,
      marginTop: scale(5),
      borderColor: commonColors.Blue,
      borderRadius: scale(2),
      color: commonColors.Blue,
      fontSize: scale(14),
      fontFamily: Fonts.montserratMedium,
      width: (deviceWidth - 80),
      height: scale(25),
      paddingHorizontal: scale(5),
      marginHorizontal: scale(5)
   },
   expiryDate: {
      borderWidth: 0.8,
      marginTop: scale(5),
      borderColor: commonColors.Blue,
      borderRadius: scale(2),
      color: commonColors.Blue,
      fontSize: scale(14),
      fontFamily: Fonts.montserratMedium,
      paddingHorizontal: scale(5),
      width: (deviceWidth - 90) / 2,
      height: scale(25),
      marginHorizontal: scale(5),
   },
   nextText: {
      fontSize: scale(20),
      fontFamily: Fonts.montserratMedium,
      color: commonColors.White,
   },
   redeemView: {
      width: scale(50),
      height: scale(25),
      borderRadius: scale(5),
      backgroundColor: commonColors.Blue,
      justifyContent: "center"
   },
   redeemText: {
      color: commonColors.White,
      fontSize: 11,
      fontFamily: Fonts.montserratMedium,
      fontWeight: "900",
      textAlign: "center"
   },
   dropDownImage: {
      // top: scale(5),
      alignSelf: "center",
      marginRight: scale(20)
   },
   orderSummaryText: {
      color: commonColors.Blue,
      opacity: 0.6,
      fontFamily: Fonts.montserratMedium,
      fontSize: scale(12)
   },
   cardView: {
      flexDirection: "row",
      marginTop: scale(20),
      marginHorizontal: scale(10)
   },
   creditcardView: {
      marginTop: scale(20)
   },
   dateText: {
      fontFamily: Fonts.montserratMedium,
      color: commonColors.Blue,
      fontSize: scale(10)
   },
   outerViewModalStyle: {
      height: deviceHeight - 200,
      flex: 1,
      backgroundColor: commonColors.LightGrey,
      justifyContent: 'center',
      justifyContent: 'flex-end',
   },
   centerView: {
      flex: 1,
      marginHorizontal: scale(30),
      justifyContent: 'flex-start',
      marginTop: scale(100),
   },
   modalView: {
      backgroundColor: 'white',
      borderRadius: 10,
      marginVertical: scale(60),
      paddingVertical: scale(20),
      paddingHorizontal: scale(20),
      shadowColor: '#000',
      shadowOffset: {
         width: 0,
         height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,
   },
   confirmationCodeText: {
      color: commonColors.Blue,
      fontSize: scale(10),
      paddingVertical: scale(20),
      fontFamily: Fonts.montserratRegular,
      textAlign: 'center'
   },
   loyaltyText: {
      color: commonColors.Blue,
      fontSize: scale(14),
      fontFamily: Fonts.montserratMedium,
      textAlign: 'center'
   },
   emailIdText: {
      marginLeft: 10,
      fontSize: 17,
      padding: scale(10),
      borderWidth: 1,
      borderColor: commonColors.Grey,
      marginVertical: scale(20),
      borderRadius: scale(5),
      width: (deviceWidth + 100) / 2,
      fontSize: 16,
      fontFamily: Fonts.montserratRegular,
      color: commonColors.Blue,
   },
   sendText: { color: commonColors.Blue, fontFamily: Fonts.montserratMedium },
   sendView: { backgroundColor: commonColors.Blue, height: scale(40), width: scale(100), justifyContent: 'center', alignItems: 'center' }
})



