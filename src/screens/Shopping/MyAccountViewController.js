import { React, Alert, CommonActions, showSimpleAlert, Request, importImages, TouchableOpacity, TextInput, useState, useEffect, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale } from '../../utils/importLibrary'

export default function MyAccountViewController({ route, navigation }) {
   const [arrMainCategories, setArrMainCategories] = useState([0, 1])
   const [isModalVisible, setModalVisible] = useState(false);

   const [loginKey, setloginKey] = useState('')
   const { ColorName, colors, icons, setScheme, } = useTheme();

   useEffect(() => {
      const unsubscribe = navigation.addListener('focus', async () => {
         await checkLogin();
      });
      return unsubscribe;
   }, [navigation])

   const checkLogin = async () => {
      let response = await Request.post('user.php',)
      console.log("response", response);
      if (response) {
         let userData = await StorageService.getItem(StorageService.STORAGE_KEYS.USER_DETAILS)
         setloginKey(userData)
      } else {
         setloginKey('')

      }

   }
   const btnTermsAndConditionClick = () => {
      navigation.navigate('TermsAndConditionsAndPolicyViewController')
   }

   const btnContactUsClick = () => {
      navigation.navigate('ContactUsViewController')
   }

   const btnStoreLocatorClick = () => {
      navigation.navigate('StoreListingViewController')
   }

   const profileViewClick = () => {
      navigation.navigate('MyProfileScreen')
   }

   const requestProduct = () => {
      if (loginKey) {
         navigation.navigate('RequestProductViewController')

      } else {
         navigation.navigate('AuthSelection', { authFlag: 1 })
      }
   }
   const alertBox = () => {
      Alert.alert(
         ConstantsText.appName,
         ConstantsText.logOutMessage,
         [
            { text: "Ok", onPress: () => onPressLogout() },
            { text: "Cancel", onPress: () => { } }
         ],
         { cancelable: false }
      );
   }
   const loginBox = () => {
      navigation.navigate('AuthSelection', { authFlag: 1 })

   }

   const onPressLogout = async () => {
      setModalVisible(true)
      let response = await Request.post('logout.php')
      setModalVisible(false)
      if (response.success == true) {

         await StorageService.saveItem(StorageService.STORAGE_KEYS.USER_DETAILS, '')
         await StorageService.clear()
         navigation.navigate('AuthSelection')
         showSimpleAlert(response.message)
      }
      else {
         if (response) {
            showSimpleAlert(response.message)
         }
      }
   }
   return (
      <View style={styles.mainViewStyle}>
         <View style={styles.headerLogoStyle}>
            <Text style={{ fontFamily: Fonts.montserratSemiBold, fontSize: 20, color: commonColors.Blue, marginBottom: 8 }}>SETTINGS</Text>
         </View>
         <View style={styles.seperatoreStyle}></View>
         <ScrollView style={{ flex: 1, }} bounces={false} showsVerticalScrollIndicator={false}>
            {loginKey ?
               <View>
                  <TouchableOpacity style={styles.optionsTextStyle} onPress={() => profileViewClick()}>
                     <Text style={styles.settingLabelTextStyle}>My Profile</Text>
                  </TouchableOpacity>
                  <View style={styles.seperatoreStyle}></View>

                  <TouchableOpacity style={styles.optionsTextStyle} onPress={() => { navigation.navigate('MyOrderViewController') }}>
                     <Text style={styles.settingLabelTextStyle}>My Orders</Text>
                  </TouchableOpacity>
                  <View style={styles.seperatoreStyle}></View>
               </View>
               : null}
            {loginKey ?
               <View>
                  <TouchableOpacity style={styles.optionsTextStyle} onPress={() => {
                     navigation.navigate('HomeScreen')
                  }}>
                     <Text style={styles.settingLabelTextStyle}>Gift Club</Text>
                  </TouchableOpacity>
                  <View style={styles.seperatoreStyle}></View>
               </View> : null}
            {loginKey ?
               <View>
                  <TouchableOpacity style={styles.optionsTextStyle} onPress={() => { navigation.navigate('NotificationViewController') }}>
                     <Text style={styles.settingLabelTextStyle}>Notification</Text>
                  </TouchableOpacity>
                  <View style={styles.seperatoreStyle}></View>
               </View> : null}
            <TouchableOpacity onPress={() => btnStoreLocatorClick()} style={[styles.optionsTextStyle, { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }]}>
               <Text style={styles.settingLabelTextStyle}>Store Locator</Text>
               <Image source={importImages.detailArrowImage} style={{ marginRight: 28 }} />
            </TouchableOpacity>
            <View style={styles.seperatoreStyle}></View>
            {loginKey ?
               <View>
                  <TouchableOpacity style={styles.optionsTextStyle} onPress={() => requestProduct()}>
                     <Text style={styles.settingLabelTextStyle}>Request a Product</Text>
                  </TouchableOpacity>
                  <View style={styles.seperatoreStyle}></View></View> : null}
            {loginKey ?
               <View>
                  <TouchableOpacity style={styles.optionsTextStyle} onPress={() => navigation.navigate('ScannerScreen')}>
                     <Text style={styles.settingLabelTextStyle}>View QR Code</Text>
                  </TouchableOpacity>
                  <View style={styles.seperatoreStyle}></View></View> : null}
            <TouchableOpacity onPress={() => btnTermsAndConditionClick()} style={[styles.optionsTextStyle, { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }]}>
               <Text style={styles.settingLabelTextStyle}>T&C & Policy</Text>
               <Image source={importImages.detailArrowImage} style={{ marginRight: 28 }} />
            </TouchableOpacity>
            <View style={styles.seperatoreStyle}></View>

            <TouchableOpacity onPress={() => btnContactUsClick()} style={[styles.optionsTextStyle, { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }]}>
               <Text style={styles.settingLabelTextStyle}>Contact Us</Text>
               <Image source={importImages.detailArrowImage} style={{ marginRight: 28 }} />
            </TouchableOpacity>
            <View style={styles.seperatoreStyle}></View>
            <View style={{ marginBottom: scale(60) }}>
               {loginKey ?
                  <TouchableOpacity
                     onPress={alertBox}
                     style={styles.continueButtonStyle}>
                     <Text style={styles.continueTextStyle}>{'LOGOUT'}</Text>
                  </TouchableOpacity> : <TouchableOpacity
                     onPress={loginBox}
                     style={styles.continueButtonStyle}>
                     <Text style={styles.continueTextStyle}>{'LOGIN'}</Text>
                  </TouchableOpacity>}
            </View>
         </ScrollView>

         {isModalVisible &&
            <BallIndicator visible={isModalVisible} />
         }

      </View>
   )
}

const styles = StyleSheet.create({
   mainViewStyle: {
      flex: 1,
      backgroundColor: 'white'
   },
   headerLogoStyle: {
      marginLeft: 24,
      marginTop: 24
   },
   optionsTextStyle: {
      marginLeft: 36,
      marginTop: 8,
      height: 36,
      justifyContent: 'center'
   },
   seperatoreStyle: {
      marginHorizontal: scale(30),
      borderColor: commonColors.Blue,
      borderBottomWidth: StyleSheet.hairlineWidth,
      marginVertical: scale(5),
   },
   settingLabelTextStyle: {
      fontFamily: Fonts.montserratMedium,
      fontSize: 16,
      color: commonColors.Blue
   },
   continueButtonStyle: {
      backgroundColor: commonColors.Blue,
      marginVertical: 20,
      width: deviceWidth - 45,
      height: 50,
      alignItems: 'center',
      justifyContent: 'center',
      alignSelf: 'center',
      borderRadius: 10
   },

   continueTextStyle: {
      color: commonColors.White,
      fontSize: 20,
      textTransform: 'uppercase',
      fontFamily: Fonts.montserratMedium
   },

})