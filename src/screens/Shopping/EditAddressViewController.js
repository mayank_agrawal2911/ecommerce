import CustomButton from '../../components/CustomButton';
import CustomHeader from '../../components/CustomHeader';
import { React, Text, BallIndicator, Modal, FlatList, PhoneInput, Request, useRef, isValidEmail, useEffect, showSimpleAlert, TextInput, useState, ScrollView, View, StyleSheet, TouchableOpacity, Image, Fonts, commonColors, deviceWidth, useTheme, importImages, scale, deviceHeight, KeyboardAwareScrollView, } from '../../utils/importLibrary';
import countrys from '../../utils/country.json'
import { CountryPicker } from "react-native-country-codes-picker";
import { Dropdown } from 'react-native-element-dropdown';

export default function EditAddressViewController({ route, navigation }) {
    const [TempValues, setTempValues] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')
    const [selectcheckBox, setselectcheckBox] = useState(true)
    const [customerName, setcustomerName] = useState('')
    const [emailAddress, setemailAddress] = useState('')
    const [City, setCity] = useState('')
    const [CityModal, setCityModal] = useState(false)
    const [cityData, setcityData] = useState([])
    const [AreaModal, setAreaModal] = useState(false)
    const [AreaData, setAreaData] = useState([])
    const [cityID, setcityID] = useState('')
    const [AreaId, setAreaId] = useState('')
    const [Area, setArea] = useState('')
    const [streetAddress, setstreetAddress] = useState('')
    const [buildingNumber, setbuildingNumber] = useState('')
    const [country, setCountry] = useState('962');
    const [addressId, setaddressId] = useState('');
    const [isModalVisible, setModalVisible] = useState(false);
    const [TempValuesCountry, setTempValuesCountry] = useState('962')
    const [TempValuesCountryCall, setTempValuesCountryCall] = useState('')
    const [show, setShow] = useState(false);
    const [countryCode, setCountryCode] = useState('');
    const phoneInput = useRef();
    useEffect(() => {
        getAddressData()
    }, []);

    const getAddressData = async () => {
        const addressData = route.params.addressData
        var phoneNumber = addressData.customer_phone
        const selectAddress = addressData.is_default == 1 ? true : false
        console.log("addressData", addressData);
        setcustomerName(addressData.customer_name)
        setemailAddress(addressData.customer_email)
        setPhoneNumber(addressData.customer_phone)
        setArea(addressData.state)
        setbuildingNumber(addressData.street_number)
        setstreetAddress(addressData.street)
        setCity(addressData.city)
        setaddressId(addressData.id)
        setselectcheckBox(selectAddress)

        Object.entries(countrys).map(item => {
            if (item[0] == addressData.country) {
                setCountry()
                setTempValuesCountryCall(item[0])
            }
            else {
                setTempValuesCountryCall()

            }
        })
        getcityData()
    }
    const getcityData = async () => {
        const addressData = route.params.addressData

        let response = await Request.post('get-city.php')
        if (response.success == true) {
            setcityData(response.data)
            response.data.map((item) => {
                console.log("item", item);
                if (item.value == addressData.city) {
                    selectArea(item.id)
                }

            })
        }


    }
    const addcountrycode = (values) => {
        phoneInput.current.setState({ number: '' })
        setTempValuesCountry(values.callingCode[0])
        setTempValuesCountryCall(values.cca2)
        setPhoneNumber('')
        setTempValues('')
        setCountry(values)
    }

    const saveAddress = async () => {
        const ConfirmValid = validationOfField();
        if (ConfirmValid) {
            setModalVisible(true)
            let params = {
                ship_name: customerName,
                ship_phone: phoneNumber,
                ship_email: emailAddress,
                ship_street: streetAddress,
                ship_city: City,
                ship_state: Area,
                address_id: addressId,
                ship_country: TempValuesCountryCall ? TempValuesCountryCall : route.params.addressData.country,
                default_address: selectcheckBox == true ? 1 : 0,
                ship_street_number: buildingNumber
            }
            let response = await Request.post('add-address.php', params)
            setModalVisible(false)
            if (response.success == true) {
                showSimpleAlert(response.message)
                navigation.goBack()
            }
            else {
                if (response) {

                }
            }
        }
    }


    const validationOfField = () => {
        const checkEmail = isValidEmail(emailAddress)
        var number = phoneNumber
        var numbernewvalues = number.slice(0, 1)
        if (customerName == '') {
            showSimpleAlert('Please enter customer name')
            return false;
        }
        else if (phoneNumber == '') {
            showSimpleAlert('Please enter mobile number')
            return false;
        }
        else if (country === '962' && numbernewvalues != 7) {
            showSimpleAlert('Please enter valid mobile number')
            return false;
        }
        else if (country === '962' && phoneNumber.length != 9) {
            showSimpleAlert('Please enter valid mobile number')
            return false;
        }
        else if ((phoneNumber.length < 4 || phoneNumber.length > 13) && country != '962') {
            showSimpleAlert('Please enter valid mobile number')
            return false;
        }
        else if (emailAddress == '') {
            showSimpleAlert('Please enter email')
            return false;
        }
        else if (!checkEmail) {
            showSimpleAlert('Please enter valid email')
            return false;
        }
        else if (City == '') {
            showSimpleAlert('Please enter city')
            return false;
        }
        else if (Area == '') {
            showSimpleAlert('Please enter area')
            return false;
        }

        else if (streetAddress == '') {
            showSimpleAlert('Please enter street address')
            return false;
        }
        else if (buildingNumber == '') {
            showSimpleAlert('Please enter building number')
            return false;
        }
        else {
            return true;
        }
    }
    const itemSeparator = () => {
        return (
            <View style={{ borderBottomWidth: 0.4, borderBottomColor: commonColors.Grey }}></View>
        )
    }
    const cityDatarenderItem = (item, index) => {
        return (
            <View>
                <View style={{ marginVertical: scale(10), alignSelf: "center" }}>
                    {/* <TouchableOpacity onPress={() => {
                    setcityID(item.id)
                    setCity(item.name)
                    setArea('')
                    setCityModal(!CityModal)
                }}> */}
                    <Text style={{ color: commonColors.Black, }}>{item.name}</Text>
                    {/* </TouchableOpacity> */}
                </View>
                <View style={{
                    borderColor: commonColors.Blue,
                    opacity: 0.3,
                    borderBottomWidth: StyleSheet.hairlineWidth,
                    marginVertical: scale(2),
                }} />
            </View>
        )
    }
    const AreaDatarenderItem = (item, index) => {
        return (
            <View>
                <View style={{ marginVertical: scale(10), alignSelf: "center" }}>

                    <Text style={{ color: commonColors.Black, }}>{item.name}</Text>
                </View>
                <View style={{
                    borderColor: commonColors.Blue,
                    opacity: 0.3,
                    borderBottomWidth: StyleSheet.hairlineWidth,
                    marginVertical: scale(2),
                }} />
            </View>
        )
    }
    const selectArea = async (id) => {
        let params = {
            domain_id: 1,
            state_id: id
        }
        let response = await Request.post('get-area.php', params)
        if (response.data.length > 0) {

            setAreaData(response.data)
        } else {
        }
    }
    return (
        <View style={styles.container}>
            <KeyboardAwareScrollView
                contentContainerStyle={{ flexGrow: 1 }}
                bounces={false}
                keyboardShouldPersistTaps={'always'}
                showsVerticalScrollIndicator={false}
                extraScrollHeight={20}>
                <View style={{
                    marginLeft: scale(40)
                }}>
                    <CustomHeader
                        leftBtnOnPress={() => navigation.goBack()}
                        leftBtn={<Image source={importImages.backButtonArrow}></Image>}
                        headerTitle={'EDIT ADDRESS'}
                    />
                    <View style={styles.subContainer}>
                        <View style={styles.subListViewStyle}>
                            <Text style={styles.labelText}>{'Customer Name'}</Text>
                            <TextInput
                                style={styles.popUpTextInputStyle}
                                value={customerName}
                                autoCapitalize={'none'}
                                placeholderTextColor={commonColors.Grey}
                                onChangeText={(value) => { setcustomerName(value) }}
                            ></TextInput>
                        </View>
                        <View style={styles.subListViewStyle}>

                            <Text style={styles.labelText}>{'Phone Number'}</Text>
                            <View >
                                <View style={styles.inputNumber}>
                                    <PhoneInput
                                        ref={phoneInput}
                                        defaultValue={phoneNumber == '' ? route.params.addressData.customer_phone : phoneNumber}
                                        defaultCode={TempValuesCountryCall == '' ? route.params.addressData.country : TempValuesCountryCall}
                                        layout="second"
                                        placeholder={TempValuesCountryCall === '962' ? '7XXXXXXXX' : 'XXXXXX'}

                                        value={phoneNumber == '' ? route.params.addressData.customer_phone : phoneNumber}
                                        textInputProps={{
                                            keyboardType: 'phone-pad',
                                            maxLength: TempValuesCountry != '' ? TempValuesCountry === '962' ? 9 : 13 : country === '962' ? 9 : 13,
                                            placeholderTextColor: commonColors.Blue,
                                            style: {
                                                fontSize: scale(13),
                                                fontFamily: Fonts.montserratMedium,
                                                color: commonColors.Blue,
                                                borderColor: commonColors.Blue,
                                                borderLeftWidth: 1,
                                                borderTopWidth: Platform.OS == 'ios' ? 0.4 : 0,
                                                borderRightWidth: Platform.OS == 'ios' ? 1 : 1,
                                                borderBottomWidth: Platform.OS == 'ios' ? 0.4 : 0,
                                                backgroundColor: commonColors.White,
                                                right: scale(14),
                                                height: scale(39),
                                                paddingHorizontal: scale(5),
                                                width: scale(122)
                                            }
                                        }}
                                        textContainerStyle={{
                                            fontSize: scale(14),
                                            fontFamily: Fonts.montserratMedium,
                                            color: commonColors.Blue,

                                        }}
                                        countryPickerButtonStyle={{ marginStart: -10, marginEnd: -10, }}
                                        codeTextStyle={{
                                            fontSize: scale(14),
                                            fontFamily: Fonts.montserratMedium,
                                            color: commonColors.Blue,
                                        }}
                                        filterProps={{
                                            fontSize: scale(14),
                                            placeholderTextColor: commonColors.Grey,
                                            fontFamily: Fonts.montserratMedium,
                                            color: commonColors.Blue,
                                        }}
                                        containerStyle={styles.textInputStyle}
                                        onChangeCountry={(country) => addcountrycode(country)}
                                        onChangeText={(data) => {
                                            setTempValues(data)
                                            setPhoneNumber(data)

                                        }}
                                    />
                                </View>
                            </View>
                        </View>
                        <View style={styles.subListViewStyle}>
                            <Text style={styles.labelText}>{'Email Address'}</Text>
                            <TextInput
                                style={styles.popUpTextInputStyle}
                                value={emailAddress}
                                placeholderTextColor={commonColors.Grey}
                                keyboardType='email-address'
                                autoCapitalize={'none'}
                                onChangeText={(value) => { setemailAddress(value) }}
                            ></TextInput>
                        </View>

                        <View style={styles.subListViewStyle}>

                            <Text style={styles.labelText}>{'City'}</Text>

                            <TouchableOpacity style={[styles.sortAndFilterButtonStyle2, { width: scale(185) }]}>
                                <Dropdown
                                    style={styles.dropdown}
                                    placeholderStyle={styles.placeholderStyle}
                                    selectedTextStyle={styles.selectedTextStyle}
                                    data={cityData}
                                    search={false}
                                    maxHeight={300}
                                    labelField="label"
                                    dropdownPosition={'bottom'}
                                    activeColor={commonColors.White}
                                    valueField='value'
                                    placeholder={City == '' ? 'Select City' : City}
                                    renderItem={(item) => cityDatarenderItem(item)}
                                    itemContainerStyle={{ color: commonColors.Blue }}
                                    containerStyle={{ color: commonColors.Blue }}
                                    showsVerticalScrollIndicator={false}
                                    onChange={item => {
                                        console.log("itemm:---->", item)
                                        setTimeout(async () => {
                                            setcityID(item.id)
                                            setCity(item.value)
                                            setArea('')
                                        }, 200);
                                        selectArea(item.id)

                                    }}
                                />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.subListViewStyle}>
                            <Text style={styles.labelText}>{'Area'}</Text>
                            <TouchableOpacity style={[styles.sortAndFilterButtonStyle2, { width: scale(185) }]}>
                                <Dropdown
                                    style={styles.dropdown}
                                    placeholderStyle={styles.placeholderStyle}
                                    selectedTextStyle={styles.selectedTextStyle}
                                    data={AreaData}
                                    search={false}
                                    maxHeight={300}
                                    labelField="label"
                                    dropdownPosition={'bottom'}
                                    activeColor={commonColors.White}
                                    valueField='value'

                                    placeholder={Area == '' ? 'Select Area' : Area}
                                    value={Area == '' ? 'Select Area' : Area}
                                    renderItem={(item) => AreaDatarenderItem(item)}
                                    itemContainerStyle={{ color: commonColors.Blue }}
                                    containerStyle={{ color: commonColors.Blue }}
                                    showsVerticalScrollIndicator={false}
                                    onChange={item => {
                                        setAreaId(item.id)
                                        setArea(item.value)
                                    }}
                                />
                            </TouchableOpacity>

                        </View>
                        <View style={styles.subListViewStyle}>
                            <Text style={styles.labelText}>{'Street Address'}</Text>
                            <TextInput
                                style={styles.popUpTextInputStyle}
                                value={streetAddress}
                                placeholderTextColor={commonColors.Grey}
                                keyboardType='email-address'
                                autoCapitalize={'none'}
                                onChangeText={(value) => { setstreetAddress(value) }}
                            ></TextInput>
                        </View>
                        <View style={styles.subListViewStyle}>
                            <Text style={styles.labelText}>{'Building Number'}</Text>
                            <TextInput
                                style={styles.popUpTextInputStyle}
                                value={buildingNumber}
                                placeholderTextColor={commonColors.Grey}
                                onChangeText={(value) => { setbuildingNumber(value) }}
                                keyboardType='number-pad'
                            ></TextInput>
                        </View>
                        <TouchableOpacity onPress={() => {
                            setselectcheckBox(!selectcheckBox)
                        }}>
                            <View style={{
                                flexDirection: "row", paddingTop: scale(20),
                            }}>

                                <View style={{ width: scale(5) }}>
                                    <Image source={selectcheckBox ? importImages.BlueTick : importImages.BlueBox} style={{ alignSelf: "center", justifyContent: "center", marginTop: scale(2) }} />
                                </View>
                                <Text style={styles.sameShippingText}>{'Make this my default address'}</Text>

                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAwareScrollView>
            <CustomButton
                onPress={() => saveAddress()}
                title={'SAVE'}
            />

            {
                isModalVisible &&
                <BallIndicator visible={isModalVisible} />
            }
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: commonColors.White,
        flex: 1,
    },
    headerLogoStyle: {
        flexDirection: 'row',
        justifyContent: "flex-start",
        marginVertical: scale(20)
    },
    headerText: {
        color: commonColors.Blue,
        fontSize: scale(18),
        alignSelf: "center",
        paddingLeft: scale(10),
        fontFamily: Fonts.montserratSemiBold,
    },
    ButtonStyle: {
        height: scale(50),
        borderWidth: 1,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        borderColor: commonColors.Blue,
        width: deviceWidth - 30,
        marginTop: scale(100),
        marginRight: scale(25),
        backgroundColor: commonColors.Blue,
    },
    saveText: {
        fontSize: scale(20),
        fontFamily: Fonts.montserratMedium,
        color: commonColors.White,
    },
    subContainer: {
        right: scale(20)
    },
    subListViewStyle: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginVertical: scale(10),
    },
    labelText: {
        fontSize: scale(13),
        fontFamily: Fonts.montserratMedium,
        color: commonColors.Blue
    },
    popUpTextInputStyle: {
        borderWidth: 0.8,
        borderColor: commonColors.Blue,
        borderRadius: scale(2),
        color: commonColors.Blue,
        fontSize: scale(14),
        justifyContent: "center",
        fontFamily: Fonts.montserratMedium,
        paddingHorizontal: scale(5),
        width: (deviceWidth + 20) / 2,
        height: scale(40),
    },
    nextText: {
        fontSize: scale(20),
        fontFamily: Fonts.montserratMedium,
        color: commonColors.White,
    },
    inputNumber: {
        alignSelf: 'flex-start',
    },
    textInputStyle: {
        borderWidth: 1,
        borderColor: commonColors.Blue,
        width: (deviceWidth + 20) / 2,
        height: scale(40),
        fontSize: scale(14),
        fontFamily: Fonts.montserratMedium,
        color: commonColors.Blue,
    },
    sameShippingText: {
        fontFamily: Fonts.montserratMedium,
        fontSize: scale(11),
        paddingHorizontal: scale(10),
        color: commonColors.Blue
    },
    outerViewModalStyle: {
        height: deviceHeight - 200,
        backgroundColor: 'transparent',
        justifyContent: 'center',
    },
    centerView: {
        flex: 1,
        marginHorizontal: scale(30),
        justifyContent: 'center',
    },
    modalView: {
        backgroundColor: 'white',
        borderRadius: 10,
        marginVertical: scale(60),
        paddingVertical: scale(20),
        paddingHorizontal: scale(20),
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    dropdown: {
        paddingHorizontal: scale(4),
        alignSelf: 'center',
        textAlign: 'center',
        color: commonColors.Blue,
        justifyContent: 'center',
        width: '100%',
        height: '100%',
    },
    sortAndFilterButtonStyle2: {
        marginTop: scale(8),
        flexDirection: 'row',
        height: scale(40),
        width: scale(90),
        justifyContent: 'space-between',
        alignSelf: 'center',
        borderWidth: 0.8,
        borderColor: commonColors.Blue,
        borderRadius: scale(2),
    },
    placeholderStyle: {
        fontSize: scale(12),
        fontFamily: Fonts.montserratMedium,
        alignSelf: 'center',
        textAlign: 'left',
        color: commonColors.Blue
    },
    selectedTextStyle: {
        fontSize: scale(14),
        textAlign: 'left',
        fontFamily: Fonts.montserratMedium,
        color: commonColors.Blue
    },
    textItem: {
        color: commonColors.Blue,
        fontSize: scale(12),
        alignSelf: 'center',

        fontFamily: Fonts.montserratMedium
    },
})



