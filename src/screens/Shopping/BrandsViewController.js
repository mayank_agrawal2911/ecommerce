import { React, useCallback, importImages, useRef, TouchableOpacity, TextInput, useState, useEffect, showSimpleAlert, Request, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, SectionList, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale, NavigationService } from '../../utils/importLibrary'
import { useFocusEffect } from '@react-navigation/native'
import { CommonActions } from '@react-navigation/native';

export default function BrandsViewController({ route, navigation }) {
   const [brandData, setbrandData] = useState([])
   const [brandselect, setbrandselect] = useState('')
   const [searchText, setsearchText] = useState('')
   const [indexselected, setindexselected] = useState(0)
   const [arrayholder, setarrayholder] = useState([])
   const [valueArray, setvalueArray] = useState([])
   const [brandname, setbrandname] = useState()
   const [visibleIndex, setvisibleIndex] = useState('')

   const refContainer = useRef();
   let isGesture = useRef(true).current;

   const btnBackClick = () => {
      navigation.goBack()
   }

   useEffect(() => {
      const unsubscribe = navigation.addListener('focus', () => {
         getgiftVoucher();
      });
      return unsubscribe;
   }, [navigation])

   const getgiftVoucher = async () => {
      const brandData = route.params.brandData
      const brandName = route.params.brandName
      setbrandData(brandData)
      setarrayholder(brandData)
      setbrandname(brandName)
   }


   const onselectBrand = (item) => {



      // navigation.dispatch(
      //    CommonActions.navigate({
      //       name: 'ProductListingViewController',
      //       params: {
      //          productItem: item, category: route.params.category, brand: 1
      //       },
      //    })
      // );
      // navigation.navigate('ProductListingViewController', { productItem: item, category: route.params.category,  brand: 1 })
      navigation.navigate('ProductListingViewController', { productItem: item, category: route.params.category, name: brandname, brand: 1 })
   }
   const brandItem = ({ item, index }) => {
      return (
         <View style={{ paddingVertical: scale(15) }}>
            <TouchableOpacity onPress={() => onselectBrand(item, index)}>
               <Text style={{ fontSize: scale(12), fontFamily: Fonts.montserratRegular, color: commonColors.Blue }}>{item.brand_name}</Text>
            </TouchableOpacity>
         </View>
      )
   }
   const itemSeparator = () => {
      return (
         <View style={{ borderBottomWidth: 0.4, borderBottomColor: commonColors.Grey }}></View>
      )
   }
   const renderbrandDataItem = ({ item, index }) => {
      return (
         <View style={{ paddingHorizontal: scale(40), paddingVertical: scale(5) }}>
            <Text style={{ fontSize: scale(18), fontFamily: Fonts.montserratBold, color: commonColors.Blue }}>{item.key ? item.key : item.brand_key}</Text>
            <View style={{ marginTop: scale(10), borderBottomWidth: 0.4, borderBottomColor: commonColors.Grey }}></View>
            <FlatList
               data={item.value}
               style={{ flex: 1, marginLeft: scale(10) }}
               renderItem={brandItem}
               nestedScrollEnabled={true}
               ItemSeparatorComponent={itemSeparator}
            />
         </View>
      )
   }



   const selectItem = (item, index) => {
      setindexselected(index)
      setvisibleIndex(index)
      refContainer.current.scrollToIndex({
         animated: true,
         index: index,
      })
   }
   const searchFilterFunction = async (text) => {
      setsearchText(text)
      if (text == '') {
         getgiftVoucher()
         setsearchText()
      } else {
         if (text.length >= 1) {
            let params = {
               brand_name: text
            }
            let response = await Request.post('get-brand.php', params)
            setbrandData(response.data)
         }
         else {
            setsearchText(text)
         }
      }
   }
   return (
      <View style={styles.mainViewStyle}>

         <View style={styles.headerLogoStyle}>
            <Text style={styles.headerLogoText}>{'BRANDS'}</Text>
            <TouchableOpacity onPress={() => btnBackClick()} style={{ marginLeft: 16 }}>
               <Image source={importImages.backButtonArrow} style={{}} />
            </TouchableOpacity>
         </View>
         <View style={styles.searchViewStyle}>
            <View style={{ flexDirection: 'row' }}>
               <TouchableOpacity style={styles.searchClick} onPress={() => { searchFilterFunction(searchText) }}>
                  <Image source={importImages.SearchIcon} style={styles.searchIconImage} />
               </TouchableOpacity>
               <View style={styles.searchBarView}>
                  <TextInput
                     style={styles.searchBarTextinput}
                     placeholder={'Search'}
                     placeholderTextColor={commonColors.Blue}
                     value={searchText}
                     multiline={false}
                     onChangeText={(searchText) => { searchFilterFunction(searchText) }}
                     underlineColorAndroid='transparent'
                  />
               </View>
            </View>
         </View>
         {brandData.length > 0 ?
            <View>
               <View style={{ flexDirection: "row", marginBottom: scale(100), marginTop: scale(10) }}>
                  <FlatList
                     ref={refContainer}
                     bounces={false}
                     viewabilityConfig={{
                        itemVisiblePercentThreshold: 50
                     }}
                     renderItem={renderbrandDataItem}
                     data={brandData}
                     onScrollEndDrag={() => {
                        setindexselected(-1)
                     }}
                     nestedScrollEnabled={true}
                     style={{ flex: 1, }}
                     showsVerticalScrollIndicator={false}
                     showsHorizontalScrollIndicator={false}
                     keyExtractor={(item, index) => `${index}-id`}
                  />
                  <View style={{ alignItems: "center", borderColor: commonColors.Blue, borderWidth: 1, borderRadius: scale(20), marginRight: scale(10), marginTop: scale(10), height: deviceHeight - 190, width: scale(27), }}>
                     {
                        brandData.map((item, index) => {
                           return (
                              <TouchableOpacity onPress={() => { selectItem(item, index) }} style={{ width: scale(22), alignItems: 'center', marginTop: index == 0 ? 5 : 0, marginBottom: index == brandData.length - 1 ? 5 : 0, }}>
                                 <View style={{
                                    backgroundColor: indexselected == index ? commonColors.Blue : commonColors.White,
                                    borderRadius: 20 / 2,
                                    height: 20,
                                    width: 20,
                                    justifyContent: 'center',
                                    alignItems: 'center',

                                 }}>
                                    <Text style={{ fontSize: scale(10), color: indexselected == index ? commonColors.White : commonColors.Blue, alignSelf: 'center', }}>{item.key}</Text>

                                 </View>
                              </TouchableOpacity>
                           )
                        })
                     }
                  </View>

               </View></View> : <View style={styles.nodatafoundView}>
               <Text style={styles.transactionText}>{'No Brand Data found'}</Text>
            </View>}
      </View>
   )
}

const styles = StyleSheet.create({
   mainViewStyle: {
      flex: 1,
      backgroundColor: 'white'
   },
   headerLogoStyle: {
      alignItems: 'center',
      height: 44,
      flexDirection: 'row'
   },
   textStyle: {
      marginTop: 4,
      fontFamily: Fonts.montserratMedium,
      fontSize: 13,
      color: commonColors.Blue
   },
   searchBarTextinput: {
      color: commonColors.Blue,
      fontFamily: Fonts.montserratRegular,
      fontSize: 12,
   },
   searchClick: {
      marginHorizontal: 8,
      justifyContent: 'center'
   },
   searchIconImage: {
      height: scale(12),
      alignSelf: 'center',
      width: scale(15),
      marginLeft: scale(10)
   },
   searchBarView: {
      flex: 1,
      justifyContent: 'center',
      alignSelf: 'center'
   },
   searchViewStyle: {
      height: scale(35),
      marginHorizontal: 16,
      marginHorizontal: scale(15),
      alignContent: 'center',
      borderColor: commonColors.Blue,
      borderRadius: 15,
      borderWidth: 1,
      justifyContent: 'center'
   },
   headerLogoText: { width: '100%', textAlign: 'center', position: 'absolute', fontFamily: Fonts.montserratSemiBold, fontSize: 20, color: commonColors.Blue },
   line: { width: 300, height: 1, backgroundColor: 'lightgrey', alignSelf: 'center', },
   headerText: { fontSize: scale(18), color: commonColors.Blue, alignSelf: "center", marginTop: scale(20), fontFamily: Fonts.montserratBold },
   nodatafoundView: { flex: 1, height: '100%', justifyContent: 'center', alignContent: 'center', alignSelf: 'center' },
   transactionText: { fontSize: scale(14), fontFamily: Fonts.montserratMedium, alignSelf: "center" },
   brandnameText: { fontSize: scale(14), color: commonColors.Blue, paddingVertical: scale(5) },
   familynameText: { fontSize: scale(14), color: commonColors.Blue, },
   pricerangeText: { fontSize: scale(14), color: commonColors.Blue, paddingTop: scale(15) },
   imageView: { height: scale(140), width: deviceWidth - 40, }
})