import { React, importImages, TouchableOpacity, TextInput, useState, useEffect, showSimpleAlert, moment, Request, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale, Alert } from '../../utils/importLibrary'
import SkeletonContent from 'react-native-skeleton-content-nonexpo';

export default function OrderListViewController({ route, navigation }) {
    const [arrMainCategories, setArrMainCategories] = useState([])
    const [isModalVisible, setModalVisible] = useState(false);
    const [nostorefound, setnostorefound] = useState('')
    const [loginKey, setloginKey] = useState('')

    const btnBackClick = () => {
        navigation.goBack()
    }


    useEffect(() => {
        getOrderlist();
    }, [])
    const getOrderlist = async () => {
        let userData = await StorageService.getItem(StorageService.STORAGE_KEYS.USER_DETAILS)
        setloginKey(userData)
        if (userData) {
            let orderId = route.params.orderId
            let params = { order: orderId }
            setModalVisible(true)
            let response = await Request.post('get-my-order-detail.php', params)
            setModalVisible(false)
            console.log("response34", response.data);
            if (response.success == true) {
                if (response.data) {
                    setArrMainCategories(response.data.item)
                }
                else {
                    setnostorefound('No order found')
                }
            }
            else {
                if (response) {
                    showSimpleAlert(response.message)
                }
            }
        } else {
            navigation.navigate('AuthSelection', { authFlag: 1, isfavourite: 1 })

        }
    }
    const renderNewArrivalItem = ({ item, index }) => {
        return (
            <View style={styles.renderView} >
                <View style={{ marginBottom: scale(5) }}>
                    <View style={{ flexDirection: 'row', marginLeft: 12, }}>
                        <View style={styles.imageView}>
                            {item.product_img
                                ?
                                <Image source={{
                                    uri: item.product_img
                                }} resizeMode='contain' style={styles.imageStyle} />
                                :
                                <Image source={importImages.defaultFavImage} resizeMode='contain' style={styles.imageStyle} />
                            }
                        </View>
                        <View style={styles.nameView}>
                            <Text numberOfLines={1} style={[styles.nameText, { fontWeight: "700" }]}>{item.product_no}</Text>
                            <Text numberOfLines={1} style={[styles.nameText, { color: "#627B96", paddingVertical: scale(3) }]}>{item.brand}</Text>
                            <Text numberOfLines={2} style={[styles.nameText, {
                                fontFamily: Fonts.montserratMedium,
                            }]}>{item.family_name} </Text>
                            <Text numberOfLines={2} style={[styles.nameText, {
                                fontFamily: Fonts.montserratRegular,
                                fontSize: 13,
                                color: commonColors.qtySizeMlText
                            }]}>{item.fam_name} </Text>
                            <View style={{ flexDirection: 'row', paddingVertical: scale(3) }}>
                                <Text style={[styles.textStyle, { color: "#627B96", }]}>Sales : </Text>
                                <Text style={[styles.textStyle, {}]}>{item.sales_person} </Text>
                            </View>
                        </View>
                    </View>
                    <View style={{
                        borderColor: "#627B96",
                        marginHorizontal: scale(10),
                        borderBottomWidth: scale(0.5),
                    }} />
                    <View style={{ flexDirection: "row", width: "100%" }}>
                        <View style={{ flexDirection: 'column', paddingVertical: scale(3), alignItems: "center", justifyContent: "center", width: '15%', }}>
                            <Text style={[styles.labeltextStyle, { color: "#627B96", }]}>Tax% </Text>
                            <Text style={[styles.textinputStyle, { fontWeight: "bold" }]}>{item.tax ? item.tax : 0}  </Text>
                        </View>
                        <View style={{ flexDirection: 'column', paddingVertical: scale(3), alignItems: "center", justifyContent: "center", width: '15%', }}>
                            <Text style={[styles.labeltextStyle, { color: "#627B96", }]}>Disc% </Text>
                            <Text style={[styles.textinputStyle, { fontWeight: "bold" }]}>{item.discount ? item.discount : 0} </Text>
                        </View>
                        <View style={{ flexDirection: 'column', paddingVertical: scale(3), alignItems: "center", justifyContent: "center", width: '15%', }}>
                            <Text style={[styles.labeltextStyle, { color: "#627B96", }]}>Qty </Text>
                            <Text style={[styles.textinputStyle, { fontWeight: "bold" }]}>{item.qty ? item.qty : 0}  </Text>
                        </View>
                        <View style={{ flexDirection: 'column', paddingVertical: scale(3), alignItems: "center", justifyContent: "center", width: '15%', }}>
                            <Text style={[styles.labeltextStyle, { color: "#627B96", }]}>Adv </Text>
                            <Text style={[styles.textinputStyle, { fontWeight: "bold" }]}>{item.adv ? item.adv : 0} </Text>
                        </View>
                        <View style={{ flexDirection: 'column', paddingVertical: scale(3), alignItems: "center", justifyContent: "center", width: '18%', }}>
                            <Text style={[styles.labeltextStyle, { color: "#627B96", }]}>Total </Text>
                            <Text style={[styles.textinputStyle, { fontWeight: "bold" }]}>{item.total ? item.total : 0} </Text>
                        </View>
                        <View style={{ flexDirection: 'column', paddingVertical: scale(3), alignItems: "center", justifyContent: "center", width: '20%', }}>
                            <Text style={[styles.labeltextStyle, { color: "#627B96", }]}>Retail Price </Text>
                            <Text style={[styles.textinputStyle, { fontWeight: "bold" }]}>{item.retail_price ? item.retail_price : 0} </Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

    return (
        <View style={styles.mainViewStyle}>

            <View style={styles.headerLogoStyle}>
                <Text style={{ width: '100%', textAlign: 'center', position: 'absolute', fontFamily: Fonts.montserratSemiBold, fontSize: 20, color: commonColors.Blue }}>ORDERS</Text>
                <TouchableOpacity onPress={() => btnBackClick()} style={{ marginLeft: 16 }}>
                    <Image source={importImages.backButtonArrow} style={{}} />
                </TouchableOpacity>
            </View>
            <View style={{ width: 300, height: 1, backgroundColor: 'lightgrey', alignSelf: 'center', }}></View>
            {arrMainCategories && arrMainCategories.length > 0 ?
                < FlatList
                    bounces={false}
                    renderItem={renderNewArrivalItem}
                    data={arrMainCategories}
                    style={{ marginTop: 8, flex: 1, }}
                    contentContainerStyle={{ alignItems: 'center', flexGrow: 1 }}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={(item, index) => `${index}-id`}
                /> : <View style={[styles.nodatafoundView, { marginTop: isModalVisible ? 0 : scale(40) }]}>
                    <Text style={styles.transactionText}>{nostorefound}</Text>
                </View>}
            {/* {isModalVisible ?
                <View style={{}}>
                    <SkeletonContent
                        isLoading={true}
                        containerStyle={{ flexDirection: "column", marginHorizontal: scale(15), bottom: scale(5) }}
                        layout={[
                            { key: 'someotherId4', width: (deviceWidth - 50), marginVertical: scale(5), height: scale(120), borderRadius: scale(10) },
                            { key: 'otherId5', width: (deviceWidth - 50), height: scale(120), marginVertical: scale(5), borderRadius: scale(10) },
                            { key: 'Id8', width: (deviceWidth - 50), height: scale(120), marginVertical: scale(5), borderRadius: scale(10) },
                            { key: 'id9', width: (deviceWidth - 50), height: scale(120), marginVertical: scale(5), borderRadius: scale(10) },
                            { key: 'id10', width: (deviceWidth - 50), height: scale(120), marginVertical: scale(5), borderRadius: scale(10) },
                            { key: 'id11', width: (deviceWidth - 50), height: scale(120), marginVertical: scale(5), borderRadius: scale(10) },
                        ]}
                        animationDirection="horizontalLeft"
                        animationType='shiver'
                    />

                </View> : null
            } */}


            {isModalVisible ?
                <BallIndicator visible={isModalVisible} /> : null}
        </View>
    )
}
const styles = StyleSheet.create({
    mainViewStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    headerLogoStyle: {
        alignItems: 'center',
        height: 44,
        flexDirection: 'row'
    },
    textStyle: {
        marginTop: 4,
        fontFamily: Fonts.montserratMedium,
        fontSize: 13,
        color: commonColors.Blue
    },
    labeltextStyle: {
        marginTop: 4,
        fontFamily: Fonts.montserratMedium,
        fontSize: scale(10),
        color: commonColors.Blue
    },
    textinputStyle: {
        marginTop: 4,
        fontFamily: Fonts.montserratMedium,
        fontSize: scale(11),
        color: commonColors.Blue
    },
    renderView: {
        width: (deviceWidth - 30),
        marginHorizontal: 8,
        marginTop: scale(4),
        backgroundColor: commonColors.White,
        borderRadius: 10,
        shadowColor: commonColors.Blue,
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 5,
        marginVertical: scale(10),
    },
    imageView: {
        height: scale(100),
        width: "20%",
        borderWidth: 0.8,
        borderRadius: scale(8),
        borderColor: '#BFCAD4',
        marginTop: scale(10),
        justifyContent: 'center'
    },
    imageStyle: {
        alignSelf: 'center',
        height: scale(50),
        width: scale(40),
    },

    nodatafoundView: {
        marginTop: scale(40),
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center'
    },
    transactionText: {
        fontSize: scale(16),
        color: commonColors.Blue,
        fontFamily: Fonts.montserratMedium,
        alignSelf: "center"
    },
    labelText: {
        marginTop: 8,
        fontFamily: Fonts.montserratMedium,
        fontSize: scale(12),
        color: '#627B96'
    },
    nameView: {
        marginHorizontal: 8,
        marginLeft: scale(20),
        marginTop: scale(10),
        flex: 1,
    },
    nameText: {
        fontFamily: Fonts.montserratMedium,
        fontSize: scale(14),
        color: commonColors.Blue
    },
    familynameText: {
        marginTop: 4,
        fontFamily: Fonts.montserratRegular,
        fontSize: scale(12),
        color: commonColors.Blue
    },
    productNameText: {
        marginTop: 4,
        fontFamily: Fonts.montserratRegular,
        fontSize: scale(12),
        color: commonColors.qtySizeMlText
    },
})