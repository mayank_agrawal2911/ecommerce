import { React, importImages, KeyboardAwareScrollView, moment, TouchableOpacity, TextInput, useState, useEffect, showSimpleAlert, Request, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale, Keyboard } from '../../utils/importLibrary'
import { AirbnbRating } from 'react-native-ratings';
import CustomButton from '../../components/CustomButton';

export default function WriteAReviewViewController({ route, navigation }) {
   const [isModalVisible, setModalVisible] = useState(false);
   const [counting, setcounting] = useState(5);
   const [reviewTitle, setreviewTitle] = useState('');
   const [yourReview, setyourReview] = useState('');
   const [countingData, setcountingData] = useState('');
   const [productId, setproductId] = useState('');

   const btnBackClick = () => {
      navigation.goBack()
   }

   useEffect(() => {
      const unsubscribe = navigation.addListener('focus', () => {
         getreviewData();
      });
      return unsubscribe;
   }, [navigation])


   const getreviewData = async () => {
      const review = route.params.productId
      setproductId(review)
   }

   const submitReview = async () => {
      const ConfirmValid = validationOfField();
      if (ConfirmValid) {
         Keyboard.dismiss()
         setModalVisible(true)
         let params = {
            product_id: productId,
            title: reviewTitle,
            description: yourReview,
            rating: countingData == '' ? counting : countingData
         }
         let response = await Request.post('add-item-review.php', params)
         setModalVisible(false)
         if (response.success == true) {
            showSimpleAlert(response.message)
            navigation.navigate('ShoppingHomveViewController')
         }
         else {
            if (response) {
               showSimpleAlert(response.message)
            }
         }
      }
   }
   const validationOfField = () => {
      if (reviewTitle == '') {
         showSimpleAlert('Please enter review title')
         return false;
      }
      else if (yourReview == '') {
         showSimpleAlert('Please enter your review ')
         return false;
      }
      else {
         return true;
      }
   }

   const ratingCompleted = (rating) => {
      setcountingData(rating)
   }
   return (
      <View style={styles.mainViewStyle}>
         <KeyboardAwareScrollView
            contentContainerStyle={{ flexGrow: 1 }}
            bounces={false}
            keyboardShouldPersistTaps={'always'}
            showsVerticalScrollIndicator={false}
            extraScrollHeight={10}>
            <View style={styles.headerLogoStyle}>
               <Text style={styles.reviewHeader}>{'Write a Review'}</Text>
               <TouchableOpacity onPress={() => btnBackClick()} style={{ marginLeft: 16 }}>
                  <Image source={importImages.backButtonArrow} style={{}} />
               </TouchableOpacity>
            </View>
            <View style={styles.line}></View>

            <View style={styles.reviewtitleView}>
               <Text style={styles.titleText}>REVIEW TITLE</Text>
               <TextInput
                  keyboardType={'default'}
                  autoFocus={false}
                  value={reviewTitle}
                  placeholder={'Review Title'}
                  blurOnSubmit={false}
                  returnKeyType={"next"}
                  autoCapitalize='none'
                  onChangeText={(value) => { setreviewTitle(value) }}
                  style={styles.textInputStyle}
               />
            </View>
            <View style={styles.yourReviewView}>
               <Text style={[styles.titleText, { alignSelf: 'flex-start' }]}>YOUR REVIEW</Text>
               <TextInput
                  keyboardType={'default'}
                  value={yourReview}
                  multiline={true}
                  placeholder={'Your Review'}
                  blurOnSubmit={false}
                  autoCapitalize='none'
                  onChangeText={(value) => { setyourReview(value) }}
                  style={styles.textInputStyle2}
               />
            </View>
            <View style={styles.reviewView}>
               <Text style={[styles.titleText, { alignSelf: 'flex-start' }]}>YOUR RATING</Text>
               <View style={{ bottom: scale(35), }}>
                  <AirbnbRating
                     type='custom'
                     count={counting}
                     defaultRating={5}
                     reviews={false}
                     unSelectedColor={commonColors.Grey}
                     selectedColor={commonColors.Blue}
                     onFinishRating={ratingCompleted}
                     size={20}
                  />
               </View>
            </View>
            <Text style={styles.clickText}>{'Click to rate on scale of 1-5)'}</Text>
            <CustomButton
               onPress={() => submitReview()}
               title={'SUBMIT'}
               style={styles.submitButton}
            />
            {isModalVisible &&
               <BallIndicator visible={isModalVisible} />
            }
         </KeyboardAwareScrollView>
      </View>
   )
}

const styles = StyleSheet.create({
   mainViewStyle: {
      flex: 1,
      backgroundColor: 'white'
   },
   line: {
      width: 300,
      height: 1,
      backgroundColor: 'lightgrey',
      alignSelf: 'center',
   },
   reviewtitleView: {
      flexDirection: "row",
      marginVertical: scale(20),
      marginHorizontal: scale(20)
   },
   yourReviewView: {
      flexDirection: "row",
      marginHorizontal: scale(20)
   },
   reviewHeader: {
      width: '100%',
      textAlign: 'center',
      position: 'absolute',
      fontFamily: Fonts.montserratSemiBold,
      fontSize: scale(20),
      color: commonColors.Blue
   },
   headerLogoStyle: {
      alignItems: 'center',
      height: scale(44),
      flexDirection: 'row'
   },
   submitButton: {
      width: deviceWidth - 40,
      marginHorizontal: scale(20)
   },
   titleText: {
      alignSelf: 'center',
      color: commonColors.Blue,
      paddingHorizontal: scale(10),
      fontSize: scale(14),
      fontFamily: Fonts.montserratMedium
   },
   reviewView: {
      flexDirection: "row",
      marginHorizontal: scale(20),
      marginTop: scale(20)
   },
   textInputStyle: {
      borderWidth: 0.4,
      borderColor: commonColors.Grey,
      color: commonColors.Blue,
      fontSize: scale(16),
      paddingHorizontal: scale(5),
      fontFamily: Fonts.montserratMedium,
      alignSelf: 'center',
      height: scale(40),
      width: deviceWidth - 180
   },
   textInputStyle2: {
      borderWidth: 0.4,
      borderColor: commonColors.Grey,
      color: commonColors.Blue,
      fontSize: scale(16),
      paddingHorizontal: scale(5),
      fontFamily: Fonts.montserratMedium,
      alignSelf: 'center',
      height: scale(80),
      width: deviceWidth - 180
   },
   clickText: {
      alignSelf: "center",
      bottom: scale(25),
      paddingLeft: scale(100),
      color: commonColors.Blue
   }
})
