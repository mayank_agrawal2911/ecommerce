import { React, importImages, Request, AppState, TouchableOpacity, TextInput, useState, useEffect, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, showSimpleAlert, Alert, scale, Platform } from '../../utils/importLibrary'
import SkeletonContent from 'react-native-skeleton-content-nonexpo';

export default function MyFavoriteViewController({ route, navigation }) {
   const [isModalVisible, setModalVisible] = useState(false);
   const [arrMainCategories, setArrMainCategories] = useState([])
   const [loginKey, setloginKey] = useState('')
   const [noDataText, setnoDataText] = useState('')
   const [disable, setdisable] = useState(false)

   useEffect(() => {
      const unsubscribe = navigation.addListener('focus', () => {
         checkLogin();
      });
      return unsubscribe;
   }, [navigation])


   const checkLogin = async () => {
      let userData = await StorageService.getItem(StorageService.STORAGE_KEYS.USER_DETAILS)
      setloginKey(userData)
      if (userData) {
         getFavouriteItemList()
      }
      else {
         Alert.alert(
            ConstantsText.appName,
            "Please Signin to your account",
            [
               {
                  text: "OK", onPress: () => {
                     navigation.navigate('AuthSelection', { authFlag: 1, isfavourite: 1 })
                  }
               }
            ],
         );
      }
   }


   /**
    * Api Intigration
    */
   const getFavouriteItemList = async () => {
      setModalVisible(true)
      let response = await Request.post('favorite-item-list.php',)
      setModalVisible(false)
      if (response.success == true) {
         if (response.data.length > 0) {
            setArrMainCategories(response.data)
         }
         else {
            setArrMainCategories([])
            setnoDataText('No Favourite Product Found')
         }
      }
      else {
         if (response) {
            showSimpleAlert(response.message)
         }
      }
   }

   const favoriteUnFavoriteButtonClick = async (item, index) => {
      setdisable(true)
      let params = { wish_id: item.wish_id }
      let response = await Request.post('delete-favorite-item.php', params)
      if (response.success == true) {
         setModalVisible(true)
         let response = await Request.post('favorite-item-list.php',)
         setModalVisible(false)

         if (response.success == true) {
            if (response.data.length > 0) {
               setArrMainCategories(response.data)
               setdisable(false)
            }
            else {
               setArrMainCategories([])
               setnoDataText('No Favourite Product Found')
               setdisable(false)
            }
         }
         else {
            if (response) {
               showSimpleAlert(response.message)
            }
         }
      }
      else {

         if (response) {
            showSimpleAlert(response.message)
         }
      }
   }

   const favouriteRedirect = (favItem) => {
      console.log("seoUrlData", favItem);
      if (favItem.seo_url == '') {
         navigation.navigate('ProductDetailViewController', { seoUrl: favItem.product_id })

      } else {
         navigation.navigate('ProductDetailViewController', { seoUrl: favItem.seo_url })

      }

   }
   const renderNewArrivalItem = (item, index) => {
      const favItem = item.item
      return (
         <View style={styles.renderView} >
            <TouchableOpacity onPress={() => favouriteRedirect(favItem)}>
               <View style={{ flexDirection: 'row', marginLeft: 12, }}>
                  <View style={styles.imageView}>
                     {favItem.picture ?
                        <Image source={{ uri: favItem.picture }} resizeMode='contain' style={styles.imageStyle} />
                        :
                        <Image source={importImages.defaultFavImage} resizeMode='contain' style={styles.imageStyle} />
                     }
                  </View>
                  <View style={styles.nameView}>
                     <Text numberOfLines={2} style={[styles.nameText, { height: scale(45) }]}>{favItem.brand_name}</Text>
                     <Text numberOfLines={2} style={[styles.familynameText, { height: scale(30), width: scale(210), paddingRight: scale(5) }]}>{favItem.family_name}</Text>
                     <Text numberOfLines={2} style={[styles.productNameText, { height: scale(18), width: scale(210), paddingRight: scale(5) }]}>{favItem.fam_name}</Text>

                     <View style={{ flexDirection: 'row', height: scale(30) }}>
                        <Text style={styles.priceText}>Price : </Text>
                        <Text style={styles.priceText}>{favItem.main_price} JD's</Text>
                     </View>
                  </View>
                  <TouchableOpacity disabled={disable} style={{ marginRight: scale(15), marginTop: 0, width: scale(25), height: scale(25), justifyContent: 'flex-start', alignItems: 'center' }} onPress={() => favoriteUnFavoriteButtonClick(favItem, index)}>
                     <Image source={importImages.favSelected} />
                  </TouchableOpacity>
               </View>
            </TouchableOpacity>
         </View>
      )
   }

   return (
      <View style={styles.mainViewStyle}>
         <View style={styles.headerLogoStyle}>
            <Text style={styles.headerText}>MY FAVOURITE</Text>
         </View>
         <View style={styles.borderline}></View>
         <View style={{ flex: 1, height: deviceHeight - 200, marginTop: scale(10) }} bounces={false}>
            {
               arrMainCategories && arrMainCategories.length > 0 ?
                  <FlatList
                     bounces={false}
                     renderItem={loginKey ? renderNewArrivalItem : ''}
                     data={arrMainCategories}
                     style={{ flex: 1, marginBottom: scale(55) }}
                     contentContainerStyle={{ flexGrow: 1, }}
                     showsVerticalScrollIndicator={false}
                     showsHorizontalScrollIndicator={false}
                     keyExtractor={(item, index) => `${index}-id`}
                  /> :
                  <View style={styles.noDataView}>
                     <Text style={styles.transactionText}>{noDataText}</Text>
                  </View>
            }

         </View>
         {/*         
         {isModalVisible ?
            <View style={{ flexDirection: "column", }}>

               <SkeletonContent
                  isLoading={true}
                  containerStyle={{ flexDirection: "column", marginHorizontal: scale(15) }}
                  layout={[
                     { key: 'someotherId2', width: (deviceWidth - 30), marginVertical: scale(5), height: scale(150), borderRadius: scale(10) },
                     { key: 'otherId3', width: (deviceWidth - 30), height: scale(150), marginVertical: scale(5), borderRadius: scale(10) },
                     { key: 'Id4', width: (deviceWidth - 30), height: scale(150), marginVertical: scale(5), borderRadius: scale(10) },
                     { key: 'id5', width: (deviceWidth - 30), height: scale(150), marginVertical: scale(5), borderRadius: scale(10) },
                  ]}
                  animationDirection="horizontalLeft"
                  animationType='shiver'
               />

            </View> : null
         } */}
         {isModalVisible ?
            <BallIndicator visible={isModalVisible} /> : null}
      </View >
   )
}

const styles = StyleSheet.create({
   mainViewStyle: {
      flex: 1,
      backgroundColor: 'white'
   },
   headerLogoStyle: {
      justifyContent: 'center',
      alignItems: 'center',
      height: 44
   },
   transactionText: {
      color: commonColors.Blue,
      fontSize: 20,
      fontFamily: Fonts.montserratMedium,
   },
   renderView: {
      width: (deviceWidth - 16),
      marginHorizontal: 8,
      marginTop: scale(4),
      backgroundColor: commonColors.White,
      borderRadius: 10,
      shadowColor: commonColors.Blue,
      shadowOffset: { width: 0, height: 0 },
      shadowOpacity: 0.5,
      shadowRadius: 2,
      elevation: 5,
      height: scale(150),
      marginVertical: scale(10),
      justifyContent: 'center'
   },
   headerText: {
      fontFamily: Fonts.montserratSemiBold,
      fontSize: 20,
      color: commonColors.Blue
   },
   borderline: {
      width: 300,
      height: 1,
      backgroundColor: 'lightgrey',
      alignSelf: 'center',
   },
   noDataView: {
      flex: 1,
      height: deviceHeight,
      justifyContent: 'center',
      alignContent: 'center',
      alignSelf: 'center'
   },
   imageView: {
      height: "100%",
      width: "30%",
      alignSelf: 'center',
      borderWidth: 2,
      borderRadius: scale(10),
      borderColor: '#BFCAD4',
      marginVertical: scale(10),
      justifyContent: 'center'
   },
   imageStyle: {
      alignSelf: 'center',
      height: scale(80),
      width: scale(80),
   },
   nameView: {
      marginHorizontal: 8,
      marginLeft: scale(20),
      marginTop: 0,
      flex: 1,
   },
   nameText: {
      fontFamily: Fonts.montserratMedium,
      fontSize: scale(18),
      color: commonColors.Blue
   },
   familynameText: {
      marginTop: 4,
      fontFamily: Fonts.montserratRegular,
      fontSize: 13,
      color: commonColors.Blue
   },
   productNameText: {
      marginTop: 4,
      fontFamily: Fonts.montserratRegular,
      fontSize: 13,
      color: commonColors.qtySizeMlText
   },
   priceText: {
      marginTop: 8,
      fontFamily: Fonts.montserratRegular,
      fontSize: 17,
      color: commonColors.Blue
   }
})