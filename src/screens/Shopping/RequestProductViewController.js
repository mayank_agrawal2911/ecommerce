import { React, Keyboard, Request, importImages, TouchableOpacity, TextInput, useState, useEffect, StyleSheet, Text, ScrollView, FlatList, StorageService, View, Header, Image, commonColors, TouchableWithoutFeedback, Fonts, Colors, deviceHeight, deviceWidth, ConstantsText, importIconsWhite, Modal, ImagePickerView, useTheme, SwitchView, BallIndicator, scale, showSimpleAlert } from '../../utils/importLibrary'

export default function RequestProductViewController({ route, navigation }) {
    const [reviewText, setreviewText] = useState('')
    const [isModalVisible, setModalVisible] = useState(false);

    const btnBackClick = () => {
        navigation.goBack()
    }
    const submitButtonValidation = () => {
        const ConfirmValid = validationofField();
        if (ConfirmValid) {
            Keyboard.dismiss()
            callAPI()
        }
    }
    const validationofField = () => {

        if (reviewText.trim() == '') {
            showSimpleAlert('Please enter your request')
            return false;
        }
        else {
            return true;
        }
    }

    const callAPI = async () => {
        setModalVisible(true)
        let params = { comment: reviewText }
        let response = await Request.post('request-product.php', params)
        setModalVisible(false)
        if (response.success == true) {
            setreviewText('')
            showSimpleAlert(response.message)
            navigation.goBack()
        }
        else {
            if (response) {
                showSimpleAlert(response.message)
            }
        }
    }
    return (
        <View style={styles.mainViewStyle}>
            <TouchableOpacity onPress={() => btnBackClick()} style={{ marginLeft: 16, marginTop: 16 }}>
                <Image source={importImages.backButtonArrow} style={{}} />
            </TouchableOpacity>

            <View style={styles.headerLogoStyle}>
                <Text style={{ fontFamily: Fonts.montserratSemiBold, fontSize: 24, color: commonColors.Blue, marginBottom: 8 }}>Request a product</Text>
            </View>
            <View style={styles.seperatoreStyle}></View>
            <View style={{ marginTop: scale(20) }}>
                <Text style={{ paddingHorizontal: scale(40), color: commonColors.Black, fontSize: scale(14), fontFamily: Fonts.montserratMedium }}>{'Cannot find your desire ? type your request here and we will prepare your product and send you an email'}</Text>

                <TextInput
                    style={styles.popUpTextInputStyle}
                    value={reviewText}
                    placeholderTextColor={commonColors.Grey}
                    autoCapitalize={'none'}
                    multiline={true}
                    keyboardType='email-address'
                    onChangeText={(value) => { setreviewText(value) }}
                ></TextInput>
            </View>
            <TouchableOpacity style={styles.ButtonStyle}
                onPress={() => submitButtonValidation()}
            >
                <Text style={styles.continueText}>SUBMIT</Text>
            </TouchableOpacity>
            {isModalVisible &&
                <BallIndicator visible={isModalVisible} />
            }
        </View>
    )
}
const styles = StyleSheet.create({
    mainViewStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    headerLogoStyle: {
        marginLeft: 40,
        marginTop: 24
    },
    optionsTextStyle: {
        marginLeft: 52,
        marginTop: 8,
        height: 30,
        justifyContent: 'center'
    },
    seperatoreStyle: {
        marginHorizontal: 40,
        height: 0.5,
        backgroundColor: commonColors.seperatorColor,
        marginTop: 8
    },
    textStyle: {
        fontFamily: Fonts.montserratRegular,
        fontSize: 15,
        color: commonColors.lightBlue
    },
    popUpTextInputStyle: {
        borderWidth: 0.8,
        borderColor: commonColors.Blue,
        borderRadius: scale(2),
        color: commonColors.Blue,
        fontSize: scale(14),
        fontFamily: Fonts.montserratMedium,
        width: (deviceWidth) / 1.3,
        marginHorizontal: scale(40),
        paddingHorizontal: scale(5),
        marginTop: scale(30),
        height: scale(45),
    },
    ButtonStyle: {
        height: 50,
        borderWidth: 1,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        borderColor: commonColors.Blue,
        width: deviceWidth - 80,
        backgroundColor: commonColors.Blue,
        marginTop: 50
    },
    continueText: {
        fontSize: 17,
        fontFamily: Fonts.montserratMedium,
        color: commonColors.White,
    },
})