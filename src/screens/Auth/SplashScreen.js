
import { React, View, importImages, Image, useEffect, useTheme, commonColors, StorageService, CommonActions, Request } from '../../utils/importLibrary'
export default function SplashScreen({ route, navigation }) {
   const { ColorName, colors, icons, setScheme } = useTheme();

   /** 
   * Life cycle method 
   */
   useEffect(async () => {
      await load()
   }
   )

   /**
    * Session management of screens
    */
   const load = async () => {
    
      const resetAction = CommonActions.reset({
         index: 0,
         routes: [{ name: "TabNavigator" }]
      });
      navigation.dispatch(resetAction);

   }

   /**
    * Render Method
    */
   return (
      <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: commonColors.White }}>
         <Image source={importImages.splashImg} />
      </View>

   );
}