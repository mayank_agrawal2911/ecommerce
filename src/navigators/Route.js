import { React, useEffect, useFocusEffect, useState, StorageService, SafeAreaView, NavigationService, TouchableWithoutFeedback, createNativeStackNavigator, useTheme, NavigationContainer, createBottomTabNavigator, createDrawerNavigator, Icon, getFocusedRouteNameFromRoute, View, commonColors, Image, importImages, Fonts, scale, Request } from '../utils/importLibrary'
/** IMPORTING SCREENS */

// Splash Screen
import SplashScreen from '../screens/Auth/SplashScreen'

// Auth Screens
import LoginScreen from '../screens/Auth/LoginScreen'
import AuthSelection from '../screens/Auth/AuthSelection'
import VerificationScreen from '../screens/Auth/VerificationScreen'
import SignUpScreen from '../screens/Auth/SignUpScreen'
import QrCodeScreen from '../screens/Auth/QrCodeScreen'
// Home Screens
import HomeScreen from '../screens/Home/HomeScreen'
import PointsScreen from '../screens/Home/PointsScreen'
import SpentScreen from '../screens/Home/SpentScreen'
import NotificationScreen from '../screens/Home/NotificationScreen'
import TierScreen from '../screens/Home/TierScreen'
import PointsDetailsScreen from '../screens/Home/PointsDetailsScreen'
import OrderScreen from '../screens/Home/OrderScreen'

// Card Screen
import CardScreen from '../screens/Card/CardScreen'

// Cart Screen
import CartScreen from '../screens/Cart/CartScreen'

// Scanner Screen
import ScannerScreen from '../screens/Scanner/ScannerScreen'

// Menu Screens
import SettingScreen from '../screens/Menu/SettingScreen'
import AboutScreen from '../screens/Menu/AboutScreen'
import AboutTierScreen from '../screens/Menu/AboutTierScreen'
import MyProfileScreen from '../screens/Menu/MyProfileScreen'


//Shopping App Screen
import ShoppingHomveViewController from '../screens/Shopping/ShoppingHomveViewController'
import CategoryViewController from '../screens/Shopping/CategoryViewController'
import MyBasketViewController from '../screens/Shopping/MyBasketViewController'
import MyFavoriteViewController from '../screens/Shopping/MyFavoriteViewController'
import MyAccountViewController from '../screens/Shopping/MyAccountViewController'
import CheckOutViewController from '../screens/Shopping/CheckOutViewController'
import NewAddressViewController from '../screens/Shopping/NewAddressViewController'
import BillingAddressViewController from '../screens/Shopping/BillingAddressViewController'
import PlacedOrderViewController from '../screens/Shopping/PlaceOrderViewController'
import ProductDetailViewController from '../screens/Shopping/ProductDetailViewController'
import ShippingDateViewController from '../screens/Shopping/ShippingDateViewController'
import BrandsViewController from '../screens/Shopping/BrandsViewController'
import MyProfileViewController from '../screens/Shopping/MyProfileViewController'
import ProductListingViewController from '../screens/Shopping/ProductListingViewController'
import StoreListingViewController from '../screens/Shopping/StoreListingViewController'
import WriteAReviewViewController from '../screens/Shopping/WriteAReviewViewController'
import TermsAndConditionsAndPolicyViewController from '../screens/Shopping/TermsAndConditionsAndPolicyViewController'
import ContactUsViewController from '../screens/Shopping/ContactUsViewController'
import FilterOptionViewController from '../screens/Shopping/FilterOptionViewController'
import RequestProductViewController from '../screens/Shopping/RequestProductViewController'
import PolicyViewController from '../screens/Shopping/PolicyViewController'
import GiftVoucherViewController from '../screens/Shopping/GiftVoucherViewController'
import SeeAllViewController from '../screens/Shopping/SeeAllViewController'
import ReviewDetails from '../screens/Shopping/ReviewDetails'
import ProductDetails from '../screens/Shopping/ProductDetails'
import EditAddressViewController from '../screens/Shopping/EditAddressViewController'
import MyOrderViewController from '../screens/Shopping/MyOrderViewController'
import NotificationViewController from '../screens/Shopping/NotificationViewController'
import OrderDetailsViewController from '../screens/Shopping/OrderDetailsViewController'
import DigitalGiftVoucherController from '../screens/Shopping/DigitalGiftVoucherController'
import StorelistDetails from '../screens/Shopping/StorelistDetails'
import QrcodeProductScreen from '../screens/Shopping/QrcodeProductScreen'
import { Platform, StatusBar } from 'react-native'
import { Component } from 'react'
import { getToken } from '../api/Request'
import OrderListViewController from '../screens/Shopping/OrderListViewController'
import CreditCardViewController from '../screens/Shopping/CreditCardViewController'
import ThankyouViewController from '../screens/Shopping/ThankyouViewController'
import Shareyourpurchase from '../screens/Shopping/Shareyourpurchase'
import dynamicLinks from '@react-native-firebase/dynamic-links';

const Stack = createNativeStackNavigator();
const ShopStack = createNativeStackNavigator();
const CategoryStack = createNativeStackNavigator()
const Tab = createBottomTabNavigator();
var colorThemes = ''

/**
 * Stack navigation screens
 */



const StackNavigator = () => {
    return (
        <Stack.Navigator initialRouteName='SplashScreen'>
            <Stack.Screen name={'SplashScreen'} component={SplashScreen} options={{ headerShown: false, }} />
            <Stack.Screen name={'AuthSelection'} component={AuthSelection} options={{ headerShown: false, }} />
            <Stack.Screen name={'LoginScreen'} component={LoginScreen} options={{ headerShown: false, }} />
            <Stack.Screen name={'QrCodeScreen'} component={QrCodeScreen} options={{ headerShown: false, }} />
            <Stack.Screen name={'VerificationScreen'} component={VerificationScreen} options={{ headerShown: false, }} />
            <Stack.Screen name={'SignUpScreen'} component={SignUpScreen} options={{ headerShown: false, }} />
            <Stack.Screen name={'TabNavigator'} component={TabNavigator} options={{ headerShown: false, }} />
            <Stack.Screen name={'PointsScreen'} component={PointsScreen} options={{ headerShown: false, }} />
            <Stack.Screen name={'SpentScreen'} component={SpentScreen} options={{ headerShown: false, }} />
            <Stack.Screen name={'AboutScreen'} component={AboutScreen} options={{ headerShown: false, }} />
            <Stack.Screen name={'AboutTierScreen'} component={AboutTierScreen} options={{ headerShown: false, }} />
            <Stack.Screen name={'MyProfileScreen'} component={MyProfileScreen} options={{ headerShown: false, }} />
            <Stack.Screen name={'NotificationScreen'} component={NotificationScreen} options={{ headerShown: false, }} />
            <Stack.Screen name={'TierScreen'} component={TierScreen} options={{ headerShown: false, }} />
            <Stack.Screen name={'PointsDetailsScreen'} component={PointsDetailsScreen} options={{ headerShown: false, }} />
            <Stack.Screen name={'OrderScreen'} component={OrderScreen} options={{ headerShown: false, }} />
            <Stack.Screen name={'NewAddressViewController'} component={NewAddressViewController} options={{ headerShown: false, }} />
            <Stack.Screen name={'PlacedOrderViewController'} component={PlacedOrderViewController} options={{ headerShown: false, }} />
            <Stack.Screen name={'BillingAddressViewController'} component={BillingAddressViewController} options={{ headerShown: false, }} />
            <Stack.Screen name={'BrandsViewController'} component={BrandsViewController} options={{ headerShown: false, }} />
            <Stack.Screen name={'CheckOutViewController'} component={CheckOutViewController} options={{ headerShown: false, }} />
            <Stack.Screen name={'MyProfileViewController'} component={MyProfileViewController} options={{ headerShown: false, }} />
            <Stack.Screen name={'ProductDetailViewController'} component={ProductDetailViewController} options={{ headerShown: false, }} />
            <Stack.Screen name={'StoreListingViewController'} component={StoreListingViewController} options={{ headerShown: false, }} />
            <Stack.Screen name={'WriteAReviewViewController'} component={WriteAReviewViewController} options={{ headerShown: false, }} />
            <Stack.Screen name={'TermsAndConditionsAndPolicyViewController'} component={TermsAndConditionsAndPolicyViewController} options={{ headerShown: false, }} />
            <Stack.Screen name={'ContactUsViewController'} component={ContactUsViewController} options={{ headerShown: false, }} />
            <Stack.Screen name={'ShippingDateViewController'} component={ShippingDateViewController} options={{ headerShown: false, }} />
            <Stack.Screen name={'RequestProductViewController'} component={RequestProductViewController} options={{ headerShown: false, animation: 'flip' }} />
            <Stack.Screen name={'FilterOptionViewController'} component={FilterOptionViewController} options={{ headerShown: false, animation: 'slide_from_bottom' }} />
            <Stack.Screen name={'PolicyViewController'} component={PolicyViewController} options={{ headerShown: false, animation: 'slide_from_bottom' }} />
            <Stack.Screen name={'GiftVoucherViewController'} component={GiftVoucherViewController} options={{ headerShown: false, animation: 'slide_from_bottom' }} />
            <Stack.Screen name={'SeeAllViewController'} component={SeeAllViewController} options={{ headerShown: false, animation: 'fade_from_bottom' }} />
            <Stack.Screen name={'ReviewDetails'} component={ReviewDetails} options={{ headerShown: false, animation: 'fade_from_bottom' }} />
            <Stack.Screen name={'ProductDetails'} component={ProductDetails} options={{ headerShown: false, animation: 'fade_from_bottom' }} />
            <Stack.Screen name={'EditAddressViewController'} component={EditAddressViewController} options={{ headerShown: false, animation: 'fade_from_bottom' }} />
            <Stack.Screen name={'MyOrderViewController'} component={MyOrderViewController} options={{ headerShown: false, animation: 'fade_from_bottom' }} />
            <Stack.Screen name={'NotificationViewController'} component={NotificationViewController} options={{ headerShown: false, animation: 'fade_from_bottom' }} />
            <Stack.Screen name={'OrderDetailsViewController'} component={OrderDetailsViewController} options={{ headerShown: false, animation: 'fade_from_bottom' }} />
            <Stack.Screen name={'HomeScreen'} component={HomeScreen} options={{ headerShown: false, }} />
            <Stack.Screen name={'DigitalGiftVoucherController'} component={DigitalGiftVoucherController} options={{ headerShown: false, }} />
            <Stack.Screen name={'StorelistDetails'} component={StorelistDetails} options={{ headerShown: false, }} />
            <Stack.Screen name={'OrderListViewController'} component={OrderListViewController} options={{ headerShown: false, }} />
            <Stack.Screen name={'ScannerScreen'} component={ScannerScreen} options={{ headerShown: false, }} />

            <Stack.Screen name={'CreditCardViewController'} component={CreditCardViewController} options={{ headerShown: false, }} />
            <Stack.Screen name={'ThankyouViewController'} component={ThankyouViewController} options={{ headerShown: false, }} />

            <Stack.Screen name={'Shareyourpurchase'} component={Shareyourpurchase} options={{ headerShown: false, }} />
            {/* <Stack.Screen name={'ProductListingViewController'} component={ProductListingViewController} options={{ headerShown: false, }} /> */}

        </Stack.Navigator>
    )
}



/**
 * Tab navigation screens
 */
const TabNavigatorLoyaltyApp = () => {
    return (
        <Tab.Navigator
            initialRouteName='HomeScreen'
            backBehavior='initialRoute'

            screenOptions={({ route, navigation }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;
                    if (route.name === 'HomeScreen') {
                        iconName = focused ? importImages.homeiconactive : importImages.homeiconinactive
                    }
                    else if (route.name === 'CardScreen') {
                        iconName = focused ? importImages.cardiconactive : importImages.cardiconinactive
                    }
                    else if (route.name === 'ScannerScreen') {
                        iconName = focused ? importImages.scannericonactive : importImages.scannericoninactive
                    }
                    else if (route.name === 'CartScreen') {
                        iconName = focused ? importImages.carticonactive : importImages.carticoninactive
                    }
                    else if (route.name === 'SettingScreen') {
                        iconName = focused ? importImages.menuiconactive : importImages.menuiconinactive
                    }
                    return <Image source={iconName}
                    />
                },
                tabBarStyle: { backgroundColor: commonColors.White },
                tabBarShowLabel: false,
            })}
        >
            <Tab.Screen name={'HomeScreen'} component={HomeScreen} options={{ headerShown: false, }} />
            <Tab.Screen name={'CardScreen'} component={CardScreen} options={{ headerShown: false, }} />
            <Tab.Screen name={'ScannerScreen'} component={ScannerScreen} options={{ headerShown: false, }} />
            <Tab.Screen name={'CartScreen'} component={CartScreen} options={{ headerShown: false, }} />
            <Tab.Screen name={'SettingScreen'} component={SettingScreen} options={{ headerShown: false, }} />
        </Tab.Navigator>
    )
}
const ShopStackNavigator = () => {
    return (
        <ShopStack.Navigator initialRouteName='ShoppingHomveViewController'>
            <ShopStack.Screen name={'ShoppingHomveViewController'} component={ShoppingHomveViewController} options={{ headerShown: false, }} />
            <ShopStack.Screen name={'QrcodeProductScreen'} component={QrcodeProductScreen} options={{ headerShown: false, }} />
            <ShopStack.Screen name={'ProductListingViewController'} component={ProductListingViewController} options={{ headerShown: false, }} />
            <ShopStack.Screen name={'FilterOptionViewController'} component={FilterOptionViewController} options={{ headerShown: false, animation: 'slide_from_bottom' }} />
            <ShopStack.Screen name={'AuthSelection'} component={AuthSelection} options={{ headerShown: false, }} />
            <ShopStack.Screen name={'SeeAllViewController'} component={SeeAllViewController} options={{ headerShown: false, animation: 'fade_from_bottom' }} />
        </ShopStack.Navigator>
    )
}

const CategoryStacknavigator = () => {
    return (
        <CategoryStack.Navigator initialRouteName='CategoryViewController'>
            <CategoryStack.Screen name={'CategoryViewController'} component={CategoryViewController} options={{ headerShown: false, }} />
            <CategoryStack.Screen name={'QrcodeProductScreen'} component={QrcodeProductScreen} options={{ headerShown: false, }} />
            <CategoryStack.Screen name={'ProductListingViewController'} component={ProductListingViewController} options={{ headerShown: false, }} />
            <CategoryStack.Screen name={'FilterOptionViewController'} component={FilterOptionViewController} options={{ headerShown: false, animation: 'slide_from_bottom' }} />
            <CategoryStack.Screen name={'AuthSelection'} component={AuthSelection} options={{ headerShown: false, }} />

        </CategoryStack.Navigator>
    )
}



const TabNavigator = () => {
    return (
        <Tab.Navigator
            initialRouteName='ShopStackNavigator'
            backBehavior='initialRoute'
            // screenListeners={{ tabPress: e => console.log('TAPPED', e) }}
            screenOptions={({ route, navigation }) => ({

                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;
                    if (route.name === 'ShopStackNavigator') {
                        iconName = focused ? importImages.tabHome : importImages.tabHome
                    }
                    else if (route.name === 'CategoryStacknavigator') {
                        iconName = focused ? importImages.tabCategory : importImages.tabCategory
                    }
                    else if (route.name === 'CheckOutViewController') {
                        iconName = focused ? importImages.cardiconactive : importImages.cardiconinactive
                    }
                    else if (route.name === 'MyBasketViewController') {
                        iconName = focused ? importImages.tabMyBasket : importImages.tabMyBasket
                    }
                    else if (route.name === 'MyFavoriteViewController') {
                        iconName = focused ? importImages.tabFavorite : importImages.tabFavorite
                    }
                    else if (route.name === 'MyAccountViewController') {
                        iconName = focused ? importImages.tabMyAccount : importImages.tabMyAccount
                    }
                    return <View style={{ flexDirection: "column" }}>
                        <View style={{ borderTopWidth: focused ? 5 : 0, borderColor: commonColors.Blue, borderRadius: scale(10), marginVertical: scale(5) }}>
                        </View>
                        <Image source={iconName} style={{
                            height: scale(25), width: scale(25), alignSelf: "center"
                        }} />
                    </View>
                },
                tabBarStyle: {
                    position: "absolute", backgroundColor: commonColors.White, height: 64, borderTopLeftRadius: 32,
                    borderTopRightRadius: 32, shadowColor: commonColors.Blue,
                    shadowOffset: { width: 1, height: 1 },
                    shadowOpacity: 0.5,
                    shadowRadius: 20,
                },
                tabBarHideOnKeyboard: true,
                tabBarShowLabel: true,
                tabBarLabelStyle: { fontFamily: Fonts.montserratRegular, fontSize: scale(10), color: '#002855', paddingBottom: scale(5) }
            })}
        >
            <Tab.Screen name={'ShopStackNavigator'} component={ShopStackNavigator} options={{ tabBarLabel: 'Home', headerShown: false, }} />
            {/* <Tab.Screen name={'CategoryViewController'} component={CategoryViewController} options={{ tabBarLabel: 'Category', headerShown: false, }} /> */}
            <Tab.Screen name={'CategoryStacknavigator'} component={CategoryStacknavigator} options={{ tabBarLabel: 'Category', headerShown: false, }} />

            <Tab.Screen name={'MyBasketViewController'} component={MyBasketViewController} options={{ tabBarLabel: 'My Basket', headerShown: false, tabBarBadge: <BasketTabBadgeCount /> }} />
            <Tab.Screen name={'MyFavoriteViewController'} component={MyFavoriteViewController} options={{ tabBarLabel: 'Favourite', headerShown: false, tabBarBadge: <TabBadgeCount /> }} />
            <Tab.Screen name={'MyAccountViewController'} component={MyAccountViewController} options={{ tabBarLabel: 'My Account', headerShown: false, }} />
        </Tab.Navigator>
    )
}

/**
 * Navigation of all screens
 */
export default function Route({ route, navigate }) {

    const { ColorName, colors, icons, setScheme } = useTheme();
    colorThemes = colors
    useEffect(() => {

        getData()
    })
    const handleDynamicLink = (link) => {
        console.log("link", link);
        if (link) {
            navigationCheck(link)

        }
    };
    const navigationCheck = (link) => {

        console.log("link:-->", link);
        var productId = link.url.substring(link.url.lastIndexOf("=") + 1);
        setTimeout(() => {
            NavigationService.resetnavigate('ProductDetailViewController', { seoUrl: productId })
        }, 1000);

    }
    const getData = async () => {

        console.log("getdata");

        if (Platform.OS == 'ios') {
            // console.log("ios",);
            // const unsubscribe = await dynamicLinks().getInitialLink(handleDynamicLink)
            // const unsubscribe2 = dynamicLinks().onLink(handleDynamicLink)
            // return () => { unsubscribe, unsubscribe2 };
            await dynamicLinks().getInitialLink().then((link) => {

                handleDynamicLink(link)

            }).catch((error) => {
            })
            dynamicLinks().onLink(handleDynamicLink)
        }
        else {
            await dynamicLinks().getInitialLink().then((link) => {

                handleDynamicLink(link)

            }).catch((error) => {
            })
            dynamicLinks().onLink(handleDynamicLink)
        }

        /**
         * manage the dynamic link and navigation when app is in foreground mode/ running mode
         */


        const requestOptions = {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json',
                'Authorization': "Bearer " + await getToken()
            },
        };
        try {
            await fetch(
                'https://ikasco.com/Shopping-App/api/home2.php', requestOptions)
                .then(response => {
                    response.json()
                        .then(async data => {
                            await StorageService.saveItem(StorageService.STORAGE_KEYS.BANNER_DETAILS, data.data.banners)
                            await StorageService.saveItem(StorageService.STORAGE_KEYS.CATEGORY, data.data.categories)
                        });
                })
        }
        catch (error) {
            console.error(error);
        }
    }

    return (
        <SafeAreaView style={{ backgroundColor: commonColors.White, flex: 1 }}>
            <StatusBar barStyle="dark-content" backgroundColor={commonColors.White}
            />
            <View style={{ flex: 1, backgroundColor: commonColors.White }}>
                <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
                <SafeAreaView style={{ backgroundColor: commonColors.White, flex: 1 }}>
                    <NavigationContainer independent={true}
                        ref={navigator => NavigationService.setTopLevelNavigator(navigator)}
                    >
                        <StackNavigator />

                    </NavigationContainer>
                </SafeAreaView>
            </View>
        </SafeAreaView>
    );
}
export class TabBadgeCount extends Component {

    constructor(props) {
        super(props);
        this.state = {
            badgeCount: 0
        }
    }
    async componentDidMount() {
        await this.onScreenFocus()
    }

    componentDidUpdate(previousProps, previousState) {
        setTimeout(() => {
            this.onScreenFocus()
        }, 1000);
    }

    onScreenFocus = async () => {
        let userData = await StorageService.getItem(StorageService.STORAGE_KEYS.USER_DETAILS)
        const requestOptions = {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json',
                'Authorization': "Bearer " + await getToken()
            },
        };

        if (userData) {
            try {
                await fetch(
                    'https://ikasco.com/Shopping-App/api/favorite-item-list.php', requestOptions)
                    .then(response => {
                        response.json()
                            .then(data => {
                                if (data.data.length > 0) {
                                    this.setState({
                                        badgeCount: data.data.length
                                    })
                                }
                                else {
                                    this.setState({
                                        badgeCount: 0
                                    })
                                }
                            });
                    })
            }
            catch (error) {
                console.error(error);
            }
        }
        else {
            this.setState({
                badgeCount: 0
            })
        }
    }

    render() {
        return this.state.badgeCount;
    }
}

export class BasketTabBadgeCount extends Component {

    constructor(props) {
        super(props);
        this.state = {
            badgeCount: 0
        }
    }
    async componentDidMount() {
        await this.onScreenFocus()
    }

    componentDidUpdate(previousProps, previousState) {
        setTimeout(() => {
            this.onScreenFocus()
        }, 2000);
    }

    onScreenFocus = async () => {

        let userData = await StorageService.getItem(StorageService.STORAGE_KEYS.USER_DETAILS)
        if (userData) {
            const requestOptions = {
                method: 'POST',
                headers: {
                    'Accept': '*/*',
                    'Content-Type': 'application/json',
                    'Authorization': "Bearer " + await getToken()
                },
                body: JSON.stringify({ func: 'fetch', domain_id: 1, })
            };
            try {
                await fetch(
                    'https://ikasco.com/Shopping-App/api/manage-cart.php', requestOptions)
                    .then(response => {
                        response.json()
                            .then(data => {
                                if (data.data.products.length > 0) {
                                    this.setState({
                                        badgeCount: data.data.cart_count

                                    })
                                }
                                else {
                                    this.setState({
                                        badgeCount: 0
                                    })
                                }
                            });
                    })
            }
            catch (error) {
                console.error(error);
            }
        }
        else {
            this.setState({
                badgeCount: 0
            })
        }
    }

    render() {
        return this.state.badgeCount;
    }
}