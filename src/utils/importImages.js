/**
 * All Images and Icons of app
 */
import { Icon, React, commonColors, Image } from '../utils/importLibrary'
const Path = "../assets/Images/";
import FlashMessage, { renderFlashMessageIcon } from "react-native-flash-message";

/**
 * Common Images
 */
export const importImages = {

   // Splash Screen Image and App Icon

   splashImg: require(`${Path}blue_logo.png`),
   appIcon: require(`${Path}App-icon-1024.png`),

   // Tab Icons

   homeiconactive: require(`${Path}home-active.png`),
   homeiconinactive: require(`${Path}home.png`),
   cardiconactive: require(`${Path}e-card-active.png`),
   cardiconinactive: require(`${Path}e-card.png`),
   scannericonactive: require(`${Path}scan-active.png`),
   scannericoninactive: require(`${Path}scan.png`),
   carticonactive: require(`${Path}cart-active.png`),
   carticoninactive: require(`${Path}cart.png`),
   menuiconactive: require(`${Path}more-active.png`),
   menuiconinactive: require(`${Path}more.png`),

   // SignUp Screen Images

   dateOfbirthImg: require(`${Path}dob-icon.png`),
   emailImg: require(`${Path}email-icon.png`),
   genderImg: require(`${Path}gender-icon.png`),
   mobileNumberImg: require(`${Path}mobile-icon.png`),
   nameImg: require(`${Path}name-icon.png`),
   passwordImg: require(`${Path}password-icon.png`),
   backImg: require(`${Path}blue_back-icon.png`),

   // Tab Screen Images

   whiteBackArrowImage: require(`${Path}white_back-icon.png`),
   blueBackArrowImage: require(`${Path}blue_back-icon.png`),
   cardImage: require(`${Path}card-blue.png`),
   myProfile: require(`${Path}my-profile-icon.png`),
   webProfile: require(`${Path}website-icon.png`),
   facebookImage: require(`${Path}fb-icon.png`),
   instaImage: require(`${Path}ig-icon.png`),
   aboutGigftImage: require(`${Path}info-icon.png`),
   barcode: require(`${Path}barcode-scan.png`),
   crossImage: require(`${Path}cross-icon.png`),
   blankCheckbox: require(`${Path}blankCheckBox.png`),
   Checkbox: require(`${Path}checkbox.png`),
   visa: require(`${Path}visa.png`),
   amex: require(`${Path}amex-svgrepo-com.png`),
   mastercard: require(`${Path}layer1.png`),
   dropDown: require(`${Path}dropdown.png`),
   leftArrow: require(`${Path}leftArrow.png`),
   greenTick: require(`${Path}greenTick.png`),
   BlueTick: require(`${Path}BlueTick.png`),
   BlueBox: require(`${Path}BlueBox.png`),
   downArrow: require(`${Path}downArrow.png`),

   tabHome: require(`${Path}tabHome.png`),
   tabCategory: require(`${Path}tabCategory.png`),
   tabMyBasket: require(`${Path}tabBasket.png`),
   tabFavorite: require(`${Path}tabFavorite.png`),
   tabMyAccount: require(`${Path}tabAccount.png`),
   headerLogo: require(`${Path}logo.png`),
   mikeIcon: require(`${Path}mikeIcon.png`),
   SearchIcon: require(`${Path}SearchIcon.png`),
   cameraIcon: require(`${Path}cameraIcon.png`),
   topBanner: require(`${Path}topBanner.png`),
   plusIcon: require(`${Path}plusIcon.png`),
   burberry: require(`${Path}burberry.png`),
   YSL: require(`${Path}YSL.png`),
   favSelected: require(`${Path}favSelected.png`),
   favUnselected: require(`${Path}favUnselected.png`),
   sendReview: require(`${Path}sendReview.png`),
   videoBanner: require(`${Path}videoBanner.png`),
   Clarins: require(`${Path}Clarins.png`),
   YSL1: require(`${Path}YSL1.png`),
   salesImage: require(`${Path}salesImage.png`),
   Fragrances: require(`${Path}Fragrances.png`),
   makeup: require(`${Path}makeup.png`),
   FragrancesLarge: require(`${Path}FragrancesLarge.png`),
   dropDownArrow: require(`${Path}dropDownArrow.png`),
   myFav1: require(`${Path}myFav1.png`),
   myFav2: require(`${Path}myFav2.png`),
   deleteItem: require(`${Path}deleteItem.png`),
   QTYPlus: require(`${Path}QTYPlus.png`),
   QTYMinus: require(`${Path}QTYMinus.png`),
   verticleLine: require(`${Path}verticleLine.png`),
   unCheckImage: require(`${Path}unCheckImage.png`),
   radioUnCheckImage: require(`${Path}radioUnCheckImage.png`),
   detailArrowImage: require(`${Path}detailArrowImage.png`),
   backButtonArrow: require(`${Path}backButtonArrow.png`),
   LocationIcon: require(`${Path}LocationIcon.png`),
   PhoneIcon: require(`${Path}PhoneIcon.png`),
   ProductDetailLargeImage: require(`${Path}ProductDetailLargeImage.png`),
   img1: require(`${Path}img1.png`),
   img2: require(`${Path}img2.png`),
   img3: require(`${Path}img3.png`),
   img4: require(`${Path}img4.png`),
   dropDownProductImage: require(`${Path}dropDownProductImage.png`),
   cartImage: require(`${Path}cartImage.png`),
   pencilEdiIcon: require(`${Path}pencilEdiIcon.png`),
   downArrowButton: require(`${Path}downArrowButton.png`),
   defaultFavImage: require(`${Path}defaultFavImage.png`),
   defaultProductImage: require(`${Path}product-default-img.png`),
   defaultBannerImage: require(`${Path}main-banner-default-img.png`),
   defaultsaleBannerImage: require(`${Path}sale-banner-default-img.png`),
   defaultcategoryBannerImage: require(`${Path}category-default-img.png`),
   lineImage: require(`${Path}Line45.png`),
   plusImage: require(`${Path}plus-sign.png`),
   minusImage: require(`${Path}minus-sign.png`),
   leftArrowimage: require(`${Path}arrow-icon.png`),
   sendsms: require(`${Path}sendsms.png`),
   giftbox: require(`${Path}giftbox.png`),
   futurecalendar: require(`${Path}futurecalendar.png`),
   offerImage: require(`${Path}offer-icon.png`),
   thankyoucross: require(`${Path}Thankyoucross.png`),
   thankyou: require(`${Path}ThankYou.png`),
   PlayImage: require(`${Path}play.png`),
   PauseImage: require(`${Path}pause.png`),
   giftImage: require(`${Path}giftIcon.png`)
}

/**
 * White Theme Images
 */
export const importIconsWhite = {
   BackImage: require(`${Path}blue_back-icon.png`),
   Card: require(`${Path}card-white.png`),

}

/**
 * Silver Theme Images
 */
export const importIconsSilver = {
   BackImage: require(`${Path}white_back-icon.png`),
   Card: require(`${Path}card-grey.png`),
}

/**
 * Blue Theme Images
 */
export const importIconsBlue = {
   BackImage: require(`${Path}white_back-icon.png`),
   Card: require(`${Path}card-blue.png`),

}