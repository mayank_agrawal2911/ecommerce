
// White theme colors

export const whiteThemeColors = {
  homebackground: '#FFFFFF',
  homeTextbackground: '#002855',
  topbackground: '#000000',
  primary: '#F8D0C1',
  text: '#000000',
  error: '#D32F2F',
};

// Sliver theme colors

export const silverThemeColors = {
  homebackground: '#AFAFAF',
  homeTextbackground: '#FFFFFF',
  topbackground: '#FFFFFF',
  primary: '#B9BBBB',
  text: '#FFFFFF',
  error: '#EF9A9A',
};

// Blue theme colors

export const blueThemeColors = {
  homebackground: '#002855',
  homeTextbackground: '#FFFFFF',
  topbackground: '#FFFFFF',
  primary: '#B9BBBB',
  text: '#FFFFFF',
  error: '#EF9A9A',
};

// Common colors

export const commonColors = {
  Black: '#000000',
  White: '#FFFFFF',
  Transparent: '#000000AA',
  Blue: '#002855',
  Grey: '#AFAFAF',
  DarkGrey: '#606060',
  LightGrey: '#68686810',
  Red: '#FF0A24',
  tabBarInactiveTintColor: '#606060',
  pageControlColor: '#7F93AA',
  lightBlue: '#082042',
  seperatorColor: '#CCD1D8',
  qtySizeMlText: '#657D98',
  modalBackground: 'rgba(0, 0, 0, 0.75)',
}