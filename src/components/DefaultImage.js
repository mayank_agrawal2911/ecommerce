import React, { Component } from 'react';
import { View, ImageBackground, StyleSheet, Image, ActivityIndicator } from 'react-native';

import FastImage from 'react-native-fast-image';
import Color from '../Common/Color';
import _ from 'lodash';
// TODO: convert this class in functional component(ADD hooks)
// flow types

export default function DefaultImage(props) {
  const [failToLoad, setFailToLoad] = useState(false);
  const [loadComplete, setLoadComplete] = useState(false);
  
  useEffect(() => {      
    setFailToLoad(false)  
  }, [props]);

  const renderImageView =() => {
    let {
      source,
      resizeMode,
      defaultImage,
      defaultImageResizeMode,
      defaultImageStyle = {},
      imageStyle = {},
      errorImage,
      errorImageStyle = {},
      style = {}, // while style is set it will overright all image styles
    } = props;

    if (failToLoad) {
      // renders while image load failed
      return (
        <FastImage
          source={errorImage}
          resizeMode={resizeMode || 'cover'}
          style={style ? style : [styles.errorImageStyle, errorImageStyle]}
        />
      )
    } else if (loadComplete) {
      // after cache network image
      return (
        <FastImage
          source={source}
          resizeMode={resizeMode || 'cover'}
          style={style ? style : [styles.imageStyle, imageStyle]}
        />
      )
    } else {
      // default image rendering
      return (
        <ImageBackground
          source={defaultImage}
          style={style ? style : [styles.defaultImageStyle, defaultImageStyle]}
          resizeMode={defaultImageResizeMode || 'cover'}
        >
          {props.isBase64 &&
            <Image
              source={source}
              resizeMode={resizeMode || 'cover'}
              style={style ? style : [styles.imageStyle, imageStyle]}
            />
            ||
            <View style={[style, { justifyContent: 'center', alignItems: 'center' }]}>
              <FastImage
                source={source}
                onError={() => setFailToLoad(true)}
                onLoad={() => setLoadComplete(true)}
                onLoadStart={() => setLoadComplete(false)}
                resizeMode={resizeMode || 'cover'}
                style={style} // I know, Hack for load image! :)
              />
              <View style={styles.loaderStyle}>
                <ActivityIndicator
                  size={'small'}
                  color={Color.redColor}
                  animating={true}
                />
              </View>
            </View>
          }
        </ImageBackground>
      )
    }
  }

  let { containerStyle } = props;
    return (
      <View style={[styles.container, containerStyle]}>
        {renderImageView()}
      </View>
    );
  

  
  
}

let styles = StyleSheet.create({
  container: {
  },
  imageStyle: {
    height: 150,
    width: 150
  },
  defaultImageStyle: {
    height: 150,
    width: 150
  },
  errorImageStyle: {
    height: 150,
    width: 150
  },
  loaderStyle: {
    position: "absolute",
    alignItems: "center",
    justifyContent: 'center',
  }
});
