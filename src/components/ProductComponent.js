import { React, PixelRatio, importImages, TouchableOpacity, StyleSheet, Text, View, Image, commonColors, Fonts, deviceWidth, scale, deviceHeight } from '../utils/importLibrary'
import FastImage from 'react-native-fast-image';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';

export default function ProductComponent(props) {
   return (

      <TouchableOpacity disabled={props.disabled} style={{ marginVertical: 8, justifyContent: 'center', width: (deviceWidth - 32) / 2, marginHorizontal: 8 }} onPress={() => props.mainCateoriesClick(props.item, props.index)}>

         <View style={{ height: scale(330), backgroundColor: commonColors.White, borderRadius: 10, shadowColor: commonColors.pageControlColor, shadowOpacity: 1.75, elevation: 5, }}>
            <View style={{ height: scale(180) }}>
               {props.item.segment ? null : props.isShowOffer ? null : props.item.has_gift != 0 ?
                  <FastImage
                     style={{ height: 20, marginLeft: 130, top: 20 }}
                     source={importImages.giftImage}
                     resizeMode={FastImage.resizeMode.contain}
                  /> : null}

               {
                  props.item.family_pic ?
                     <View>

                        <FastImage
                           style={{ marginVertical: scale(5), alignSelf: 'center', height: scale(150), width: scale(100), }}
                           source={{
                              uri: props.item.family_pic,
                              priority: FastImage.priority.high,
                           }}
                           resizeMode={FastImage.resizeMode.contain}
                        />
                     </View>
                     :
                     <View>
                        <FastImage
                           style={{ marginVertical: scale(5), alignSelf: 'center', height: scale(100), width: scale(100), marginTop: scale(5) }}

                           source={importImages.defaultProductImage}
                           resizeMode={FastImage.resizeMode.contain}
                        />
                     </View>
               }

               {
                  props.item.segment ? <View style={{ position: 'absolute', marginLeft: 8, marginTop: 4, justifyContent: 'center', alignItems: 'center', width: 40, height: 20, backgroundColor: commonColors.Blue }}>
                     <Text style={{ fontFamily: Fonts.montserratMedium, fontSize: scale(10), color: commonColors.White }}>{props.item.segment}</Text>
                  </View> :
                     props.isShowOffer ? <View style={{ position: 'absolute', marginLeft: 8, marginTop: 4, justifyContent: 'center', alignItems: 'center', width: 40, height: 20, backgroundColor: commonColors.Blue }}>
                        <Text style={{ fontFamily: Fonts.montserratMedium, fontSize: 8, color: commonColors.White }}>OFFER</Text>
                     </View> : null
               }


               {props.isShowColorShade ?
                  props.index % 2 != 0 ? <View style={{ flexDirection: "row", alignSelf: 'center', marginTop: 16 }}>
                     {props.colorCombination.map((colorItem, i) => {
                        return (
                           <TouchableOpacity
                              key={i}
                              disabled={props.disabled}
                              onPress={() => props.colorCodeChangedIndex(i)}
                              style={props.colorSelectedIndex === i ? [styles.selectedColorSection, { borderWidth: 1, borderColor: props.colorSelectedIndex === i ? colorItem : colorItem }] : [styles.colorSection, { backgroundColor: props.colorSelectedIndex === i ? colorItem : colorItem }]}>
                              {
                                 props.colorSelectedIndex === i ? <View style={[styles.colorSection, { marginTop: 1, marginLeft: 1, backgroundColor: props.colorSelectedIndex === i ? colorItem : colorItem }]} /> : null
                              }
                           </TouchableOpacity>

                        )
                     })}
                  </View> : <View style={{ flexDirection: "row", alignSelf: 'center', marginTop: 16, height: 8 }} />
                  : null}


            </View>
            <View style={{ marginHorizontal: 12, height: scale(50), marginVertical: scale(5), marginTop: scale(5), }}>
               <Text numberOfLines={2} style={{ fontFamily: Fonts.montserratMedium, fontWeight: '600', fontSize: 13, color: commonColors.Blue }}>{props.item.brand_name}</Text>
               <Text numberOfLines={2} style={{ fontFamily: Fonts.montserratMedium, fontSize: 10, color: commonColors.Blue }}>{props.item.family_name}</Text>
            </View>

            <View style={{ flexDirection: 'row', height: scale(12), marginHorizontal: scale(10), justifyContent: "center" }}>
               {/* <Text style={{ fontFamily: Fonts.montserratMedium, fontSize: scale(10), color: commonColors.Blue }}>Price : </Text> */}
               {/* {
                     props.index % 2 != 0 ? <Text style={{ marginTop: 8, fontFamily: Fonts.montserratMedium, fontSize: 10, color: commonColors.Blue, textDecorationColor: commonColors.Red, textDecorationLine: 'line-through', textDecorationStyle: 'double' }}>80 - 55 JD's </Text> : null
                  } */}
               {
                  props.item.min_price != props.item.max_price ? <Text style={{ fontFamily: Fonts.montserratMedium, fontSize: scale(10), textAlign: 'center', color: commonColors.Blue }}>{props.item.min_price} - {props.item.max_price} JD's</Text> : <Text style={{ fontFamily: Fonts.montserratMedium, fontSize: scale(10), color: commonColors.Blue, textAlign: 'center' }}>{props.item.main_price} JD's</Text>
               }
            </View>
            <View style={{ height: scale(15), marginVertical: scale(5), }}>
               {
                  props.item.offer_name ? <Text style={{ fontFamily: Fonts.montserratBold, fontSize: scale(12), color: commonColors.Blue, textAlign: 'center', }}>{props.item.offer_name} </Text> : null
               }
            </View>
            <View style={{ height: scale(15), marginVertical: scale(2), }}>
               {
                  props.item.dmax_price != '0.00' && props.item.dmax_price != '' ?

                     < Text style={{ fontFamily: Fonts.montserratMedium, fontSize: scale(10), color: commonColors.Blue, textAlign: 'center', }}>{props.item.dmax_price} JD's</Text>
                     : null
               }
            </View>
            <View style={{ marginHorizontal: 12, flexDirection: 'row', marginVertical: scale(3), }}>
               <TouchableOpacity style={{ marginRight: 12 }} disabled={props.disabled} onPress={() => props.favoriteUnFavoriteButtonClick(props.item, props.index)}>
                  {props.isFavourite == true ?
                     <Image source={importImages.favSelected} />
                     :
                     <Image source={importImages.favUnselected} />}
               </TouchableOpacity>
               <TouchableOpacity disabled={props.disabled} style={{}} onPress={() => props.sendReviewButtonClick(props.item, props.index)}>
                  <Image source={importImages.sendReview} />
               </TouchableOpacity>
            </View>
         </View>
      </TouchableOpacity >

   )
}

const styles = StyleSheet.create({
   colorSection: {
      height: 10,
      width: 10,
      borderRadius: 5,
      marginRight: 10,
      marginTop: 2,
      backgroundColor: 'blue'
   },
   selectedColorSection: {
      height: 14,
      width: 14,
      borderRadius: 7,
      marginRight: 10,
      marginTop: 0,
   },
})