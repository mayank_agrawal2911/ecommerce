
import { React, Text, TouchableOpacity, View, commonColors, TouchableWithoutFeedback, StyleSheet, Fonts, importIconsSilver, importIconsWhite, useTheme, scale, deviceWidth } from '../utils/importLibrary'
export default function CustomButton(props) {
    const { ColorName, colors, icons, setScheme } = useTheme();

    return (
        <View >
            <TouchableOpacity
                style={[styles.container, props.style]}
                activeOpacity={props.activeOpacity || 0.8}
                onPress={props.onPress}
                disabled={props.disabled}
            >
                <Text style={[styles.titleStyle, props.titleStyle,]}>{props.title}</Text>
            </TouchableOpacity>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        height: scale(50),
        borderRadius: 5,
        paddingHorizontal: 10,
        alignSelf: 'stretch',
        backgroundColor: commonColors.Blue,
        width: deviceWidth - 20,
        minWidth: 120,
        maxWidth: deviceWidth - 20,
        marginVertical: scale(20),
        marginHorizontal: scale(10)
    },
    titleStyle: {
        color: commonColors.White,
        fontSize: Fonts.LargeSize,
        paddingLeft: scale(2)
    },
    leftBtnStyle: {
        width: 35,
        alignItems: 'center',
        justifyContent: 'center',
        height: 40
    },

});



