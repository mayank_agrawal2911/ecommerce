
import { React, Text, TouchableOpacity, View, commonColors, TouchableWithoutFeedback, StyleSheet, Fonts, importIconsSilver, importIconsWhite, useTheme, scale } from '../utils/importLibrary'
export default function CustomHeader(props) {
    const { ColorName, colors, icons, } = useTheme();

    return (
        <View style={[styles.container, props.style]}>
            <TouchableOpacity onPress={props.leftBtnOnPress}>
                <View style={[styles.leftBtnStyle, props.leftBtnStyle]}>
                    {props.leftBtn != null ? props.leftBtn
                        : props.leftBtnOnPress ? icons.backIcon : null}
                </View>
            </TouchableOpacity>
            <View>
                <Text style={[styles.titleStyle, props.titleStyle]}>{props.headerTitle}</Text>
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: 60,
        alignItems: 'center',
        right: scale(30)
    },
    titleStyle: {
        color: commonColors.Blue,
        fontFamily: Fonts.montserratSemiBold,
        fontSize: scale(18),
    },
    leftBtnStyle: {
        width: 35,
        alignItems: 'center',
        justifyContent: 'center',
        height: 40
    },

});



